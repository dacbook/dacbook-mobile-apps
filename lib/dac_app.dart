import 'package:dac_apps/presentation/bottom_navbar/bottom_navbar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: depend_on_referenced_packages
import 'package:get/get.dart';

import 'presentation/splashscreen/splashscreen_view.dart';

class DacApp extends StatelessWidget {
  const DacApp({super.key});

  @override
  Widget build(BuildContext context) {
    
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));

    return  GetMaterialApp(home: const  SplashScreenView(),
    routes: {
        'dacbottombar': (BuildContext ctx) => DacBottomNavbar(),
      
   },
    );
  }
}