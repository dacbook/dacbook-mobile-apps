/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsImageGen {
  const $AssetsImageGen();

  /// File path: assets/image/avatar.png
  AssetGenImage get avatar => const AssetGenImage('assets/image/avatar.png');

  /// File path: assets/image/avatar1.png
  AssetGenImage get avatar1 => const AssetGenImage('assets/image/avatar1.png');

  /// File path: assets/image/avatar2.png
  AssetGenImage get avatar2 => const AssetGenImage('assets/image/avatar2.png');

  /// File path: assets/image/avatar3.png
  AssetGenImage get avatar3 => const AssetGenImage('assets/image/avatar3.png');

  /// File path: assets/image/avatar4.png
  AssetGenImage get avatar4 => const AssetGenImage('assets/image/avatar4.png');

  /// File path: assets/image/avatar5.png
  AssetGenImage get avatar5 => const AssetGenImage('assets/image/avatar5.png');

  /// File path: assets/image/dac_splash_logo.png
  AssetGenImage get dacSplashLogo =>
      const AssetGenImage('assets/image/dac_splash_logo.png');

  /// File path: assets/image/dac_splash_logo_backup.png
  AssetGenImage get dacSplashLogoBackup =>
      const AssetGenImage('assets/image/dac_splash_logo_backup.png');

  /// File path: assets/image/example_album1.jpeg
  AssetGenImage get exampleAlbum1 =>
      const AssetGenImage('assets/image/example_album1.jpeg');

  /// File path: assets/image/example_album2.jpeg
  AssetGenImage get exampleAlbum2 =>
      const AssetGenImage('assets/image/example_album2.jpeg');

  /// File path: assets/image/example_album3.jpeg
  AssetGenImage get exampleAlbum3 =>
      const AssetGenImage('assets/image/example_album3.jpeg');

  /// File path: assets/image/example_album4.jpeg
  AssetGenImage get exampleAlbum4 =>
      const AssetGenImage('assets/image/example_album4.jpeg');

  /// File path: assets/image/example_album5.jpeg
  AssetGenImage get exampleAlbum5 =>
      const AssetGenImage('assets/image/example_album5.jpeg');

  /// File path: assets/image/example_album6.jpeg
  AssetGenImage get exampleAlbum6 =>
      const AssetGenImage('assets/image/example_album6.jpeg');

  /// File path: assets/image/icon_instagram.png
  AssetGenImage get iconInstagram =>
      const AssetGenImage('assets/image/icon_instagram.png');

  /// File path: assets/image/icon_line.png
  AssetGenImage get iconLine =>
      const AssetGenImage('assets/image/icon_line.png');

  /// File path: assets/image/icon_signal.png
  AssetGenImage get iconSignal =>
      const AssetGenImage('assets/image/icon_signal.png');

  /// File path: assets/image/icon_telegram.png
  AssetGenImage get iconTelegram =>
      const AssetGenImage('assets/image/icon_telegram.png');

  /// File path: assets/image/icon_whatsapp.png
  AssetGenImage get iconWhatsapp =>
      const AssetGenImage('assets/image/icon_whatsapp.png');

  /// File path: assets/image/image_intro1.jpg
  AssetGenImage get imageIntro1 =>
      const AssetGenImage('assets/image/image_intro1.jpg');

  /// File path: assets/image/image_intro2.jpg
  AssetGenImage get imageIntro2 =>
      const AssetGenImage('assets/image/image_intro2.jpg');

  /// File path: assets/image/image_intro3.jpg
  AssetGenImage get imageIntro3 =>
      const AssetGenImage('assets/image/image_intro3.jpg');

  /// File path: assets/image/image_login.png
  AssetGenImage get imageLogin =>
      const AssetGenImage('assets/image/image_login.png');

  /// File path: assets/image/image_register.png
  AssetGenImage get imageRegister =>
      const AssetGenImage('assets/image/image_register.png');

  /// File path: assets/image/logo.png
  AssetGenImage get logo => const AssetGenImage('assets/image/logo.png');

  /// File path: assets/image/logo_dac.png
  AssetGenImage get logoDac => const AssetGenImage('assets/image/logo_dac.png');

  /// File path: assets/image/logo_dac_lable.png
  AssetGenImage get logoDacLable =>
      const AssetGenImage('assets/image/logo_dac_lable.png');

  /// File path: assets/image/logo_digital_alumni.png
  AssetGenImage get logoDigitalAlumni =>
      const AssetGenImage('assets/image/logo_digital_alumni.png');

  /// File path: assets/image/logo_ilmci.png
  AssetGenImage get logoIlmci =>
      const AssetGenImage('assets/image/logo_ilmci.png');

  /// File path: assets/image/ssbackground_amerika.png
  AssetGenImage get ssbackgroundAmerika =>
      const AssetGenImage('assets/image/ssbackground_amerika.png');

  /// File path: assets/image/ssbackground_indonesia.png
  AssetGenImage get ssbackgroundIndonesia =>
      const AssetGenImage('assets/image/ssbackground_indonesia.png');

  /// File path: assets/image/ssbackground_indonesia_backup.png
  AssetGenImage get ssbackgroundIndonesiaBackup =>
      const AssetGenImage('assets/image/ssbackground_indonesia_backup.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        avatar,
        avatar1,
        avatar2,
        avatar3,
        avatar4,
        avatar5,
        dacSplashLogo,
        dacSplashLogoBackup,
        exampleAlbum1,
        exampleAlbum2,
        exampleAlbum3,
        exampleAlbum4,
        exampleAlbum5,
        exampleAlbum6,
        iconInstagram,
        iconLine,
        iconSignal,
        iconTelegram,
        iconWhatsapp,
        imageIntro1,
        imageIntro2,
        imageIntro3,
        imageLogin,
        imageRegister,
        logo,
        logoDac,
        logoDacLable,
        logoDigitalAlumni,
        logoIlmci,
        ssbackgroundAmerika,
        ssbackgroundIndonesia,
        ssbackgroundIndonesiaBackup
      ];
}

class Assets {
  Assets._();

  static const $AssetsImageGen image = $AssetsImageGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider() => AssetImage(_assetName);

  String get path => _assetName;

  String get keyName => _assetName;
}
