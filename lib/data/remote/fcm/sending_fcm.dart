import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

sendingFcm({String? fcmtoken, String? bodydata, String? titledata, String? feedid, String? bookid, String? notificationCategory}) async {
  debugPrint('fcm token : $fcmtoken');
  debugPrint('body data : $bodydata');
  Map<String, String>? keydata = {
    'Authorization':
        'key=AAAA2XCqQ0c:APA91bH2MFm1gkKGapTCCLxPZzbaAFvhQ_z8SMUYb0Qwvc2bDrL_zEy2OFx3SBOkk_4sluBki6S3PejGxpSFynk_HFQP-lBbQgt5N5cMcW3F1j9FFXoUB73TbLdoeZlh6paUy36OA_yl',
    'Content-Type': 'application/json',
  };

  Map data = 
    {
      "data": {
        "title": titledata,
        "body": bodydata,
        "notificationCategory": notificationCategory,
        "bookid": bookid,
        "feedid": feedid,
        "url": "dac-uri"
      },
      "notification": {
         "title": titledata,
        "body": bodydata
       // "content_available": true
      },
      "to":
          fcmtoken
     // "click_action": "FLUTTER_NOTIFICATION_CLICK"
    };

  Map<String, String> bodyData = {
    "title": "ebeasiswa-title",
    "body": "ebeasiswa-body",
    "url": "ebeasiswa-uri"
  };
  Map<String, String> notificationData = {
    "title": "ebeasiswa-title",
    "body": "ebeasiswa-body"
    // "content_available": true
  };
  var url = Uri.parse('https://fcm.googleapis.com/fcm/send');
  var datajson = jsonEncode(data);
  // Await the http get response, then decode the json-formatted response.
  var response = await http.post(url, headers: keydata, body: datajson);
  if (response.statusCode == 200) {
    debugPrint('success sending fcm');
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
}
