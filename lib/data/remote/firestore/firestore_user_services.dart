// ignore_for_file: constant_identifier_names

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/data/model/avatar/avatar_model.dart';
import 'package:dac_apps/data/model/followers/followers_model.dart';
import 'package:dac_apps/presentation/login/login_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../../../presentation/bottom_navbar/bottom_navbar_view.dart';
import '../../../presentation/login/login_controller.dart';
import '../../model/user/user_model.dart';
import '../fcm/sending_fcm.dart';
import 'firestore_notification_services.dart';

// ignore: camel_case_types
enum authProblems { UserNotFound, PasswordNotValid, NetworkError }

class FireStoreUserServices {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final BoxStorage _boxStorage = BoxStorage();
  //   final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final FireStoreNotificationServices _fireStoreNotificationServices =
      FireStoreNotificationServices();
  //final LoginController logincontroller = Get.put(LoginController());

  Future<UserModel> loginCurrentUser(String uid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $uid');
    return _db
        .collection('user')
        .doc(uid)
        .get()
        .then((value) => UserModel.fromJson(value.data()!, value.id));
  }

  Future loginUser({String? email, String? password}) async {
    try {
      var result = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email!, password: password!);
      // since something changed, let's notify the listeners...
      _boxStorage.setUserId(result.credential!.accessToken!);
      return result;
    } catch (e) {
      // throw the Firebase AuthException that we caught
      throw Exception(e);
    }
  }

  Future logout() async {
    var result = await FirebaseAuth.instance.signOut();
    debugPrint('sign out success');
    return result;
  }

  Future<UserModel> getCurrentUser() async {
    String uid = _boxStorage.getUserId();
    debugPrint('current UserModel : $uid');
    if (uid.isEmpty || uid == '') {
      Get.offAll(() => const LoginView());
    }
    return _db
        .collection('user')
        .doc(uid)
        .get()
        .then((value) => UserModel.fromJson(value.data()!, value.id));
  }

  Future<UserModel> getCurrentFollower(String uid) async {
    debugPrint('current follower : $uid');
    return _db
        .collection('user')
        .doc(uid)
        .get()
        .then((value) => UserModel.fromJson(value.data()!, value.id));
  }

  Stream<List<UserModel>> getCurrentUserStream(String uid) {
    // String uid = _boxStorage.getUserId();
    debugPrint('current UserModel : $uid');
    if (uid.isEmpty || uid == '') {
      Get.offAll(() => const LoginView());
    }
     return _db.collection('user').where('uid', isEqualTo: uid).snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => UserModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<AvatarModel>> getAvatar() {
    return _db.collection('mAvatar').snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => AvatarModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<UserModel>> getFollowers() {
    return _db.collection('user').snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => UserModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FollowerModel>> getRekanAlumniUser(String uid) {
    debugPrint('clog => $uid - is follower');
    return _db
        .collection('followers')
        .doc(uid)
        .collection('follow')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FollowerModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<UserModel>> getFollowingUser(String uid) {
    debugPrint('clog => $uid - is follower');
    return _db
        .collection('user')
        .where('followers', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => UserModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<UserModel>> getFollowersUser(String uid) {
    debugPrint('clog => $uid - is follower');
    List<String> listitems = [uid];
    return _db
        .collection('user')
        .doc(uid)
        .collection('followersdata')
        .where('isfollowers', isEqualTo: true)
        .where('requestFollow', isEqualTo: false)
        .where('isblocked', isEqualTo: false)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              // .where((element) => !listitems.contains(element['uid']))
              .map(
                (doc) => UserModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FollowerModel>> getRekanAlumniCurrentUser() {
    String uid = _boxStorage.getUserId();
    debugPrint('clog => $uid - is follower');
    return _db
        .collection('followers')
        .doc(uid)
        .collection('follow')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FollowerModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FollowerModel>> getFollowingCurrentUser() {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('followers')
        .doc(uid)
        .collection('follow')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FollowerModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Future<bool> updateFcmToken() async {
    String fcmtoken = _boxStorage.getUserFcmToken();
    String uid = _boxStorage.getUserId();
    Map<String, dynamic> data = {'fcmtoken': fcmtoken};
    _db.collection('user').doc(uid).update(data);
    debugPrint('update fcmtoken $fcmtoken');
    return true;
  }

  Future<bool> updateFollow(UserModel userModel) async {
    String localUid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    _db.collection('user').doc(localUid).update({
      'following': FieldValue.arrayUnion([userModel.uid]),
      'followers': FieldValue.arrayUnion([userModel.uid]),
    });
    // _db.collection('user').doc(userModel.uid).update({
    //   'followers': FieldValue.arrayUnion([localUid]),
    // });

    _db
        .collection('user')
        .doc(localUid)
        .collection('followersdata')
        .doc(userModel.uid)
        .set({
      'uid': userModel.uid,
      'isblocked': false,
      'isfollowers': false,
      'requestFollow': true,
      'isfollowing': true,
      'username': userModel.username,
      'avatar': userModel.avatar,
      'followers': userModel.uid,
      'following': localUid,
    });

    _db.collection('user').doc(userModel.uid).update({
      'followers': FieldValue.arrayUnion([localUid])
    });

    _db
        .collection('user')
        .doc(userModel.uid)
        .collection('followersdata')
        .doc(localUid)
        .set({
      'uid': localUid,
      'isblocked': false,
      'isfollowers': false,
      'requestFollow': true,
      'isfollowing': true,
      'username': username,
      'avatar': avatar,
      'followers': localUid,
      'following': userModel.uid,
    });
    // ignore: unnecessary_brace_in_string_interps
    String bodydata = '${username}, mulai mengikuti kamu';
    String titledata = 'wah sepertinya follower kamu bertambah';
    UserModel usermodel = await loginCurrentUser(userModel.uid);
    sendingFcm(
        fcmtoken: usermodel.fcmtoken,
        bodydata: bodydata,
        titledata: titledata,
        feedid: '',
        bookid: '',
        notificationCategory: 'followers');
    _fireStoreNotificationServices.addNotification(
      userid: usermodel.uid,
      body: bodydata,
      title: titledata,
    );
    return true;
  }

  Future<bool> updateConfirmFollow(
      UserModel userModel, bool isfollow, bool isrequestfollow) async {
    String localUid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    _db.collection('user').doc(localUid).update({
      'followers': FieldValue.arrayUnion([userModel.uid]),
      'following': FieldValue.arrayUnion([userModel.uid])
    });
    _db.collection('user').doc(userModel.uid).update({
      'followers': FieldValue.arrayUnion([localUid]),
      'following': FieldValue.arrayUnion([localUid])
    });

    _db
        .collection('user')
        .doc(localUid)
        .collection('followersdata')
        .doc(userModel.uid)
        .update({
      'uid': userModel.uid,
      'isfollowers': true,
      'isblocked': false,
      'isfollowing': true,
      'requestFollow': false,
      'username': userModel.username,
      'avatar': userModel.avatar,
      'followers': userModel.uid,
      'following': localUid,
    });
    _db
        .collection('user')
        .doc(userModel.uid)
        .collection('followersdata')
        .doc(localUid)
        .update({
      'uid': localUid,
      'isfollowers': true,
      'isblocked': false,
      'isfollowing': true,
      'requestFollow': false,
      'username': username,
      'avatar': avatar,
      'followers': localUid,
      'following': userModel.uid,
    });

    String bodydata = '$username, mulai mengikuti kamu';
    String titledata = 'Yeayy, follower baru !!';
    UserModel usermodel = await loginCurrentUser(userModel.uid);
    sendingFcm(
        fcmtoken: usermodel.fcmtoken,
        bodydata: bodydata,
        titledata: titledata,
        feedid: '',
        bookid: '',
        notificationCategory: 'followers');
    _fireStoreNotificationServices.addNotification(
      userid: usermodel.uid,
      body: bodydata,
      title: titledata,
    );

    return true;
  }

  Future<bool> updateUnFollow(UserModel userModel, bool isfollow,
      bool isrequestfollow, bool isfollowing) async {
    String localUid = _boxStorage.getUserId();
    debugPrint('clog ==> $isfollow - $isrequestfollow');

    // _db.collection('user').doc(userModel.uid).update({
    //   'followers': FieldValue.arrayRemove([localUid]),
    //  // 'following': FieldValue.arrayRemove([localUid]),
    // });
    if (isfollow == false && isrequestfollow == false && isfollowing == false) {
      _db.collection('user').doc(localUid).update({
        'followers': FieldValue.arrayRemove([userModel.uid]),
        'following': FieldValue.arrayRemove([userModel.uid]),
      });
      _db.collection('user').doc(userModel.uid).update({
        'followers': FieldValue.arrayRemove([localUid]),
        'following': FieldValue.arrayRemove([localUid]),
      });
      _db
          .collection('user')
          .doc(localUid)
          .collection('followersdata')
          .doc(userModel.uid)
          .update({
        //'isblocked': false,
        'isfollowers': false,
        'isfollowing': false,
        'requestFollow': false,
      });
      _db
          .collection('user')
          .doc(userModel.uid)
          .collection('followersdata')
          .doc(localUid)
          .update({
        'isfollowers': false,
        'isfollowing': false,
        //'isblocked': false,
        'requestFollow': false,
      });
    }
    if (isfollow == false && isrequestfollow == true && isfollowing == true) {
      _db.collection('user').doc(localUid).update({
        // 'followers': FieldValue.arrayRemove([userModel.uid]),
        'following': FieldValue.arrayRemove([userModel.uid]),
      });
      _db
          .collection('user')
          .doc(localUid)
          .collection('followersdata')
          .doc(userModel.uid)
          .update({
        'isfollowers': false,
        //'isblocked': false,
        'isfollowing': true,
        'requestFollow': true,
      });
      _db
          .collection('user')
          .doc(userModel.uid)
          .collection('followersdata')
          .doc(localUid)
          .update({
        'isfollowers': true,
        'isfollowing': false,
        //'isblocked': false,
        'requestFollow': true,
      });
    }
    // if(isfollow == false && isrequestfollow == true && isfollowing == true){
    //    _db.collection('user').doc(localUid).update({
    //     'followers': FieldValue.arrayRemove([userModel.uid]),
    //     'following': FieldValue.arrayRemove([userModel.uid]),
    //   });
    //   _db.collection('user').doc(userModel.uid).update({
    //     'followers': FieldValue.arrayRemove([localUid]),
    //     'following': FieldValue.arrayRemove([localUid]),
    //   });
    //   _db
    //       .collection('user')
    //       .doc(localUid)
    //       .collection('followersdata')
    //       .doc(userModel.uid)
    //       .update({
    //     'isfollowers': false,
    //     //'isblocked': false,
    //     'isfollowing': false,
    //     'requestFollow': false,
    //   });
    //   _db
    //       .collection('user')
    //       .doc(userModel.uid)
    //       .collection('followersdata')
    //       .doc(localUid)
    //       .update({
    //     'isfollowers': false,
    //     //'isblocked': false,
    //     'isfollowing': false,
    //     'requestFollow': false,
    //   });
    // }

    return true;
  }

  Future<void> updatealluserfollowingandfollower() async {
    debugPrint('update follower all user');
    String localUid = _boxStorage.getUserId();
    _db.collection('user').doc(localUid).update({
      'followers': FieldValue.arrayUnion([localUid]),
      'following': FieldValue.arrayUnion([localUid]),
    });
    debugPrint('update follower all user is done');
  }

  Future<List<dynamic>> getFollowerData(String uid) async {
    dynamic data;
    final DocumentReference document = _db.collection("user").doc(uid);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      data = snapshot.data();
    });
    return data['followers'] ?? [];
  }

  Future<List<dynamic>> getFollowingData(String uid) async {
    dynamic data;
    final DocumentReference document = _db.collection("user").doc(uid);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      data = snapshot.data();
    });
    return data['following'] ?? [];
  }

  Future<int> getFollowerCount(String uid) async {
    if (uid == '' || uid.isEmpty) {
      uid = _boxStorage.getUserId();
    }
    int data;
    QuerySnapshot followerlist =
        await _db.collection('user').doc(uid).collection('followersdata').get();
    if (followerlist.size == 0) {
      data = 0;
    } else {
      List<DocumentSnapshot> followerlistcountData = followerlist.docs;
      debugPrint("follower count : ${followerlistcountData.length}");
      data = followerlistcountData.length;
    }

    return data;
  }

  Future<int> getFollowingCount(String uid) async {
    if (uid == '' || uid.isEmpty) {
      uid = _boxStorage.getUserId();
    }
    int data;
    QuerySnapshot followerlist =
        await _db.collection('user').doc(uid).collection('followersdata').get();
    if (followerlist.size == 0) {
      data = 0;
    } else {
      List<DocumentSnapshot> followerlistcountData = followerlist.docs;
      debugPrint("follower count : ${followerlistcountData.length}");
      data = followerlistcountData.length;
    }

    return data;
  }

  Future<dynamic> apiCheckisrequestfollowcurrentuser(
      {required String localuid, required String followersid}) async {
    dynamic data = false;
    bool isdata = false;
    debugPrint('uid : $localuid - follower id : $followersid');
    try {
      final DocumentSnapshot document = await _db
          .collection('user')
          .doc(followersid)
          .collection('followersdata')
          .doc(localuid)
          .get();

      if (document.exists) {
        debugPrint(
            'doc isfollowers : ${document.exists} - ${document['isfollowers']}');
        return document['isfollowers'];
      } else {
        return false;
      }
    } catch (e) {
      debugPrint('error: $e');
      // Get.snackbar('Error', '$e');
      return false;
    }
  }

  Future<dynamic> apiCheckisrequestfollow(
      {required String uid, required String followersid}) async {
    dynamic data = false;
    // bool isdata = false;
    try {
      final DocumentSnapshot document = await _db
          .collection('user')
          .doc(followersid)
          .collection('followersdata')
          .doc(uid)
          .get();

      if (document.exists) {
        debugPrint(
            'doc requestFollow : ${document.exists} - ${document['requestFollow']}');
        return document['requestFollow'];
      } else {
        return false;
      }
    } on FirebaseException catch (e) {
      //Get.snackbar('Error', '$e');
      return false;
    }
  }

  Future<dynamic> apicheckisfollowinguser(
      {required String uid, required String followersid}) async {
    dynamic data = false;
    // bool isdata = false;
    try {
      final DocumentSnapshot document = await _db
          .collection('user')
          .doc(uid)
          .collection('followersdata')
          .doc(followersid)
          .get();

      if (document.exists) {
        debugPrint(
            'doc requestFollow : ${document.exists} - ${document['isfollowing']}');
        return document['isfollowing'];
      } else {
        return false;
      }
    } on FirebaseException catch (e) {
      //Get.snackbar('Error', '$e');
      return false;
    }
  }

  Future<String?> registration(
      {required String email,
      required String password,
      String? username,
      String? avatar,
      String? pin,
      String? phone,
      String? userRole,
      String? userStatus,
      String? userCoverProfile,
      String? fcmtoken,
      bool? isgoogleaccount}) async {
    try {
      debugPrint('is google account = $isgoogleaccount');
      if (isgoogleaccount == true) {
        String uid = _boxStorage.getUserId();
        Map<String, dynamic> data = {
          'uid': uid,
          'email': email,
          'password': password,
          'username': username,
          'avatar': avatar,
          'pin': pin,
          'phone': phone,
          'userRole': userRole,
          'userStatus': userStatus,
          'userCoverProfile': userCoverProfile,
          'fcmtoken': fcmtoken,
          'followers': [uid],
        };
        final querycreateuserwithgoogleaccount = _db.collection('user');
        DocumentReference docAddlikes =
            querycreateuserwithgoogleaccount.doc(uid);
        docAddlikes.set(data);
        _db.collection('followers').doc(uid).collection('follow');
      } else {
        await FirebaseAuth.instance
            .createUserWithEmailAndPassword(
          email: email,
          password: password,
        )
            .then((value) {
          _boxStorage.setUserId(value.user!.uid);
          Map<String, dynamic> data = {
            'uid': value.user!.uid,
            'email': email,
            'password': password,
            'username': username,
            'avatar': avatar,
            'pin': pin,
            'phone': phone,
            'userRole': userRole,
            'userStatus': userStatus,
            'userCoverProfile': userCoverProfile,
            'fcmtoken': fcmtoken,
            'followers': [value.user!.uid],
          };
          _db.collection('user').doc(value.user!.uid).set(data);
          _db.collection('followers').doc(value.user!.uid).collection('follow');
          _boxStorage.setUserAvatar(avatar!);
          _boxStorage.setUserName(username!);
        });
      }

      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      } else {
        return e.message;
      }
    } catch (e) {
      return e.toString();
    }
  }

  Future login({
    required String email,
    required String password,
  }) async {
    debugPrint('$email - $password');
    try {
      // await FirebaseAuth.instance.signOut();
      UserCredential result = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      debugPrint('er : $result');
      // ignore: unnecessary_null_comparison
      if (result != null) {
        debugPrint('er 2 : $result');
        _boxStorage.setUserId(result.user!.uid);
        //_boxStorage.setUserEmail(result.user!.email!);
        debugPrint('run cloduser..');
        String useriddata = _boxStorage.getUserId();
        debugPrint('uid : $useriddata');
        UserModel usermodel = await loginCurrentUser(useriddata);
        if (usermodel.userRole == 'kepala sekolah') {
          //  logincontroller.onSavedRememberMe(false);
          logout();
          _boxStorage.clearCache();
          //logincontroller.isLogginProcess.value = false;
          //await googleSignIn.signOut();
          Get.offAll(const LoginView());
        } else {
          updateFcmToken();
          String username = usermodel.username;
          String photo = usermodel.avatar;
          _boxStorage.setUserAvatar(photo);
          _boxStorage.setUserName(username);
          Get.snackbar('Berhasil Masuk', 'Halo : $username');
          Future.delayed(const Duration(seconds: 3), () async {
            Get.off(() => const DacBottomNavbar());
            debugPrint('data $email - $username - $useriddata - $photo');
          });
        }
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        debugPrint('No user found for that email.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      } else if (e.code == 'wrong-password') {
        debugPrint('Wrong password provided for that user.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      } else if (e.code == 'invalid-email') {
        debugPrint('error Case invalid-email.');
        Get.snackbar('Information', e.code.toString());
        _boxStorage.clearCache();
      }
      debugPrint('Case ${e.code}');
      // Get.snackbar('Information', e.code.toString());
      _boxStorage.clearCache();
      Get.to(const LoginView());
    }
  }

  Future<String?> updateUser(
      {required String uid, required Map<String, Object> data}) async {
    try {
      await _db.collection('user').doc(uid).update(data);
      return 'Success';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      } else {
        return e.message;
      }
    } catch (e) {
      return e.toString();
    }
  }

  Future<bool> resetPassword({required String email}) async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      return true;
    } catch (e) {
      // ignore: avoid_print
      print(e);
      Get.snackbar('Error', '$e'); // showError(title: '...', error: e);
      return false;
    }
  }
}
