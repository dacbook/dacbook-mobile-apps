import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/data/model/feed/feed_model.dart';
import 'package:dac_apps/data/model/feed_comment/feed_comment_model.dart';
import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:dac_apps/data/remote/fcm/sending_fcm.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import 'firestore_notification_services.dart';
import 'firestore_user_services.dart';

class FireStoreFeedServices {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final FireStoreNotificationServices _fireStoreNotificationServices =
      FireStoreNotificationServices();

  Stream<FeedModel> getFeedStoriesDetail(String feedid) {
    debugPrint('feed ID : $feedid');
    return _db
        .collection('story')
        .doc(feedid)
        .snapshots()
        .map((snapshot) => FeedModel.fromJson(snapshot.data()!, snapshot.id));
  }

  Stream<List<FeedModel>> getFeedStories() {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('story')
        .where('author', isEqualTo: uid)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getFeedStoriesFiltered(String filter) {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('story')
        .where('author', isEqualTo: uid)
        .where('category', isEqualTo: filter)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getFeedStoriesFollower(String uid) {
    return _db
        .collection('story')
        .orderBy('date', descending: true)
        .where('author', isEqualTo: uid)
        .where('category', arrayContainsAny: ['all'])
        .where('mediacategory', isEqualTo: 'image')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getFeedStoriesFilteredFollower(
      String filter, String uid) {
    return _db
        .collection('story')
        .orderBy('date', descending: true)
        .where('author', isEqualTo: uid)
        .where('category', arrayContainsAny: [filter])
        .where('mediacategory', isEqualTo: 'image')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getRelateFeedfromfollower() {
    String uid = _boxStorage.getUserId();
    debugPrint('userid : $uid');
    return _db
        .collection('story')
        .orderBy('createdAt', descending: true)
        .where('followers', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getRelateFeedfromfollowerfilteredAll() {
    String uid = _boxStorage.getUserId();
    debugPrint('filtered : userid : $uid');
    return _db
        .collection('story')
        .orderBy('createdAt', descending: true)
        .where('author', isEqualTo: uid)
        .where('category', arrayContainsAny: ['all'])
        .where('mediacategory', isEqualTo: 'image')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getRelateFeedfromfollowerfiltered(String filter) {
    String uid = _boxStorage.getUserId();
    debugPrint('filtered : $filter userid : $uid');
    return _db
        .collection('story')
        .orderBy('date', descending: true)
        .where('author', isEqualTo: uid)
        .where('category', arrayContainsAny: [filter])
        .where('mediacategory', isEqualTo: 'image')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getFeedComment(String id) {
    String uid = _boxStorage.getUserId();
    debugPrint('userid : $uid');
    return _db
        .collection('story')
        .doc(id)
        .collection('comment')
        .orderBy('createdAt', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getFeedCommentReply(
      String feedid, String commentid) {
    return _db
        .collection('story')
        .doc(feedid)
        .collection('comment')
        .doc(commentid)
        .collection('commentreply')
        .orderBy('date', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  addLikeFeed(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final f = DateFormat('hh:mm dd-MM-yyyy');
    debugPrint('userid : $uid');
    final queryaddlikes =
        _db.collection('story').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryaddlikes.doc(uid);
    Map<String, dynamic> dataAddLikes = {
      'feedid': feedmodel.id,
      'id': uid,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'date': f.format(DateTime.now()),
    };
    docAddlikes.set(dataAddLikes);
    final updateCountLikes = _db.collection('story');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(1),
      'followersfavorites': FieldValue.arrayUnion([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  removeLikeFeed(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    num favoritescount = feedmodel.favorites;
    debugPrint('count : $favoritescount');
    if (favoritescount == 0) {
      favoritescount = 0;
    } else {
      favoritescount = -1;
    }
    final queryremovelikes =
        _db.collection('story').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryremovelikes.doc(uid);
    docAddlikes.delete();
    final updateCountLikes = _db.collection('story');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(favoritescount),
      'followersfavorites': FieldValue.arrayRemove([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  addCommentFeed(String comment, FeedModel feedModel) async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint('userid : $uid');
    final collRef =
        _db.collection('story').doc(feedModel.id).collection('comment');
    _db.collection('story').doc(feedModel.id).update({
      'commentcount': FieldValue.increment(1),
    });
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedModel.id,
      'id': docReference.id,
      'commentid': docReference.id,
      'author': feedModel.author,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'comment': comment,
      'mediaFeed': '',
      'createdAt': now.toString(),
      'date': date,
      'time': time,
      'fromUid': uid,
      'toUid': feedModel.author,
    };

    docReference.set(data);
    debugPrint('success add comment');
    if (feedModel.author != uid) {
      String bodydata = comment;
      String titledata = '$username menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedModel.author);
      sendingFcm(
          fcmtoken: usermodel.fcmtoken,
          bodydata: bodydata,
          titledata: titledata,
          feedid: feedModel.id,
          bookid: '',
          notificationCategory: 'story');
      _fireStoreNotificationServices.addNotification(
          userid: usermodel.uid,
          body: bodydata,
          title: titledata,
          feedid: feedModel.id,
          notificationCategory: 'story');
    }
  }

  addCommentFeedReply(String feedid, String commentid, String reply,
      FeedCommentModel feedCommentModel) async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint(
        'userid : $uid - ${feedCommentModel.feedid} - $reply - ${feedCommentModel.commentid}');
    final collRef = _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedid,
      'id': docReference.id,
      'uid': uid,
      'author': feedCommentModel.author,
      'authorName': feedCommentModel.username,
      'username': username,
      'avatar': avatar,
      'comment': feedCommentModel.comment,
      'commentid': commentid,
      'reply': reply,
      'mediaFeed': '',
      'createdAt': now.toString(),
      'date': date,
      'time': time,
      'toUid': feedCommentModel.fromUid,
      'fromUid': uid,
    };
    docReference.set(data);

    _db.collection('story').doc(feedCommentModel.feedid).update({
      'commentcount': FieldValue.increment(1),
    });
    _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .update({
      'countComment': FieldValue.increment(1),
    });
    debugPrint('success add comment');
    if (feedCommentModel.toUid != uid) {
      String bodydata = reply;
      String titledata = '$username menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedCommentModel.uid);
      sendingFcm(
          fcmtoken: usermodel.fcmtoken,
          bodydata: bodydata,
          titledata: titledata,
          feedid: feedCommentModel.feedid,
          bookid: '',
          notificationCategory: 'story');
      _fireStoreNotificationServices.addNotification(
          userid: usermodel.uid,
          body: 'menanggapi, $bodydata',
          title: titledata,
          feedid: feedCommentModel.feedid,
          notificationCategory: 'story');
    }
  }

  deleteFeed(FeedModel feedmodel) {
    final collRef = _db.collection('story');

    DocumentReference docReference = collRef.doc(feedmodel.id);
    docReference.delete();
  }

  deleteCommentReply(FeedCommentModel feedCommentModel) {
    _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .update({
      'countComment': FieldValue.increment(-1),
    });
    final collRef = _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    _db.collection('story').doc(feedCommentModel.feedid).update({
      'commentcount': FieldValue.increment(-1),
    });
    DocumentReference docReference = collRef.doc(feedCommentModel.id);
    docReference.delete();
  }

  deleteCommentallReply(FeedCommentModel feedCommentModel) {
    final collRef = _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    docReference.delete();
  }

  deleteComment(FeedCommentModel feedCommentModel) {
    deleteCommentallReply(feedCommentModel);
    final collRef = _db
        .collection('story')
        .doc(feedCommentModel.feedid)
        .collection('comment');
    _db.collection('story').doc(feedCommentModel.feedid).update({
      'commentcount': FieldValue.increment(-1),
    });
    DocumentReference docReference = collRef.doc(feedCommentModel.commentid);
    docReference.delete();
  }
}
