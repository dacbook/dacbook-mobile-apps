import 'package:cloud_firestore/cloud_firestore.dart';

import '../../model/contact/contact_model.dart';

class FireStoreContactServices {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  String docIdMContact = "5OzPxLfwqXWmmqE31oK7";

  Future<ContactModel> getMcontact() async {
    return _db
        .collection('mContact')
        .doc(docIdMContact)
        .get()
        .then((value) => ContactModel.fromJson(value.data()!, value.id));
  }

  
}