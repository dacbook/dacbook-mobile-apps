import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/notification/notification_model.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import '../../local/box/box_storage.dart';

class FireStoreNotificationServices {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final BoxStorage _boxStorage = BoxStorage();
  String date = '';
  String time = '';
  String isnow = '';
  //String notificationCategory = '';

  Stream<List<NotificationModel>> getNotification() {
    String uid = _boxStorage.getUserId();
    debugPrint('uid => $uid');
    return _db
        .collection('notification')
        .where('usertargetid', isEqualTo: uid)
        .orderBy('createdAt', descending: false)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => NotificationModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  addNotification({
    String? userid,
    String? body,
    String? title,
    String? notificationCategory,
    String? feedid,
    String? bookid,
  }) {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    getLocaltime();
    final collRef = _db.collection('notification');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'usertargetid': userid,
      'id': docReference.id,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'body': body,
      'title': title,
      'createdAt': isnow.toString(),
      'date': date,
      'time': time,
      'notificationCategory': notificationCategory,
      'feedid': feedid,
      'bookid': bookid,
    };
    docReference.set(data);
  }

  getLocaltime() {
    final now = DateTime.now();

    date = DateFormat('dd-MM-yyyy').format(now);
    time = DateFormat('hh:mm').format(now);
    isnow = now.toString();
  }
}
