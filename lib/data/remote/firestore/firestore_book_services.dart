import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/data/model/bookmember/book_member_model.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import '../../local/box/box_storage.dart';
import '../../model/book_position_rank/book_position_rank_model.dart';
import '../../model/feed/feed_model.dart';
import '../../model/feed_comment/feed_comment_model.dart';
import '../../model/user/user_model.dart';
import '../fcm/sending_fcm.dart';
import 'firestore_notification_services.dart';
import 'firestore_user_services.dart';

class FireStoreBookServices {
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final FireStoreNotificationServices _fireStoreNotificationServices =
      FireStoreNotificationServices();

  String? currentUsername;
  String? currentPhotourl;

  Stream<List<BookModel>> getBook() {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('book')
        .where('bookmember', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Future<BookModel> getSingleBook(String bookid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $bookid');
    return _db
        .collection('book').doc(bookid)
        .get()
        .then((value) => BookModel.fromJson(value.data()!, value.id));
  }
  Future<FeedModel> getSingleForum(String forumid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $forumid');
    return _db
        .collection('bookforum').doc(forumid)
        .get()
        .then((value) => FeedModel.fromJson(value.data()!, value.id));
  }
  Future<FeedModel> getSingleAgendaBook(String forumid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $forumid');
    return _db
        .collection('bookagenda').doc(forumid)
        .get()
        .then((value) => FeedModel.fromJson(value.data()!, value.id));
  }
  Future<FeedModel> getSingleStory(String forumid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $forumid');
    return _db
        .collection('story').doc(forumid)
        .get()
        .then((value) => FeedModel.fromJson(value.data()!, value.id));
  }

  Stream<List<BookModel>> getBookSubbook(String bookid) {
    String uid = _boxStorage.getUserId();
    debugPrint('book id : $bookid');
    return _db
        .collection('book')
        .where('bookmasterid', isEqualTo: bookid)
        .where('ismasterbook', isEqualTo: false)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Future<BookModel> getBookDetail(String bookid) async {
    String uid = _boxStorage.getUserId();
    debugPrint('current UserModel : $uid');
    return _db
        .collection('book')
        .doc(bookid)
        .get()
        .then((value) => BookModel.fromJson(value.data()!, value.id));
  }

  Stream<BookModel> getBookDetailStream(String bookid) {
    String uid = _boxStorage.getUserId();
    debugPrint('book id : $bookid');
    return _db
        .collection('book')
        .doc(bookid)
        .snapshots()
        .map((event) => BookModel.fromJson(event.data()!, event.id));
  }

  Stream<List<BookPositionRank>> getPositionRank(String bookid) {
    debugPrint('book id : $bookid');
    return _db
        .collection('book')
        .doc(bookid)
        .collection('positionrank')
        .orderBy('positionLevel', descending: false)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookPositionRank.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<BookmemberModel>> getBookmember(String bookid) {
    debugPrint('book id : $bookid');
    return _db
        .collection('book')
        .doc(bookid)
        .collection('bookmember')
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookmemberModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getGalleryVideo(String bookid) {
    debugPrint('book id : $bookid');
    return _db
        .collection('bookgallery')
        .orderBy('date', descending: true)
        .where('bookid', isEqualTo: bookid)
        .where('category', arrayContainsAny: ['video'])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  deleteMemberbook(String bookid, String uid, bool isadmin) {
    final collRef = _db.collection('book');
    DocumentReference docReference = collRef.doc(bookid);
    Map<String, dynamic> data = {
      'bookmember': FieldValue.arrayRemove([uid]),
      'bookadmin': FieldValue.arrayRemove([uid]),
      'totalmember': FieldValue.increment(-1),
    };
    docReference.update(data);
    final collRef2 =
        _db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference2 = collRef2.doc(uid);
    docReference2.delete();
    if(isadmin == true){
      final collRef3 =
        _db.collection('book').doc(bookid).collection('bookadmin');
    DocumentReference docReference3 = collRef3.doc(uid);
    docReference3.delete();
    }
  }

  addAdminbook(String bookid, String uid) {
    final collRef = _db.collection('book');
    DocumentReference docReference = collRef.doc(bookid);
    Map<String, dynamic> data = {
      'bookadmin': FieldValue.arrayUnion([uid]),
    };
    docReference.update(data);

    final collRef2 =
        _db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference2 = collRef2.doc(uid);
    Map<String, dynamic> data2 = {
      'memberlevel': 'admin',
    };
    docReference2.update(data2);
    debugPrint('add to admin success');
  }

  removeAdminbook(String bookid, String uid) {
    final collRef = _db.collection('book');
    DocumentReference docReference = collRef.doc(bookid);
    Map<String, dynamic> data = {
      'bookadmin': FieldValue.arrayRemove([uid]),
    };
    docReference.update(data);

    final collRef2 =
        _db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference2 = collRef2.doc(uid);
    Map<String, dynamic> data2 = {
      'memberlevel': 'anggota',
    };
    docReference2.update(data2);
    debugPrint('remove to admin success');
  }

  Stream<List<BookModel>> getAllBook() {
    return _db.collection('book').snapshots().map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => BookModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  createPositionRank(
      {required String positionLevel,
      required String positionName,
      required String bookid,
      bool? isupdate,
      String? id}) {
    debugPrint(
        'position data : $positionLevel $positionName , $bookid , $isupdate, $id');
    final collRef =
        _db.collection('book').doc(bookid).collection('positionrank');

    if (isupdate == true) {
      DocumentReference docReference = collRef.doc(id);
      Map<String, dynamic> data = {
        'positionName': positionName,
        'positionLevel': positionLevel,
      };
      docReference.update(data);
    } else {
      DocumentReference docReference = collRef.doc();
      Map<String, dynamic> data = {
        'id': docReference.id,
        'bookmasterid': id,
        'positionName': positionName,
        'positionLevel': positionLevel,
      };
      docReference.set(data);
    }

    debugPrint('success create position rank');
  }

  updatepositionmember(String bookid, String uid, String positionName, String positionLevel) {
    final collRef = _db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference = collRef.doc(uid);
    Map<String, dynamic> data = {
      'positionlevel': positionLevel,
      'positionname': positionName,
    };
    docReference.update(data);
  }

  deletepositionmember(String bookid, String uid) {
    final collRef = _db.collection('book').doc(bookid).collection('bookmember');
    DocumentReference docReference = collRef.doc(uid);
    Map<String, dynamic> data = {
      'positionlevel': FieldValue.delete(),
    };
    docReference.update(data);
  }

  Future<List<dynamic>> getBookmemberBuku(String bookid) async {
    dynamic data;
    final DocumentReference document = _db.collection("book").doc(bookid);

    await document.get().then<dynamic>((DocumentSnapshot snapshot) async {
      data = snapshot.data();
      debugPrint('data => ${data['followers']}');
    });
    return data['bookmember'] ?? [];
  }

  updateforumcurrentuser(String bookid, String uid) async {
    FirebaseFirestore.instance
        .collection('bookforum')
        .where('bookid', isEqualTo: bookid)
        .get()
        .then((snapshot) {
      for (DocumentSnapshot ds in snapshot.docs) {
        ds.reference.update({
          'followers': FieldValue.arrayUnion([uid]),
        });
      }
    });
  }

  Stream<FeedModel> getForumStoriesDetail(String feedid) {
    debugPrint('forum ID : $feedid');
    return _db
        .collection('bookforum')
        .doc(feedid)
        .snapshots()
        .map((snapshot) => FeedModel.fromJson(snapshot.data()!, snapshot.id));
  }

  Stream<FeedModel> getbookagendaDetail(String feedid) {
    debugPrint('forum ID : $feedid');
    return _db
        .collection('bookagenda')
        .doc(feedid)
        .snapshots()
        .map((snapshot) => FeedModel.fromJson(snapshot.data()!, snapshot.id));
  }

  Stream<List<FeedModel>> getforumstory(String bookid) {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('bookforum')
        .orderBy('date', descending: false)
        .where('bookid', isEqualTo: bookid)
        .where('followers', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getagendabook(
      String bookid, String categorykegiatan) {
    String uid = _boxStorage.getUserId();
    debugPrint('book id: $bookid - $categorykegiatan');
    return _db
        .collection('bookagenda')
        .where('categoryKegiatan', isEqualTo: categorykegiatan)
        .where('bookid', isEqualTo: bookid)
        .orderBy('date', descending: true)
        //.where('followers', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedModel>> getforumstorycurrentuser() {
    String uid = _boxStorage.getUserId();
    return _db
        .collection('bookforum')
        .orderBy('date', descending: true)
        //.where('bookid', isEqualTo: bookid)
        .where('followers', arrayContainsAny: [uid])
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getForumComment(String id) {
    String uid = _boxStorage.getUserId();
    debugPrint('userid : $uid');
    return _db
        .collection('bookforum')
        .doc(id)
        .collection('comment')
        .orderBy('createdAt', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getBookagendaComment(String id) {
    String uid = _boxStorage.getUserId();
    debugPrint('userid : $uid - id book : $id');
    return _db
        .collection('bookagenda')
        .doc(id)
        .collection('comment')
        .orderBy('date', descending: true)
        .where('feedid', isEqualTo: id)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getForumCommentReply(
      String feedid, String commentid) {
    return _db
        .collection('bookforum')
        .doc(feedid)
        .collection('comment')
        .doc(commentid)
        .collection('commentreply')
        .orderBy('date', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Stream<List<FeedCommentModel>> getBookAgendaCommentReply(
      String feedid, String commentid) {
    debugPrint('agenda id : $feedid - comment id : $commentid');
    return _db
        .collection('bookagenda')
        .doc(feedid)
        .collection('comment')
        .doc(commentid)
        .collection('commentreply')
        .orderBy('date', descending: true)
        .snapshots()
        .map(
          (snapshot) => snapshot.docs
              .map(
                (doc) => FeedCommentModel.fromJson(doc.data(), doc.id),
              )
              .toList(),
        );
  }

  Future<bool> addLikeforumCheck(FeedModel feedmodel) async {
    String uid = _boxStorage.getUserId();
    bool islikes = feedmodel.followersfavorites.contains(uid);

    if (islikes == false) {
      addLikeForum(feedmodel);
      debugPrint("sucess likes $islikes - ${feedmodel.id} .");
    } else {
      removeLikeForum(feedmodel);
      debugPrint("sucess dislikes $islikes - ${feedmodel.id}.");
    }

    return islikes;
  }

  Future<bool> addLikeBookagendaCheck(FeedModel feedmodel) async {
    String uid = _boxStorage.getUserId();
    bool islikes = feedmodel.followersfavorites.contains(uid);

    if (islikes == false) {
      addLikeBookagenda(feedmodel);
      debugPrint("sucess likes $islikes - ${feedmodel.id} .");
    } else {
      removeLikeBookagenda(feedmodel);
      debugPrint("sucess dislikes $islikes - ${feedmodel.id}.");
    }

    return islikes;
  }

  addLikeForum(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final f = DateFormat('hh:mm dd-MM-yyyy');
    debugPrint('userid : $uid');
    final queryaddlikes =
        _db.collection('bookforum').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryaddlikes.doc(uid);
    Map<String, dynamic> dataAddLikes = {
      'feedid': feedmodel.id,
      'id': uid,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'date': f.format(DateTime.now()),
    };
    docAddlikes.set(dataAddLikes);
    final updateCountLikes = _db.collection('bookforum');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(1),
      'followersfavorites': FieldValue.arrayUnion([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  addLikeBookagenda(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final f = DateFormat('hh:mm dd-MM-yyyy');
    debugPrint('userid : $uid');
    final queryaddlikes =
        _db.collection('bookagenda').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryaddlikes.doc(uid);
    Map<String, dynamic> dataAddLikes = {
      'feedid': feedmodel.id,
      'id': uid,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'date': f.format(DateTime.now()),
    };
    docAddlikes.set(dataAddLikes);
    final updateCountLikes = _db.collection('bookagenda');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(1),
      'followersfavorites': FieldValue.arrayUnion([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  removeLikeForum(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    num favoritescount = feedmodel.favorites;
    debugPrint('count : $favoritescount');
    if (favoritescount == 0) {
      favoritescount = 0;
    } else {
      favoritescount = -1;
    }
    final queryremovelikes =
        _db.collection('bookforum').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryremovelikes.doc(uid);
    docAddlikes.delete();
    final updateCountLikes = _db.collection('bookforum');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(favoritescount),
      'followersfavorites': FieldValue.arrayRemove([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  removeLikeBookagenda(FeedModel feedmodel) {
    String uid = _boxStorage.getUserId();
    num favoritescount = feedmodel.favorites;
    debugPrint('count : $favoritescount');
    if (favoritescount == 0) {
      favoritescount = 0;
    } else {
      favoritescount = -1;
    }
    final queryremovelikes =
        _db.collection('bookagenda').doc(feedmodel.id).collection('like');
    DocumentReference docAddlikes = queryremovelikes.doc(uid);
    docAddlikes.delete();
    final updateCountLikes = _db.collection('bookagenda');
    DocumentReference docReference = updateCountLikes.doc(feedmodel.id);
    Map<String, dynamic> dataCountLikes = {
      'favorites': FieldValue.increment(favoritescount),
      'followersfavorites': FieldValue.arrayRemove([uid]),
    };
    docReference.update(dataCountLikes);
    debugPrint('success add comment');
  }

  addCommentForum(String comment, FeedModel feedModel, List<File> imageListData,
      String category) async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    List<String> imageUrls = [];
    if (imageListData != []) {
      imageUrls = await uploadFiles(imageListData);
    }
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint('userid : $uid');
    _db.collection('bookforum').doc(feedModel.id).update({
      'commentcount': FieldValue.increment(1),
    });
    final collRef =
        _db.collection('bookforum').doc(feedModel.id).collection('comment');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedModel.id,
      'id': docReference.id,
      'commentid': docReference.id,
      'author': feedModel.author,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'comment': comment,
      'category': [category],
      'mediaFeed': '',
      'mediaFeedList': imageUrls,
      'createdAt': now.toString(),
      'date': now.toString(),
      'time': time,
      'fromUid': uid,
      'toUid': feedModel.author,
    };
    docReference.set(data);
    debugPrint('success add comment');
    if (feedModel.author != uid) {
      String bodydata = comment;
      String titledata = '$username menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedModel.author);
      sendingFcm(fcmtoken: usermodel.fcmtoken, bodydata: bodydata,titledata: titledata, feedid: feedModel.id, bookid: feedModel.bookid, notificationCategory: 'bookforum');
      _fireStoreNotificationServices.addNotification(
        userid: usermodel.uid,
        body: bodydata,
        title: titledata,
        feedid: feedModel.id,
        bookid: feedModel.bookid,
        notificationCategory: 'bookforum'
      );
    }
  }

  addCommentBookAgenda(String comment, FeedModel feedModel) async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint('userid : $uid');
    final collRef =
        _db.collection('bookagenda').doc(feedModel.id).collection('comment');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedModel.id,
      'id': docReference.id,
      'commentid': docReference.id,
      'author': feedModel.author,
      'uid': uid,
      'username': username,
      'avatar': avatar,
      'comment': comment,
      'mediaFeed': '',
      'createdAt': now.toString(),
      'date': now.toString(),
      'time': time,
      'fromUid': uid,
      'toUid': feedModel.author,
    };
    docReference.set(data);
      _db.collection('bookagenda').doc(feedModel.id).update({
      'commentcount': FieldValue.increment(1),
    });
    debugPrint('success add comment');
    if (feedModel.author != uid) {
      String bodydata = comment;
      String titledata = '$username menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedModel.author);
       sendingFcm(fcmtoken: usermodel.fcmtoken, bodydata: bodydata,titledata: titledata, feedid: feedModel.id, bookid: feedModel.bookid, notificationCategory: 'bookagenda');
      _fireStoreNotificationServices.addNotification(
        userid: usermodel.uid,
        body: bodydata,
        title: titledata,
        feedid: feedModel.id,
        bookid: feedModel.bookid,
        notificationCategory: 'bookagenda'
      );
    }
  }

  addCommentForumReply(String feedid, String commentid, String reply,
      FeedCommentModel feedCommentModel, String bookid) async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint(
        'userid : $uid - ${feedCommentModel.feedid} - $reply - ${feedCommentModel.commentid}');
    _db.collection('bookforum').doc(feedCommentModel.feedid).update({
      'commentcount': FieldValue.increment(1),
    });

    final collRef = _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedid,
      'id': docReference.id,
      'uid': uid,
      'author': feedCommentModel.author,
      'authorName': feedCommentModel.username,
      'username': username,
      'avatar': avatar,
      'comment': feedCommentModel.comment,
      'commentid': commentid,
      'reply': reply,
      'mediaFeed': '',
      'countComment': FieldValue.increment(1),
      'createdAt': now.toString(),
      'date': now.toString(),
      'time': time,
      'toUid': feedCommentModel.fromUid,
      'fromUid': uid,
    };
    docReference.set(data);
    _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .update({
      'countComment': FieldValue.increment(1),
    });
    debugPrint('success add comment');
    if (feedCommentModel.toUid != uid) {
      String bodydata = reply;
      String titledata = '$username menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedCommentModel.uid);
      sendingFcm(fcmtoken: usermodel.fcmtoken, bodydata: bodydata,titledata: titledata, feedid: feedid, bookid: bookid, notificationCategory: 'bookforum');
      _fireStoreNotificationServices.addNotification(
        userid: usermodel.uid,
        body: 'menanggapi, $bodydata',
        title: titledata,
        feedid: feedCommentModel.id,
        bookid: bookid,
        notificationCategory: 'bookforum'
      );
    }
  }

  addCommentBookAgendaReply(String feedid, String commentid, String reply,
      FeedCommentModel feedCommentModel, String bookid) async {
      await  loaduserprofile();
    String uid = _boxStorage.getUserId();
    final now = DateTime.now();
    final date = DateFormat('dd-MM-yyyy').format(now);
    final time = DateFormat('hh:mm').format(now);
    debugPrint(
        'userid : $uid - ${feedCommentModel.feedid} - $reply - ${feedCommentModel.commentid}');
    final collRef = _db
        .collection('bookagenda')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    Map<String, dynamic> data = {
      'feedid': feedid,
      'id': docReference.id,
      'uid': uid,
      'author': feedCommentModel.author,
      'authorName': feedCommentModel.username,
      'username': currentUsername,
      'avatar': currentPhotourl,
      'comment': feedCommentModel.comment,
      'commentid': commentid,
      'reply': reply,
      'mediaFeed': '',
      'createdAt': now.toString(),
      'date': now.toString(),
      'time': time,
      'toUid': feedCommentModel.fromUid,
      'fromUid': uid,
    };
    docReference.set(data);
    _db
        .collection('bookagenda')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .update({
      'countComment': FieldValue.increment(1),
    });
     _db.collection('bookagenda').doc(feedCommentModel.feedid).update({
      'commentcount': FieldValue.increment(1),
    });
    debugPrint('success add comment agenda');
    if (feedCommentModel.toUid != uid) {
      String bodydata = reply;
      String titledata = '$currentUsername menanggapi';
      UserModel usermodel =
          await _fireStoreUserServices.loginCurrentUser(feedCommentModel.uid);
     sendingFcm(fcmtoken: usermodel.fcmtoken, bodydata: bodydata,titledata: titledata, feedid: feedid, bookid: bookid, notificationCategory: 'bookagenda');
      _fireStoreNotificationServices.addNotification(
        userid: usermodel.uid,
        body: 'menanggapi, $bodydata',
        title: titledata,
         feedid: feedCommentModel.id,
        bookid: bookid,
        notificationCategory: 'bookagenda'
      );
    }
  }

  deleteCommentReplyForum(FeedCommentModel feedCommentModel) {
    _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .update({
      'countComment': FieldValue.increment(-1),
    });
    _db.collection('bookforum').doc(feedCommentModel.feedid)
        // .collection('comment')
        // .doc(feedCommentModel.commentid)
        .update({
      'commentcount': FieldValue.increment(-1),
    });

    final collRef = _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc(feedCommentModel.id);
    docReference.delete();
  }

  deleteCommentaReplyForum(FeedCommentModel feedCommentModel) {
    final collRef = _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    docReference.delete();
    _db.collection('bookforum').doc(feedCommentModel.feedid)
        // .collection('comment')
        // .doc(feedCommentModel.commentid)
        .update({
      'commentcount': FieldValue.increment(-1),
    });
  }

  deleteCommentaReplyBookAgenda(FeedCommentModel feedCommentModel) {
    final collRef = _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment')
        .doc(feedCommentModel.commentid)
        .collection('commentreply');
    DocumentReference docReference = collRef.doc();
    docReference.delete();
  }

  deleteCommentForum(FeedCommentModel feedCommentModel) {
    deleteCommentaReplyForum(feedCommentModel);
    final collRef = _db
        .collection('bookforum')
        .doc(feedCommentModel.feedid)
        .collection('comment');
    _db.collection('bookforum').doc(feedCommentModel.feedid)
        // .collection('comment')
        // .doc(feedCommentModel.commentid)
        .update({
      'commentcount': FieldValue.increment(-1),
    });
    DocumentReference docReference = collRef.doc(feedCommentModel.commentid);
    docReference.delete();
  }

  deleteCommentBookAgenda(FeedCommentModel feedCommentModel) {
    deleteCommentaReplyBookAgenda(feedCommentModel);
    final collRef = _db
        .collection('bookagenda')
        .doc(feedCommentModel.feedid)
        .collection('comment');
    DocumentReference docReference = collRef.doc(feedCommentModel.commentid);
    docReference.delete();
  }

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    // print(imageUrls);
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('bookforum/comment/${imageData.path}');
    UploadTask uploadTask = storageReference.putFile(imageData);
    await uploadTask.whenComplete(() => null);

    return await storageReference.getDownloadURL();
  }


  Future loaduserprofile()async{
     String uid = _boxStorage.getUserId();
    var collection = FirebaseFirestore.instance.collection('user');
var docSnapshot = await collection.doc(uid).get();
if (docSnapshot.exists) {
  Map<String, dynamic>? data = docSnapshot.data();
   currentUsername = data?['username'];
   currentPhotourl = data?['avatar'];
   debugPrint('get profile $currentUsername success');
}
  }
}
