// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookModel _$$_BookModelFromJson(Map<String, dynamic> json) => _$_BookModel(
      id: json['id'] as String,
      bookmasterid: json['bookmasterid'] as String,
      ismasterbook: json['ismasterbook'] as bool,
      bookname: json['bookname'] as String,
      description: json['description'] as String,
      bookimage: json['bookimage'] as String,
      date: json['date'] as String,
      time: json['time'] as String,
      isofficial: json['isofficial'] as String,
      ispublic: json['ispublic'] as String,
      istype: json['istype'] as String,
      ownerid: json['ownerid'] as String,
      pin: json['pin'] as String,
      sharelink: json['sharelink'] as String,
      status: json['status'] as String,
      totalmember: json['totalmember'] as int,
      bookmember: (json['bookmember'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      bookadmin: (json['bookadmin'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      bookmemberrequest: (json['bookmemberrequest'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$$_BookModelToJson(_$_BookModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'bookmasterid': instance.bookmasterid,
      'ismasterbook': instance.ismasterbook,
      'bookname': instance.bookname,
      'description': instance.description,
      'bookimage': instance.bookimage,
      'date': instance.date,
      'time': instance.time,
      'isofficial': instance.isofficial,
      'ispublic': instance.ispublic,
      'istype': instance.istype,
      'ownerid': instance.ownerid,
      'pin': instance.pin,
      'sharelink': instance.sharelink,
      'status': instance.status,
      'totalmember': instance.totalmember,
      'bookmember': instance.bookmember,
      'bookadmin': instance.bookadmin,
      'bookmemberrequest': instance.bookmemberrequest,
    };
