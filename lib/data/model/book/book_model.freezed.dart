// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookModel _$BookModelFromJson(Map<String, dynamic> json) {
  return _BookModel.fromJson(json);
}

/// @nodoc
mixin _$BookModel {
  String get id => throw _privateConstructorUsedError;
  String get bookmasterid => throw _privateConstructorUsedError;
  bool get ismasterbook => throw _privateConstructorUsedError;
  String get bookname => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  String get bookimage => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get isofficial => throw _privateConstructorUsedError;
  String get ispublic => throw _privateConstructorUsedError;
  String get istype => throw _privateConstructorUsedError;
  String get ownerid => throw _privateConstructorUsedError;
  String get pin => throw _privateConstructorUsedError;
  String get sharelink => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  int get totalmember => throw _privateConstructorUsedError;
  List<String> get bookmember => throw _privateConstructorUsedError;
  List<String> get bookadmin => throw _privateConstructorUsedError;
  List<String> get bookmemberrequest => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookModelCopyWith<BookModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookModelCopyWith<$Res> {
  factory $BookModelCopyWith(BookModel value, $Res Function(BookModel) then) =
      _$BookModelCopyWithImpl<$Res, BookModel>;
  @useResult
  $Res call(
      {String id,
      String bookmasterid,
      bool ismasterbook,
      String bookname,
      String description,
      String bookimage,
      String date,
      String time,
      String isofficial,
      String ispublic,
      String istype,
      String ownerid,
      String pin,
      String sharelink,
      String status,
      int totalmember,
      List<String> bookmember,
      List<String> bookadmin,
      List<String> bookmemberrequest});
}

/// @nodoc
class _$BookModelCopyWithImpl<$Res, $Val extends BookModel>
    implements $BookModelCopyWith<$Res> {
  _$BookModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? bookmasterid = null,
    Object? ismasterbook = null,
    Object? bookname = null,
    Object? description = null,
    Object? bookimage = null,
    Object? date = null,
    Object? time = null,
    Object? isofficial = null,
    Object? ispublic = null,
    Object? istype = null,
    Object? ownerid = null,
    Object? pin = null,
    Object? sharelink = null,
    Object? status = null,
    Object? totalmember = null,
    Object? bookmember = null,
    Object? bookadmin = null,
    Object? bookmemberrequest = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterid: null == bookmasterid
          ? _value.bookmasterid
          : bookmasterid // ignore: cast_nullable_to_non_nullable
              as String,
      ismasterbook: null == ismasterbook
          ? _value.ismasterbook
          : ismasterbook // ignore: cast_nullable_to_non_nullable
              as bool,
      bookname: null == bookname
          ? _value.bookname
          : bookname // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      bookimage: null == bookimage
          ? _value.bookimage
          : bookimage // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      isofficial: null == isofficial
          ? _value.isofficial
          : isofficial // ignore: cast_nullable_to_non_nullable
              as String,
      ispublic: null == ispublic
          ? _value.ispublic
          : ispublic // ignore: cast_nullable_to_non_nullable
              as String,
      istype: null == istype
          ? _value.istype
          : istype // ignore: cast_nullable_to_non_nullable
              as String,
      ownerid: null == ownerid
          ? _value.ownerid
          : ownerid // ignore: cast_nullable_to_non_nullable
              as String,
      pin: null == pin
          ? _value.pin
          : pin // ignore: cast_nullable_to_non_nullable
              as String,
      sharelink: null == sharelink
          ? _value.sharelink
          : sharelink // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      totalmember: null == totalmember
          ? _value.totalmember
          : totalmember // ignore: cast_nullable_to_non_nullable
              as int,
      bookmember: null == bookmember
          ? _value.bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as List<String>,
      bookadmin: null == bookadmin
          ? _value.bookadmin
          : bookadmin // ignore: cast_nullable_to_non_nullable
              as List<String>,
      bookmemberrequest: null == bookmemberrequest
          ? _value.bookmemberrequest
          : bookmemberrequest // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookModelCopyWith<$Res> implements $BookModelCopyWith<$Res> {
  factory _$$_BookModelCopyWith(
          _$_BookModel value, $Res Function(_$_BookModel) then) =
      __$$_BookModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String bookmasterid,
      bool ismasterbook,
      String bookname,
      String description,
      String bookimage,
      String date,
      String time,
      String isofficial,
      String ispublic,
      String istype,
      String ownerid,
      String pin,
      String sharelink,
      String status,
      int totalmember,
      List<String> bookmember,
      List<String> bookadmin,
      List<String> bookmemberrequest});
}

/// @nodoc
class __$$_BookModelCopyWithImpl<$Res>
    extends _$BookModelCopyWithImpl<$Res, _$_BookModel>
    implements _$$_BookModelCopyWith<$Res> {
  __$$_BookModelCopyWithImpl(
      _$_BookModel _value, $Res Function(_$_BookModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? bookmasterid = null,
    Object? ismasterbook = null,
    Object? bookname = null,
    Object? description = null,
    Object? bookimage = null,
    Object? date = null,
    Object? time = null,
    Object? isofficial = null,
    Object? ispublic = null,
    Object? istype = null,
    Object? ownerid = null,
    Object? pin = null,
    Object? sharelink = null,
    Object? status = null,
    Object? totalmember = null,
    Object? bookmember = null,
    Object? bookadmin = null,
    Object? bookmemberrequest = null,
  }) {
    return _then(_$_BookModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      bookmasterid: null == bookmasterid
          ? _value.bookmasterid
          : bookmasterid // ignore: cast_nullable_to_non_nullable
              as String,
      ismasterbook: null == ismasterbook
          ? _value.ismasterbook
          : ismasterbook // ignore: cast_nullable_to_non_nullable
              as bool,
      bookname: null == bookname
          ? _value.bookname
          : bookname // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      bookimage: null == bookimage
          ? _value.bookimage
          : bookimage // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      isofficial: null == isofficial
          ? _value.isofficial
          : isofficial // ignore: cast_nullable_to_non_nullable
              as String,
      ispublic: null == ispublic
          ? _value.ispublic
          : ispublic // ignore: cast_nullable_to_non_nullable
              as String,
      istype: null == istype
          ? _value.istype
          : istype // ignore: cast_nullable_to_non_nullable
              as String,
      ownerid: null == ownerid
          ? _value.ownerid
          : ownerid // ignore: cast_nullable_to_non_nullable
              as String,
      pin: null == pin
          ? _value.pin
          : pin // ignore: cast_nullable_to_non_nullable
              as String,
      sharelink: null == sharelink
          ? _value.sharelink
          : sharelink // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      totalmember: null == totalmember
          ? _value.totalmember
          : totalmember // ignore: cast_nullable_to_non_nullable
              as int,
      bookmember: null == bookmember
          ? _value._bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as List<String>,
      bookadmin: null == bookadmin
          ? _value._bookadmin
          : bookadmin // ignore: cast_nullable_to_non_nullable
              as List<String>,
      bookmemberrequest: null == bookmemberrequest
          ? _value._bookmemberrequest
          : bookmemberrequest // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookModel implements _BookModel {
  const _$_BookModel(
      {required this.id,
      required this.bookmasterid,
      required this.ismasterbook,
      required this.bookname,
      required this.description,
      required this.bookimage,
      required this.date,
      required this.time,
      required this.isofficial,
      required this.ispublic,
      required this.istype,
      required this.ownerid,
      required this.pin,
      required this.sharelink,
      required this.status,
      required this.totalmember,
      final List<String> bookmember = const [],
      final List<String> bookadmin = const [],
      final List<String> bookmemberrequest = const []})
      : _bookmember = bookmember,
        _bookadmin = bookadmin,
        _bookmemberrequest = bookmemberrequest;

  factory _$_BookModel.fromJson(Map<String, dynamic> json) =>
      _$$_BookModelFromJson(json);

  @override
  final String id;
  @override
  final String bookmasterid;
  @override
  final bool ismasterbook;
  @override
  final String bookname;
  @override
  final String description;
  @override
  final String bookimage;
  @override
  final String date;
  @override
  final String time;
  @override
  final String isofficial;
  @override
  final String ispublic;
  @override
  final String istype;
  @override
  final String ownerid;
  @override
  final String pin;
  @override
  final String sharelink;
  @override
  final String status;
  @override
  final int totalmember;
  final List<String> _bookmember;
  @override
  @JsonKey()
  List<String> get bookmember {
    if (_bookmember is EqualUnmodifiableListView) return _bookmember;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookmember);
  }

  final List<String> _bookadmin;
  @override
  @JsonKey()
  List<String> get bookadmin {
    if (_bookadmin is EqualUnmodifiableListView) return _bookadmin;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookadmin);
  }

  final List<String> _bookmemberrequest;
  @override
  @JsonKey()
  List<String> get bookmemberrequest {
    if (_bookmemberrequest is EqualUnmodifiableListView)
      return _bookmemberrequest;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_bookmemberrequest);
  }

  @override
  String toString() {
    return 'BookModel(id: $id, bookmasterid: $bookmasterid, ismasterbook: $ismasterbook, bookname: $bookname, description: $description, bookimage: $bookimage, date: $date, time: $time, isofficial: $isofficial, ispublic: $ispublic, istype: $istype, ownerid: $ownerid, pin: $pin, sharelink: $sharelink, status: $status, totalmember: $totalmember, bookmember: $bookmember, bookadmin: $bookadmin, bookmemberrequest: $bookmemberrequest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.bookmasterid, bookmasterid) ||
                other.bookmasterid == bookmasterid) &&
            (identical(other.ismasterbook, ismasterbook) ||
                other.ismasterbook == ismasterbook) &&
            (identical(other.bookname, bookname) ||
                other.bookname == bookname) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.bookimage, bookimage) ||
                other.bookimage == bookimage) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.isofficial, isofficial) ||
                other.isofficial == isofficial) &&
            (identical(other.ispublic, ispublic) ||
                other.ispublic == ispublic) &&
            (identical(other.istype, istype) || other.istype == istype) &&
            (identical(other.ownerid, ownerid) || other.ownerid == ownerid) &&
            (identical(other.pin, pin) || other.pin == pin) &&
            (identical(other.sharelink, sharelink) ||
                other.sharelink == sharelink) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.totalmember, totalmember) ||
                other.totalmember == totalmember) &&
            const DeepCollectionEquality()
                .equals(other._bookmember, _bookmember) &&
            const DeepCollectionEquality()
                .equals(other._bookadmin, _bookadmin) &&
            const DeepCollectionEquality()
                .equals(other._bookmemberrequest, _bookmemberrequest));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        bookmasterid,
        ismasterbook,
        bookname,
        description,
        bookimage,
        date,
        time,
        isofficial,
        ispublic,
        istype,
        ownerid,
        pin,
        sharelink,
        status,
        totalmember,
        const DeepCollectionEquality().hash(_bookmember),
        const DeepCollectionEquality().hash(_bookadmin),
        const DeepCollectionEquality().hash(_bookmemberrequest)
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookModelCopyWith<_$_BookModel> get copyWith =>
      __$$_BookModelCopyWithImpl<_$_BookModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookModelToJson(
      this,
    );
  }
}

abstract class _BookModel implements BookModel {
  const factory _BookModel(
      {required final String id,
      required final String bookmasterid,
      required final bool ismasterbook,
      required final String bookname,
      required final String description,
      required final String bookimage,
      required final String date,
      required final String time,
      required final String isofficial,
      required final String ispublic,
      required final String istype,
      required final String ownerid,
      required final String pin,
      required final String sharelink,
      required final String status,
      required final int totalmember,
      final List<String> bookmember,
      final List<String> bookadmin,
      final List<String> bookmemberrequest}) = _$_BookModel;

  factory _BookModel.fromJson(Map<String, dynamic> json) =
      _$_BookModel.fromJson;

  @override
  String get id;
  @override
  String get bookmasterid;
  @override
  bool get ismasterbook;
  @override
  String get bookname;
  @override
  String get description;
  @override
  String get bookimage;
  @override
  String get date;
  @override
  String get time;
  @override
  String get isofficial;
  @override
  String get ispublic;
  @override
  String get istype;
  @override
  String get ownerid;
  @override
  String get pin;
  @override
  String get sharelink;
  @override
  String get status;
  @override
  int get totalmember;
  @override
  List<String> get bookmember;
  @override
  List<String> get bookadmin;
  @override
  List<String> get bookmemberrequest;
  @override
  @JsonKey(ignore: true)
  _$$_BookModelCopyWith<_$_BookModel> get copyWith =>
      throw _privateConstructorUsedError;
}
