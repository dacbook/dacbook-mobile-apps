// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_NotificationModel _$$_NotificationModelFromJson(Map<String, dynamic> json) =>
    _$_NotificationModel(
      id: json['id'] as String,
      uid: json['uid'] as String,
      usertargetid: json['usertargetid'] as String,
      avatar: json['avatar'] as String,
      username: json['username'] as String,
      title: json['title'] as String,
      body: json['body'] as String,
      date: json['date'] as String,
      time: json['time'] as String,
      createdAt: json['createdAt'] as String,
      path: json['path'] as String? ?? '',
      pathvalue: json['pathvalue'] as String? ?? '',
    );

Map<String, dynamic> _$$_NotificationModelToJson(
        _$_NotificationModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'usertargetid': instance.usertargetid,
      'avatar': instance.avatar,
      'username': instance.username,
      'title': instance.title,
      'body': instance.body,
      'date': instance.date,
      'time': instance.time,
      'createdAt': instance.createdAt,
      'path': instance.path,
      'pathvalue': instance.pathvalue,
    };
