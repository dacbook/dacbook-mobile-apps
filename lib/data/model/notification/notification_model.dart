import 'package:freezed_annotation/freezed_annotation.dart';

part 'notification_model.freezed.dart';
part 'notification_model.g.dart';

@freezed
class NotificationModel with _$NotificationModel {
  //const HomeMenuModel._();
  const factory NotificationModel({
    required String id,
    required String uid,
    required String usertargetid,
    required String avatar,
    required String username,
    required String title,
    required String body,
    required String date,
    required String time,
    required String createdAt,
    @Default('') String path,
    @Default('') String pathvalue,
  }) = _NotificationModel;

  factory NotificationModel.fromJson(Map<String, dynamic> json, String id) =>
      _$NotificationModelFromJson(json);
}
