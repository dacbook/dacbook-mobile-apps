// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feed_comment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FeedCommentModel _$$_FeedCommentModelFromJson(Map<String, dynamic> json) =>
    _$_FeedCommentModel(
      id: json['id'] as String,
      uid: json['uid'] as String,
      feedid: json['feedid'] as String,
      author: json['author'] as String,
      comment: json['comment'] as String,
      commentid: json['commentid'] as String,
      time: json['time'] as String,
      username: json['username'] as String,
      avatar: json['avatar'] as String,
      date: json['date'] as String? ?? '',
      createdAt: json['createdAt'] as String? ?? '',
      mediaFeed: json['mediaFeed'] as String? ?? '',
      reply: json['reply'] as String? ?? '',
      fromUid: json['fromUid'] as String? ?? '',
      toUid: json['toUid'] as String? ?? '',
      authorName: json['authorName'] as String? ?? '',
      mediaFeedList: (json['mediaFeedList'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      category: (json['category'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      countComment: json['countComment'] as int? ?? 0,
    );

Map<String, dynamic> _$$_FeedCommentModelToJson(_$_FeedCommentModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'feedid': instance.feedid,
      'author': instance.author,
      'comment': instance.comment,
      'commentid': instance.commentid,
      'time': instance.time,
      'username': instance.username,
      'avatar': instance.avatar,
      'date': instance.date,
      'createdAt': instance.createdAt,
      'mediaFeed': instance.mediaFeed,
      'reply': instance.reply,
      'fromUid': instance.fromUid,
      'toUid': instance.toUid,
      'authorName': instance.authorName,
      'mediaFeedList': instance.mediaFeedList,
      'category': instance.category,
      'countComment': instance.countComment,
    };
