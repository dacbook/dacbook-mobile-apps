import 'package:freezed_annotation/freezed_annotation.dart';

part 'feed_comment_model.freezed.dart';
part 'feed_comment_model.g.dart';

@freezed
class FeedCommentModel with _$FeedCommentModel {
  //const HomeMenuModel._();
  const factory FeedCommentModel({
    required String id,
    required String uid,
    required String feedid,
    required String author,
    required String comment,
    required String commentid,
    required String time,
    required String username,
    required String avatar,
    @Default('') String date,
    @Default('') String createdAt,
    @Default('') String mediaFeed,
    @Default('') String reply,
    @Default('') String fromUid,
    @Default('') String toUid,
    @Default('') String authorName,
    @Default([]) List<String> mediaFeedList,
    @Default([]) List<String> category,
    @Default(0) int countComment,
  }) = _FeedCommentModel;

  factory FeedCommentModel.fromJson(Map<String, dynamic> json, String id) =>
      _$FeedCommentModelFromJson(json);
}
