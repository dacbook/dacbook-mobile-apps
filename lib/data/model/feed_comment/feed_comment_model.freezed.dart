// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'feed_comment_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FeedCommentModel _$FeedCommentModelFromJson(Map<String, dynamic> json) {
  return _FeedCommentModel.fromJson(json);
}

/// @nodoc
mixin _$FeedCommentModel {
  String get id => throw _privateConstructorUsedError;
  String get uid => throw _privateConstructorUsedError;
  String get feedid => throw _privateConstructorUsedError;
  String get author => throw _privateConstructorUsedError;
  String get comment => throw _privateConstructorUsedError;
  String get commentid => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get createdAt => throw _privateConstructorUsedError;
  String get mediaFeed => throw _privateConstructorUsedError;
  String get reply => throw _privateConstructorUsedError;
  String get fromUid => throw _privateConstructorUsedError;
  String get toUid => throw _privateConstructorUsedError;
  String get authorName => throw _privateConstructorUsedError;
  List<String> get mediaFeedList => throw _privateConstructorUsedError;
  List<String> get category => throw _privateConstructorUsedError;
  int get countComment => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FeedCommentModelCopyWith<FeedCommentModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeedCommentModelCopyWith<$Res> {
  factory $FeedCommentModelCopyWith(
          FeedCommentModel value, $Res Function(FeedCommentModel) then) =
      _$FeedCommentModelCopyWithImpl<$Res, FeedCommentModel>;
  @useResult
  $Res call(
      {String id,
      String uid,
      String feedid,
      String author,
      String comment,
      String commentid,
      String time,
      String username,
      String avatar,
      String date,
      String createdAt,
      String mediaFeed,
      String reply,
      String fromUid,
      String toUid,
      String authorName,
      List<String> mediaFeedList,
      List<String> category,
      int countComment});
}

/// @nodoc
class _$FeedCommentModelCopyWithImpl<$Res, $Val extends FeedCommentModel>
    implements $FeedCommentModelCopyWith<$Res> {
  _$FeedCommentModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? uid = null,
    Object? feedid = null,
    Object? author = null,
    Object? comment = null,
    Object? commentid = null,
    Object? time = null,
    Object? username = null,
    Object? avatar = null,
    Object? date = null,
    Object? createdAt = null,
    Object? mediaFeed = null,
    Object? reply = null,
    Object? fromUid = null,
    Object? toUid = null,
    Object? authorName = null,
    Object? mediaFeedList = null,
    Object? category = null,
    Object? countComment = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      feedid: null == feedid
          ? _value.feedid
          : feedid // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      commentid: null == commentid
          ? _value.commentid
          : commentid // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeed: null == mediaFeed
          ? _value.mediaFeed
          : mediaFeed // ignore: cast_nullable_to_non_nullable
              as String,
      reply: null == reply
          ? _value.reply
          : reply // ignore: cast_nullable_to_non_nullable
              as String,
      fromUid: null == fromUid
          ? _value.fromUid
          : fromUid // ignore: cast_nullable_to_non_nullable
              as String,
      toUid: null == toUid
          ? _value.toUid
          : toUid // ignore: cast_nullable_to_non_nullable
              as String,
      authorName: null == authorName
          ? _value.authorName
          : authorName // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeedList: null == mediaFeedList
          ? _value.mediaFeedList
          : mediaFeedList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      category: null == category
          ? _value.category
          : category // ignore: cast_nullable_to_non_nullable
              as List<String>,
      countComment: null == countComment
          ? _value.countComment
          : countComment // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FeedCommentModelCopyWith<$Res>
    implements $FeedCommentModelCopyWith<$Res> {
  factory _$$_FeedCommentModelCopyWith(
          _$_FeedCommentModel value, $Res Function(_$_FeedCommentModel) then) =
      __$$_FeedCommentModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String uid,
      String feedid,
      String author,
      String comment,
      String commentid,
      String time,
      String username,
      String avatar,
      String date,
      String createdAt,
      String mediaFeed,
      String reply,
      String fromUid,
      String toUid,
      String authorName,
      List<String> mediaFeedList,
      List<String> category,
      int countComment});
}

/// @nodoc
class __$$_FeedCommentModelCopyWithImpl<$Res>
    extends _$FeedCommentModelCopyWithImpl<$Res, _$_FeedCommentModel>
    implements _$$_FeedCommentModelCopyWith<$Res> {
  __$$_FeedCommentModelCopyWithImpl(
      _$_FeedCommentModel _value, $Res Function(_$_FeedCommentModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? uid = null,
    Object? feedid = null,
    Object? author = null,
    Object? comment = null,
    Object? commentid = null,
    Object? time = null,
    Object? username = null,
    Object? avatar = null,
    Object? date = null,
    Object? createdAt = null,
    Object? mediaFeed = null,
    Object? reply = null,
    Object? fromUid = null,
    Object? toUid = null,
    Object? authorName = null,
    Object? mediaFeedList = null,
    Object? category = null,
    Object? countComment = null,
  }) {
    return _then(_$_FeedCommentModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      feedid: null == feedid
          ? _value.feedid
          : feedid // ignore: cast_nullable_to_non_nullable
              as String,
      author: null == author
          ? _value.author
          : author // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      commentid: null == commentid
          ? _value.commentid
          : commentid // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeed: null == mediaFeed
          ? _value.mediaFeed
          : mediaFeed // ignore: cast_nullable_to_non_nullable
              as String,
      reply: null == reply
          ? _value.reply
          : reply // ignore: cast_nullable_to_non_nullable
              as String,
      fromUid: null == fromUid
          ? _value.fromUid
          : fromUid // ignore: cast_nullable_to_non_nullable
              as String,
      toUid: null == toUid
          ? _value.toUid
          : toUid // ignore: cast_nullable_to_non_nullable
              as String,
      authorName: null == authorName
          ? _value.authorName
          : authorName // ignore: cast_nullable_to_non_nullable
              as String,
      mediaFeedList: null == mediaFeedList
          ? _value._mediaFeedList
          : mediaFeedList // ignore: cast_nullable_to_non_nullable
              as List<String>,
      category: null == category
          ? _value._category
          : category // ignore: cast_nullable_to_non_nullable
              as List<String>,
      countComment: null == countComment
          ? _value.countComment
          : countComment // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FeedCommentModel implements _FeedCommentModel {
  const _$_FeedCommentModel(
      {required this.id,
      required this.uid,
      required this.feedid,
      required this.author,
      required this.comment,
      required this.commentid,
      required this.time,
      required this.username,
      required this.avatar,
      this.date = '',
      this.createdAt = '',
      this.mediaFeed = '',
      this.reply = '',
      this.fromUid = '',
      this.toUid = '',
      this.authorName = '',
      final List<String> mediaFeedList = const [],
      final List<String> category = const [],
      this.countComment = 0})
      : _mediaFeedList = mediaFeedList,
        _category = category;

  factory _$_FeedCommentModel.fromJson(Map<String, dynamic> json) =>
      _$$_FeedCommentModelFromJson(json);

  @override
  final String id;
  @override
  final String uid;
  @override
  final String feedid;
  @override
  final String author;
  @override
  final String comment;
  @override
  final String commentid;
  @override
  final String time;
  @override
  final String username;
  @override
  final String avatar;
  @override
  @JsonKey()
  final String date;
  @override
  @JsonKey()
  final String createdAt;
  @override
  @JsonKey()
  final String mediaFeed;
  @override
  @JsonKey()
  final String reply;
  @override
  @JsonKey()
  final String fromUid;
  @override
  @JsonKey()
  final String toUid;
  @override
  @JsonKey()
  final String authorName;
  final List<String> _mediaFeedList;
  @override
  @JsonKey()
  List<String> get mediaFeedList {
    if (_mediaFeedList is EqualUnmodifiableListView) return _mediaFeedList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_mediaFeedList);
  }

  final List<String> _category;
  @override
  @JsonKey()
  List<String> get category {
    if (_category is EqualUnmodifiableListView) return _category;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_category);
  }

  @override
  @JsonKey()
  final int countComment;

  @override
  String toString() {
    return 'FeedCommentModel(id: $id, uid: $uid, feedid: $feedid, author: $author, comment: $comment, commentid: $commentid, time: $time, username: $username, avatar: $avatar, date: $date, createdAt: $createdAt, mediaFeed: $mediaFeed, reply: $reply, fromUid: $fromUid, toUid: $toUid, authorName: $authorName, mediaFeedList: $mediaFeedList, category: $category, countComment: $countComment)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FeedCommentModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.feedid, feedid) || other.feedid == feedid) &&
            (identical(other.author, author) || other.author == author) &&
            (identical(other.comment, comment) || other.comment == comment) &&
            (identical(other.commentid, commentid) ||
                other.commentid == commentid) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.avatar, avatar) || other.avatar == avatar) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.mediaFeed, mediaFeed) ||
                other.mediaFeed == mediaFeed) &&
            (identical(other.reply, reply) || other.reply == reply) &&
            (identical(other.fromUid, fromUid) || other.fromUid == fromUid) &&
            (identical(other.toUid, toUid) || other.toUid == toUid) &&
            (identical(other.authorName, authorName) ||
                other.authorName == authorName) &&
            const DeepCollectionEquality()
                .equals(other._mediaFeedList, _mediaFeedList) &&
            const DeepCollectionEquality().equals(other._category, _category) &&
            (identical(other.countComment, countComment) ||
                other.countComment == countComment));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        uid,
        feedid,
        author,
        comment,
        commentid,
        time,
        username,
        avatar,
        date,
        createdAt,
        mediaFeed,
        reply,
        fromUid,
        toUid,
        authorName,
        const DeepCollectionEquality().hash(_mediaFeedList),
        const DeepCollectionEquality().hash(_category),
        countComment
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FeedCommentModelCopyWith<_$_FeedCommentModel> get copyWith =>
      __$$_FeedCommentModelCopyWithImpl<_$_FeedCommentModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FeedCommentModelToJson(
      this,
    );
  }
}

abstract class _FeedCommentModel implements FeedCommentModel {
  const factory _FeedCommentModel(
      {required final String id,
      required final String uid,
      required final String feedid,
      required final String author,
      required final String comment,
      required final String commentid,
      required final String time,
      required final String username,
      required final String avatar,
      final String date,
      final String createdAt,
      final String mediaFeed,
      final String reply,
      final String fromUid,
      final String toUid,
      final String authorName,
      final List<String> mediaFeedList,
      final List<String> category,
      final int countComment}) = _$_FeedCommentModel;

  factory _FeedCommentModel.fromJson(Map<String, dynamic> json) =
      _$_FeedCommentModel.fromJson;

  @override
  String get id;
  @override
  String get uid;
  @override
  String get feedid;
  @override
  String get author;
  @override
  String get comment;
  @override
  String get commentid;
  @override
  String get time;
  @override
  String get username;
  @override
  String get avatar;
  @override
  String get date;
  @override
  String get createdAt;
  @override
  String get mediaFeed;
  @override
  String get reply;
  @override
  String get fromUid;
  @override
  String get toUid;
  @override
  String get authorName;
  @override
  List<String> get mediaFeedList;
  @override
  List<String> get category;
  @override
  int get countComment;
  @override
  @JsonKey(ignore: true)
  _$$_FeedCommentModelCopyWith<_$_FeedCommentModel> get copyWith =>
      throw _privateConstructorUsedError;
}
