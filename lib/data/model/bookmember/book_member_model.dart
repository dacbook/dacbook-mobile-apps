import 'package:freezed_annotation/freezed_annotation.dart';

part 'book_member_model.freezed.dart';
part 'book_member_model.g.dart';

@freezed
class BookmemberModel with _$BookmemberModel {
  //const HomeMenuModel._();
  const factory BookmemberModel({
    required String bookmember,
    required String avatar,
    required String date,
    required String memberlevel,
    required String time,
    required String uid,
    required String username,
    @Default('') String positionlevel,
    @Default('') String positionname,

    // required String useravatar,
    // @Default([]) List<String> followers,
  }) = _BookmemberModel;

  factory BookmemberModel.fromJson(Map<String, dynamic> json, String id) =>
      _$BookmemberModelFromJson(json);
}
