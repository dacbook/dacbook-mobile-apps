// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_member_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookmemberModel _$BookmemberModelFromJson(Map<String, dynamic> json) {
  return _BookmemberModel.fromJson(json);
}

/// @nodoc
mixin _$BookmemberModel {
  String get bookmember => throw _privateConstructorUsedError;
  String get avatar => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get memberlevel => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String get uid => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get positionlevel => throw _privateConstructorUsedError;
  String get positionname => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookmemberModelCopyWith<BookmemberModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookmemberModelCopyWith<$Res> {
  factory $BookmemberModelCopyWith(
          BookmemberModel value, $Res Function(BookmemberModel) then) =
      _$BookmemberModelCopyWithImpl<$Res, BookmemberModel>;
  @useResult
  $Res call(
      {String bookmember,
      String avatar,
      String date,
      String memberlevel,
      String time,
      String uid,
      String username,
      String positionlevel,
      String positionname});
}

/// @nodoc
class _$BookmemberModelCopyWithImpl<$Res, $Val extends BookmemberModel>
    implements $BookmemberModelCopyWith<$Res> {
  _$BookmemberModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? bookmember = null,
    Object? avatar = null,
    Object? date = null,
    Object? memberlevel = null,
    Object? time = null,
    Object? uid = null,
    Object? username = null,
    Object? positionlevel = null,
    Object? positionname = null,
  }) {
    return _then(_value.copyWith(
      bookmember: null == bookmember
          ? _value.bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      memberlevel: null == memberlevel
          ? _value.memberlevel
          : memberlevel // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      positionlevel: null == positionlevel
          ? _value.positionlevel
          : positionlevel // ignore: cast_nullable_to_non_nullable
              as String,
      positionname: null == positionname
          ? _value.positionname
          : positionname // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookmemberModelCopyWith<$Res>
    implements $BookmemberModelCopyWith<$Res> {
  factory _$$_BookmemberModelCopyWith(
          _$_BookmemberModel value, $Res Function(_$_BookmemberModel) then) =
      __$$_BookmemberModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String bookmember,
      String avatar,
      String date,
      String memberlevel,
      String time,
      String uid,
      String username,
      String positionlevel,
      String positionname});
}

/// @nodoc
class __$$_BookmemberModelCopyWithImpl<$Res>
    extends _$BookmemberModelCopyWithImpl<$Res, _$_BookmemberModel>
    implements _$$_BookmemberModelCopyWith<$Res> {
  __$$_BookmemberModelCopyWithImpl(
      _$_BookmemberModel _value, $Res Function(_$_BookmemberModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? bookmember = null,
    Object? avatar = null,
    Object? date = null,
    Object? memberlevel = null,
    Object? time = null,
    Object? uid = null,
    Object? username = null,
    Object? positionlevel = null,
    Object? positionname = null,
  }) {
    return _then(_$_BookmemberModel(
      bookmember: null == bookmember
          ? _value.bookmember
          : bookmember // ignore: cast_nullable_to_non_nullable
              as String,
      avatar: null == avatar
          ? _value.avatar
          : avatar // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      memberlevel: null == memberlevel
          ? _value.memberlevel
          : memberlevel // ignore: cast_nullable_to_non_nullable
              as String,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      positionlevel: null == positionlevel
          ? _value.positionlevel
          : positionlevel // ignore: cast_nullable_to_non_nullable
              as String,
      positionname: null == positionname
          ? _value.positionname
          : positionname // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookmemberModel implements _BookmemberModel {
  const _$_BookmemberModel(
      {required this.bookmember,
      required this.avatar,
      required this.date,
      required this.memberlevel,
      required this.time,
      required this.uid,
      required this.username,
      this.positionlevel = '',
      this.positionname = ''});

  factory _$_BookmemberModel.fromJson(Map<String, dynamic> json) =>
      _$$_BookmemberModelFromJson(json);

  @override
  final String bookmember;
  @override
  final String avatar;
  @override
  final String date;
  @override
  final String memberlevel;
  @override
  final String time;
  @override
  final String uid;
  @override
  final String username;
  @override
  @JsonKey()
  final String positionlevel;
  @override
  @JsonKey()
  final String positionname;

  @override
  String toString() {
    return 'BookmemberModel(bookmember: $bookmember, avatar: $avatar, date: $date, memberlevel: $memberlevel, time: $time, uid: $uid, username: $username, positionlevel: $positionlevel, positionname: $positionname)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookmemberModel &&
            (identical(other.bookmember, bookmember) ||
                other.bookmember == bookmember) &&
            (identical(other.avatar, avatar) || other.avatar == avatar) &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.memberlevel, memberlevel) ||
                other.memberlevel == memberlevel) &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.positionlevel, positionlevel) ||
                other.positionlevel == positionlevel) &&
            (identical(other.positionname, positionname) ||
                other.positionname == positionname));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, bookmember, avatar, date,
      memberlevel, time, uid, username, positionlevel, positionname);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookmemberModelCopyWith<_$_BookmemberModel> get copyWith =>
      __$$_BookmemberModelCopyWithImpl<_$_BookmemberModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookmemberModelToJson(
      this,
    );
  }
}

abstract class _BookmemberModel implements BookmemberModel {
  const factory _BookmemberModel(
      {required final String bookmember,
      required final String avatar,
      required final String date,
      required final String memberlevel,
      required final String time,
      required final String uid,
      required final String username,
      final String positionlevel,
      final String positionname}) = _$_BookmemberModel;

  factory _BookmemberModel.fromJson(Map<String, dynamic> json) =
      _$_BookmemberModel.fromJson;

  @override
  String get bookmember;
  @override
  String get avatar;
  @override
  String get date;
  @override
  String get memberlevel;
  @override
  String get time;
  @override
  String get uid;
  @override
  String get username;
  @override
  String get positionlevel;
  @override
  String get positionname;
  @override
  @JsonKey(ignore: true)
  _$$_BookmemberModelCopyWith<_$_BookmemberModel> get copyWith =>
      throw _privateConstructorUsedError;
}
