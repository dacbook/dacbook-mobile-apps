// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'book_member_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_BookmemberModel _$$_BookmemberModelFromJson(Map<String, dynamic> json) =>
    _$_BookmemberModel(
      bookmember: json['bookmember'] as String,
      avatar: json['avatar'] as String,
      date: json['date'] as String,
      memberlevel: json['memberlevel'] as String,
      time: json['time'] as String,
      uid: json['uid'] as String,
      username: json['username'] as String,
      positionlevel: json['positionlevel'] as String? ?? '',
      positionname: json['positionname'] as String? ?? '',
    );

Map<String, dynamic> _$$_BookmemberModelToJson(_$_BookmemberModel instance) =>
    <String, dynamic>{
      'bookmember': instance.bookmember,
      'avatar': instance.avatar,
      'date': instance.date,
      'memberlevel': instance.memberlevel,
      'time': instance.time,
      'uid': instance.uid,
      'username': instance.username,
      'positionlevel': instance.positionlevel,
      'positionname': instance.positionname,
    };
