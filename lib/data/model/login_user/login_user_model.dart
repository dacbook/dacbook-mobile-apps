import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_user_model.freezed.dart';

@freezed
class LoginUserModel with _$LoginUserModel {
  //const HomeMenuModel._();
  const factory LoginUserModel({
    required String email,
    required String password,
  }) = _LoginUserModel;

}