import 'package:freezed_annotation/freezed_annotation.dart';

part 'avatar_model.freezed.dart';
part 'avatar_model.g.dart';

@freezed
class AvatarModel with _$AvatarModel {
  //const HomeMenuModel._();
  const factory AvatarModel({
    required String id,
    @Default('') String name,
    @Default('') String url,

    // required String useravatar,
    // @Default([]) List<String> followers,
  }) = _AvatarModel;

  factory AvatarModel.fromJson(Map<String, dynamic> json, String id) =>
      _$AvatarModelFromJson(json);
}
