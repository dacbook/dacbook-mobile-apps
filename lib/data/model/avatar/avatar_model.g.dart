// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'avatar_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AvatarModel _$$_AvatarModelFromJson(Map<String, dynamic> json) =>
    _$_AvatarModel(
      id: json['id'] as String,
      name: json['name'] as String? ?? '',
      url: json['url'] as String? ?? '',
    );

Map<String, dynamic> _$$_AvatarModelToJson(_$_AvatarModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'url': instance.url,
    };
