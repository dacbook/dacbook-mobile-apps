// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'book_position_rank_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BookPositionRank _$BookPositionRankFromJson(Map<String, dynamic> json) {
  return _BookPositionRank.fromJson(json);
}

/// @nodoc
mixin _$BookPositionRank {
  String get id => throw _privateConstructorUsedError;
  String get positionName => throw _privateConstructorUsedError;
  String get positionLevel => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BookPositionRankCopyWith<BookPositionRank> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BookPositionRankCopyWith<$Res> {
  factory $BookPositionRankCopyWith(
          BookPositionRank value, $Res Function(BookPositionRank) then) =
      _$BookPositionRankCopyWithImpl<$Res, BookPositionRank>;
  @useResult
  $Res call({String id, String positionName, String positionLevel});
}

/// @nodoc
class _$BookPositionRankCopyWithImpl<$Res, $Val extends BookPositionRank>
    implements $BookPositionRankCopyWith<$Res> {
  _$BookPositionRankCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? positionName = null,
    Object? positionLevel = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      positionName: null == positionName
          ? _value.positionName
          : positionName // ignore: cast_nullable_to_non_nullable
              as String,
      positionLevel: null == positionLevel
          ? _value.positionLevel
          : positionLevel // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BookPositionRankCopyWith<$Res>
    implements $BookPositionRankCopyWith<$Res> {
  factory _$$_BookPositionRankCopyWith(
          _$_BookPositionRank value, $Res Function(_$_BookPositionRank) then) =
      __$$_BookPositionRankCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String id, String positionName, String positionLevel});
}

/// @nodoc
class __$$_BookPositionRankCopyWithImpl<$Res>
    extends _$BookPositionRankCopyWithImpl<$Res, _$_BookPositionRank>
    implements _$$_BookPositionRankCopyWith<$Res> {
  __$$_BookPositionRankCopyWithImpl(
      _$_BookPositionRank _value, $Res Function(_$_BookPositionRank) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? positionName = null,
    Object? positionLevel = null,
  }) {
    return _then(_$_BookPositionRank(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      positionName: null == positionName
          ? _value.positionName
          : positionName // ignore: cast_nullable_to_non_nullable
              as String,
      positionLevel: null == positionLevel
          ? _value.positionLevel
          : positionLevel // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BookPositionRank implements _BookPositionRank {
  const _$_BookPositionRank(
      {required this.id,
      required this.positionName,
      required this.positionLevel});

  factory _$_BookPositionRank.fromJson(Map<String, dynamic> json) =>
      _$$_BookPositionRankFromJson(json);

  @override
  final String id;
  @override
  final String positionName;
  @override
  final String positionLevel;

  @override
  String toString() {
    return 'BookPositionRank(id: $id, positionName: $positionName, positionLevel: $positionLevel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BookPositionRank &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.positionName, positionName) ||
                other.positionName == positionName) &&
            (identical(other.positionLevel, positionLevel) ||
                other.positionLevel == positionLevel));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, positionName, positionLevel);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BookPositionRankCopyWith<_$_BookPositionRank> get copyWith =>
      __$$_BookPositionRankCopyWithImpl<_$_BookPositionRank>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BookPositionRankToJson(
      this,
    );
  }
}

abstract class _BookPositionRank implements BookPositionRank {
  const factory _BookPositionRank(
      {required final String id,
      required final String positionName,
      required final String positionLevel}) = _$_BookPositionRank;

  factory _BookPositionRank.fromJson(Map<String, dynamic> json) =
      _$_BookPositionRank.fromJson;

  @override
  String get id;
  @override
  String get positionName;
  @override
  String get positionLevel;
  @override
  @JsonKey(ignore: true)
  _$$_BookPositionRankCopyWith<_$_BookPositionRank> get copyWith =>
      throw _privateConstructorUsedError;
}
