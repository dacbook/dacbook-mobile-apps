class FollowerListModel {
  final String? currentid;

  const FollowerListModel(this.currentid);

  @override
  String toString() {
    return 'follower: {currentid: ${currentid}}';
  }
}