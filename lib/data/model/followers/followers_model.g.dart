// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'followers_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_FollowerModel _$$_FollowerModelFromJson(Map<String, dynamic> json) =>
    _$_FollowerModel(
      uid: json['uid'] as String,
      followers: json['followers'] as String,
      following: json['following'] as String,
      isfollowers: json['isfollowers'] as bool? ?? false,
      isblocked: json['isblocked'] as bool? ?? false,
      requestFollow: json['requestFollow'] as bool? ?? false,
      username: json['username'] as String,
      useravatar: json['useravatar'] as String,
    );

Map<String, dynamic> _$$_FollowerModelToJson(_$_FollowerModel instance) =>
    <String, dynamic>{
      'uid': instance.uid,
      'followers': instance.followers,
      'following': instance.following,
      'isfollowers': instance.isfollowers,
      'isblocked': instance.isblocked,
      'requestFollow': instance.requestFollow,
      'username': instance.username,
      'useravatar': instance.useravatar,
    };
