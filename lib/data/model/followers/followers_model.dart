import 'package:freezed_annotation/freezed_annotation.dart';

part 'followers_model.freezed.dart';
part 'followers_model.g.dart';

@freezed
class FollowerModel with _$FollowerModel {
  //const HomeMenuModel._();
  const factory FollowerModel({
    required String uid,
    required String followers,
    required String following,
    @Default(false)  bool isfollowers,
    @Default(false)  bool isblocked,
    @Default(false)  bool requestFollow,
    required String username,
    required String useravatar,
  }) = _FollowerModel;

   factory FollowerModel.fromJson(Map<String, dynamic> json, String id) => _$FollowerModelFromJson(json);
}