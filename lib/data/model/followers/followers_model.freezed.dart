// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'followers_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FollowerModel _$FollowerModelFromJson(Map<String, dynamic> json) {
  return _FollowerModel.fromJson(json);
}

/// @nodoc
mixin _$FollowerModel {
  String get uid => throw _privateConstructorUsedError;
  String get followers => throw _privateConstructorUsedError;
  String get following => throw _privateConstructorUsedError;
  bool get isfollowers => throw _privateConstructorUsedError;
  bool get isblocked => throw _privateConstructorUsedError;
  bool get requestFollow => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get useravatar => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FollowerModelCopyWith<FollowerModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FollowerModelCopyWith<$Res> {
  factory $FollowerModelCopyWith(
          FollowerModel value, $Res Function(FollowerModel) then) =
      _$FollowerModelCopyWithImpl<$Res, FollowerModel>;
  @useResult
  $Res call(
      {String uid,
      String followers,
      String following,
      bool isfollowers,
      bool isblocked,
      bool requestFollow,
      String username,
      String useravatar});
}

/// @nodoc
class _$FollowerModelCopyWithImpl<$Res, $Val extends FollowerModel>
    implements $FollowerModelCopyWith<$Res> {
  _$FollowerModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? followers = null,
    Object? following = null,
    Object? isfollowers = null,
    Object? isblocked = null,
    Object? requestFollow = null,
    Object? username = null,
    Object? useravatar = null,
  }) {
    return _then(_value.copyWith(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      followers: null == followers
          ? _value.followers
          : followers // ignore: cast_nullable_to_non_nullable
              as String,
      following: null == following
          ? _value.following
          : following // ignore: cast_nullable_to_non_nullable
              as String,
      isfollowers: null == isfollowers
          ? _value.isfollowers
          : isfollowers // ignore: cast_nullable_to_non_nullable
              as bool,
      isblocked: null == isblocked
          ? _value.isblocked
          : isblocked // ignore: cast_nullable_to_non_nullable
              as bool,
      requestFollow: null == requestFollow
          ? _value.requestFollow
          : requestFollow // ignore: cast_nullable_to_non_nullable
              as bool,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      useravatar: null == useravatar
          ? _value.useravatar
          : useravatar // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_FollowerModelCopyWith<$Res>
    implements $FollowerModelCopyWith<$Res> {
  factory _$$_FollowerModelCopyWith(
          _$_FollowerModel value, $Res Function(_$_FollowerModel) then) =
      __$$_FollowerModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String uid,
      String followers,
      String following,
      bool isfollowers,
      bool isblocked,
      bool requestFollow,
      String username,
      String useravatar});
}

/// @nodoc
class __$$_FollowerModelCopyWithImpl<$Res>
    extends _$FollowerModelCopyWithImpl<$Res, _$_FollowerModel>
    implements _$$_FollowerModelCopyWith<$Res> {
  __$$_FollowerModelCopyWithImpl(
      _$_FollowerModel _value, $Res Function(_$_FollowerModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? uid = null,
    Object? followers = null,
    Object? following = null,
    Object? isfollowers = null,
    Object? isblocked = null,
    Object? requestFollow = null,
    Object? username = null,
    Object? useravatar = null,
  }) {
    return _then(_$_FollowerModel(
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      followers: null == followers
          ? _value.followers
          : followers // ignore: cast_nullable_to_non_nullable
              as String,
      following: null == following
          ? _value.following
          : following // ignore: cast_nullable_to_non_nullable
              as String,
      isfollowers: null == isfollowers
          ? _value.isfollowers
          : isfollowers // ignore: cast_nullable_to_non_nullable
              as bool,
      isblocked: null == isblocked
          ? _value.isblocked
          : isblocked // ignore: cast_nullable_to_non_nullable
              as bool,
      requestFollow: null == requestFollow
          ? _value.requestFollow
          : requestFollow // ignore: cast_nullable_to_non_nullable
              as bool,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      useravatar: null == useravatar
          ? _value.useravatar
          : useravatar // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_FollowerModel implements _FollowerModel {
  const _$_FollowerModel(
      {required this.uid,
      required this.followers,
      required this.following,
      this.isfollowers = false,
      this.isblocked = false,
      this.requestFollow = false,
      required this.username,
      required this.useravatar});

  factory _$_FollowerModel.fromJson(Map<String, dynamic> json) =>
      _$$_FollowerModelFromJson(json);

  @override
  final String uid;
  @override
  final String followers;
  @override
  final String following;
  @override
  @JsonKey()
  final bool isfollowers;
  @override
  @JsonKey()
  final bool isblocked;
  @override
  @JsonKey()
  final bool requestFollow;
  @override
  final String username;
  @override
  final String useravatar;

  @override
  String toString() {
    return 'FollowerModel(uid: $uid, followers: $followers, following: $following, isfollowers: $isfollowers, isblocked: $isblocked, requestFollow: $requestFollow, username: $username, useravatar: $useravatar)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FollowerModel &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.followers, followers) ||
                other.followers == followers) &&
            (identical(other.following, following) ||
                other.following == following) &&
            (identical(other.isfollowers, isfollowers) ||
                other.isfollowers == isfollowers) &&
            (identical(other.isblocked, isblocked) ||
                other.isblocked == isblocked) &&
            (identical(other.requestFollow, requestFollow) ||
                other.requestFollow == requestFollow) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.useravatar, useravatar) ||
                other.useravatar == useravatar));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, uid, followers, following,
      isfollowers, isblocked, requestFollow, username, useravatar);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FollowerModelCopyWith<_$_FollowerModel> get copyWith =>
      __$$_FollowerModelCopyWithImpl<_$_FollowerModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_FollowerModelToJson(
      this,
    );
  }
}

abstract class _FollowerModel implements FollowerModel {
  const factory _FollowerModel(
      {required final String uid,
      required final String followers,
      required final String following,
      final bool isfollowers,
      final bool isblocked,
      final bool requestFollow,
      required final String username,
      required final String useravatar}) = _$_FollowerModel;

  factory _FollowerModel.fromJson(Map<String, dynamic> json) =
      _$_FollowerModel.fromJson;

  @override
  String get uid;
  @override
  String get followers;
  @override
  String get following;
  @override
  bool get isfollowers;
  @override
  bool get isblocked;
  @override
  bool get requestFollow;
  @override
  String get username;
  @override
  String get useravatar;
  @override
  @JsonKey(ignore: true)
  _$$_FollowerModelCopyWith<_$_FollowerModel> get copyWith =>
      throw _privateConstructorUsedError;
}
