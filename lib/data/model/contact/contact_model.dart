import 'package:freezed_annotation/freezed_annotation.dart';

part 'contact_model.freezed.dart';
part 'contact_model.g.dart';

@freezed
class ContactModel with _$ContactModel {
  //const HomeMenuModel._();
  const factory ContactModel({
    required String id,
    @Default('') String email,
    @Default('') String whatsapp,
    @Default('') String phone,

    // required String useravatar,
    // @Default([]) List<String> followers,
  }) = _ContactModel;

  factory ContactModel.fromJson(Map<String, dynamic> json, String id) =>
      _$ContactModelFromJson(json);
}
