// ignore_for_file: invalid_use_of_protected_member

import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/data/model/followers/follower_list.dart';
import 'package:dac_apps/data/model/followers/followers_model.dart';
import 'package:flutter/foundation.dart';
import 'package:get/state_manager.dart';
import 'package:collection/collection.dart';

import '../../data/model/user/user_model.dart';
import '../../data/remote/firestore/firestore_user_services.dart';

class ProfileMedsosController extends GetxController {
  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
    // isFollow.close();
  }

  final BoxStorage _boxStorage = BoxStorage();

  var isFollowerUid = ''.obs;

  var followerUserList = [].obs;
  var isFollow = false.obs;

  Future<bool> checkCurrentFollowerList(String uid) async {
    String localuid = _boxStorage.getUserId();
    List<dynamic> list = await _fireStoreUserServices.getFollowerData(localuid);
    bool isfollowed = list.contains(uid);
    if (isfollowed == true) {
      return true;
    }
    return false;
  }
  Future<bool> checkCurrentFollowingList(String uid) async {
    String localuid = _boxStorage.getUserId();
    List<dynamic> list = await _fireStoreUserServices.getFollowingData(localuid);
    bool isfollowed = list.contains(uid);
    if (isfollowed == true) {
      return true;
    }
    return false;
  }

  checkisrequestfollowcurrentuser({required String uid, required String followersid}) async{
     bool isfollowers =  await _fireStoreUserServices.apiCheckisrequestfollowcurrentuser(localuid: uid, followersid: followersid);
    debugPrint('is current followers : $isfollowers');
    return isfollowers;
  }

  checkisrequestfollow({required String uid, required String followersid}) async{
    bool isrequestfollow =  await _fireStoreUserServices.apiCheckisrequestfollow(uid: uid, followersid: followersid);
    debugPrint('is follower request  = $isrequestfollow');
    return isrequestfollow;
  }
  checkisfollowinguser({required String uid, required String followersid}) async{
    bool isrequestfollow =  await _fireStoreUserServices.apicheckisfollowinguser(uid: uid, followersid: followersid);
    debugPrint('is following user  = $isrequestfollow');
    return isrequestfollow;
  }


  following(UserModel usermodel, FollowerModel followerModel) async {
    if (followerModel.isfollowers == false) {
      // await _fireStoreUserServices.updateFollow(usermodel);
    } else {
      // await _fireStoreUserServices.updateUnFollow(usermodel, followerModel);
    }
  }

  requestFollow(UserModel usermodel) async {
    await _fireStoreUserServices.updateFollow(usermodel);
    debugPrint('success request follower from ${usermodel.uid}');
  }

  confirmFollow(UserModel usermodel,  bool isfollow, bool isrequestfollow) async {
    await _fireStoreUserServices.updateConfirmFollow(usermodel , isfollow, isrequestfollow);
    debugPrint('success request follower from ${usermodel.uid}');
  }

  cancelFollow(UserModel usermodel, bool isfollow, bool isrequestfollow, bool isfollowing) async {
    await _fireStoreUserServices.updateUnFollow(usermodel, isfollow, isrequestfollow, isfollowing);
    debugPrint('success remove follower from ${usermodel.uid}');
  }

  Future<int> getFollowerCount(String uid) async {
    int countdata = await _fireStoreUserServices.getFollowerCount(uid);
    return countdata;
  }
  
}
