import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../data/model/user/user_model.dart';
import '../../../data/remote/firestore/firestore_user_services.dart';
import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '../profile_follower_current_user_view.dart';
import '../profile_medsos_controller.dart';
import '../profile_medsos_view.dart';

class FollowingUserView extends StatefulWidget {
  const FollowingUserView({super.key, required this.usermodel});
  final UserModel usermodel;
  @override
  State<FollowingUserView> createState() =>
      _FollowingUserViewState();
}

class _FollowingUserViewState extends State<FollowingUserView> {
  String name = "";

  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();

  bool isrequestfollcurrentuser = false;
  bool isrequestfoll = false;
  bool isfollowing = false;

  bool isneedfollback = false;
  bool isdontneedfollback = false;

  final BoxStorage _boxStorage = BoxStorage();
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: AppBar(
            automaticallyImplyLeading: true,
            flexibleSpace: Padding(
              padding: const EdgeInsets.only(top: 35.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      height: 45,
                      width: Get.width * 0.7,
                      child: Card(
                        child: TextField(
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              hintText: 'Search...'),
                          onChanged: (val) {
                            setState(() {
                              name = val;
                            });
                          },
                        ),
                      ),
                    ),
                  ]),
            ),
            backgroundColor: ColorName.whiteprimary,
          ),
        ),
        body: StreamBuilder(
          stream: db
              .collection('user')
              .doc(widget.usermodel.uid)
              .collection('followersdata')
              .where('isfollowing', isEqualTo: true)
              .snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // debugPrint('lenght : ${snapshot.data!.length}');
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 1.0));
            }
            if (snapshot.connectionState == ConnectionState.none) {
              return const Text('Server not found - Error 500');
            }
            if (snapshot.connectionState == ConnectionState.done ||
                !snapshot.hasData ||
                snapshot.data.docs.length == 0 ||
                snapshot.data.docs == []) {
              debugPrint(
                  'is data : ${snapshot.data.docs} - data length : ${snapshot.hasData}');
              return SizedBox(
                height: Get.height * 0.4,
                width: Get.width,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                        'Tidak ada data',
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              );
            } else {
              //    String localuid = _boxStorage.getUserId();

              // List<String> listitems= [localuid];
              // List<DocumentSnapshot> doclist = snapshot.data.docs.where((DocumentSnapshot doc){
              //   return !listitems.contains(doc['uid']);
              // }).toList();

              // doclist.removeWhere((element) => element[localuid]);
              // debugPrint('list after : $doclist');
              return ListView.builder(
                  itemCount: snapshot.data.docs.length,
                  itemBuilder: (context, index) {
                    if (name.isEmpty) {
                      return GestureDetector(
                        onTap: () async {
                        
                          UserModel userdata =
                              await _fireStoreUserServices.loginCurrentUser(
                                  snapshot.data.docs[index]['uid']);
                          debugPrint(
                              'followers => ${userdata.username.toString()}');
                          isrequestfollcurrentuser =
                              await profileMedsosController
                                  .checkisrequestfollowcurrentuser(
                                      uid: widget.usermodel.uid,
                                      followersid: userdata.uid);
                          isrequestfoll = await profileMedsosController
                              .checkisrequestfollow(
                                  uid: widget.usermodel.uid,
                                  followersid: userdata.uid);
                          isfollowing = await profileMedsosController
                              .checkisfollowinguser(
                                  uid: widget.usermodel.uid,
                                  followersid: userdata.uid);
                          debugPrint(
                              'is status follow from current : $isrequestfollcurrentuser - is status follow : $isrequestfoll');
                                String localuid = _boxStorage.getUserId();
                           if (localuid == snapshot.data.docs[index]['uid']) {
                            Get.to(const ProfileMedsos());
                          } else {
                            Get.to(ProfileFollower(
                              usermodel: userdata,
                              isrequestfollowcurrentuser:
                                  isrequestfollcurrentuser,
                              isrequestfollow: isrequestfoll,
                              isfollowing: isfollowing,
                            ));
                          }
                          setState(() {});
                        },
                        child: ListTile(
                          leading: SizedBox(
                            height: 70,
                            width: 60,
                            child: Card(
                              elevation: 5,
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: snapshot.data.docs[index]['avatar'] != ''
                                    ? Image.network(
                                        snapshot.data.docs[index]['avatar'])
                                    : Assets.image.logoDigitalAlumni.image(
                                        fit: BoxFit.fitWidth, height: 35),
                              ),
                            ),
                          ),
                          title: Text(
                            snapshot.data.docs[index]['username'],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: Colors.black54,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          // subtitle: isFollowers == false
                          //     ? const Text(
                          //         "mengikuti",
                          //         maxLines: 1,
                          //         overflow: TextOverflow.ellipsis,
                          //         style: TextStyle(
                          //             color: Colors.black54,
                          //             fontSize: 16,
                          //             fontWeight: FontWeight.bold),
                          //       )
                          //     : const Text(
                          //         "rekan alumni",
                          //         maxLines: 1,
                          //         overflow: TextOverflow.ellipsis,
                          //         style: TextStyle(
                          //             color: Colors.black54,
                          //             fontSize: 16,
                          //             fontWeight: FontWeight.bold),
                          //       ),
                          // leading: CircleAvatar(
                          //   backgroundImage: NetworkImage(data['image']),
                          // ),
                        ),
                      );
                    }
                    if (snapshot.data.docs[index]['username']
                        .toString()
                        .toLowerCase()
                        .startsWith(name.toLowerCase())) {
                      return GestureDetector(
                        onTap: () async {
                          UserModel userdata =
                              await _fireStoreUserServices.loginCurrentUser(
                                  snapshot.data.docs[index]['uid']);
                          isrequestfollcurrentuser =
                              await profileMedsosController
                                  .checkisrequestfollowcurrentuser(
                                      uid: widget.usermodel.uid,
                                      followersid: userdata.uid);
                          isrequestfoll = await profileMedsosController
                              .checkisrequestfollow(
                                  uid: widget.usermodel.uid,
                                  followersid: userdata.uid);
                          isfollowing = await profileMedsosController
                              .checkisfollowinguser(
                                  uid: widget.usermodel.uid,
                                  followersid: userdata.uid);
                          debugPrint(
                              'is status follow from current : $isrequestfollcurrentuser - is status follow : $isrequestfoll');

                             String localuid = _boxStorage.getUserId();
                          if (localuid == snapshot.data.docs[index]['uid']) {
                            Get.to(const ProfileMedsos());
                          } else {
                            Get.to(ProfileFollower(
                              usermodel: userdata,
                              isrequestfollowcurrentuser:
                                  isrequestfollcurrentuser,
                              isrequestfollow: isrequestfoll,
                              isfollowing: isfollowing,
                            ));
                          }

                          setState(() {});
                        },
                        child: ListTile(
                          title: Text(
                            snapshot.data.docs[index]['username'],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: Colors.black54,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          // subtitle: Text(
                          //   isFollowers.toString(),
                          //   maxLines: 1,
                          //   overflow: TextOverflow.ellipsis,
                          //   style: const TextStyle(
                          //       color: Colors.black54,
                          //       fontSize: 16,
                          //       fontWeight: FontWeight.bold),
                          // ),
                          // leading: CircleAvatar(
                          //   backgroundImage: NetworkImage(data['image']),
                          // ),
                        ),
                      );
                    }
                    return Container();
                  });
            }
          },
        ));
  }
}
