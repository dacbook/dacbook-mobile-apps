import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../data/remote/firestore/firestore_user_services.dart';
import '../../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
String? value;
String? seletectedCategory;

class MyFileAddView extends StatefulWidget {
  const MyFileAddView({super.key});
  @override
  State<MyFileAddView> createState() => _MyFileAddViewState();
}

class _MyFileAddViewState extends State<MyFileAddView> {
  var txtControllerTitle = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerCategory = TextEditingController();
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  bool iscameraStatusActive = false;
  bool isloading = false;
  String category = '';

  uploadImagetFirebase() async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    List<dynamic> followerList =
        await fireStoreUserServices.getFollowerData(uid);
    File file = File(imageFromCamera!.path);
    String? fileName = file.path.split('/').last;
    await FirebaseStorage.instance
        .ref()
        .child('/myfile/')
        .child(uid)
        .child(fileName)
        .putFile(File(file.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${file.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child('/myfile/')
            .child(uid)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');

          final collRef = db.collection('user').doc(uid).collection('myfile');

          DocumentReference docReference = collRef.doc();

          Map<String, dynamic> data = {
            'id': docReference.id,
            'author': uid,
            'category': category,
            'username': username,
            'userimage': avatar,
            'albumname': txtControllerTitle.text,
            'albumyear': txtControllerDescription.text,
            'mediaFeedList': url,
            //'bookid': widget.bookModel.id,
            //'followers': followerList,
            'favorites': 0,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
          };

          docReference.set(data);

          return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
      Navigator.pop(context);
    });
  }

  List<String> imageUrls = [];

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    print(imageUrls);
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    Reference storageReference =
        FirebaseStorage.instance.ref().child('myfile/${imageData.path}');
    if (isImage == true) {
      UploadTask uploadTask = storageReference.putFile(imageData);
      await uploadTask.whenComplete(() => null);
    }
    if (isVideo == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'video/mp4'));
      await uploadTask.whenComplete(() => null);
    }
    if (isPdf == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'application/pdf'));
      await uploadTask.whenComplete(() => null);
    }

    return await storageReference.getDownloadURL();
  }

  uploadCloudFirestore() async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    final now = DateTime.now();
    // List<dynamic> followerList =
    //     await fireStoreUserServices.getFollowerData(uid);
    if (thumnail != null) {
      String? fileName = thumnail!.split('/').last;
      await FirebaseStorage.instance
          .ref()
          .child('/myfile/')
          .child(uid)
          .child(fileName)
          .putFile(File(thumnail!))
          .then((taskSnapshot) {
        debugPrint("task done ==> $thumnail");
        if (taskSnapshot.state == TaskState.success) {
          FirebaseStorage.instance
              .ref()
              .child('/myfile/')
              .child(uid)
              .child(fileName)
              .getDownloadURL()
              .then((url) {
            try {
              final time = DateFormat('hh:mm');
              final date = DateTime.now();

              final collRef =
                  db.collection('user').doc(uid).collection('myfile');

              DocumentReference docReference = collRef.doc();
              Map<String, dynamic> data = {
                'id': docReference.id,
                'author': uid,
                'category': category,
                'username': username,
                'userimage': avatar,
                //  'subscription': seletectedCategory,
                'albumname': txtControllerTitle.text,
                'albumyear': txtControllerDescription.text,
                'mediaFeedList': imageUrls,
                'thumbnail': url,

                'favorites': 0,
                'date': now,
                'time': time.format(DateTime.now()),
              };

              docReference.set(data);
            } catch (e) {
              debugPrint(e.toString());
              return e.toString();
            }
          });
        }
      });
    } else {
      try {
        final time = DateFormat('hh:mm');
        final date = DateTime.now();

        final collRef = db.collection('user').doc(uid).collection('myfile');

        DocumentReference docReference = collRef.doc();
        Map<String, dynamic> data = {
          'id': docReference.id,
          'author': uid,
          'category': category,
          'username': username,
          'userimage': avatar,
          //  'subscription': seletectedCategory,
          'albumname': txtControllerTitle.text,
          'albumyear': txtControllerDescription.text,
          'mediaFeedList': imageUrls,
          'thumbnail': thumbnailName,

          'favorites': 0,
          'date': date.toString(),
          'time': time.format(DateTime.now()),
        };

        docReference.set(data);
      } catch (e) {
        debugPrint(e.toString());
        return e.toString();
      }
    }

    // ignore: use_build_context_synchronously
    Navigator.pop(context);
    return 'Success';
  }

  // final FeederController feederController = Get.put(FeederController());
  // final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(category),
            // GestureDetector(
            //     onTap: () {
            //       Get.to(BookGallerySettingCategory(
            //         bookid: widget.bookModel.id,
            //       ));
            //     },
            //     child: const Icon(Icons.add))
          ],
        ),
        centerTitle: true,
        backgroundColor: ColorName.redprimary,
      ),
      body: LoadingOverlay(
        isLoading: isloading,
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView(
            children: [
              isImage == true
                  ? Wrap(
                      children: imagefiles.map((imageone) {
                        return SizedBox(
                            child: Card(
                          child: SizedBox(
                            height: 100,
                            width: 100,
                            child: Image.file(File(imageone.path)),
                          ),
                        ));
                      }).toList(),
                    )
                  : Container(),
              image == null
                  ? Container()
                  : Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Image.file(image!),
                    ),
              thumbnailName == null
                  ? Container()
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(thumbnailName!),
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: SizedBox(
                  child: TextFormField(
                    controller: txtControllerTitle,
                    // obscureText: txtControllerDescription,
                    keyboardType: TextInputType.text,
                    onChanged: (value) => txtControllerTitle,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Album name cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Masukan Nama Album',
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: SizedBox(
                  child: TextFormField(
                    controller: txtControllerDescription,
                    // obscureText: txtControllerDescription,
                    keyboardType: TextInputType.number,
                    maxLength: 4,
                    onChanged: (value) => txtControllerDescription,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "year cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Masukan Tahun',
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                              onTap: () => showModalBottomSheet(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20.0),
                                          topRight: Radius.circular(20.0)),
                                    ),
                                    context: context,
                                    builder: (BuildContext context) {
                                      return showBottomSheet(context);
                                    },
                                  ),
                              child: const Icon(
                                Icons.attachment,
                                color: ColorName.blackgrey,
                                size: 26,
                              )),
                        ],
                      ),
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                ColorName.redprimary),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.redprimary)))),
                        onPressed: () async {
                          if (txtControllerDescription.text.length == 4 && txtControllerDescription.text != '' && txtControllerTitle.text != '' &&
                              isloading != true) {
                            setState(() {
                              isloading = true;
                            });
                            if (iscameraStatusActive == true) {
                              uploadImagetFirebase();
                            } else {
                              if (imagefiles.length > 5) {
                                Get.snackbar(
                                    'Informasi', 'Batas maksimal foto hanya 5');
                              } else {
                                List<File> imageListData = [];
                                if (isPdf == true) {
                                  imageListData.add(_pdf!);
                                } else {
                                  imageListData = imagefiles
                                      .map<File>((xfile) => File(xfile.path))
                                      .toList();
                                }

                                imageUrls = await uploadFiles(imageListData);
                                debugPrint(imageUrls.toString());
                                await uploadCloudFirestore();
                                imagefiles = [];
                                image = null;
                                thumbnailName = null;
                                isImage = false;
                                _pdf = null;
                              }
                            }
                            setState(() {
                              isloading = false;
                            });
                            // ignore: use_build_context_synchronously
                           // Navigator.pop(context);
                          } else {
                            Get.snackbar('Informasi', 'Data Album masih kosong');
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Kirim Cerita".toUpperCase(),
                                  style: const TextStyle(fontSize: 14)),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final ImagePicker imgpicker = ImagePicker();
  //VideoPlayerController? _videoPlayerController;
  List<XFile> imagefiles = [];
  File? _video;
  File? _pdf;
  String? thumnail;
  String? thumbnailName;
  bool isImage = false;
  bool isVideo = false;
  bool isPdf = false;

  getFileImages() async {
    isImage = true;
    isVideo = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickMultiImage(imageQuality: 20);
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != [] || pickedfiles.isNotEmpty) {
        imagefiles = pickedfiles;
        thumnail = pickedfiles[0].path;
        setState(() {});
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFilePdf() async {
    isImage = false;
    isVideo = false;
    isPdf = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['doc', 'pdf', 'xls', 'xlsx'],
    );
    try {
      if (result != null) {
        _pdf = File(result.files.single.path!);
        PlatformFile file = result.files.first;
        thumbnailName = file.name;
      } else {
        // User canceled the picker
      }
      setState(() {});
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  // getFileVideo() async {
  //   isVideo = true;
  //   isImage = false;
  //   isPdf = false;
  //   try {
  //     var pickedfiles = await imgpicker.pickVideo(source: ImageSource.gallery);

  //     // thumnail
  //     final String? _path = await VideoThumbnail.thumbnailFile(
  //       video: pickedfiles!.path,
  //       thumbnailPath: (await getTemporaryDirectory()).path,

  //       /// path_provider
  //       imageFormat: ImageFormat.PNG,
  //       //maxHeight: 120,
  //       quality: 10,
  //     );
  //     //you can use ImageCourse.camera for Camera capture
  //     if (pickedfiles != null) {
  //       _video = File(pickedfiles.path);
  //       _videoPlayerController = VideoPlayerController.file(_video!)
  //         ..initialize().then((_) {
  //           setState(() {});
  //           _videoPlayerController!.play();
  //         });
  //       thumnail = _path;
  //       imagefiles.add(pickedfiles);
  //       setState(() {});
  //     } else {
  //       print("No image is selected.");
  //     }
  //   } catch (e) {
  //     print("error while picking file.");
  //   }
  //   // ignore: use_build_context_synchronously
  //   Navigator.pop(context);
  // }

  File? image;
  XFile? imageFromCamera;
  String? imagefilenamePath;

  Future getCamera() async {
    imagefiles = [];
    final box = GetStorage();
    //final ImagePicker picker = ImagePicker();
    imageFromCamera = await ImagePicker().pickImage(source: ImageSource.camera);

    image = File(imageFromCamera!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        image = File(imageFromCamera!.path);
        imagefilenamePath = fileName;
        // getting a directory path for saving
        final String path = image.toString();
        box.write('photosstory', imageFromCamera.toString());
        debugPrint('clog ==> ${path.toString()}');
      });
    } else {
      setState(() {
        imagefilenamePath = 'Ukuran photo belum sesuai';
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
    // ignore: use_build_context_synchronously
  }

  Future getCamera2() async {
    imagefiles = [];
    final box = GetStorage();
    //final ImagePicker picker = ImagePicker();
    imageFromCamera = await ImagePicker().pickImage(source: ImageSource.camera);
    if (imageFromCamera == null) {
      null;
    }
    image = File(imageFromCamera!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        image = File(imageFromCamera!.path);
        imagefilenamePath = fileName;
        // getting a directory path for saving
        final String path = image.toString();
        box.write('photosstory', imageFromCamera.toString());
        debugPrint('clog ==> ${path.toString()}');
      });
    } else {
      setState(() {
        imagefilenamePath = 'Ukuran photo belum sesuai';
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                category = 'foto';
              });
              getFileImages();
            },
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt_outlined,
                    size: 60,
                    semanticLabel: 'foto',
                    color: Colors.white,
                  ),
                )),
          ),
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       category = 'video';
          //     });
          //     getFileVideo();
          //   },
          //   child: const Card(
          //       color: ColorName.carnationPink,
          //       child: Padding(
          //         padding: EdgeInsets.all(8.0),
          //         child: Icon(
          //           Icons.video_camera_front,
          //           size: 60,
          //           color: Colors.white,
          //         ),
          //       )),
          // ),
          GestureDetector(
            onTap: () {
              setState(() {
                category = 'file';
              });
              getFilePdf();
            },
            child: const Card(
                color: ColorName.downy,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.file_present,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
