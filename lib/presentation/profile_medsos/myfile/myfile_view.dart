// ignore: depend_on_referenced_packages
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:dac_apps/presentation/profile_medsos/myfile/_myfile_file.dart';
import 'package:dac_apps/presentation/profile_medsos/myfile/_myfile_foto.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import 'myfile_add_view.dart';

final db = FirebaseFirestore.instance;
final boxstorage = BoxStorage();
String? albumname;
String? albumyears;
final ImagePicker imgpicker = ImagePicker();

class MyFileView extends StatefulWidget {
  const MyFileView({super.key});
  @override
  State<MyFileView> createState() => _MyFileViewState();
}

class _MyFileViewState extends State<MyFileView>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  // ignore: unused_element
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  // ignore: unused_element
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary, title: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: ()=> Get.to(const MyFileAddView()),
            child: const Icon(Icons.add)),
        ],
      ),),
      body: ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            DefaultTabController(
              length: 2,
              child: Column(
                children: [
                  Container(
                    color: Colors.white,
                    child: TabBar(
                      controller: tabController,
                      labelColor: ColorName.redprimary,
                      unselectedLabelColor: Colors.black,
                      indicatorColor: ColorName.redprimary,
                      indicatorPadding:
                          const EdgeInsets.symmetric(horizontal: 10),
                      labelStyle: const TextStyle(
                        fontSize: 14,
                        fontFamily: 'Roboto',
                      ),
                      tabs: const [
                        Tab(
                          child: Text('Foto'),
                        ),
                        // Tab(
                        //   child: Text("Video"),
                        // ),
                        Tab(
                          child: Text("File"),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Get.height * .8,
                    child: TabBarView(
                      controller: tabController,
                      children: const [
                        //KarirUserView(uid: 'Xha4gwjMe5ZfR1SLfR2i'),
                        MyFileFoto(),
                      //  MyFileFoto(),
                        MyFileFile(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            )
          ]),
    );
  }
}

class MyFileViewAdd extends StatefulWidget {
  const MyFileViewAdd({super.key});

  @override
  State<MyFileViewAdd> createState() => _MyFileViewAddState();
}

class _MyFileViewAddState extends State<MyFileViewAdd> {
  final ImagePicker imgpicker = ImagePicker();
  List<XFile> imagefiles = [];
  File? _pdf;
  String? thumnail;
  String? thumbnailName;
  bool isImage = false;
  bool isVideo = false;
  bool isPdf = false;
  String category = '';
  bool isloading = false;

  getFileImages() async {
    imagefiles = [];
    isImage = true;
    isVideo = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickMultiImage();
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != [] || pickedfiles.isNotEmpty) {
        imagefiles = pickedfiles;
        thumnail = pickedfiles[0].path;
        setState(() {});
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFilePdf() async {
    imagefiles = [];
    isImage = false;
    isVideo = false;
    isPdf = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['doc', 'pdf', 'xls', 'xlsx'],
    );
    try {
      if (result != null) {
        _pdf = File(result.files.single.path!);
        PlatformFile file = result.files.first;
        thumbnailName = file.name;
      } else {
        // User canceled the picker
      }
      setState(() {});
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                category = 'foto';
              });
              getFileImages();
            },
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt_outlined,
                    size: 60,
                    semanticLabel: 'foto',
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                category = 'file';
              });
              getFilePdf();
            },
            child: const Card(
                color: ColorName.downy,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.file_present,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary),
      body: LoadingOverlay(
        isLoading: isloading,
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  thumbnailName == null
                      ? Container()
                      : Center(
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(thumbnailName!),
                          ),
                        ),
                  thumbnailName == null
                      ? imagefiles == [] || imagefiles.isEmpty
                          ? GestureDetector(
                              onTap: () => showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20.0),
                                      topRight: Radius.circular(20.0)),
                                ),
                                context: context,
                                builder: (BuildContext context) {
                                  return showBottomSheet(context);
                                },
                              ),
                              child: const Icon(
                                Icons.upload_file,
                                size: 200,
                                color: ColorName.blackgrey,
                              ),
                            )
                          : Center(
                              child: Wrap(
                                children: imagefiles.map((imageone) {
                                  return SizedBox(
                                      child: Card(
                                    child: SizedBox(
                                      height: 100,
                                      width: 100,
                                      child: Image.file(File(imageone.path)),
                                    ),
                                  ));
                                }).toList(),
                              ),
                            )
                      : Container(),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: TextField(
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // Used a ternary operator to check if isUpdate is true then display
                        // Update Todo.
                        labelText: 'Masukan nama album',
                        hintText: 'Masukan nama album',
                      ),
                      onChanged: (String val) {
                        // Storing the value of the text entered in the variable value.
                        albumname = val;
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      maxLength: 4,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        // Used a ternary operator to check if isUpdate is true then display
                        // Update Todo.
                        labelText: 'Masukan tahun',
                        hintText: 'Masukan tahun',
                      ),
                      onChanged: (String val) {
                        // Storing the value of the text entered in the variable value.
                        albumyears = val;
                      },
                    ),
                  ),
                  TextButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.lightBlueAccent),
                      ),
                      onPressed: () async {
                        // Check to see if isUpdate is true then update the value else add the value
                        if (albumname != '' &&
                            albumyears != '' &&
                            isloading != true) {
                          setState(() {
                            isloading = true;
                          });
                          if (imagefiles.length > 5) {
                            Get.snackbar(
                                'Informasi', 'Batas maksimal foto hanya 5');
                          } else {
                            List<File> imageListData = [];
                            if (isPdf == true) {
                              imageListData.add(_pdf!);
                            } else {
                              imageListData = imagefiles
                                  .map<File>((xfile) => File(xfile.path))
                                  .toList();
                            }

                            imageUrls = await uploadFiles(imageListData);
                            debugPrint(imageUrls.toString());
                            await uploadCloudFirestore();
                          }
                          setState(() {
                            isloading = false;
                          });
                          // ignore: use_build_context_synchronously
                          Navigator.pop(context);
                        } else {}
                      },
                      child: const Text('Tambahkan file',
                          style: TextStyle(color: Colors.white))),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<String> imageUrls = [];

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    print(imageUrls);
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    Reference storageReference =
        FirebaseStorage.instance.ref().child('myfiles/${imageData.path}');
    if (isImage == true) {
      UploadTask uploadTask = storageReference.putFile(imageData);
      await uploadTask.whenComplete(() => null);
    }
    if (isVideo == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'video/mp4'));
      await uploadTask.whenComplete(() => null);
    }
    if (isPdf == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'application/pdf'));
      await uploadTask.whenComplete(() => null);
    }

    return await storageReference.getDownloadURL();
  }

  uploadCloudFirestore() async {
    String uid = boxstorage.getUserId();
    String username = boxstorage.getUserName();
    String avatar = boxstorage.getUserAvatar();
    // List<dynamic> followerList =
    //     await fireStoreUserServices.getFollowerData(uid);
    if (thumnail != null) {
      String? fileName = thumnail!.split('/').last;
      await FirebaseStorage.instance
          .ref()
          .child('/myfiles/')
          .child(uid)
          .child(fileName)
          .putFile(File(thumnail!))
          .then((taskSnapshot) {
        debugPrint("task done ==> $thumnail");
        if (taskSnapshot.state == TaskState.success) {
          FirebaseStorage.instance
              .ref()
              .child('/myfiles/')
              .child(uid)
              .child(fileName)
              .getDownloadURL()
              .then((url) {
            try {
              final time = DateFormat('hh:mm');
              final date = DateFormat('dd-MM-yyyy');

              final collRef =
                  db.collection('user').doc(uid).collection('myfile');
              DocumentReference docReference = collRef.doc();

              Map<String, dynamic> data = {
                'id': docReference.id,
                'author': uid,
                'category': category,
                'username': username,
                'userimage': avatar,
                'albumname': albumname,
                'albumyear': albumyears,
                'mediaFeedList': imageUrls,
                'thumbnail': url,
                'favorites': 0,
                'date': date.format(DateTime.now()),
                'time': time.format(DateTime.now()),
              };

              docReference.set(data);
            } catch (e) {
              debugPrint(e.toString());
              return e.toString();
            }
          });
        }
      });
    } else {
      try {
        final time = DateFormat('hh:mm');
        final date = DateFormat('dd-MM-yyyy');

        final collRef = db.collection('user').doc(uid).collection('myfile');
        DocumentReference docReference = collRef.doc();

        Map<String, dynamic> data = {
          'id': docReference.id,
          'author': uid,
          'category': category,
          'username': username,
          'userimage': avatar,
          'albumname': albumname,
          'albumyear': albumyears,
          'mediaFeedList': imageUrls,
          'thumbnail': thumbnailName,
          'favorites': 0,
          'date': date.format(DateTime.now()),
          'time': time.format(DateTime.now()),
        };

        docReference.set(data);
      } catch (e) {
        debugPrint(e.toString());
        return e.toString();
      }
    }

    // ignore: use_build_context_synchronously
    Navigator.pop(context);
    return 'Success';
  }
}
