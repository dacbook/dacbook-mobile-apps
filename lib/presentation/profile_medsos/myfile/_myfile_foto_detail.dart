
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;

class MyFileFotoDetail extends StatefulWidget {
  const MyFileFotoDetail(
      {super.key,
      required this.imgurl,
      required this.title,
      required this.description,
      required this.id,
      required this.uid,
      });
  final List<dynamic> imgurl;
  final String title;
  final String description;
  final String id;
  final String uid;
  @override
  State<MyFileFotoDetail> createState() => _MyFileFotoDetailState();
}

class _MyFileFotoDetailState extends State<MyFileFotoDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: SizedBox(
           child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children:  [
            GestureDetector(
              onTap: (){
                final collRef = db.collection('user').doc(widget.uid).collection('myfile');
                DocumentReference docReference = collRef.doc(widget.id);
                docReference.delete();
                Navigator.pop(context);
              },
              child: const Icon(Icons.delete)),
          ],
        ),)
      ),
      body: ListView(
        children: [
          SizedBox(
            width: 250,
            child: Text(
              widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.grey[600]!,),
            ),
          ),
          SizedBox(
            width: 250,
            child: Text(
              widget.description,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 16,
              color: Colors.grey[500]!,),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
            ),
            child: GridView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                padding: EdgeInsets.zero,
                itemCount: widget.imgurl.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: ScreenUtil().setWidth(5),
                  mainAxisSpacing: ScreenUtil().setHeight(5),
                ),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () => showImageViewer(context,
                                Image.network(widget.imgurl[index]).image,
                                useSafeArea: true,
                                swipeDismissible: true,
                                doubleTapZoomable: true),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(5),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: widget.imgurl[index],
                        fit: BoxFit.cover,
                        placeholder: (context, url) {
                          return Image.network(
                            url,
                            fit: BoxFit.cover,
                          );
                        },
                        errorWidget: (context, url, error) {
                          return const Icon(Icons.error);
                        },
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
