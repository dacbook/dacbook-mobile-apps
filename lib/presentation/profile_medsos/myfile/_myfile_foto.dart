import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../gen/colors.gen.dart';
import '../profile_medsos_view.dart';
import '_myfile_foto_detail.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();
class MyFileFoto extends StatelessWidget {
  const MyFileFoto({super.key});
  
  @override
  Widget build(BuildContext context) {
    return  StreamBuilder(
                      // Reading Items form our Database Using the StreamBuilder widget
                      stream: db
                          .collection('user').doc(localdata.getUserId()).collection('myfile')
                          .orderBy('date', descending: true)
                          .where('category',
                              isEqualTo: 'foto' ).snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasError) {
                          return const Center(
                            child: Icon(Icons.error),
                          );
                        }
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }

                        if (snapshot.connectionState == ConnectionState.done ||
                            snapshot.data.docs.length == 0 ||
                            snapshot.data == null ||
                            snapshot.data.docs == null) {
                             return SizedBox(
                            height: 100,
                            child: Center(
                              child: WidgetTextMont(
                                'Tidak ada dokumen kegiatan tersimpan',
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                textColor: Colors.grey[600]!,
                              ),
                            ),
                          );
                        } else {
                          return Padding(
                            padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(10),
                            ),
                            child: GridView.builder(
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                padding: EdgeInsets.zero,
                                semanticChildCount: 1,
                                itemCount: snapshot.data.docs.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: ScreenUtil().setWidth(5),
                                  mainAxisSpacing: ScreenUtil().setHeight(5),
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  DocumentSnapshot documentSnapshot =
                                      snapshot.data.docs[index];
                                  return GestureDetector(
                                    onTap: () {
                                      Get.to(MyFileFotoDetail(
                                        imgurl:
                                            documentSnapshot['mediaFeedList'],
                                        title: documentSnapshot['albumname'],
                                        description:
                                            documentSnapshot['albumyear'],
                                            id: documentSnapshot['id'],
                                            uid: documentSnapshot['author'],
                                      ));
                                    },
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: documentSnapshot['thumbnail'],
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            url,
                                            fit: BoxFit.cover,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const Icon(Icons.error);
                                        },
                                      ),
                                    ),
                                  );
                                }),
                          );
                        }
                      });
  }
}


class MyFileFotoPerCategory extends StatelessWidget {
  const MyFileFotoPerCategory(
      {super.key , required this.category});
  final String category;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(category.toUpperCase()),
        backgroundColor: ColorName.redprimary,
      ),
      body: StreamBuilder(
          // Reading Items form our Database Using the StreamBuilder widget
          stream: db
              .collection('bookgallery')
              .orderBy('date', descending: true)
              .where('subscription', isEqualTo: category)
              .where('category', arrayContainsAny: ['foto']).snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Icon(Icons.error),
              );
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done ||
                snapshot.data.docs.length == 0 ||
                snapshot.data == null ||
                snapshot.data.docs == null) {
              return WidgetTextMont(
                'Tidak ada foto kegiatan tersimpan',
                fontWeight: FontWeight.bold,
                fontSize: 14,
                textColor: Colors.grey[600]!,
              );
            } else {
              return Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(10),
                ),
                child: GridView.builder(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    padding: EdgeInsets.zero,
                    semanticChildCount: 1,
                    itemCount: snapshot.data.docs.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: ScreenUtil().setWidth(5),
                      mainAxisSpacing: ScreenUtil().setHeight(5),
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      DocumentSnapshot documentSnapshot =
                          snapshot.data.docs[index];
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            Get.to(MyFileFotoDetail(
                               id: documentSnapshot['id'],
                              imgurl: documentSnapshot['mediaFeedList'],
                              title: documentSnapshot['title'],
                              description: documentSnapshot['description'],
                                uid: documentSnapshot['author'],
                            ));
                          },
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(5),
                            ),
                            child: CachedNetworkImage(
                              imageUrl: documentSnapshot['thumbnail'],
                              fit: BoxFit.cover,
                              placeholder: (context, url) {
                                return Image.network(
                                  url,
                                  fit: BoxFit.cover,
                                );
                              },
                              errorWidget: (context, url, error) {
                                return const Icon(Icons.error);
                              },
                            ),
                          ),
                        ),
                      );
                    }),
              );
            }
          }),
    );
  }
}
