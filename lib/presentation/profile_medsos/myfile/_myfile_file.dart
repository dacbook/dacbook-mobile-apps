import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../gen/colors.gen.dart';
import '../profile_medsos_view.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

Future<void> launchUrlWeb(String url) async {
  final Uri urlweb = Uri.parse(url);
  if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
    throw 'Could not launch $url';
  }
}

class MyFileFile extends StatelessWidget {
  const MyFileFile({super.key});
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('user')
            .doc(localdata.getUserId())
            .collection('myfile')
            .orderBy('date', descending: true)
            .where('category', isEqualTo: 'file')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Icon(Icons.error),
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          if (snapshot.connectionState == ConnectionState.done ||
              snapshot.data.docs.length == 0 ||
              snapshot.data == null ||
              snapshot.data.docs == null) {
            return SizedBox(
              height: 100,
              child: Center(
                child: WidgetTextMont(
                  'Tidak ada dokumen kegiatan tersimpan',
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                  textColor: Colors.grey[600]!,
                ),
              ),
            );
          } else {
            return Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(10),
                ),
                child: SizedBox(
                  height: 100,
                  child: ListView.builder(
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (context, int index) {
                        DocumentSnapshot doc = snapshot.data.docs[index];
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListTile(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: 2.0, vertical: 0.0),
                            title: Text(
                              doc['thumbnail'],
                              style:
                                  const TextStyle(fontWeight: FontWeight.w400),
                            ),
                            trailing: SizedBox(
                              width: 80,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  GestureDetector(
                                      onTap: () {
                                        launchUrlWeb(doc['mediaFeedList'][0]);
                                      },
                                      child: const Icon(
                                        Icons.download,
                                        color: Colors.blueAccent,
                                      )),
                                  GestureDetector(
                                      onTap: () {
                                        final collRef = db
                                            .collection('user')
                                            .doc(localdata.getUserId())
                                            .collection('myfile');
                                        DocumentReference docReference =
                                            collRef.doc(doc['id']);
                                        docReference.delete();
                                      },
                                      child: const Icon(Icons.delete)),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ));
          }
        });
  }
}

// class MyFileFilePerCategory extends StatelessWidget {
//   const MyFileFilePerCategory(
//       {super.key, required this.category});
//   final String category;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           centerTitle: true,
//           title: Text(category.toUpperCase()),
//           backgroundColor: ColorName.redprimary,
//         ),
//         body: StreamBuilder(
//             // Reading Items form our Database Using the StreamBuilder widget
//             stream: db
//                 .collection('MyFile')
//                 .orderBy('date', descending: true)
//                 .where('bookid', isEqualTo: bookModel.id)
//                 .where('subscription', isEqualTo: category)
//                 .where('category', arrayContainsAny: ['file']).snapshots(),
//             builder: (BuildContext context, AsyncSnapshot snapshot) {
//               if (snapshot.hasError) {
//                 return const Center(
//                   child: Icon(Icons.error),
//                 );
//               }
//               if (snapshot.connectionState == ConnectionState.waiting) {
//                 return const Center(
//                   child: CircularProgressIndicator(),
//                 );
//               }

//               if (snapshot.connectionState == ConnectionState.done ||
//                   snapshot.data.docs.length == 0 ||
//                   snapshot.data == null ||
//                   snapshot.data.docs == null) {
//                 return SizedBox(
//                   height: 100,
//                   child: Center(
//                     child: WidgetTextMont(
//                       'Tidak ada dokumen kegiatan tersimpan',
//                       fontWeight: FontWeight.bold,
//                       fontSize: 14,
//                       textColor: Colors.grey[600]!,
//                     ),
//                   ),
//                 );
//               } else {
//                 return Padding(
//                     padding: EdgeInsets.only(
//                       top: ScreenUtil().setHeight(10),
//                     ),
//                     child: SizedBox(
//                       height: 100,
//                       child: ListView.builder(
//                           itemCount: snapshot.data.docs.length,
//                           itemBuilder: (context, int index) {
//                             DocumentSnapshot doc = snapshot.data.docs[index];
//                             return ListTile(
//                                 contentPadding: const EdgeInsets.symmetric(
//                                     horizontal: 2.0, vertical: 0.0),
//                                 title: Text(
//                                   doc['thumbnail'],
//                                   style: const TextStyle(
//                                       fontWeight: FontWeight.w400),
//                                 ),
//                                 trailing: const Icon(Icons.download),
//                                 onTap: () {
//                                   // Here We Will Add The Update Feature and passed the value 'true' to the is update
//                                   // feature.
//                                 });
//                           }),
//                     ));
//               }
//             }));
//   }
// }
