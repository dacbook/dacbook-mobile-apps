import 'dart:ui';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/data/model/feed/feed_model.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/model/book/book_model.dart';
import '../../data/model/user/user_model.dart';
import '../../data/remote/firestore/firestore_feed_services.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../account/account_view.dart';
import '../book_forum/book_forum_view.dart';
import '../profile/profile_view.dart';
import 'following/following_current_user.dart';
import 'following/following_user.dart';
import 'myfile/myfile_view.dart';
import 'profile_content/profile_content.dart';
import 'profile_medsos_controller.dart';
import 'profile_mypost_detail.dart';
import 'rekanalumni_current_user/rekanalumni_current_user_view.dart';

part '_feed_galery_view.dart';

enum TypeOperation {
  upload,
  download,
  delete,
  none,
}

class ProfileMedsos extends StatefulWidget {
  const ProfileMedsos({super.key});
  @override
  // ignore: library_private_types_in_public_api
  _ProfileMedsosState createState() => _ProfileMedsosState();
}

class _ProfileMedsosState extends State<ProfileMedsos> {
  final GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();
  final String keyMyPosts = 'keyMyPosts';

  final FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  late SharedPreferences sharedPreferences;
  TypeOperation typeOperation = TypeOperation.download;
  bool isLoading = true;
  bool isSuccess = true;
  bool isGridView = true;

  int followercount = 0;
  String filter = '';

  final FireStoreFeedServices fireStoreFeedServices = FireStoreFeedServices();

  // int followingCount = 0;
  // int followersCount = 0;
  // int followcounting = 0;

  @override
  void initState() {
    // countFollowers();
    filter = listStoryCategory[0];
    Future(() { setState(() {}); });
    super.initState();
  }

  countFollowers() async {
    String uid = _boxStorage.getUserId();
   // followcounting = await profileMedsosController.getFollowerCount(uid);
    followercount++;
    debugPrint(followercount.toString());
  }

 Future loadtemp() async{
     setState(() {});
  }

  final BoxStorage _boxStorage = BoxStorage();
  final ProfileMedsosController profileMedsosController =
      Get.put(ProfileMedsosController());

  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();

  

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
        key: scaffoldState,
        body: RefreshIndicator(
          onRefresh: (){
          return Future(() { setState(() {}); });
          },
          child: FutureBuilder(
              future: fireStoreUserServices.getCurrentUser(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const SizedBox(
                      height: 30, width: 30, child: CircularProgressIndicator());
                }
                if (snapshot.connectionState == ConnectionState.none) {
                  return const Text('Server not found - Error 500');
                }
                if (snapshot.connectionState == ConnectionState.done) {
                  UserModel usermodel = snapshot.data!;
                 int followingCount = usermodel.following.isEmpty
                      ? 0
                      : usermodel.following.length - 1;
                 int followersCount = usermodel.followers.isEmpty
                      ? 0
                      : usermodel.followers.length - 1;
                  countFollowers();
                  //followercount = followcounting;
                  debugPrint(
                      'follow : $followingCount - follower : $followersCount');
                      
                  return Stack(
                    children: <Widget>[
                      _buildWidgetBackgroundHeader(snapshot.data!),
                      ListView(
                        children: [
                          Stack(
                            children: [
                              _buildWidgetContentProfile(snapshot.data!,followingCount,followersCount),
                              _buildWidgetPhotoProfile(snapshot.data!),
                            ],
                          ),
                          _buildWidgetLoading(),
                        ],
                      ),
                    ],
                  );
                }
                return const SizedBox();
              }),
        ));
  }

  Widget _buildWidgetLoading() {
    if (isLoading && typeOperation == TypeOperation.upload ||
        typeOperation == TypeOperation.delete) {
      return Container(
        width: double.infinity,
        height: double.infinity,
        color: ColorName.blackgrey,
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _buildWidgetPhotoProfile(UserModel userModel) {
    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(60),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: ()=> showImageViewer(
                                        context,
                                        Image.network(userModel.avatar)
                                            .image,
                                        useSafeArea: true,
                                        swipeDismissible: true,
                                        doubleTapZoomable: true),
            child: Container(
              width: ScreenUtil().setWidth(135),
              height: ScreenUtil().setHeight(135),
              decoration: BoxDecoration(
                color: ColorName.whiteprimary,
                borderRadius: BorderRadius.circular(25),
                image: DecorationImage(
                  image: NetworkImage(userModel.avatar),
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildWidgetContentProfile(UserModel usermodel,  int followcount, int followercount) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(
        top: ScreenUtil().setHeight(140),
      ),
      decoration: BoxDecoration(
        color: ColorName.whiteprimary,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(
            ScreenUtil().setWidth(10),
          ),
          topRight: Radius.circular(
            ScreenUtil().setWidth(10),
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(
          top: ScreenUtil().setWidth(10 + 56),
          right: ScreenUtil().setWidth(10),
          left: ScreenUtil().setWidth(10),
        ),
        child: Column(
          children: <Widget>[
            WidgetTextMont(
              usermodel.username,
              fontSize: 20,
              textColor: ColorName.blackgrey,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(5),
            ),
            WidgetTextMont(
              usermodel.bio == '' ? 'Biodata masih kosong' : usermodel.bio,
              textColor: ColorName.blackgrey,
              fontWeight: FontWeight.w400,
              fontSize: 14,
            ),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
          // _buildWidgetPostsFollowersFollowing(usermodel,followcount, followercount),
           ProfileContentView(uid: usermodel.uid),
            SizedBox(
              height: ScreenUtil().setHeight(10),
            ),
            _buildWidgetButtonFollowAndChat(usermodel),
            SizedBox(
              height: ScreenUtil().setHeight(20),
            ),
            _buildWidgetHeaderMyPosts(),
            // _buildTabbar(),
            _buildWidgetMyPosts(),
          ],
        ),
      ),
    );
  }

  late DocumentSnapshot documentSnapshot;
  int mypostLength = 0;

  Widget _buildWidgetMyPosts() {
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;
    return StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: isFiltered == false
            ? fireStoreFeedServices.getRelateFeedfromfollowerfilteredAll()
            : fireStoreFeedServices.getRelateFeedfromfollowerfiltered(filter),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          debugPrint('is snaps ${snapshot.data} ');
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return const Center(
              child: Icon(Icons.error),
            );
          }

          if (snapshot.data.length == 0 || snapshot.data == null) {
            return SizedBox(
              height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  WidgetTextMont(
                    'Tidak ada kegiatan tersimpan',
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    textColor: Colors.grey[600]!,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  WidgetTextMont(
                    'Buatlah sebuah cerita agar rekan alumni dapat mengetahuinya',
                    fontSize: 14,
                    textAlign: TextAlign.center,
                    textColor: Colors.grey[700]!,
                  ),
                ],
              ),
            );
          } else {
            return isGridView
                ? Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10),
                      bottom: paddingBottomScreen == 0
                          ? ScreenUtil().setHeight(10)
                          : paddingBottomScreen,
                    ),
                    child: SizedBox(
                      height: snapshot.data?.length < 6 ?  Get.height * 0.5 : snapshot.data?.length ,
                      child: GridView.builder(
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          padding: EdgeInsets.zero,
                          itemCount: snapshot.data?.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: ScreenUtil().setWidth(5),
                            mainAxisSpacing: ScreenUtil().setHeight(5),
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            FeedModel feedModel = snapshot.data[index];
                            mypostLength = snapshot.data?.length;
                            debugPrint(' post count : $mypostLength');
                            return feedModel.mediaFeedList.isNotEmpty
                                ? GestureDetector(
                                    onTap: () => 
                                    Get.to(ProfileMypostDetail(feedModel: feedModel)),
                                    // showImageViewer(
                                    //     context,
                                    //     Image.network(feedModel.mediaFeedList[0])
                                    //         .image,
                                    //     useSafeArea: true,
                                    //     swipeDismissible: true,
                                    //     doubleTapZoomable: true),
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: feedModel.mediaFeedList[0],
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            feedModel.mediaFeedList[0],
                                            fit: BoxFit.cover,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const Icon(Icons.error);
                                        },
                                      ),
                                    ),
                                  )
                                : Container();
                          }),
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(20),
                      bottom: paddingBottomScreen == 0
                          ? ScreenUtil().setHeight(20)
                          : paddingBottomScreen,
                    ),
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemCount: snapshot.data?.length,
                      separatorBuilder: (context, index) {
                        return SizedBox(
                          height: ScreenUtil().setHeight(20),
                        );
                      },
                      itemBuilder: (context, index) {
                        FeedModel feedModel = snapshot.data[index];
                        return feedModel.mediaFeedList.isNotEmpty
                            ?  GestureDetector(
                                    onTap: () => showImageViewer(
                                        context,
                                        Image.network(feedModel.mediaFeedList[0])
                                            .image,
                                        useSafeArea: true,
                                        swipeDismissible: true,
                                        doubleTapZoomable: true),
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: feedModel.mediaFeedList[0],
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            feedModel.mediaFeedList[0],
                                            fit: BoxFit.cover,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const Icon(Icons.error);
                                        },
                                      ),
                                    ),
                                  )
                            : Container();
                      },
                    ),
                  );
          }
        });
  }

  final formKey = GlobalKey<FormState>();
  final List<String> listStoryCategory = [
    'all',
    'cerita alumni',
    'cerita keluarga',
    'cerita bisnis',
    'kegiatan satuan',
  ];

  final jobRoleDropdownCtrl = TextEditingController();

  bool isFiltered = false;

  Widget _buildWidgetHeaderMyPosts() {
    return Row(
      children: <Widget>[
        Expanded(
          child: WidgetTextMont(
            'Kegiatanku',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
            width: 180,
            child: DropdownButton<String>(
              // Step 3.
              value: filter,
              // Step 4.
              items: listStoryCategory
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: const TextStyle(fontSize: 14),
                  ),
                );
              }).toList(),
              // Step 5.
              onChanged: (String? newValue) {
                setState(() {
                  filter = newValue!;
                  if (filter == 'all') {
                    isFiltered = false;
                  } else {
                    isFiltered = true;
                  }
                });
              },
            )),
        SizedBox(
          width: ScreenUtil().setWidth(5),
        ),
        isFiltered == true
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    isFiltered = false;
                    filter = 'jenis kegiatan';
                  });
                },
                child: Icon(
                  // ignore: deprecated_member_use
                  FontAwesomeIcons.close,
                  color: isGridView ? Colors.grey[800] : Colors.grey[100],
                  size: 16,
                ),
              )
            : const SizedBox.shrink(),
        SizedBox(
          width: ScreenUtil().setWidth(15),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              isGridView = false;
            });
          },
          child: Icon(
            FontAwesomeIcons.thList,
            color: isGridView ? Colors.grey[800] : Colors.grey[100],
            size: 16,
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(15),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              isGridView = true;
            });
          },
          child: Icon(
            FontAwesomeIcons.thLarge,
            color: isGridView
                ? ColorName.blackgrey.withOpacity(0.5)
                : ColorName.blackgrey,
            size: 16,
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetButtonFollowAndChat(UserModel usermodel) {
    return Column(
      children: [
        Row(
          children: <Widget>[
            Expanded(
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(ColorName.redprimary),
                    textStyle: MaterialStateProperty.all(const TextStyle(
                      fontFamily: 'Mont',
                      color: ColorName.whiteprimary,
                    ))),
                onPressed: () {
                  /* Nothing to do in here */
                  Navigator.of(context).push(MaterialPageRoute(
  builder: 
    (context) => ProfileView(
                    usermodel: usermodel,
                    isCurrentUser: true,
                  ),
  ),
).then((_) async {
  // Call setState() here or handle this appropriately
   await loadtemp();
});
                 
                },
                child: const Text(
                  'Lihat Profil',
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: ScreenUtil().setHeight(10),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(
              //  width: 100,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(ColorName.blackgrey),
                    textStyle: MaterialStateProperty.all(const TextStyle(
                      fontFamily: 'Mont',
                      color: ColorName.whiteprimary,
                    ))),
                onPressed: () {
                  /* Nothing to do in here */
               
                  Get.to( BookForumView(
                    isforumku: true,
                    bookid: '',
                    isadmin: false,
                    bookModel:  null,
                  ));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Icon(
                      Icons.chat_bubble,
                    ),
                    Text(
                      'Forum',
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              // width: 100,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(ColorName.blackgrey),
                    textStyle: MaterialStateProperty.all(const TextStyle(
                      fontFamily: 'Mont',
                      color: ColorName.whiteprimary,
                    ))),
                onPressed: () {
                  /* Nothing to do in here */
                  Get.to(const MyFileView());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Icon(
                      Icons.file_copy,
                    ),
                    Text(
                      'My File',
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              //    width: 100,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(ColorName.blackgrey),
                    textStyle: MaterialStateProperty.all(const TextStyle(
                      fontFamily: 'Mont',
                      color: ColorName.whiteprimary,
                    ))),
                onPressed: () {
                  /* Nothing to do in here */
                  Get.to(const AccountView());
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: const [
                    Icon(
                      Icons.settings,
                    ),
                    Text(
                      'Pengaturan',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildWidgetPostsFollowersFollowing(UserModel usermodel, int followcount, int followercount) {
    // int followingCount = usermodel.following.length - 1;
    // int followersCount = usermodel.followers.length - 1;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            WidgetTextMont(
              mypostLength.toString(),
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            WidgetTextMont(
              'Kegiatan',
              textColor: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ],
        ),
        GestureDetector(
          onTap: () {
            Get.to(RekanAlumniCurrentUserView(
              usermodel: usermodel,
              isCurrentUser: true,
            ));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              WidgetTextMont(
                followercount.toString(),
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
              WidgetTextMont(
                'Rekan Alumni',
                textColor: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Get.to(FollowingCurrentUserView(
              usermodel: usermodel,
            ));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              WidgetTextMont(
                followcount.toString(),
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
              WidgetTextMont(
                'Mengikuti',
                textColor: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildWidgetBackgroundHeader(UserModel userModel) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(color: ColorName.redprimary
              // image: DecorationImage(
              //   image: NetworkImage(userModel.userCoverProfile),
              //   fit: BoxFit.cover,
              // ),
              ),
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 10,
              sigmaY: 10,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.0),
              ),
            ),
          ),
        ),
        SafeArea(
          child: Padding(
            padding: EdgeInsets.only(
              // top: ScreenUtil().setHeight(14),
              right: ScreenUtil().setWidth(14),
              left: ScreenUtil().setWidth(14),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                    onTap: () async {}, child: const SizedBox.shrink()),
                GestureDetector(
                    onTap: () async {}, child: const SizedBox.shrink()),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class WidgetTextMont extends StatelessWidget {
  final String text;
  final double fontSize;
  final FontWeight fontWeight;
  final Color textColor;
  final TextAlign textAlign;

  WidgetTextMont(
    this.text, {
    this.fontSize = 20,
    this.fontWeight = FontWeight.normal,
    this.textColor = ColorName.blackgrey,
    this.textAlign = TextAlign.left,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: textColor,
        fontSize: 14,
        fontWeight: fontWeight,
        fontFamily: 'Mont',
      ),
      textAlign: textAlign,
    );
  }
}
