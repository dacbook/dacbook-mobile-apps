part of 'profile_medsos_view.dart';


class FeederGaleryUser extends StatefulWidget {
  const FeederGaleryUser({super.key, required this.entry});
  final OverlayEntry? entry;

  @override
  State<FeederGaleryUser> createState() => _FeederGaleryUserState();
}

class _FeederGaleryUserState extends State<FeederGaleryUser> {
  bool isLoading = true;
  bool isSuccess = true;
  bool isGridView = true;
  bool isFiltered = false;
  String filter = 'all';

   final List<String> listStoryCategory = [
    'cerita alumni',
    'cerita keluarga',
    'cerita karir',
    'kegiatan satuan',
  ];
  final jobRoleDropdownCtrl = TextEditingController();

  final FireStoreFeedServices fireStoreFeedServices = FireStoreFeedServices();
  @override
  Widget build(BuildContext context) {
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;
    return Column(children: [
      Row(
      children: <Widget>[
        Expanded(
          child: WidgetTextMont(
            'Kegiatanku',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          width: 180,
          child: CustomDropdown(
            hintText: 'jenis kegiatan',
            items: listStoryCategory,
            controller: jobRoleDropdownCtrl,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                filter = value;
                isFiltered = true;
                debugPrint('clog ==> status: $isFiltered - $filter');
              });
            },
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(5),
        ),
        isFiltered == true
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    isFiltered = false;
                    filter = 'jenis kegiatan';
                  });
                },
                child: Icon(
                  // ignore: deprecated_member_use
                  FontAwesomeIcons.close,
                  color: isGridView ? Colors.grey[800] : Colors.grey[100],
                  size: 16,
                ),
              )
            : const SizedBox.shrink(),
        SizedBox(
          width: ScreenUtil().setWidth(15),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              isGridView = false;
            });
          },
          child: Icon(
            // ignore: deprecated_member_use
            FontAwesomeIcons.thList,
            color: isGridView ? Colors.grey[800] : Colors.grey[100],
            size: 16,
          ),
        ),
        SizedBox(
          width: ScreenUtil().setWidth(15),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              isGridView = true;
            });
          },
          child: Icon(
            // ignore: deprecated_member_use
            FontAwesomeIcons.thLarge,
            color: isGridView
                ? ColorName.blackgrey.withOpacity(0.5)
                : ColorName.blackgrey,
            size: 16,
          ),
        ),
      ],
    ),
      StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: isFiltered == false
            ? fireStoreFeedServices.getRelateFeedfromfollowerfilteredAll()
            : fireStoreFeedServices.getRelateFeedfromfollowerfiltered(filter),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          debugPrint('is snaps ${snapshot.data} ');
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError) {
            return const Center(
              child: Icon(Icons.error),
            );
          }

          if (snapshot.data.length == 0 || snapshot.data == null) {
            return SizedBox(
              height: Get.height * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  WidgetTextMont(
                    'Tidak ada kegiatan tersimpan',
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    textColor: Colors.grey[600]!,
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(24),
                  ),
                  WidgetTextMont(
                    'Buatlah sebuah cerita agar rekan alumni dapat mengetahuinya',
                    fontSize: 14,
                    textAlign: TextAlign.center,
                    textColor: Colors.grey[700]!,
                  ),
                ],
              ),
            );
          } else {
            return isGridView
                ? Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10),
                      bottom: paddingBottomScreen == 0
                          ? ScreenUtil().setHeight(10)
                          : paddingBottomScreen,
                    ),
                    child: GridView.builder(
                        shrinkWrap: true,
                        physics: const ScrollPhysics(),
                        padding: EdgeInsets.zero,
                        itemCount: snapshot.data?.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: ScreenUtil().setWidth(5),
                          mainAxisSpacing: ScreenUtil().setHeight(5),
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          FeedModel feedModel = snapshot.data[index];
                          return GestureDetector(
                            onTap: () => showImageViewer(context,
                                Image.network(feedModel.mediaFeedList[0]).image,
                                useSafeArea: true,
                                swipeDismissible: true,
                                doubleTapZoomable: true),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(5),
                              ),
                              child: CachedNetworkImage(
                                imageUrl: feedModel.mediaFeedList[0],
                                fit: BoxFit.cover,
                                placeholder: (context, url) {
                                  return Image.asset(
                                    'assets/image/example_album1.jpeg',
                                    fit: BoxFit.cover,
                                  );
                                },
                                errorWidget: (context, url, error) {
                                  return Image.asset(
                                    'assets/image/example_album1.jpeg',
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                          );
                        }),
                  )
                : Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(20),
                      bottom: paddingBottomScreen == 0
                          ? ScreenUtil().setHeight(20)
                          : paddingBottomScreen,
                    ),
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      padding: EdgeInsets.zero,
                      itemCount: snapshot.data?.length,
                      separatorBuilder: (context, index) {
                        return SizedBox(
                          height: ScreenUtil().setHeight(20),
                        );
                      },
                      itemBuilder: (context, index) {
                        FeedModel feedModel = snapshot.data[index];
                       return  GestureDetector(
                            onTap: () => showImageViewer(context,
                                Image.network(feedModel.mediaFeedList[0]).image,
                                useSafeArea: true,
                                swipeDismissible: true,
                                doubleTapZoomable: true),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(5),
                              ),
                              child: CachedNetworkImage(
                                imageUrl: feedModel.mediaFeedList[0],
                                fit: BoxFit.cover,
                                placeholder: (context, url) {
                                  return Image.asset(
                                    'assets/image/example_album1.jpeg',
                                    fit: BoxFit.cover,
                                  );
                                },
                                errorWidget: (context, url, error) {
                                  return Image.asset(
                                    'assets/image/example_album1.jpeg',
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                          );
                      },
                    ),
                  );
          }
        })
    ],);
  }
}