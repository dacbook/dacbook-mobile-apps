import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/model/user/user_model.dart';
import '../../../data/remote/firestore/firestore_user_services.dart';
import '../following/following_current_user.dart';
import '../following/following_user.dart';
import '../profile_medsos_view.dart';
import '../rekanalumni_current_user/rekanalumni_current_user_view.dart';
import '../rekanalumni_current_user/rekanalumni_user.dart';

class ProfileContentView extends StatelessWidget {
  const ProfileContentView({super.key, required this.uid});
  final String uid;
  @override
  Widget build(BuildContext context) {
    final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();

    return SizedBox(
        child: FutureBuilder(
            future: fireStoreUserServices.getCurrentUser(),
            builder: (context, snapshot) {
              debugPrint('profile content : ${snapshot.data.toString()}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const SizedBox(
                    height: 30, width: 30, child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.none) {
                return const Text('Server not found - Error 500');
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.data != null) {
                UserModel usermodel = snapshot.data!;
                int followingCount = usermodel.following.isEmpty
                    ? 0
                    : usermodel.following.length - 1;
                int followersCount = usermodel.followers.isEmpty
                    ? 0
                    : usermodel.followers.length - 1;
                debugPrint(
                    'follow : $followingCount - follower : $followersCount');
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        WidgetTextMont(
                          usermodel.postlength.toString(),
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        WidgetTextMont(
                          'Kegiatan',
                          textColor: Colors.grey,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {
                        final boxStorage = BoxStorage();
                        String localuid = boxStorage.getUserId();
                        if (localuid == usermodel.uid) {
                          Get.to(RekanAlumniCurrentUserView(
                            usermodel: usermodel,
                            isCurrentUser: true,
                          ));
                        } else {
                          Get.to(RekanAlumniUserView(
                            usermodel: usermodel,
                            isCurrentUser: false,
                          ));
                        }
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          WidgetTextMont(
                            followersCount.toString(),
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          WidgetTextMont(
                            'Rekan Alumni',
                            textColor: Colors.grey,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        final boxStorage = BoxStorage();
                        String localuid = boxStorage.getUserId();
                        if (usermodel.uid == localuid) {
                          Get.to(FollowingCurrentUserView(
                            usermodel: usermodel,
                          ));
                        } else {
                          Get.to(FollowingUserView(
                            usermodel: usermodel,
                          ));
                        }
                      },
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          WidgetTextMont(
                            followingCount.toString(),
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                          WidgetTextMont(
                            'Mengikuti',
                            textColor: Colors.grey,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              } else {
                return Container();
              }
            }));
  }
}
