

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../data/local/box/box_storage.dart';
// import '../../../data/model/feed/feed_model.dart';
// import '../../../data/remote/firestore/firestore_book_services.dart';
// import '../../../gen/colors.gen.dart';
// import '../../feeder/feeder_controller.dart';
// import '../profile_medsos_view.dart';

// class ForumkuView extends StatefulWidget {
//   const ForumkuView({super.key});

//   @override
//   State<ForumkuView> createState() => _ForumkuViewState();
// }

// class _ForumkuViewState extends State<ForumkuView> {
  
  
//   @override
//   Widget build(BuildContext context) {
//     final BoxStorage boxStorage = BoxStorage();
//     final FeederController feedController = Get.put(FeederController());
//     final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
//     bool islikes = false;
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: ColorName.redprimary,
      
//       ),
//       body: StreamBuilder(
//         // Reading Items form our Database Using the StreamBuilder widget
//         stream: fireStoreBookServices.getforumstory(bookmodel.id),
//         builder: (BuildContext context, AsyncSnapshot snapshot) {
//           if (snapshot.connectionState == ConnectionState.waiting) {
//             return const SizedBox(
//                 width: 20,
//                 height: 20,
//                 child: CircularProgressIndicator(strokeWidth: 1.0));
//           }
//           if (snapshot.connectionState == ConnectionState.none) {
//             return const Text('Server not found - Error 500');
//           }
//           if (snapshot.connectionState == ConnectionState.done ||
//               !snapshot.hasData ||
//               snapshot.data.length == 0 ||
//               snapshot.data == []) {
//             debugPrint('this is feed = ${snapshot.data}');

//             return Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: SizedBox(
//                 height: Get.height * 0.4,
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     WidgetTextMont(
//                       'Tidak ada forum diskusi',
//                       fontWeight: FontWeight.bold,
//                       fontSize: 14,
//                       textColor: Colors.grey[600]!,
//                     ),
//                     const SizedBox(
//                       height: 24,
//                     ),
//                     WidgetTextMont(
//                       'Buatlah sebuah forum agar rekan alumni dapat berdikusi',
//                       fontSize: 14,
//                       textAlign: TextAlign.center,
//                       textColor: Colors.grey[700]!,
//                     ),
//                   ],
//                 ),
//               ),
//             );
//           } else {
//             return ListView.builder(
//               // itemExtent: snapshot.data?.docs.length,
//               physics: const NeverScrollableScrollPhysics(),
//               shrinkWrap: true,
//               itemCount: snapshot.data?.length,
//               itemBuilder: (context, int index) {
//                 String localuid = boxStorage.getUserId();
//                 FeedModel feedModel = snapshot.data[index];
//                 islikes = feedModel.followersfavorites.contains(localuid);
//                 debugPrint('clog => ${feedModel.id}');
//                 //listItem = documentSnapshot['media'];

//                 return Padding(
//                   padding: const EdgeInsets.symmetric(
//                     horizontal: 10,
//                     vertical: 15,
//                   ),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Row(
//                             children: [
//                               SizedBox(
//                                   height: 80,
//                                   child: ClipRRect(
//                                     borderRadius: BorderRadius.circular(5),
//                                     child: CachedNetworkImage(
//                                       imageUrl: feedModel.booklogo,
//                                       fit: BoxFit.cover,
//                                       width: 60,
//                                       placeholder: (context, url) {
//                                         return Image.network(
//                                           url,
//                                           fit: BoxFit.fitHeight,
//                                         );
//                                       },
//                                       errorWidget: (context, url, error) {
//                                         return const FaIcon(
//                                             FontAwesomeIcons.camera);
//                                       },
//                                     ),
//                                   )),
//                               Padding(
//                                 padding: const EdgeInsets.only(left: 8.0),
//                                 child: SizedBox(
//                                   width: 300,
//                                   child: Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: [
//                                       Text(
//                                         'Dibagikan pada ${feedModel.date} ',
//                                         overflow: TextOverflow.ellipsis,
//                                         style: const TextStyle(
//                                           fontWeight: FontWeight.w300,
//                                         ),
//                                       ),
//                                       Text(
//                                         feedModel.title,
//                                         overflow: TextOverflow.ellipsis,
//                                         style: const TextStyle(
//                                           fontSize: 18,
//                                         ),
//                                       ),
//                                       Text(
//                                         feedModel.description,
//                                         overflow: TextOverflow.ellipsis,
//                                         style: const TextStyle(
//                                           fontSize: 18,
//                                         ),
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                             ],
//                           ),
//                           const SizedBox(
//                             height: 10,
//                           ),
//                         ],
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Row(
//                                   children: [
//                                     GestureDetector(
//                                       onTap: (() async {
//                                         islikes = await fireStoreBookServices
//                                             .addLikeforumCheck(feedModel);
//                                         debugPrint('is likes = $islikes');
//                                       }),
//                                       child: islikes == true
//                                           ? const FaIcon(
//                                               FontAwesomeIcons.heartCircleCheck,
//                                               color: ColorName.redprimary)
//                                           : const FaIcon(FontAwesomeIcons.heart,
//                                               color: ColorName.redprimary),
//                                     ),
//                                     const SizedBox(
//                                       width: 10,
//                                     ),
//                                      Text('${feedModel.favorites.toString()} Suka'),
//                                     const SizedBox(
//                                       width: 20,
//                                     ),
//                                     GestureDetector(
//                                       onTap: (() {
//                                         String uid = boxStorage.getUserId();
//                                         bool islikes = feedModel
//                                             .followersfavorites
//                                             .contains(uid);
//                                         Get.to(BookForumDetailView(
//                                           feedModel: feedModel,
//                                           islikes: islikes,
//                                         ));
//                                       }),
//                                       child: const FaIcon(
//                                           FontAwesomeIcons.comment,
//                                           color: ColorName.redprimary),
//                                     ),
//                                   ],
//                                 ),
//                                 // const FaIcon(FontAwesomeIcons.bookmark,
//                                 //     color: ColorName.redprimary),
//                               ],
//                             ),
//                             const SizedBox(
//                               height: 10,
//                             ),
                           
//                           ],
//                         ),
//                       ),
//                     const  Divider(color: ColorName.redprimary, thickness: 2,)
//                     ],
//                   ),
//                 );
//               },
//             );
//           }
//         },
//       ),
//     );
//   }
// }

