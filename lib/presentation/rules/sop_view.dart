import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SOPSelectionView extends StatefulWidget {
  const SOPSelectionView({super.key});

  @override
  State<SOPSelectionView> createState() => _SOPSelectionViewState();
}

class _SOPSelectionViewState extends State<SOPSelectionView> {
    Future<void> launchSopWeb(String url) async {
  final Uri urlweb = Uri.parse(url);
  if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
    throw 'Could not launch $url';
  }
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SOP'),
        centerTitle: true,
        backgroundColor: ColorName.redprimary),
      body: SafeArea(
        child: SizedBox(
          height: Get.height * .5,
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
          GestureDetector(
            onTap: (){
              launchSopWeb('https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Fsop%2F4.4%20Tambah%20Buku%20Alumni.pdf?alt=media&token=035a3f08-de92-405d-ab2a-b0d1f1728382');
              
            },
            child: SizedBox(
              height: 50,width: Get.width,
              child: const Card(child: Text('SOP - Tambah buku alumni', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold),),))),
          GestureDetector(
            onTap: (){
              launchSopWeb('https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Fsop%2F4.5%20Tambah%20Admin.pdf?alt=media&token=af52feb2-9a1c-429d-99c7-fd68e4739009');
            
            },
            child: SizedBox(
                 height: 50,width: Get.width,
              child: const Card(child: Text('SOP - Tambah admin', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold)),))),
          GestureDetector(
            onTap: (){
               launchSopWeb('https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Fsop%2FSOP%20ADMIN%20SEKOLAH.pdf?alt=media&token=cca9eaa7-0977-4b6e-88d7-4da51303fb71');
            
           
            },
            child: SizedBox(
                 height: 50,width: Get.width,
              child: const Card(child: Text('SOP - Admin Sekolah', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold)),))),
          GestureDetector(
            onTap: (){
              launchSopWeb('https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Fsop%2FSOP%20KADISDIK.pdf?alt=media&token=f54f21bf-a84c-4d1d-9a9d-ace14b568cfe');
            
            },
            child: SizedBox(
                 height: 50,width: Get.width,
              child: const Card(child: Text('SOP - KADISDIK', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold)),))),
          
            ]),
        ),
      ),);
  }
}
