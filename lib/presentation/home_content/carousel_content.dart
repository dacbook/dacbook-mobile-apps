import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';

class CarouselContent extends StatelessWidget {
  CarouselContent({super.key});

  final List<Widget> imageSliders = [
    Assets.image.avatar.image(height: Get.height, width: Get.width * 0.8),
    Assets.image.avatar.image(height: Get.height, width: Get.width * 0.8),
    Assets.image.avatar.image(height: Get.height, width: Get.width * 0.8),
    Assets.image.avatar.image(height: Get.height, width: Get.width * 0.8),
    Assets.image.avatar.image(height: Get.height, width: Get.width * 0.8),
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      child: CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 2.0,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          pauseAutoPlayInFiniteScroll: true,
          initialPage: 2,
          autoPlay: false,
          scrollPhysics: const BouncingScrollPhysics()
        ),
        items: imageSliders,
      ),
    );
  }
}
