import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/remote/firestore/firestore_user_services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/user/user_model.dart';
import '../../gen/assets.gen.dart';
import '../profile_medsos/profile_follower_current_user_view.dart';
import '../profile_medsos/profile_medsos_controller.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class RecentFollowerView extends StatefulWidget {
  const RecentFollowerView({super.key});

  @override
  State<RecentFollowerView> createState() => _RecentFollowerViewState();
}

class _RecentFollowerViewState extends State<RecentFollowerView> {
  List<String> judul = ["Leonardo", "Ben", "Leonardo", "Ben", "Dina"];
  List<String> subjudul = [
    "Angkatan 98",
    "Angkatan 96",
    "Angkatan 20",
    "Angkatan 21",
    "Angkatan 22"
  ];

  bool isrequestfollowcurrentuser = false;
  bool isrequestfollow = false;
  bool isfollowing = false;

  bool isneedfollback = false;
  bool isdontneedfollback = false;

  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();

  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  final BoxStorage _boxStorage = BoxStorage();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
       width: Get.width,
                    height: 134,
      child: StreamBuilder(
          // Reading Items form our Database Using the StreamBuilder widget
          stream: db
              .collection('user')
              .doc(localdata.getUserId())
              .collection('followersdata')
              .snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 1.0));
            }
            if (snapshot.connectionState == ConnectionState.none) {
              return const Text('Server not found - Error 500');
            }
            if (snapshot.connectionState == ConnectionState.done ||
                !snapshot.hasData ||
                snapshot.data.docs.length == 0 ||
                snapshot.data.docs == []) {
              debugPrint('this is follower = ${snapshot.data}');
    
              return const SizedBox(child: Center(child: Text('Tidak ada rekan alumni')),);
            } else {
              String localuid = _boxStorage.getUserId();
              
              List<String> listitems= [localuid];
              List<DocumentSnapshot> doclist = snapshot.data.docs.where((DocumentSnapshot doc){
                return !listitems.contains(doc['uid']);
              }).toList();
            
             // doclist.removeWhere((element) => element[localuid]);
              debugPrint('list after : $doclist');
    
              return ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount: doclist.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext ctxt, int index) {
                 // DocumentSnapshot documentSnapshot = doclist[index];
    
                  return InkWell(
                    onTap: () async {
                      UserModel usermodel = await fireStoreUserServices
                          .loginCurrentUser(doclist[index]['uid']);
                      isrequestfollowcurrentuser = await profileMedsosController
                          .checkisrequestfollowcurrentuser(
                              uid: localuid,
                              followersid: doclist[index]['uid']);
                      isrequestfollow =
                          await profileMedsosController.checkisrequestfollow(
                              uid: localuid,
                              followersid: doclist[index]['uid']);
                      isfollowing =
                          await profileMedsosController.checkisfollowinguser(
                              uid: localuid,
                              followersid: doclist[index]['uid']);
                      debugPrint(
                          'is status request follow current user : $isrequestfollowcurrentuser - follower : $isrequestfollow');
    
                      Get.to(ProfileFollower(
                        usermodel: usermodel,
                      isrequestfollowcurrentuser: isrequestfollowcurrentuser,
                     isrequestfollow: isrequestfollow,
                     isfollowing: isfollowing,
                      ));
                      setState(() {});
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 2.0, left: 1, right: 1, bottom: 2),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 100,
                              width: Get.width * 0.3,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(12.0),
                                    child: Image.network(
                                      doclist[index]['avatar'],
                                      fit: BoxFit.fitHeight,
                                    )),
                              ),
                            ),
                            SizedBox(
                              width: 100,
                              child: Text(
                                doclist[index]['username'],
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            }
          }),
    );
  }
}
