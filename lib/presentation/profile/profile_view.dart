import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/data/model/contact/contact_model.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:timelines/timelines.dart';

import '../../data/model/user/user_model.dart';
import '../../gen/assets.gen.dart';
import '../daftar_riwayat_hidup/daftar_riwayat_hidup_view.dart';
import '../login/login_controller.dart';
import '_archivment_user_view.dart';
import '_karir_user_view.dart';
import '_school_user_view.dart';
import 'profile_controller.dart';
import 'profile_data_view.dart';
import 'profile_edit_view.dart';
import 'package:url_launcher/url_launcher.dart';

import 'riwayat_hidup_view.dart';

class ProfileView extends StatefulWidget {
  const ProfileView(
      {super.key, required this.usermodel, required this.isCurrentUser});
  final UserModel usermodel;
  final bool isCurrentUser;
  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController? tabController;

    @override
    // ignore: unused_element
    void initState() {
      tabController = TabController(length: 3, vsync: this);
      super.initState();
    }

    @override
    // ignore: unused_element
    void dispose() {
      tabController?.dispose();
      super.dispose();
    }

    final Uri urlweb = Uri.parse('https://ilmci.com/evacbook/voucher/login');
    final Uri urltelegram = Uri.parse('https://t.me/+${widget.usermodel.noHp}');
    final Uri urlline =
        Uri.parse('https://line.me/ti/p/${widget.usermodel.noHp}');
    final Uri urlWhatsapp = Uri.parse(
        'https://api.whatsapp.com/send?phone=${widget.usermodel.noWhatsapp}&text=Hello');

    Future<void> launchUrlWeb() async {
      if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urlweb';
      }
    }

    Future<void> launchUrlWhatsapp() async {
      if (!await launchUrl(urlWhatsapp, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urlWhatsapp';
      }
    }

    Future<void> launchUrlTelegram() async {
      if (!await launchUrl(urltelegram, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urltelegram';
      }
    }

    Future<void> launchUrlLine() async {
      if (!await launchUrl(urlline, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urltelegram';
      }
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorName.redprimary,
          title: const Text("Detail Profil"),
          centerTitle: true,
          elevation: 0,
        ),
        body: SizedBox(
          height: Get.height,
          child: ListView(
            children: [
              Stack(
                children: [
                  ClipPath(
                    clipper: ClipPathClass(),
                    child: Container(
                      height: 200,
                      width: Get.width,
                      color: ColorName.redprimary,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        SizedBox(
                          height: Get.height * 0.25,
                          child: Column(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 25),
                                height: 206,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.bottomRight,
                                    colors: [
                                      ColorName.orangeprimary,
                                      ColorName.orangeprimary.withOpacity(0.7),
                                      ColorName.orangeprimary.withOpacity(0.8),
                                      ColorName.redprimary.withOpacity(0.9),
                                    ],
                                  ),
                                  boxShadow: const [
                                    // to make elevation
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(1, 1),
                                      blurRadius: 2,
                                    ),
                                    // to make the coloured border
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Assets.image.logoDacLable
                                              .image(height: 50, width: 100),
                                          Assets.image.logoIlmci
                                              .image(height: 40, width: 60),
                                        ],
                                      ),
                                      const Divider(
                                        thickness: 3,
                                        color: ColorName.blueprimary,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                              width: 85.0,
                                              height: 100.0,
                                              child: Card(
                                                elevation: 5,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(4.0),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: CachedNetworkImage(
                                                      imageUrl: widget
                                                          .usermodel.avatar,
                                                      fit: BoxFit.fitHeight,
                                                      width: 40,
                                                      placeholder:
                                                          (context, url) {
                                                        return Image.network(
                                                          url,
                                                          fit: BoxFit.fitHeight,
                                                        );
                                                      },
                                                      errorWidget: (context,
                                                          url, error) {
                                                        return const FaIcon(
                                                            FontAwesomeIcons
                                                                .camera);
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              )),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, top: 2.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width: 200,
                                                  child: Text(
                                                    widget.usermodel.username,
                                                    style: const TextStyle(
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 18,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 200,
                                                  child: Text(
                                                    widget.usermodel.email,
                                                    style: const TextStyle(
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 16,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 30,
                                                ),
                                                 widget.isCurrentUser == false
                            ? const SizedBox.shrink() :
                                                Row(
                                                  children: [
                                                    const Text(
                                                      "Pin kamu ",
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          color: ColorName
                                                              .whiteprimary,
                                                          fontWeight:
                                                              FontWeight.w300),
                                                    ),
                                                    Text(
                                                      widget.usermodel.pin,
                                                      style: const TextStyle(
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Color.fromARGB(
                                                              255,
                                                              37,
                                                              255,
                                                              44)),
                                                    ),
                                                    const SizedBox(
                                                      width: 10,
                                                    ),
                                                    GestureDetector(
                                                        onTap: () async {
                                                          await Clipboard.setData(
                                                              ClipboardData(
                                                                  text: widget
                                                                      .usermodel
                                                                      .pin));
                                                          Get.snackbar(
                                                              'Tersalin',
                                                              'Pin ${widget.usermodel.pin}');
                                                        },
                                                        child: const Icon(
                                                          color: Color.fromARGB(
                                                              255, 2, 100, 180),
                                                          Icons.copy,
                                                          size: 15,
                                                        ))
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(top: 12.0),
                                        child: Text(
                                          "Powered By. PT. ILMCI INDONESIA",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: ColorName.whiteprimary,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  launchUrlWhatsapp();
                                },
                                child: Assets.image.iconWhatsapp
                                    .image(height: 40, width: 40),
                              ),
                              GestureDetector(
                                onTap: () {
                                  launchUrlLine();
                                },
                                child: Assets.image.iconLine
                                    .image(height: 40, width: 40),
                              ),
                              GestureDetector(
                                onTap: () {
                                  launchUrlTelegram();
                                },
                                child: Assets.image.iconTelegram
                                    .image(height: 40, width: 40),
                              ),
                              Assets.image.iconSignal
                                  .image(height: 40, width: 40),
                            ],
                          ),
                        ),
                        widget.isCurrentUser == true
                            ? SizedBox(
                                width: Get.width * 0.7,
                                child: Column(
                                  children: [
                                    SizedBox(
                                      width: Get.width * 0.7,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    ColorName.blueprimary),
                                            textStyle:
                                                MaterialStateProperty.all(
                                                    const TextStyle(
                                              fontFamily: 'Mont',
                                              color: ColorName.whiteprimary,
                                            ))),
                                        onPressed: () {
                                          /* Nothing to do in here */
                                          //launchURL();
                                          launchUrlWeb();
                                        },
                                        child: const Text(
                                          'Buka E-Learning',
                                        ),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    SizedBox(
                                      width: Get.width * 0.7,
                                      child: ElevatedButton(
                                        style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.all(
                                                    ColorName.blueprimary),
                                            textStyle:
                                                MaterialStateProperty.all(
                                                    const TextStyle(
                                              fontFamily: 'Mont',
                                              color: ColorName.whiteprimary,
                                            ))),
                                        onPressed: () {
                                          /* Nothing to do in here */
                                          //launchURL();
                                          Get.to(DaftarRiwayatHidupView(
                                            uid: widget.usermodel.uid,
                                          ));
                                        },
                                        child: const Text(
                                          'Daftar Riwayat Hidup',
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : const SizedBox(),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              widget.isCurrentUser == true
                                  ? GestureDetector(
                                      onTap: () {
                                        Get.to(() => ProfileEditView(
                                              usermodel: widget.usermodel,
                                            ));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: const [
                                            Icon(
                                              Icons.edit,
                                              size: 15,
                                            ),
                                            SizedBox(
                                              width: 4,
                                            ),
                                            Text('edit profil'),
                                          ],
                                        ),
                                      ),
                                    )
                                  : const SizedBox(),
                              const SizedBox(
                                height: 10,
                              ),
                              ProfileDataView(
                                  usermodel: widget.usermodel,
                                  iscurrentUser: widget.isCurrentUser),
                              // SizedBox(
                              //   width: Get.width,
                              //   child: const Padding(
                              //     padding:
                              //         EdgeInsets.only(bottom: 20.0, top: 20),
                              //     child: Text(
                              //       'Daftar Riwayat Hidup',
                              //       textAlign: TextAlign.center,
                              //       style: TextStyle(
                              //           color: ColorName.blackgrey,
                              //           fontWeight: FontWeight.bold,
                              //           fontSize: 16),
                              //     ),
                              //   ),
                              // ),
                              // DefaultTabController(
                              //   length: 3,
                              //   child: Column(
                              //     children: [
                              //       Container(
                              //         color: Colors.white,
                              //         child: TabBar(
                              //           controller: tabController,
                              //           labelColor: ColorName.redprimary,
                              //           unselectedLabelColor: Colors.black,
                              //           indicatorColor: ColorName.redprimary,
                              //           indicatorPadding:
                              //               const EdgeInsets.symmetric(
                              //                   horizontal: 10),
                              //           labelStyle: const TextStyle(
                              //             fontSize: 14,
                              //             fontFamily: 'Roboto',
                              //           ),
                              //           tabs: const [
                              //             Tab(
                              //               child: Text('Karir'),
                              //             ),
                              //             Tab(
                              //               child: Text("Pendidikan"),
                              //             ),
                              //             Tab(
                              //               child: Text("Penghargaan"),
                              //             ),
                              //           ],
                              //         ),
                              //       ),
                              //       SizedBox(
                              //         height: 200,
                              //         child: TabBarView(
                              //           controller: tabController,
                              //           children: [
                              //             KarirUserView(
                              //                 uid: widget.usermodel.uid),
                              //             PendidikanUserView(
                              //                 uid: widget.usermodel.uid),
                              //             ArchivmentUserView(
                              //               uid: widget.usermodel.uid,
                              //             ),
                              //           ],
                              //         ),
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              const SizedBox(
                                height: 40,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}

class LabelProfile extends StatelessWidget {
  const LabelProfile({
    Key? key,
    // this.data,
    this.cek,
    this.trailing,
    this.iscurrentuser,
    this.ishide,
    required this.label,
    required this.data,
    this.onTap,
  }) : super(key: key);

  // final ProfileModel? data;
  final RxBool? cek;
  final bool? trailing;
  final bool? ishide;
  final bool? iscurrentuser;
  final String label;
  final String data;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 5),
        Row(
          children: [
            Text(
              label,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
            ),
            ishide == false ? const SizedBox.shrink() :
            Text(
              '  *disembunyikan',
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 10,
                  color: ColorName.blueprimary.withOpacity(0.7)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            
              ishide == true && iscurrentuser == false ? 
              Text(
              "*" * data.length,
              style: const TextStyle(
                  overflow: TextOverflow.ellipsis,
                  fontSize: 18,
                  fontWeight: FontWeight.w400),
            ) :
              Text(
              data,
              style: const TextStyle(
                  overflow: TextOverflow.ellipsis,
                  fontSize: 18,
                  fontWeight: FontWeight.w400),
            ),
            
            // Text(
            //   data,
            //   style: const TextStyle(
            //       overflow: TextOverflow.ellipsis,
            //       fontSize: 18,
            //       fontWeight: FontWeight.w400),
            // ),
            trailing == true
                ? !cek!.value
                    ? InkWell(
                        onTap: onTap,
                        child: Row(
                          children: const [
                            Text(
                              "Belum Verifikasi",
                              style: TextStyle(color: Colors.redAccent),
                            ),
                            FaIcon(
                              FontAwesomeIcons.angleRight,
                              color: Colors.redAccent,
                              size: 14,
                            ),
                          ],
                        ),
                      )
                    : const Text(
                        "Sudah Verifikasi",
                        style: TextStyle(color: Colors.blue),
                      )
                : const Text(""),
          ],
        ),
        const SizedBox(height: 15),
        Container(
          height: 2,
          color: Colors.grey[300],
        ),
        const SizedBox(height: 15),
      ],
    );
  }
}

class ClipPathClass extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0.0, size.height - 50);

    path.quadraticBezierTo(
      size.width / 2,
      size.height,
      size.width,
      size.height - 50,
    );
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
