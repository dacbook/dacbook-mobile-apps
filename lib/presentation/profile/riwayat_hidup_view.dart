import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../../gen/colors.gen.dart';
import '../account_menu/riwayatHidup/_archivment_view.dart';
import '../account_menu/riwayatHidup/_karir_view.dart';
import '../account_menu/riwayatHidup/_pendidikan_view.dart';
import '_archivment_user_view.dart';
import '_karir_user_view.dart';
import '_school_user_view.dart';

class DaftarRiwayatHidupView extends StatefulWidget {
  const DaftarRiwayatHidupView({super.key, required this.uid});
  final String uid;
  @override
  State<DaftarRiwayatHidupView> createState() => _DaftarRiwayatHidupViewState();
}

class _DaftarRiwayatHidupViewState extends State<DaftarRiwayatHidupView>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    TabController? tabController;

    @override
    // ignore: unused_element
    void initState() {
      tabController = TabController(length: 3, vsync: this);
      super.initState();
    }

    @override
    // ignore: unused_element
    void dispose() {
      tabController?.dispose();
      super.dispose();
    }

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorName.redprimary,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox.shrink(),
              IconButton(
                icon: const Icon(Icons.add),
                onPressed: () {
                  showAlertDialogPanelAdmin(context);
                  // if (tabController!.index == 0) {
                  //   Get.to(const KarirView());
                  // } else if (tabController!.index == 1) {
                  //   Get.to(const ArchivmentView());
                  // } else if (tabController!.index == 2) {
                  //   Get.to(const PendidikanView());
                  // }
                  // if (ctx[index].id == 5)
                  //   {Get.to(const KeluargaView())},
                },
              ),
            ],
          ),
          bottom: TabBar(
              controller: tabController,
              labelColor: ColorName.redprimary,
              unselectedLabelColor: Colors.black,
              indicatorColor: ColorName.redprimary,
              indicatorPadding: const EdgeInsets.symmetric(horizontal: 10),
              labelStyle: const TextStyle(
                fontSize: 14,
                fontFamily: 'Roboto',
              ),
              tabs: const [
                Tab(
                  child: Text(
                    'Karir',
                    style: TextStyle(color: ColorName.whiteprimary),
                  ),
                ),
                Tab(
                  child: Text("Pendidikan",
                      style: TextStyle(color: ColorName.whiteprimary)),
                ),
                Tab(
                  child: Text("Penghargaan",
                      style: TextStyle(color: ColorName.whiteprimary)),
                ),
              ]),
        ),
        body: SafeArea(
          child: TabBarView(
            controller: tabController,
            children: [
              KarirUserView(uid: widget.uid),
              PendidikanUserView(uid: widget.uid),
              ArchivmentUserView(
                uid: widget.uid,
              ),
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialogPanelAdmin(BuildContext context) {
    // set up the buttons
    Widget menubutton = SizedBox(
      height: 200,
      child: Column(
        children: [
          TextButton(
            child: Text(
              "tambah karir".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              Get.to(() => const KarirView());
            },
          ),
          TextButton(
            child: Text("tambah pendidikan".toUpperCase(),
                style: const TextStyle(fontWeight: FontWeight.bold)),
            onPressed: () {
                  Get.to(() => const PendidikanView());
            },
          ),
          TextButton(
            child: Text("tambah penghargaan".toUpperCase(),
                style: const TextStyle(fontWeight: FontWeight.bold)),
            onPressed: () {
         
               Get.to(() => const ArchivmentView());
            },
          ),
          TextButton(
            child: const Text("Cancel"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Daftar Riwayat Hidup"),
      content: menubutton,
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
