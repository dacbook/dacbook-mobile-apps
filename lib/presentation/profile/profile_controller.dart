import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../data/model/profile/profile_model.dart';
import '../../data/remote/firestore/firestore_user_services.dart';

class ProfileController extends GetxController {
  ProfileModel? profileModel;

  var isLoading = true.obs;
  var isError = false.obs;
  var errmsg = "".obs;

  var isAvatarSelected = "".obs;

  final _boxStorage = BoxStorage();

  var statusVerifikasi = false.obs;

  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();

  // EmailAuth emailAuth = EmailAuth(sessionName: "Test Verifikasi E-Beasiswa");
  // var suksesSend = false.obs;
  // final emailController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    //storageVerifikasi();
    // onFetchProfile();
  }

  @override
  void onClose() {
    super.onClose();
    //isAvatarSelected.close();
  }

  updateUserData({
    required String uid,
    required UserModel usermodel,
    String? username,
    String? noktp,
    String? address,
    String? biodata,
    String? nohandphone,
    String? noline,
    String? nowhatsapp,
    String? nosignal,
    String? nokakaotalk,
    String? tgllahir,
    String? nonisn,
    String? noijazah,
    String? nowechat,
    String? karir,
    String? jabatan,
    String? status,
    String? pendidikan,
    String? penghargaan,
    bool? ishideKtp,
    bool? ishideTanggalLahir,
    bool? ishideEmail,
    bool? ishideNoHandphone,
    bool? ishideNoIjazah,
  }) async {
    String isAvatarSelected = _boxStorage.getUserAvatarSelectedCache();
    String uusername = username != '' ? username! : usermodel.username;
    String uavatar =
        isAvatarSelected != '' ? isAvatarSelected : usermodel.avatar;
    String unoktp = noktp != '' ? noktp! : usermodel.noKtp;
    String uaddress = address != '' ? address! : usermodel.address;
    String ubiodata = biodata != '' ? biodata! : usermodel.bio;
    String unohandphone = nohandphone != '' ? nohandphone! : usermodel.noHp;
    String unoline = noline != '' ? noline! : usermodel.noLine;
    String unowhatsapp = nowhatsapp != '' ? nowhatsapp! : usermodel.noWhatsapp;
    String unosignal = nosignal != '' ? nosignal! : usermodel.noSignal;
    String unokakaotalk =
        nokakaotalk != '' ? nokakaotalk! : usermodel.noKakaoTalk;
    bool uishidektp = ishideKtp ?? false;
    bool uishideNoIjazah = ishideNoIjazah ?? false;
    bool uishideNoHandphone = ishideNoHandphone ?? false;
    bool uishideEmail = ishideEmail ?? false;
    bool uishideTanggalLahir = ishideTanggalLahir ?? false;

    String utglahir = tgllahir != '' ? tgllahir! : usermodel.birthday;
    String unonisn = nonisn != '' ? nonisn! : usermodel.nonisn;
    String unoijazah = noijazah != '' ? noijazah! : usermodel.noijazah;
    String unowechat = nowechat != '' ? nowechat! : usermodel.noWechat;
    String ukarir = karir != '' ? karir! : usermodel.karir;
    String ujabatan = jabatan != '' ? jabatan! : usermodel.jabatan;
    String ustatus = status != '' ? status! : usermodel.statusMariage;
    String upendidikan = pendidikan != '' ? pendidikan! : usermodel.pendidikan;
    String upenghargaan =
        penghargaan != '' ? penghargaan! : usermodel.penghargaan;

    Map<String, Object> userData = {
      'username': uusername,
      'noKtp': unoktp,
      'address': uaddress,
      'bio': ubiodata,
      'noHp': unohandphone,
      'noLine': unoline,
      'noWhatsapp': unowhatsapp,
      'noSignal': unosignal,
      'noKakaoTalk': unokakaotalk,
      'ishideNoKtp': uishidektp,
      'ishideTanggalLahir': uishideTanggalLahir,
      'ishideNoIjazah': uishideNoIjazah,
      'ishideNoHandphone': uishideNoHandphone,
      'ishideEmail': uishideEmail,
      'avatar': uavatar,
      'penghargaan': upenghargaan,
      'pendidikan': upendidikan,
      'birthday': utglahir,
      'statusMariage': ustatus,
      'jabatan': ujabatan,
      'karir': ukarir,
      'noijazah': unoijazah,
      'nonisn': unonisn,
      'nowechat': unowechat,
    };

    fireStoreUserServices.updateUser(uid: uid, data: userData);
    var collectionstory = FirebaseFirestore.instance.collection('story').where('author', isEqualTo: _boxStorage.getUserId());
    var collectionfollowerdata = FirebaseFirestore.instance.collectionGroup('followersdata').where('uid', isEqualTo:  _boxStorage.getUserId()).where('username', isEqualTo: _boxStorage.getUserName());
    var collectionbookmember = FirebaseFirestore.instance.collectionGroup('bookmember').where('uid', isEqualTo:  _boxStorage.getUserId()).where('username', isEqualTo: _boxStorage.getUserName());
    var qsStory = await collectionstory.get();
    for (var doc in qsStory.docs) {
      await doc.reference.update({
        'userimage': uavatar,
        'username': uusername,
      });
    }
    var qsBookmember = await collectionbookmember.get();
    for (var doc in qsBookmember.docs) {
      await doc.reference.update({
        'avatar': uavatar,
        'username': uusername,
        'userimage': uavatar,
      });
    }
    var qsFollowerData = await collectionfollowerdata.get();
    for (var doc in qsFollowerData.docs) {
      await doc.reference.update({
        'avatar': uavatar,
        'username': uusername,
      });
    }
    _boxStorage.deleteUserAvatarSelectedCache();
  }
}
