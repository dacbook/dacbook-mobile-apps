import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../data/local/box/box_storage.dart';
import '../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class KarirUserView extends StatelessWidget {
  const KarirUserView({super.key, required this.uid});
  final String uid;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db
          .collection('user')
          .doc(uid)
          .collection('karir')
          .orderBy('tahun', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
           shrinkWrap: true,
           // physics: const NeverScrollableScrollPhysics(),
          itemCount: snapshot.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
            return ListTile(
              leading: Column(
                children: [
                  Text(documentSnapshot['tahun'], style: const TextStyle(color: ColorName.redprimary, fontWeight: FontWeight.bold, fontSize: 14),),
                  const Text('s/d'),
                  Text(documentSnapshot['totahun'],  style: const TextStyle(color: ColorName.redprimary,fontWeight: FontWeight.bold, fontSize: 14)),
                ],
              ),
              title: Text(documentSnapshot['instansi']),
              subtitle: Row(
                children: [
                  Text(documentSnapshot['jabatan']),
                  const Text(' - '),
                  Text(documentSnapshot['posisi']),
                ],
              ),
              onTap: () {},
             
            );
          },
        );
      },
    );
  }
}
