import 'dart:io';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/assets.gen.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:dac_apps/presentation/login/login_controller.dart';
import 'package:dac_apps/presentation/profile_medsos/profile_medsos_view.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../app/widgets/check_avatar_container.dart';
import '../../data/local/box/box_storage.dart';
import '../../data/model/user/user_model.dart';
import '../bottom_navbar/bottom_navbar_view.dart';
import '_profile_edit_avatar.dart';
import 'profile_controller.dart';

class ProfileEditView extends StatefulWidget {
  const ProfileEditView({super.key, required this.usermodel});
  final UserModel usermodel;
  @override
  State<ProfileEditView> createState() => _ProfileEditViewState();
}

class _ProfileEditViewState extends State<ProfileEditView> {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _noktp = TextEditingController();
  final TextEditingController _address = TextEditingController();
  final TextEditingController _statusMariage = TextEditingController();
  final TextEditingController _biodata = TextEditingController();
  final TextEditingController _nohandphone = TextEditingController();
  final TextEditingController _nowhatsapp = TextEditingController();
  final TextEditingController _noline = TextEditingController();
  final TextEditingController _nokakaotalk = TextEditingController();
  final TextEditingController _nosignal = TextEditingController();

  final TextEditingController _tgllahir = TextEditingController();
  final TextEditingController _nonisn = TextEditingController();
  final TextEditingController _noijazah = TextEditingController();
  final TextEditingController _nowechat = TextEditingController();
  final TextEditingController _karir = TextEditingController();
  final TextEditingController _jabatan = TextEditingController();
  final TextEditingController _status = TextEditingController();
  final TextEditingController _pendidikan = TextEditingController();
  final TextEditingController _penghargaan = TextEditingController();

  final ProfileController profileController = Get.put(ProfileController());

  final _boxStorage = BoxStorage();

  bool isloading = false;

  bool ishideNoKtp = false;
  bool ishideTanggalLahir = false;
  bool ishideEmail = false;
  bool ishideNoHandphone = false;
  bool ishideNoIjazah = false;
  bool ishideNoNISN = false;

  String isAvatar = '';
  String bday = '';

  @override
  void initState() {
    super.initState();
    localget();
  }

  localget() {
    setState(() {
      ishideNoKtp = widget.usermodel.ishideNoKtp;
      ishideTanggalLahir = widget.usermodel.ishideTanggalLahir;
      ishideEmail = widget.usermodel.ishideEmail;
      ishideNoHandphone = widget.usermodel.ishideNoHandphone;
      ishideNoIjazah = widget.usermodel.ishideNoIjazah;
      bday = widget.usermodel.birthday;
      isAvatar = widget.usermodel.avatar;
      debugPrint('is hide KTP : $ishideNoKtp');
      debugPrint('is hide no ijazah : $ishideNoIjazah');
      debugPrint('is hide tgl lahir : $ishideTanggalLahir');
    });
  }

  Future<void> fetchProfile() async {
   UserModel user = await loginCurrentUser(boxStorage.getUserId());
    // debugPrint('$listbookdata');
    setState(() {
      user;
    });
  }

  final FirebaseFirestore _db = FirebaseFirestore.instance;

    Future<UserModel> loginCurrentUser(String uid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $uid');
    return _db
        .collection('user')
        .doc(uid)
        .get()
        .then((value) => UserModel.fromJson(value.data()!, value.id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: ColorName.redprimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  _boxStorage.deleteUserAvatarSelectedCache();
                },
                child: const Icon(FontAwesomeIcons.close)),
            const Text('Edit Profile'),
            GestureDetector(
                onTap: () async {
                  if (isloading == false) {
                    setState(() {
                      isloading = true;
                    });
                    if (image != null) {
                      _boxStorage.deleteUserAvatarSelectedCache();
                      File file = File(imagePicked!.path);
                      String? fileName = file.path.split('/').last;
                      await FirebaseStorage.instance
                          .ref()
                          .child('/useravatar/')
                          .child(fileName)
                          .putFile(File(file.path))
                          .then((taskSnapshot) {
                        if (taskSnapshot.state == TaskState.success) {
                          FirebaseStorage.instance
                              .ref()
                              .child('/useravatar/')
                              .child(fileName)
                              .getDownloadURL()
                              .then((url) {
                            debugPrint('url foto : $url');
                            setState(() {
                              
                              _boxStorage.setUserAvatarSelectedCache(url);
                              debugPrint(isloading.toString());
                              profileController.updateUserData(
                                uid: widget.usermodel.uid,
                                usermodel: widget.usermodel,
                                username: _username.text,
                                noktp: _noktp.text,
                                address: _address.text,
                                biodata: _biodata.text,
                                nohandphone: _nohandphone.text,
                                noline: _noline.text,
                                nowhatsapp: _nowhatsapp.text,
                                nosignal: _nosignal.text,
                                nokakaotalk: _nokakaotalk.text,
                                noijazah: _noijazah.text,
                                nonisn: _nonisn.text,
                                tgllahir: _tgllahir.text,
                                nowechat: _nowechat.text,
                                karir: _karir.text,
                                penghargaan: _penghargaan.text,
                                pendidikan: _pendidikan.text,
                                status: _status.text,
                                jabatan: _jabatan.text,
                                ishideKtp: ishideNoKtp,
                                ishideNoIjazah: ishideNoIjazah,
                                ishideTanggalLahir: ishideTanggalLahir
                              );

                              fetchProfile();

                              setState(() {
                                image = null;
                              });
                            });
                          });
                        }
                      });
                    } else {
                      debugPrint(isloading.toString());
                      profileController.updateUserData(
                          uid: widget.usermodel.uid,
                          usermodel: widget.usermodel,
                          username: _username.text,
                          noktp: _noktp.text,
                          address: _address.text,
                          biodata: _biodata.text,
                          nohandphone: _nohandphone.text,
                          noline: _noline.text,
                          nowhatsapp: _nowhatsapp.text,
                          nosignal: _nosignal.text,
                          nokakaotalk: _nokakaotalk.text,
                          noijazah: _noijazah.text,
                          nonisn: _nonisn.text,
                          tgllahir: _tgllahir.text,
                          nowechat: _nowechat.text,
                          karir: _karir.text,
                          penghargaan: _penghargaan.text,
                          pendidikan: _pendidikan.text,
                          status: _status.text,
                          jabatan: _jabatan.text,
                           ishideKtp: ishideNoKtp,
                                ishideNoIjazah: ishideNoIjazah,
                                ishideTanggalLahir: ishideTanggalLahir);
                      setState(() {
                        image = null;
                      });
                      setState(() {
                        isloading = true;
                      });
                       fetchProfile();
                    }
                    debugPrint(isloading.toString());

                    Get.snackbar('Data telah diterima', 'Berhasil update data');
                    Get.to(const DacBottomNavbar());
                  }
                },
                child: const Icon(FontAwesomeIcons.check)),
          ],
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            isloading == true ? const CircularProgressIndicator() : Container(),
            ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              _boxStorage.deleteUserAvatarSelectedCache();
                              isAvatar = '';
                              getImage();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: ColorName.blackprimary.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              height: 90,
                              width: 90,
                              child: CachedNetworkImage(
                                imageUrl: widget.usermodel.avatar,
                                fit: BoxFit.fitHeight,
                                placeholder: (context, url) {
                                  return Image.network(
                                    widget.usermodel.avatar,
                                    fit: BoxFit.fitHeight,
                                  );
                                },
                                errorWidget: (context, url, error) {
                                  return const Icon(Icons.error);
                                },
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const ProfileEditAvatar()),
                              );
                              setState(() {
                                image = null;
                                isAvatar =
                                    _boxStorage.getUserAvatarSelectedCache();
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: ColorName.blackprimary.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              height: 90,
                              width: 90,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Assets.image.avatar3
                                    .image(fit: BoxFit.fitHeight),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Text(
                        'Edit foto atau gunakan avatar',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 14,
                            color: ColorName.blueprimary),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      isAvatar != ''
                          ? Container(
                              decoration: BoxDecoration(
                                color: ColorName.blackprimary.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              height: 90,
                              width: 90,
                              child: CachedNetworkImage(
                                imageUrl: isAvatar,
                                fit: BoxFit.fitHeight,
                                placeholder: (context, url) {
                                  return Image.network(
                                    isAvatar,
                                    fit: BoxFit.fitHeight,
                                  );
                                },
                                errorWidget: (context, url, error) {
                                  return const Icon(Icons.error);
                                },
                              ),
                            )
                          : Container(),
                      image != null
                          ? Container(
                              decoration: BoxDecoration(
                                color: ColorName.blackprimary.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(25),
                              ),
                              height: 90,
                              width: 90,
                              child: Image.file(
                                image!,
                                fit: BoxFit.fitHeight,
                              ))
                          : Container(),
                      const SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(
                    left: 20,
                  ),
                  child: Text(
                    'Formulir Perubahan Profil',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        color: ColorName.blueprimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Username',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _username,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _username,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.username,
                          //labelText: 'Username',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Row(
                    children: [
                      SizedBox(
                        width: Get.width * 0.7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'Nomor KTP',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: ColorName.blackprimary
                                          .withOpacity(0.7)),
                                ),
                                Text(
                                  '  *Tidak wajib diisi',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10,
                                      color: ColorName.redprimary
                                          .withOpacity(0.7)),
                                ),
                              ],
                            ),
                            TextFormField(
                              controller: _noktp,
                              keyboardType: TextInputType.number,
                              onSaved: (String? value) {},
                              onChanged: (value) => _noktp,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "username cannot be empty";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                border: const UnderlineInputBorder(),
                                hintText: widget.usermodel.noKtp,
                                //labelText: 'No KTP',
                              ),
                            ),
                          ],
                        ),
                      ),
                      Switch(
                        // thumb color (round icon)
                        activeColor: ColorName.redprimary,
                        activeTrackColor: Colors.grey,
                        inactiveThumbColor: Colors.blueGrey.shade600,
                        inactiveTrackColor: Colors.grey.shade400,
                        splashRadius: 50.0,
                        // boolean variable value
                        value: ishideNoKtp,
                        // changes the state of the switch
                        onChanged: (value) => setState(() {
                          ishideNoKtp = value;
                          if (value == true) {
                            const snackBar = SnackBar(
                              content: Text('Nomor KTP tidak terpublikasi'),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          } else {
                            const snackBar = SnackBar(
                              content: Text('Nomor KTP terpublikasi'),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          }
                        }),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: _dropdownIsMariageStatus(),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Alamat',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _address,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _address,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.address,
                          // labelText: 'Address',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    children: [
                      Row(
                      //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                              width: Get.width * 0.7,
                            child: GestureDetector(
                                onTap: () {
                                  showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(1900),
                                    lastDate: DateTime(2099),
                                  ).then((date) {
                                    //tambahkan setState dan panggil variabel _dateTime.
                                    setState(() {
                                      _tgllahir.text =
                                          DateFormat('yyyy-MM-dd').format(date!);
                                    });
                                  });
                                },
                                child: Row(
                                  children: [
                                    const Icon(Icons.date_range),
                                    _tgllahir.text != ''
                                        ? Text(
                                            'Tanggal lahir $bday',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14,
                                                color: ColorName.blackprimary
                                                    .withOpacity(0.7)),
                                          )
                                        : Text(
                                            'Tanggal lahir ${_tgllahir.text}',
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14,
                                                color: ColorName.blackprimary
                                                    .withOpacity(0.7)),
                                          ),
                                  ],
                                )),
                          ),
                          Switch(
                            // thumb color (round icon)
                            activeColor: ColorName.redprimary,
                            activeTrackColor: Colors.grey,
                            inactiveThumbColor: Colors.blueGrey.shade600,
                            inactiveTrackColor: Colors.grey.shade400,
                            splashRadius: 50.0,
                            // boolean variable value
                            value: ishideTanggalLahir,
                            // changes the state of the switch
                            onChanged: (value) => setState(() {
                              ishideTanggalLahir = value;
                              if (value == true) {
                                const snackBar = SnackBar(
                                  content:
                                      Text('Tanggal lahir tidak terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              } else {
                                const snackBar = SnackBar(
                                  content: Text('Tanggal lahir terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              }
                            }),
                          ),
                        ],
                      ),
                      // ignore: unnecessary_null_comparison
                      _tgllahir != null || _tgllahir.text != ''
                          ? Text(
                              _tgllahir.text,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color:
                                      ColorName.blackprimary.withOpacity(0.7)),
                            )
                          : Text(
                              widget.usermodel.birthday,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color:
                                      ColorName.blackprimary.withOpacity(0.7)),
                            ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Row(
                    children: [
                      SizedBox(
                         width: Get.width * 0.7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  'No NISN',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: ColorName.blackprimary.withOpacity(0.7)),
                                ),
                                Text(
                                  '  *Tidak wajib diisi',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 10,
                                      color: ColorName.redprimary.withOpacity(0.7)),
                                )
                                ,
                              ],
                            ),
                            TextFormField(
                              controller: _nonisn,
                              keyboardType: TextInputType.text,
                              onSaved: (String? value) {},
                              onChanged: (value) => _nonisn,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "username cannot be empty";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                border: const UnderlineInputBorder(),
                                hintText: widget.usermodel.nonisn,
                                // labelText: 'Address',
                              ),
                            ),
                          ],
                        ),
                      ),
                       Switch(
                            // thumb color (round icon)
                            activeColor: ColorName.redprimary,
                            activeTrackColor: Colors.grey,
                            inactiveThumbColor: Colors.blueGrey.shade600,
                            inactiveTrackColor: Colors.grey.shade400,
                            splashRadius: 50.0,
                            // boolean variable value
                            value: ishideNoNISN,
                            // changes the state of the switch
                            onChanged: (value) => setState(() {
                              ishideNoNISN = value;
                              if (value == true) {
                                const snackBar = SnackBar(
                                  content:
                                      Text('NISN tidak terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              } else {
                                const snackBar = SnackBar(
                                  content: Text('NISN terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              }
                            }),
                          ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: SizedBox(
                    
                    child: Row(
                      children: [
                        SizedBox(
                           width: Get.width * 0.7,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    'No Ijazah',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: ColorName.blackprimary.withOpacity(0.7)),
                                  ),
                                  Text(
                                    '  *Tidak wajib diisi',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 10,
                                        color: ColorName.redprimary.withOpacity(0.7)),
                                  ),
                                ],
                              ),
                              TextFormField(
                                controller: _noijazah,
                                keyboardType: TextInputType.text,
                                onSaved: (String? value) {},
                                onChanged: (value) => _noijazah,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "username cannot be empty";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  border: const UnderlineInputBorder(),
                                  hintText: widget.usermodel.noijazah,
                                  // labelText: 'Address',
                                ),
                              ),
                            ],
                          ),
                        ),
                        Switch(
                            // thumb color (round icon)
                            activeColor: ColorName.redprimary,
                            activeTrackColor: Colors.grey,
                            inactiveThumbColor: Colors.blueGrey.shade600,
                            inactiveTrackColor: Colors.grey.shade400,
                            splashRadius: 50.0,
                            // boolean variable value
                            value: ishideNoIjazah,
                            // changes the state of the switch
                            onChanged: (value) => setState(() {
                              ishideNoIjazah = value;
                              if (value == true) {
                                const snackBar = SnackBar(
                                  content:
                                      Text('No Ijazah tidak terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              } else {
                                const snackBar = SnackBar(
                                  content: Text('No Ijazah terpublikasi'),
                                );
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              }
                            }),
                          ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Jabatan',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _jabatan,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _jabatan,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.jabatan,
                          // labelText: 'Address',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Karir',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _karir,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _karir,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.karir,
                          // labelText: 'Address',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Pendidikan',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _pendidikan,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _pendidikan,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.pendidikan,
                          // labelText: 'Address',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Penghargaan',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _penghargaan,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _penghargaan,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.penghargaan,
                          // labelText: 'Address',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Bio',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _biodata,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => _biodata,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.bio,
                          //labelText: 'Biodata',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Nomor Handphone',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _nohandphone,
                        keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _nohandphone,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noHp,
                          // labelText: 'No Handphone',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Nomor We chat',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _nowechat,
                         keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _nowechat,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "we chat cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noWechat,
                          // labelText: 'No Handphone',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Kontak Line',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _noline,
                        keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _noline,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "line cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noLine,
                          // labelText: 'No Line',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Kontak Whatsapp',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _nowhatsapp,
                         keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _nowhatsapp,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "whatsapp cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noWhatsapp,
                          // labelText: 'No Whatsapp',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Kontak Signal',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _nosignal,
                        keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _nosignal,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "signal cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noSignal,
                          // labelText: 'No Signal',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Kontak Kakao Talk',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: ColorName.blackprimary.withOpacity(0.7)),
                      ),
                      TextFormField(
                        controller: _nokakaotalk,
                         keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) => _nokakaotalk,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "kakao talk cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const UnderlineInputBorder(),
                          hintText: widget.usermodel.noKakaoTalk,
                          // labelText: 'No Kakao Talk',
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _dropdownIsMariageStatus() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
          child: SizedBox(
            width: Get.width,
            child: CustomDropdown(
              hintText: 'Status Perkawinan',
              items: const ['Lajang', 'Sudah Menikah', 'Duda', 'Janda'],
              controller: _status,
              excludeSelected: false,
              onChanged: (value) {
                setState(() {
                  _status.text = value;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  File? image;
  XFile? imagePicked;

  Future getImage() async {
    setState(() {
      isAvatar = '';
    });
    imagePicked = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 20);
    image = File(imagePicked!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    //final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        isAvatar = _boxStorage.getUserAvatarSelectedCache();
        image = File(imagePicked!.path);
        final String path = image.toString();
        debugPrint('clog ==> ${path.toString()}');
      });
    } else {
      setState(() {
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
  }
}
