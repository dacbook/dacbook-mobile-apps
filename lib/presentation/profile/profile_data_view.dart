import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:flutter/material.dart';

import 'profile_view.dart';

class ProfileDataView extends StatefulWidget {
  const ProfileDataView({super.key, required this.usermodel,required this.iscurrentUser});
  final UserModel usermodel;
  final bool iscurrentUser;

  @override
  State<ProfileDataView> createState() => _ProfileDataViewState();
}

class _ProfileDataViewState extends State<ProfileDataView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
         LabelProfile(
          label: "Nama Lengkap",
          data: widget.usermodel.username,
          ishide: false,
          iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "No KTP",
          data: widget.usermodel.noKtp,
          ishide: widget.usermodel.ishideNoKtp,
           iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "NISN",
          data: widget.usermodel.nonisn,
          ishide: false,
           iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "no ijazah",
          data: widget.usermodel.noijazah,
          ishide:  widget.usermodel.ishideNoIjazah,
           iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Tanggal lahir",
          data: widget.usermodel.birthday,
          ishide: widget.usermodel.ishideTanggalLahir,
           iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Karir",
          data: widget.usermodel.karir,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Jabatan",
          data: widget.usermodel.jabatan,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "status pernikahan",
          data: widget.usermodel.statusMariage,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "pendidikan",
          data: widget.usermodel.pendidikan,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "penghargaan",
          data: widget.usermodel.penghargaan,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
       
        LabelProfile(
          label: "Alamat",
          data: widget.usermodel.address,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Status",
          data: widget.usermodel.statusMariage,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Biodata",
          data: widget.usermodel.bio,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        LabelProfile(
          label: "Nomor Telepon",
          data: widget.usermodel.noHp,
           ishide: false,
            iscurrentuser: widget.iscurrentUser,
        ),
        /*
        LabelProfile(
          label: "Nomor Whatsapp",
          data: widget.usermodel.noWhatsapp,
        ),
        LabelProfile(
          label: "Nomor Line",
          data: widget.usermodel.noLine,
        ),
        LabelProfile(
          label: "Nomor Kakao Talk",
          data: widget.usermodel.noKakaoTalk,
        ),
        LabelProfile(
          label: "Nomor Signal",
          data: widget.usermodel.noSignal,
        ),
        LabelProfile(
          label: "Nomor Wechat",
          data: widget.usermodel.noWechat,
        ),
        */
      ],
    );
  }
}
