import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/data/model/avatar/avatar_model.dart';
import 'package:dac_apps/data/remote/firestore/firestore_user_services.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import 'profile_controller.dart';

final db = FireStoreUserServices();
final boxStorage = BoxStorage();
final ProfileController profileController = Get.put(ProfileController());

class ProfileEditAvatar extends StatelessWidget {
  const ProfileEditAvatar({super.key});
  @override
  Widget build(BuildContext context) {
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: Column(
            children: [
              const Text(
                'Pilih Avatar Kamu',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    color: ColorName.blueprimary),
              ),
              StreamBuilder(
                // Reading Items form our Database Using the StreamBuilder widget
                stream: db.getAvatar(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10),
                      bottom: paddingBottomScreen == 0
                          ? ScreenUtil().setHeight(10)
                          : paddingBottomScreen,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: GridView.builder(
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          padding: EdgeInsets.zero,
                          itemCount: snapshot.data?.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: ScreenUtil().setWidth(5),
                            mainAxisSpacing: ScreenUtil().setHeight(5),
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            AvatarModel avatarModel = snapshot.data[index];
                            return GestureDetector(
                              onTap: () {
                                boxStorage.setUserAvatarSelectedCache(
                                    avatarModel.url);

                                Navigator.pop(context);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      ColorName.blackprimary.withOpacity(0.3),
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: avatarModel.url,
                                  fit: BoxFit.fitHeight,
                                  placeholder: (context, url) {
                                    return Image.network(
                                      avatarModel.url,
                                      fit: BoxFit.fitHeight,
                                    );
                                  },
                                  errorWidget: (context, url, error) {
                                    return const Icon(Icons.error);
                                  },
                                ),
                              ),
                            );
                          }),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
