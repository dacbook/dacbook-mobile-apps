import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';

import '../../data/local/box/box_storage.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class PendidikanUserView extends StatelessWidget {
  const PendidikanUserView({super.key, required this.uid});
  final String uid;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db
          .collection('user')
          .doc(uid)
          .collection('pendidikan')
          .orderBy('tahun', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        debugPrint('snap : ${snapshot.data}');
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          shrinkWrap: true,
          //  physics: const NeverScrollableScrollPhysics(),
          itemCount: snapshot.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
            return ListTile(
              leading: Column(
                children: [
                  Text(documentSnapshot['tahun'], style: const TextStyle(color: ColorName.redprimary, fontWeight: FontWeight.bold, fontSize: 14),),
                  const Text('s/d'),
                  Text(documentSnapshot['totahun'],  style: const TextStyle(color: ColorName.redprimary,fontWeight: FontWeight.bold, fontSize: 14)),
                ],
              ),
              title: Text(documentSnapshot['instansi']),
              subtitle: Row(
                children: [
                  Text(documentSnapshot['gelar']),
                  const Text(' - '),
                  Text(documentSnapshot['keterangan']),
                ],
              ),
              onTap: () {},
            );
          },
        );
      },
    );
  }
}
