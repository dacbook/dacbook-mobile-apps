import 'dart:convert';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../../gen/assets.gen.dart';
import '../book_review/book_review_view.dart';
import '../bookgroup/_bookgroup_detail_view.dart';
import '../bookgroup/bookgroup_component/_bookgroup_info_view.dart';
import '../bookgroup/bookgroup_controller.dart';
import '../profile_medsos/profile_follower_current_user_view.dart';
import '../profile_medsos/profile_medsos_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';

final db = FirebaseFirestore.instance;
String? value;

class SearchView extends StatefulWidget {
  const SearchView({super.key});

  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  String name = "";
  String keyword = '';

  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();

  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();
  final BoxStorage _boxStorage = BoxStorage();
  final BookGroupController bookGroupController = BookGroupController();

  final List<String> listSearch = [
    'user',
    'book',
  ];

  var txtControllerSearch = TextEditingController();
  bool isFiltered = false;

  bool isrequestfollowcurrentuser = false;
  bool isrequestfollow = false;
  bool isfollowing = false;

  bool isneedfollback = false;
  bool isdontneedfollback = false;

  Widget _dropdownSearchFilter() {
    return SizedBox(
      width: Get.width * 0.3,
      child: CustomDropdown(
        hintText: 'Filter',
        items: listSearch,
        controller: txtControllerSearch,
        excludeSelected: false,
        onChanged: (value) {
          setState(() {
            txtControllerSearch.text = value;
            if (value == 'user') {
              isFiltered = true;
            }
            if (value == 'book') {
              isFiltered = false;
            }
          });
          debugPrint('selected => $isFiltered - $value');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: Padding(
              padding: const EdgeInsets.only(top: 35.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Assets.image.logoDacLable
                        .image(fit: BoxFit.fitWidth, height: 50),
                  ]),
            ),
            backgroundColor: ColorName.whiteprimary,
          ),
        ),
        body: ListView(
          children: [
            Row(
              children: [
                _dropdownSearchFilter(),
                isFiltered == false
                    ? SizedBox(
                        height: 55,
                        width: Get.width * 0.7,
                        child: Card(
                          child: TextField(
                            decoration: const InputDecoration(
                                prefixIcon: Icon(Icons.search),
                                hintText: 'Cari Buku Alumni...'),
                            onChanged: (val) {
                              setState(() {
                                keyword = val;
                              });
                            },
                          ),
                        ),
                      )
                    : SizedBox(
                        height: 55,
                        width: Get.width * 0.7,
                        child: Card(
                          child: TextField(
                            decoration: const InputDecoration(
                                prefixIcon: Icon(Icons.search),
                                hintText: 'Cari Rekan Alumni...'),
                            onChanged: (val) {
                              setState(() {
                                name = val;
                              });
                            },
                          ),
                        ),
                      ),
              ],
            ),
            isFiltered == false
                ? StreamBuilder(
                    // Reading Items form our Database Using the StreamBuilder widget
                    stream: fireStoreBookServices.getAllBook(),

                    builder:
                        (BuildContext context, AsyncSnapshot snapshotbook) {
                      if (snapshotbook.connectionState ==
                          ConnectionState.waiting) {
                        return const SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(strokeWidth: 1.0));
                      }
                      if (snapshotbook.connectionState ==
                          ConnectionState.none) {
                        return const Text('Server not found - Error 500');
                      }
                      if (snapshotbook.connectionState ==
                          ConnectionState.done) {
                        debugPrint('data buku : ${snapshotbook.data?.length}');
                        if (!snapshotbook.hasData ||
                            snapshotbook.data.length == 0) {
                          return SizedBox(
                            height: Get.height * 0.4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Tidak ada data buku',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.grey[600]),
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                              ],
                            ),
                          );
                        }
                      }

                      return ListView.builder(
                          // itemExtent: snapshot.data?.docs.length,
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshotbook.data?.length,
                          itemBuilder: (context, int index) {
                            BookModel documentSnapshot =
                                snapshotbook.data[index];
                            //listItem = documentSnapshot['media'];

                            if (keyword.isEmpty) {
                              /*
                              return GestureDetector(
                                onTap: () async {
                                  bool isjoined = await bookGroupController
                                      .checkIsMemberOrNot(
                                          documentSnapshot.bookmember);
                                  debugPrint('$isjoined');
                                  if (isjoined == true) {
                                    Get.snackbar(
                                        'Informasi', "Selamat datang kembali");
                                    Get.to(
                                        () => BookGroupAdministratorDetailView(
                                              id: documentSnapshot.id,
                                              doc: documentSnapshot,
                                            ));
                                  } else {
                                    Get.snackbar('Informasi',
                                        "anda tidak terdaftar pada buku ini");
                                    showModalBottomSheet(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return showBottomSheet(
                                            context, documentSnapshot);
                                      },
                                    );
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 2,
                                    vertical: 2,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Card(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  SizedBox(
                                                      height: 70,
                                                      width: 70,
                                                      child: Card(
                                                        color: Colors.white
                                                            .withOpacity(0.9),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl:
                                                                documentSnapshot
                                                                    .bookimage,
                                                            fit:
                                                                BoxFit.fitWidth,
                                                            width: Get.width *
                                                                0.94,
                                                            placeholder:
                                                                (context, url) {
                                                              return Image
                                                                  .asset(
                                                                'assets/image/example_album1.jpeg',
                                                                fit: BoxFit
                                                                    .cover,
                                                              );
                                                            },
                                                            errorWidget:
                                                                (context, url,
                                                                    error) {
                                                              return Image
                                                                  .asset(
                                                                'assets/image/example_album1.jpeg',
                                                                fit: BoxFit
                                                                    .cover,
                                                              );
                                                            },
                                                          ),
                                                        ),
                                                      )),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      documentSnapshot
                                                                  .ismasterbook ==
                                                              true
                                                          ? const Text(
                                                              "Master book",
                                                              style: TextStyle(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: ColorName
                                                                      .redprimary),
                                                            )
                                                          : const Text(
                                                              "Sub book",
                                                              style: TextStyle(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: ColorName
                                                                      .orangeprimary),
                                                            ),
                                                      Text(
                                                        documentSnapshot
                                                            .bookname,
                                                        style: const TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        'dibuat pada ${documentSnapshot.date} ',
                                                        style: const TextStyle(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      SizedBox(
                                                        width: 220,
                                                        child: Text(
                                                          softWrap: false,
                                                          overflow:
                                                              TextOverflow
                                                                  .ellipsis,
                                                          documentSnapshot
                                                              .description,
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              const FaIcon(
                                                  FontAwesomeIcons.angleRight,
                                                  color: ColorName.redprimary)
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            */
                              return Container();
                            }

                            if (documentSnapshot.bookname
                                .toString()
                                .toLowerCase()
                                .startsWith(keyword.toLowerCase())) {
                              return GestureDetector(
                                onTap: () async {
                                  bool isjoined = await bookGroupController
                                      .checkIsMemberOrNot(
                                          documentSnapshot.bookmember);
                                  bool isadmin = await bookGroupController
                                      .checkIsAdminOrNot(
                                          documentSnapshot.bookadmin);
                                  debugPrint(
                                      'is join : $isjoined - is admin : $isadmin');
                                  if (isjoined == true) {
                                    Get.snackbar(
                                        'Informasi', "Selamat datang kembali");
                                    Get.to(() => BookGroupInfo(
                                        uid: documentSnapshot.id,
                                        documentSnapshot: documentSnapshot,
                                        isadmin: isadmin));
                                  } else {
                                    Get.to(BookReviewView(
                                      bookModel: documentSnapshot,
                                    ));
                                    // Get.snackbar('Informasi',
                                    //     "anda tidak terdaftar pada buku ini");
                                    // showModalBottomSheet(
                                    //   context: context,
                                    //   builder: (BuildContext context) {
                                    //     return showBottomSheet(
                                    //         context, documentSnapshot);
                                    //   },
                                    // );
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 2,
                                    vertical: 2,
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Card(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  SizedBox(
                                                      height: 70,
                                                      width: 70,
                                                      child: Card(
                                                        color: Colors.white
                                                            .withOpacity(0.9),
                                                        child: ClipRRect(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          child:
                                                              CachedNetworkImage(
                                                            imageUrl:
                                                                documentSnapshot
                                                                    .bookimage,
                                                            fit:
                                                                BoxFit.fitWidth,
                                                            width: Get.width *
                                                                0.94,
                                                            placeholder:
                                                                (context, url) {
                                                              return Image
                                                                  .network(
                                                                url,
                                                                fit: BoxFit
                                                                    .cover,
                                                              );
                                                            },
                                                            errorWidget:
                                                                (context, url,
                                                                    error) {
                                                              return const Icon(
                                                                  Icons.error);
                                                            },
                                                          ),
                                                        ),
                                                      )),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      documentSnapshot
                                                                  .ismasterbook ==
                                                              true
                                                          ? const Text(
                                                              "Master book",
                                                              style: TextStyle(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: ColorName
                                                                      .redprimary),
                                                            )
                                                          : const Text(
                                                              "Sub book",
                                                              style: TextStyle(
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: ColorName
                                                                      .orangeprimary),
                                                            ),
                                                      Text(
                                                        documentSnapshot
                                                            .bookname,
                                                        style: const TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                        'dibuat pada ${documentSnapshot.date} ',
                                                        style: const TextStyle(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      SizedBox(
                                                        width: 220,
                                                        child: Text(
                                                          softWrap: false,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          documentSnapshot
                                                              .description,
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              const FaIcon(
                                                  FontAwesomeIcons.angleRight,
                                                  color: ColorName.redprimary)
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            }

                            return const SizedBox();
                          });
                    },
                  )
                : StreamBuilder(
                    stream: _fireStoreUserServices.getFollowers(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      // debugPrint('lenght : ${snapshot.data!.length}');
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(strokeWidth: 1.0));
                      }
                      if (snapshot.connectionState == ConnectionState.none) {
                        return const Text('Server not found - Error 500');
                      }
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (!snapshot.hasData ||
                            snapshot.data.docs.length == 0 ||
                            snapshot.data == null) {
                          return SizedBox(
                            height: Get.height * 0.4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const <Widget>[Text('Tidak ada data')],
                            ),
                          );
                        }
                      }
                      return ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) {
                            UserModel usermodel = snapshot.data![index];

                            if (name.isEmpty) {
                              return Container();
                            }
                            if (usermodel.username
                                    .toString()
                                    .toLowerCase()
                                    .startsWith(name.toLowerCase()) ||
                                usermodel.email
                                    .toString()
                                    .toLowerCase()
                                    .startsWith(name.toLowerCase())) {
                              return GestureDetector(
                                onTap: () async {
                                  String localuid = _boxStorage.getUserId();
                                  isrequestfollowcurrentuser =
                                      await profileMedsosController
                                          .checkisrequestfollowcurrentuser(
                                              uid: localuid,
                                              followersid: usermodel.uid);
                                  isrequestfollow =
                                      await profileMedsosController
                                          .checkisrequestfollow(
                                              uid: localuid,
                                              followersid: usermodel.uid);
                                  isfollowing = await profileMedsosController
                                      .checkisfollowinguser(
                                          uid: localuid,
                                          followersid: usermodel.uid);
                                  debugPrint(
                                      'is status request follow current user (${usermodel.uid}) : isrequestfollowcurrentuser $isrequestfollowcurrentuser - isrequestfollow : $isrequestfollow');

                                  if (usermodel.uid == localuid) {
                                    Get.to(const ProfileMedsos());
                                  } else {
                                    Get.to(ProfileFollower(
                                      usermodel: usermodel,
                                      isrequestfollowcurrentuser:
                                          isrequestfollowcurrentuser,
                                      isrequestfollow: isrequestfollow,
                                      isfollowing: isfollowing,
                                    ));
                                  }

                                  setState(() {});
                                },
                                child: ListTile(
                                  title: Text(
                                    usermodel.username,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Text(
                                    usermodel.email,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  // leading: CircleAvatar(
                                  //   backgroundImage: NetworkImage(data['image']),
                                  // ),
                                ),
                              );
                            }
                            return Container();
                          });
                    },
                  ),
          ],
        ));
  }
}

showBottomSheet(BuildContext context, BookModel? bookmodel) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: const EdgeInsets.only(top: 20),
    child: Column(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.9,
          child: TextField(
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              // Used a ternary operator to check if isUpdate is true then display
              // Update Todo.
              labelText: "masukan pin anda",
              hintText: 'Enter An PIN',
            ),
            onChanged: (String val) {
              // Storing the value of the text entered in the variable value.
              value = val;
            },
          ),
        ),
        TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(ColorName.blueprimary),
            ),
            onPressed: () {
              final BoxStorage boxStorage = BoxStorage();
              if (bookmodel!.pin == value) {
                String localuid = boxStorage.getUserId();
                String username = boxStorage.getUserName();
                String avatar = boxStorage.getUserAvatar();
                final time = DateFormat('hh:mm');
                final date = DateFormat('dd-MM-yyyy');
                db.collection('book').doc(bookmodel.id).update({
                  'bookmember': FieldValue.arrayUnion([localuid]),
                  'totalmember': FieldValue.increment(1)
                });
                db
                    .collection('book')
                    .doc(bookmodel.id)
                    .collection('bookmember')
                    .doc(localuid)
                    .set({
                  'bookmember': localuid,
                  'date': date.format(DateTime.now()),
                  'time': time.format(DateTime.now()),
                  'username': username,
                  'avatar': avatar,
                  'uid': localuid,
                  'memberlevel': 'anggota',
                });
                Get.snackbar('Informasi', "Selamat anda berhasil bergabung");
                Navigator.pop(context);
                Get.to(() => BookGroupAdministratorDetailView(
                      id: bookmodel.id,
                      doc: bookmodel,
                    ));
              } else {
                Get.snackbar('Informasi', "Pin anda salah");
              }
              Navigator.pop(context);
            },
            child:
                const Text('Bergabung', style: TextStyle(color: Colors.white))),
      ],
    ),
  );
}
