import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/remote/firestore/firestore_book_services.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
String? value;

class BookForumAddView extends StatefulWidget {
  const BookForumAddView({super.key, required this.bookmodel});
  final BookModel bookmodel;
  @override
  State<BookForumAddView> createState() => _BookForumAddViewState();
}

class _BookForumAddViewState extends State<BookForumAddView> {
  var txtControllerTitle = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerCategory = TextEditingController();
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  bool iscameraStatusActive = false;
  bool isloading = false;

  uploadImagetFirebase() async {
    iscameraStatusActive = true;
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    List<dynamic> followerList =
        await fireStoreBookServices.getBookmemberBuku(widget.bookmodel.id);
    File file = File(imageFromCamera!.path);
    String? fileName = file.path.split('/').last;
    await FirebaseStorage.instance
        .ref()
        .child('/bookforumphotos/')
        .child(uid)
        .child(fileName)
        .putFile(File(file.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${file.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child('/bookforumphotos/')
            .child(uid)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');

          final collRef =
              db.collection('bookforumuser').doc(uid).collection('story');

          final collRef2 = db.collection('bookforum');
          DocumentReference docReference = collRef.doc();
          DocumentReference docReference2 = collRef2.doc(docReference.id);
          debugPrint('book id : ${widget.bookmodel.id}');
          Map<String, dynamic> data = {
            'id': docReference.id,
            'bookid': widget.bookmodel.id,
            'booklogo': widget.bookmodel.bookimage,
            'author': uid,
            'category': [category],
            'mediaFeedList': imageUrls,
            'thumbnail': url,
            'username': username,
            'userimage': avatar,
            'description': txtControllerDescription.text,
            'mediaFeed': url,
            'followers': followerList,
            'title': txtControllerTitle.text,
            'bookmasterid': widget.bookmodel.bookmasterid,
            'favorites': 0,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
          };

          docReference.set(data);
          docReference2.set(data);

          return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
      Navigator.pop(context);
    });
  }

  List<String> imageUrls = [];

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    print(imageUrls);
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    String uid = _boxStorage.getUserId();
    Reference storageReference =
        FirebaseStorage.instance.ref().child('bookforum/$uid/${imageData.path}');
    UploadTask uploadTask = storageReference.putFile(imageData);
    await uploadTask.whenComplete(() => null);

    return await storageReference.getDownloadURL();
  }

  uploadCloudFirestore() async {
    iscameraStatusActive = true;
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();

    try {
      List<dynamic> followerList =
          await fireStoreBookServices.getBookmemberBuku(widget.bookmodel.id);
      final time = DateFormat('hh:mm');
      final date = DateFormat('dd-MM-yyyy');

      final collRef =
          db.collection('bookforumuser').doc(uid).collection('story');

      final collRef2 = db.collection('bookforum');
      DocumentReference docReference = collRef.doc();
      DocumentReference docReference2 = collRef2.doc(docReference.id);
      debugPrint('book id : ${widget.bookmodel.id}');
      Map<String, dynamic> data = {
        'id': docReference.id,
        'bookid': widget.bookmodel.id,
        'booklogo': widget.bookmodel.bookimage,
        'author': uid,
        'username': username,
        'userimage': avatar,
        'description': txtControllerDescription.text,
        'mediaFeed': [widget.bookmodel.bookimage],
        'category': [category],
        'mediaFeedList': imageUrls,
        'followers': followerList,
        'title': txtControllerTitle.text,
        'bookmasterid': widget.bookmodel.bookmasterid,
        'favorites': 0,
        'date': date.format(DateTime.now()),
        'time': time.format(DateTime.now()),
      };

      docReference.set(data);
      docReference2.set(data);
    } catch (e) {
      debugPrint(e.toString());
      return e.toString();
    }
    debugPrint('proses upload selesai');
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
    return 'Success';
  }

  uploadMultiBucketFiles() async {
    debugPrint(imagefiles.length.toString());
    try {
      for (int i = 0; i < imagefiles.length; i++) {
        //  File file = File(imageFromCamera!.path);
        //  String? fileName = file.path.split('/').last;
        debugPrint("task done ==> ${imagefiles[i].path}");
        await FirebaseStorage.instance
            .ref()
            .child('/feedPhotos/$i')
            .putFile(File(imagefiles[i].path))
            .then((taskSnapshot) {
          // download url when it is uploaded
          if (taskSnapshot.state == TaskState.success) {
            FirebaseStorage.instance
                .ref()
                .child('/feedPhotos/$i')
                .child(imagefiles[i].path)
                .getDownloadURL()
                .then((url) {
              imageUrls.add(url);
              debugPrint("Here is the URL of Image $url");

              return url;
            }).catchError((onError) {
              debugPrint("Got Error $onError");
            });
          }
          Navigator.pop(context);
        });
      }
      //upload the list of imageUrls to firebase as an array

    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _videoPlayerController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tambah forum'),
        centerTitle: true,
        backgroundColor: ColorName.redprimary,
      ),
      body: LoadingOverlay(
        isLoading: isloading,
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView(
            children: [
              isImage == true
                  ? Wrap(
                      children: imagefiles.map((imageone) {
                        return SizedBox(
                            child: Card(
                          child: SizedBox(
                            height: 100,
                            width: 100,
                            child: Image.file(File(imageone.path)),
                          ),
                        ));
                      }).toList(),
                    )
                  : Container(),
              if (_video != null)
                _videoPlayerController!.value.isInitialized
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRect(
                          child: AspectRatio(
                            aspectRatio:
                                _videoPlayerController!.value.aspectRatio *
                                    16 /
                                    9,
                            child: VideoPlayer(_videoPlayerController!),
                          ),
                        ),
                      )
                    : Container(),
              image == null
                  ? Container()
                  : Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Image.file(image!),
                    ),
              thumbnailName == null
                  ? Container()
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(thumbnailName!),
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: SizedBox(
                  child: TextFormField(
                    controller: txtControllerTitle,
                    // obscureText: txtControllerDescription,
                    keyboardType: TextInputType.text,
                    onChanged: (value) => txtControllerTitle,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "title cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Masukan title',
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: SizedBox(
                  child: TextFormField(
                    controller: txtControllerDescription,
                    // obscureText: txtControllerDescription,
                    keyboardType: TextInputType.text,
                    onChanged: (value) => txtControllerDescription,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "description cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Masukan deskripsi',
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 140,
                      child: GestureDetector(
                          onTap: () => showModalBottomSheet(
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20.0),
                                      topRight: Radius.circular(20.0)),
                                ),
                                context: context,
                                builder: (BuildContext context) {
                                  return showBottomSheet(context);
                                },
                              ),
                          child: Row(
                            children: const [
                              Text('Tambahkan file'),
                              Icon(
                                Icons.attachment,
                                color: ColorName.blackgrey,
                                size: 26,
                              ),
                            ],
                          )),
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                ColorName.redprimary),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.redprimary)))),
                        onPressed: () async {
                           if (txtControllerDescription.text != '' &&
                              isloading != true) {
                            setState(() {
                              isloading = true;
                            });
                            if (iscameraStatusActive == true) {
                              uploadImagetFirebase();
                            } else {
                              if (imagefiles.length > 5) {
                                Get.snackbar(
                                    'Informasi', 'Batas maksimal foto hanya 5');
                              } else {
                                List<File> imageListData = [];
                                if (isPdf == true) {
                                  imageListData.add(_pdf!);
                                } else {
                                  imageListData = imagefiles
                                      .map<File>((xfile) => File(xfile.path))
                                      .toList();
                                }

                                imageUrls = await uploadFiles(imageListData);
                                debugPrint(imageUrls.toString());
                                await uploadCloudFirestore();
                              }
                            }
                            setState(() {
                              isloading = false;
                            });
                            // ignore: use_build_context_synchronously
                            Navigator.pop(context);
                          } else {}
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Bentuk forum".toUpperCase(),
                                  style: const TextStyle(fontSize: 14)),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  File? image;
  XFile? imageFromCamera;
  final ImagePicker imgpicker = ImagePicker();
  VideoPlayerController? _videoPlayerController;
  List<XFile> imagefiles = [];
  File? _video;
  File? _pdf;
  String? thumnail;
  String? thumbnailName;
  bool isImage = false;
  bool isVideo = false;
  bool isPdf = false;

  String category = '';

  getFileImages() async {
    isImage = true;
    isVideo = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickMultiImage();
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != [] || pickedfiles.isNotEmpty) {
        imagefiles = pickedfiles;
        thumnail = pickedfiles[0].path;
        setState(() {
          category = 'image';
        });
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFilePdf() async {
    isImage = false;
    isVideo = false;
    isPdf = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['doc', 'pdf', 'xls', 'xlsx'],
    );
    try {
      if (result != null) {
        _pdf = File(result.files.single.path!);
        int sizeInBytes = _pdf!.lengthSync();
        double sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb < 10) {
          PlatformFile file = result.files.first;
          thumbnailName = file.name;
          setState(() {
            category = 'pdf';
          });
          // This file is Longer the
        } else {
          debugPrint('file too big');
          isPdf = false;
          Get.snackbar('File terlalu besar', 'ukuran file harus dibawah 10mb');
        }
      } else {
        // User canceled the picker
      }
      setState(() {});
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFileVideo() async {
    isVideo = true;
    isImage = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickVideo(source: ImageSource.gallery);

      // thumnail
      final String? _path = await VideoThumbnail.thumbnailFile(
        video: pickedfiles!.path,
        thumbnailPath: (await getTemporaryDirectory()).path,

        /// path_provider
        imageFormat: ImageFormat.PNG,
        //maxHeight: 120,
        quality: 10,
      );
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != null) {
        _video = File(pickedfiles.path);
        _videoPlayerController = VideoPlayerController.file(_video!)
          ..initialize().then((_) {
            setState(() {});
            _videoPlayerController!.play();
          });
        thumnail = _path;
        imagefiles.add(pickedfiles);
        setState(() {
          category = 'video';
        });
      } else {
        debugPrint("No image is selected.");
      }
    } catch (e) {
      debugPrint("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () => getFileImages(),
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt,
                    size: 60,
                    semanticLabel: 'foto',
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () => getFileVideo(),
            child: const Card(
                color: ColorName.carnationPink,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.video_call,
                    semanticLabel: 'video',
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
