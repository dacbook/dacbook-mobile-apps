import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/data/remote/firestore/firestore_book_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_plus/flutter_swiper_plus.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/feed/feed_model.dart';
import '../../data/remote/firestore/firestore_feed_services.dart';
import '../../gen/colors.gen.dart';
import '../feeder/feeder_controller.dart';
import '../login/login_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';
import 'book_forum_add.dart';
import 'book_forum_detail_view.dart';

class BookForumView extends StatelessWidget {
   BookForumView(
      {super.key, required this.bookid, required this.isforumku, required this.isadmin,  this.bookModel});
  final String bookid;
  final bool isforumku;
  final bool isadmin;
   BookModel? bookModel;
  @override
  Widget build(BuildContext context) {
    final BoxStorage boxStorage = BoxStorage();
    final FeederController feedController = Get.put(FeederController());
    final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
    bool islikes = false;

    List<String> masterReport = [
      '1. Blokir konten yang berpotensi melanggar',
      '2. Blokir pengguna untuk potensi pelanggaran',
      '3. Melaporkan konten pengguna yang melanggar',
      '4. Melaporkan konten yang melanggar',
      '5. Ini adalah spam',
      '6. Saya hanya tidak menyukainya',
      '7. Kekerasan atau organisasi berbahaya',
      '8. Informasi palsu',
    ];

    void reportFeed(context) {
      showModalBottomSheet(
          //  isScrollControlled: true,
          context: context,
          builder: (context) {
            return Container(
              padding: const EdgeInsets.all(8),
              height: 300,
              //  height: MediaQuery.of(context).viewInsets.bottom,
              alignment: Alignment.center,
              child: ListView.builder(
                  itemCount: masterReport.length,
                  itemBuilder: (context, index) {
                    return SizedBox(
                      height: 50,
                      child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                masterReport[index].toUpperCase(),
                                style: const TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          onTap: () {
                            Get.snackbar('Laporan diterima',
                                'Laporan anda sedang diproses');
                            Navigator.of(context).pop();
                          }),
                    );
                  }),
            );
          });
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          Container(),
         isforumku == true ? const SizedBox.shrink() : GestureDetector(
            onTap: (){
              Get.to(BookForumAddView(bookmodel: bookModel!,));
            },
            child: const Icon(Icons.add))
        ]),
      ),
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: isforumku == false
            ? fireStoreBookServices.getforumstory(bookid)
            : fireStoreBookServices.getforumstorycurrentuser(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(strokeWidth: 1.0));
          }
          if (snapshot.connectionState == ConnectionState.none) {
            return const Text('Server not found - Error 500');
          }
          if (snapshot.connectionState == ConnectionState.done ||
              !snapshot.hasData ||
              snapshot.data.length == 0 ||
              snapshot.data == []) {
            debugPrint('this is feed = ${snapshot.data}');

            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: Get.height * 0.4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    WidgetTextMont(
                      'Tidak ada forum diskusi',
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      textColor: Colors.grey[600]!,
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    WidgetTextMont(
                      'Buatlah sebuah forum agar rekan alumni dapat berdikusi',
                      fontSize: 14,
                      textAlign: TextAlign.center,
                      textColor: Colors.grey[700]!,
                    ),
                  ],
                ),
              ),
            );
          } else {
            return ListView.builder(
              // itemExtent: snapshot.data?.docs.length,
              physics: const BouncingScrollPhysics(),
             // shrinkWrap: true,
              itemCount: snapshot.data?.length,
              itemBuilder: (context, int index) {
                String localuid = boxStorage.getUserId();
                FeedModel feedModel = snapshot.data[index];
                islikes = feedModel.followersfavorites.contains(localuid);
                debugPrint('clog => ${feedModel.id}');
                //listItem = documentSnapshot['media'];
                 String jumlahkomentar = '';
              if(feedModel.commentcount < 1000){
                jumlahkomentar = feedModel.commentcount.toString();
              } else if(feedModel.commentcount >= 5000){
                jumlahkomentar = '5rb';
              } 
               else if(feedModel.commentcount > 4000){
                jumlahkomentar = '4rb';
              } else if(feedModel.commentcount > 3000){
                jumlahkomentar = '3rb';
              } else if(feedModel.commentcount > 2000){
                jumlahkomentar = '2rb';
              } else if(feedModel.commentcount > 1000){
                jumlahkomentar = '1rb';
              }

                return Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 15,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                  height: 80,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: CachedNetworkImage(
                                      imageUrl: feedModel.booklogo,
                                      fit: BoxFit.cover,
                                      width: 60,
                                      placeholder: (context, url) {
                                        return Image.network(
                                          url,
                                          fit: BoxFit.fitHeight,
                                        );
                                      },
                                      errorWidget: (context, url, error) {
                                        return const FaIcon(
                                            FontAwesomeIcons.camera);
                                      },
                                    ),
                                  )),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: SizedBox(
                                      width: 300,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                'Dibagikan pada ${feedModel.date} ',
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w300,
                                                ),
                                              ),
                                              GestureDetector(
                                                  onTap: () =>
                                                      reportFeed(context),
                                                  child: const Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 10.0),
                                                    child: Text(
                                                      'Laporkan',
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                          Text(
                                            feedModel.title,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                              fontSize: 18,
                                            ),
                                          ),
                                          Text(
                                            feedModel.description,
                                            overflow: TextOverflow.ellipsis,
                                            style: const TextStyle(
                                              fontSize: 18,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    GestureDetector(
                                      onTap: (() async {
                                        islikes = await fireStoreBookServices
                                            .addLikeforumCheck(feedModel);
                                        debugPrint('is likes = $islikes');
                                      }),
                                      child: islikes == true
                                          ? const FaIcon(
                                              FontAwesomeIcons.heartCircleCheck,
                                              color: ColorName.redprimary)
                                          : const FaIcon(FontAwesomeIcons.heart,
                                              color: ColorName.redprimary),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                        '${feedModel.favorites.toString()} Suka'),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: (() {
                                            String uid = boxStorage.getUserId();
                                            bool islikes = feedModel
                                                .followersfavorites
                                                .contains(uid);
                                            Get.to(BookForumDetailView(
                                              feedModel: feedModel,
                                              islikes: islikes,
                                              isadmin: isadmin,
                                            ));
                                          }),
                                          child: const FaIcon(
                                              FontAwesomeIcons.comment,
                                              color: ColorName.redprimary),
                                        ),
                                         Padding(
                                        padding: const EdgeInsets.only(left:8.0),
                                        child: feedModel.commentcount == 0 ? Container(): Text('$jumlahkomentar menanggapi'),
                                      ),
                                      ],
                                    ),
                                  ],
                                ),
                                // const FaIcon(FontAwesomeIcons.bookmark,
                                //     color: ColorName.redprimary),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                      const Divider(
                        color: ColorName.redprimary,
                        thickness: 2,
                      )
                    ],
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}
