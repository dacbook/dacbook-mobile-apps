import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/data/remote/firestore/firestore_book_services.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_plus/flutter_swiper_plus.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:readmore/readmore.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../app/widgets/video_player_custom.dart';
import '../../app/widgets/video_player_custome_file.dart';
import '../../data/local/box/box_storage.dart';
import '../../data/model/feed/feed_model.dart';
import '../../gen/colors.gen.dart';
import '../feeder/feeder_controller.dart';
import 'book_forum_comment_view.dart';

class BookForumDetailView extends StatefulWidget {
  const BookForumDetailView(
      {super.key,
      required this.feedModel,
      required this.islikes,
      required this.isadmin});
  final FeedModel feedModel;
  final bool islikes;
  final bool isadmin;
  @override
  State<BookForumDetailView> createState() => _BookForumDetailViewState();
}

class _BookForumDetailViewState extends State<BookForumDetailView> {
  var txtControllerComment = TextEditingController();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  final boxStorage = BoxStorage();
  final FeederController feedController = Get.put(FeederController());
  bool islikes = false;
  int countKeyboardComment = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorName.redprimary,
        ),
        body: StreamBuilder(
            // Reading Items form our Database Using the StreamBuilder widget
            stream: fireStoreBookServices
                .getForumStoriesDetail(widget.feedModel.id),
            builder: (context, snapshot) {
              debugPrint('detail feed snap : ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const SizedBox(
                    height: 30, width: 30, child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.none) {
                return const Text('Server not found - Error 500');
              }

              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasData) {
                FeedModel feedData = snapshot.data!;
                islikes = feedData.followersfavorites
                    .contains(boxStorage.getUserId());
                String jumlahkomentar = '';
                if (feedData.commentcount < 1000) {
                  jumlahkomentar = feedData.commentcount.toString();
                } else if (feedData.commentcount > 5000) {
                  jumlahkomentar = '5rb';
                } else if (feedData.commentcount > 4000) {
                  jumlahkomentar = '4rb';
                } else if (feedData.commentcount > 3000) {
                  jumlahkomentar = '3rb';
                } else if (feedData.commentcount > 2000) {
                  jumlahkomentar = '2rb';
                } else if (feedData.commentcount > 1000) {
                  jumlahkomentar = '1rb';
                }

                return ListView(children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 15,
                    ),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                  height: 50,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: CachedNetworkImage(
                                      imageUrl: feedData.booklogo,
                                      fit: BoxFit.fitHeight,
                                      width: 50,
                                      placeholder: (context, url) {
                                        return Image.network(
                                          url,
                                          fit: BoxFit.fitHeight,
                                        );
                                      },
                                      errorWidget: (context, url, error) {
                                        return const FaIcon(
                                            FontAwesomeIcons.camera);
                                      },
                                    ),
                                  )),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      feedData.title,
                                      style: const TextStyle(
                                        fontSize: 18,
                                      ),
                                    ),
                                    Text(
                                      'Dibagikan pada ${feedData.date} ',
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          feedData.category[0] == 'image'
                              ? SizedBox(
                                  height: 250,
                                  child: feedData.mediaFeedList.length > 1
                                      ? Swiper(
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: CachedNetworkImage(
                                                imageUrl: feedData
                                                    .mediaFeedList[index],
                                                fit: BoxFit.fitHeight,
                                                width: Get.width * 0.94,
                                                placeholder: (context, url) {
                                                  return Image.network(
                                                    url,
                                                    fit: BoxFit.fitHeight,
                                                  );
                                                },
                                                errorWidget:
                                                    (context, url, error) {
                                                  return const FaIcon(
                                                      FontAwesomeIcons.camera);
                                                },
                                              ),
                                            );
                                          },
                                          indicatorLayout:
                                              PageIndicatorLayout.COLOR,
                                          autoplay: false,
                                          itemCount:
                                              feedData.mediaFeedList.length,
                                          pagination: const SwiperPagination(),
                                          //control: const SwiperControl(),
                                        )
                                      : ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: CachedNetworkImage(
                                            imageUrl: feedData.mediaFeedList[0],
                                            fit: BoxFit.cover,
                                            width: Get.width * 0.94,
                                            placeholder: (context, url) {
                                              return Image.network(
                                                url,
                                                fit: BoxFit.cover,
                                              );
                                            },
                                            errorWidget: (context, url, error) {
                                              return const FaIcon(
                                                  FontAwesomeIcons.camera);
                                            },
                                          ),
                                        ),
                                )
                              : Container(),
                          widget.feedModel.category[0] == 'video'
                              ? VideoPlayerCustomDac(
                                  videourl: widget.feedModel.mediaFeedList[0],
                                )
                              : Container(),
                          const SizedBox(height: 5),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                feedData.mediaFeedList.isEmpty
                                    ? ReadMoreText(
                                        '${feedData.username} ${feedData.description}',
                                        trimLines: 2,
                                        colorClickableText:
                                            ColorName.redprimary,
                                        trimMode: TrimMode.Line,
                                        trimCollapsedText: 'Selengkapnya',
                                        trimExpandedText: 'Lebih sedikit',
                                        moreStyle: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : Container(),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            fireStoreBookServices
                                                .addLikeforumCheck(feedData);

                                            debugPrint('is likess : $islikes');
                                          },
                                          child: islikes == true
                                              ? const FaIcon(
                                                  FontAwesomeIcons
                                                      .heartCircleCheck,
                                                  color: ColorName.redprimary)
                                              : const FaIcon(
                                                  FontAwesomeIcons.heart,
                                                  color: ColorName.redprimary),
                                        ),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        Row(
                                          children: [
                                            GestureDetector(
                                              onTap: () {},
                                              child: const FaIcon(
                                                  FontAwesomeIcons.comment,
                                                  color: ColorName.redprimary),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 8.0),
                                              child: feedData.commentcount == 0
                                                  ? Container()
                                                  : Text(
                                                      '$jumlahkomentar menanggapi'),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    // const FaIcon(FontAwesomeIcons.bookmark,
                                    //     color: ColorName.redprimary),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text('${feedData.favorites} Suka'),
                              ],
                            ),
                          ),
                          const SizedBox(height: 16),
                          feedData.mediaFeedList.isEmpty
                              ? const SizedBox()
                              : ReadMoreText(
                                  '${feedData.username} ${feedData.description}',
                                  trimLines: 2,
                                  colorClickableText: ColorName.redprimary,
                                  trimMode: TrimMode.Line,
                                  trimCollapsedText: 'Selengkapnya',
                                  trimExpandedText: 'Lebih sedikit',
                                  moreStyle: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                          Column(
                            children: [
                              isImage == true
                                  ? Padding(
                                      padding: const EdgeInsets.only(
                                          top: 20.0, bottom: 4),
                                      child: Wrap(
                                        children: imagefiles.map((imageone) {
                                          return SizedBox(
                                              child: Card(
                                            child: SizedBox(
                                              height: 90,
                                              width: 90,
                                              child: Image.file(
                                                  File(imageone.path)),
                                            ),
                                          ));
                                        }).toList(),
                                      ),
                                    )
                                  : Container(),
                              if (_video != null ||
                                  _videoPlayerController != null)
                                _videoPlayerController!.value.isInitialized
                                    ? Padding(
                                        padding:
                                            const EdgeInsets.only(top: 12.0),
                                        child: VideoPlayerCustomFileDac(
                                          videourl: _videoPlayerController!,
                                        ))
                                    : Container(),
                              image == null
                                  ? Container()
                                  : Padding(
                                      padding: const EdgeInsets.only(
                                          top: 12.0, bottom: 4),
                                      child: Image.file(image!),
                                    ),
                              thumbnailName == null
                                  ? Container()
                                  : Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Text(thumbnailName!),
                                      ),
                                    ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20.0, left: 10, right: 10),
                                child: SizedBox(
                                  child: TextFormField(
                                    controller: txtControllerComment,
                                    keyboardType: TextInputType.multiline,
                                    onChanged: (value) {
                                      value.length;
                                      debugPrint(value.length.toString());
                                      countKeyboardComment = value.length;
                                      txtControllerComment;
                                    },
                                    maxLines: null,
                                    maxLength: 1000,
                                    onSaved: (String? value) {},
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "comment cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Berikan tanggapan kamu',
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Row(
                                      children: [
                                        SizedBox(
                                          width: 140,
                                          child: GestureDetector(
                                              onTap: () => showModalBottomSheet(
                                                    shape:
                                                        const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(
                                                                      20.0),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20.0)),
                                                    ),
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return showBottomSheet(
                                                          context);
                                                    },
                                                  ),
                                              child: Row(
                                                children: const [
                                                  Text('Tambahkan file'),
                                                  Icon(
                                                    Icons.attachment,
                                                    color: ColorName.blackgrey,
                                                    size: 26,
                                                  ),
                                                ],
                                              )),
                                        ),
                                        ElevatedButton(
                                            style: ButtonStyle(
                                                foregroundColor:
                                                    MaterialStateProperty.all<
                                                        Color>(Colors.white),
                                                backgroundColor:
                                                    MaterialStateProperty.all<Color>(
                                                        ColorName.redprimary),
                                                shape: MaterialStateProperty.all<
                                                        RoundedRectangleBorder>(
                                                    RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.circular(8),
                                                        side: const BorderSide(color: ColorName.redprimary)))),
                                            onPressed: () async {
                                              if (countKeyboardComment == 0) {
                                                Get.snackbar('Informasi',
                                                    'Komentar masih kosong');
                                              } else {
                                                List<File> imageListData = [];
                                                imageListData = imagefiles
                                                    .map<File>((xfile) =>
                                                        File(xfile.path))
                                                    .toList();
                                                fireStoreBookServices
                                                    .addCommentForum(
                                                        '${txtControllerComment.text}  ',
                                                        widget.feedModel,
                                                        imageListData,
                                                        category);
                                                txtControllerComment.clear();
                                                setState(() {
                                                  image = null;
                                                  _video = null;
                                                  thumbnailName = null;
                                                  imagefiles = [];
                                                  isImage = false;
                                                  isVideo = false;
                                                  isPdf = false;
                                                  // _videoPlayerController!.dispose();
                                                  _videoPlayerController = null;
                                                });
                                              }
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text("Kirim".toUpperCase(),
                                                      style: const TextStyle(
                                                          fontSize: 12)),
                                                ],
                                              ),
                                            )),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          BookForumCommentView(
                            feedId: widget.feedModel.id,
                            isadmin: widget.isadmin,
                            bookid: feedData.bookid,
                          )
                        ]),
                  )
                ]);
              }
              return const SizedBox();
            }));
  }

  File? image;
  XFile? imageFromCamera;
  final ImagePicker imgpicker = ImagePicker();
  VideoPlayerController? _videoPlayerController;
  List<XFile> imagefiles = [];
  File? _video;
  File? _pdf;
  String? thumnail;
  String? thumbnailName;
  bool isImage = false;
  bool isVideo = false;
  bool isPdf = false;

  String category = '';

  getFileImages() async {
    isImage = true;
    isVideo = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickMultiImage();
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != [] || pickedfiles.isNotEmpty) {
        imagefiles = pickedfiles;
        thumnail = pickedfiles[0].path;
        setState(() {
          category = 'image';
        });
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFilePdf() async {
    isImage = false;
    isVideo = false;
    isPdf = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['doc', 'pdf', 'xls', 'xlsx'],
    );
    try {
      if (result != null) {
        _pdf = File(result.files.single.path!);
        int sizeInBytes = _pdf!.lengthSync();
        double sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb < 10) {
          PlatformFile file = result.files.first;
          thumbnailName = file.name;
          setState(() {
            category = 'pdf';
          });
          // This file is Longer the
        } else {
          debugPrint('file too big');
          isPdf = false;
          Get.snackbar('File terlalu besar', 'ukuran file harus dibawah 10mb');
        }
      } else {
        // User canceled the picker
      }
      setState(() {});
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFileVideo() async {
    isVideo = true;
    isImage = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickVideo(source: ImageSource.gallery);

      // thumnail
      final String? _path = await VideoThumbnail.thumbnailFile(
        video: pickedfiles!.path,
        thumbnailPath: (await getTemporaryDirectory()).path,

        /// path_provider
        imageFormat: ImageFormat.PNG,
        //maxHeight: 120,
        quality: 10,
      );
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != null) {
        _video = File(pickedfiles.path);
        _videoPlayerController = VideoPlayerController.file(_video!)
          ..initialize().then((_) {
            setState(() {});
            _videoPlayerController!.play();
          });
        thumnail = _path;
        imagefiles.add(pickedfiles);
        setState(() {
          category = 'video';
        });
      } else {
        debugPrint("No image is selected.");
      }
    } catch (e) {
      debugPrint("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () => getFileImages(),
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt,
                    size: 60,
                    semanticLabel: 'foto',
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () => getFileVideo(),
            child: const Card(
                color: ColorName.carnationPink,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.video_call,
                    semanticLabel: 'video',
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
