import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../feeder/_feed_add_view.dart';
import '../feeder/feeder_view.dart';
import '../home_content/carousel_content.dart';
import '../home_content/recent_follower_view.dart';
import '../notification/notification_view.dart';
import '../profile_header/profile_header_view.dart';

final db = FirebaseFirestore.instance;
String? value;

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Assets.image.logoDacLable
                      .image(fit: BoxFit.fitWidth, height: 90),
                  Padding(
                    padding: const EdgeInsets.only(right: 25.0),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return showBottomSheet(context, false, null);
                                },
                              );
                            },
                            child: const FaIcon(
                              FontAwesomeIcons.plusSquare,
                              color: ColorName.redprimary,
                            )),
                        const SizedBox(
                          width: 20,
                        ),
                        GestureDetector(
                          onTap: (){
                            Get.to(const NotificationView());
                          },
                          child: const FaIcon(FontAwesomeIcons.bell,
                              color: ColorName.redprimary),
                        ),
                      ],
                    ),
                  ),
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: ListView(
        // shrinkWrap: true,
        children: [
          Column(
            children: [
            ProfileHeaderView()
              //const FirstMenuView(),
              //const SecondMenuView(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:const [
                 Text(
                  "Rekan Alumni",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.black),
                ),
                 RecentFollowerView(),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          //CarouselContent(),
   const  FeederView()
        ],
      ),
    );
  }
}

showBottomSheet(
    BuildContext context, bool isUpdate, DocumentSnapshot? documentSnapshot) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: const EdgeInsets.only(top: 20),
    child: SizedBox(
      height: MediaQuery.of(context).size.height * 0.35,
      child: Column(
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 40,
              child: const Text(
                'Ceritakan Perjalananmu Disini',
                style: TextStyle(
                  color: ColorName.redprimary,
                  fontSize: 20, fontWeight: FontWeight.bold),
              )),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () => Get.to(const AddFeederView(
                    category: 'cerita alumni',
                  )),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const FaIcon(FontAwesomeIcons.bookOpenReader,
                          color: ColorName.redprimary),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: const Text('Buat Cerita Alumni',
                            style: TextStyle(fontSize: 16)),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => Get.to(const AddFeederView(
                    category: 'cerita keluarga',
                  )),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const FaIcon(FontAwesomeIcons.book,
                          color: ColorName.redprimary),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: const Text('Buat Cerita Keluarga',
                            style: TextStyle(fontSize: 16)),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => Get.to(const AddFeederView(
                    category: 'cerita bisnis',
                  )),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const FaIcon(FontAwesomeIcons.bookBookmark,
                          color: ColorName.redprimary),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: const Text('Buat Cerita bisnis',
                            style: TextStyle(fontSize: 16)),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => Get.to(const AddFeederView(
                    category: 'kegiatan satuan',
                  )),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const FaIcon(FontAwesomeIcons.userGroup,
                          color: ColorName.redprimary),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: const Text('Buat Cerita Kegiatan Satuan',
                            style: TextStyle(fontSize: 16)),
                      ),
                    ],
                  ),
                ),
                // TextButton(
                //     style: ButtonStyle(
                //       backgroundColor:
                //           MaterialStateProperty.all(Colors.lightBlueAccent),
                //     ),
                //     onPressed: () {
                //       // Check to see if isUpdate is true then update the value else add the value
                //       if (isUpdate) {
                //         db.collection('feed').doc(documentSnapshot?.id).update({
                //           'feed': value,
                //         });
                //       } else {
                //         db.collection('feed').add({'feed': value});
                //       }
                //       Navigator.pop(context);
                //     },
                //     child: isUpdate
                //         ? const Text(
                //             'UPDATE',
                //             style: TextStyle(color: Colors.white),
                //           )
                //         : const Text('ADD', style: TextStyle(color: Colors.white))),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
