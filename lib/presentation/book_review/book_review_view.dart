import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:dac_apps/presentation/bottom_navbar/bottom_navbar_view.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/model/bookmember/book_member_model.dart';
import '../../data/model/user/user_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../../gen/assets.gen.dart';
import '../bookgroup/_bookgroup_detail_view.dart';
import '../bookgroup/bookgroup_component/_bookgroup_info_view.dart';
import '../bookgroup/bookgroup_controller.dart';
import '../profile_medsos/profile_follower_current_user_view.dart';
import '../profile_medsos/profile_medsos_controller.dart';

final db = FirebaseFirestore.instance;

class BookReviewView extends StatefulWidget {
  const BookReviewView({
    super.key,
    required this.bookModel,
  });
  final BookModel bookModel;
  @override
  State<BookReviewView> createState() => _BookReviewViewState();
}

class _BookReviewViewState extends State<BookReviewView>
    with SingleTickerProviderStateMixin {
  TabController? tabController;
  bool isjoined = false;
  late String linkshort;
  String ilmcisharemessage = '';

  @override
  // ignore: unused_element
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    checkrequest();
    super.initState();
  }

  @override
  // ignore: unused_element
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

  checkrequest() async {
    isjoined = await bookGroupController
        .checkIsMemberRequestJoin(widget.bookModel.bookmemberrequest);
    debugPrint('is request status " $isjoined');
  }

  createDynamicLink(String id) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://ilmci.page.link/',
      link: Uri.parse('https://development.dac.com/book?$id'),
      androidParameters: const AndroidParameters(
        packageName: 'com.ilmci.dac.android.development',
        minimumVersion: 1,
      ),
    );
    var dynamicUrl =
        await FirebaseDynamicLinks.instance.buildShortLink(parameters);
    dynamicUrl.shortUrl;
    debugPrint('dynamic uri : ${dynamicUrl.shortUrl}');

    return dynamicUrl;
  }

  convertmessage(String urishort) {
    String link = urishort;
    String bookname = widget.bookModel.bookname;
    String bookpin = widget.bookModel.pin;
    ilmcisharemessage = ''' Kepada semua $bookname, 
 Mari kita bergabung di Digital Alumni & Community Book (DAC Book), karya anak bangsa bertema perdamaian dunia n Aksi belneg (setia kpd Pancasila) serta nikmati berbagai manfaat lainnya 
 
 Daftar Baru (Register): 
 1. Buka link : $link
 2. Install 
 3. Register 
 
 Cari Buku (Sign In):
 1. klik menu buku, Cari $bookname/ Klik link buku yang di lampirkan ( $link ). 
 2. Masukkan PIN $bookpin / klik permintaan bergabung - admin 
 Mari bersatu dlm kerangka NKRI (PANCASILA) & buktikan Merah Putihmu 🇮🇩 
 (tiktok: ac book) ''';
    setState(() {
      isloading = false;
    });
    Share.share(ilmcisharemessage);
  }

  final FireStoreBookServices fireStoreBookServices =
      Get.put(FireStoreBookServices());

  final BookGroupController bookGroupController = BookGroupController();

  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();

  bool isrequestfollcurrentuser = false;
  bool isrequestfoll = false;
  bool isneedfollback = false;
  bool isdontneedfollback = false;

  bool isloading = false;

  final boxStorage = BoxStorage();

  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(children: [
            _header(),
            Column(
              children: [
                _body(),
              ],
            ),
          ]),
          _bottombutton(),
        ],
      ),
    );
  }

  Widget _header() {
    return Stack(
      children: [
        Container(
          height: Get.height * .5,
          width: Get.width,
          color: ColorName.orangeGlow.withOpacity(0.5),
        ),
        Column(
          children: [
            _appbarCustom(),
            _bookcover(),
          ],
        ),
      ],
    );
  }

  Widget _appbarCustom() {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const SizedBox(
                width: 50,
                height: 40,
                child: Card(
                    color: ColorName.redprimary,
                    child: Icon(
                      Icons.arrow_back,
                      size: 30,
                      color: ColorName.whiteprimary,
                    ))),
          ),
          const SizedBox(),
        ],
      ),
    );
  }

  Widget _bottombutton() {
    return Positioned(
        bottom: 60,
        left: 15,
        right: 15,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: ColorName.redprimary,
                    textStyle: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.bold)),
                onPressed: () {
                  // showAlertDialogEnterBookPin(context);
                  Get.to(ConfirmData(
                    bookModel: widget.bookModel,
                    category: 'requestpin',
                  ));
                  debugPrint('tes');
                },
                child: const Text('Masukan pin')),
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: ColorName.redprimary,
                    textStyle: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.bold)),
                onPressed: () {
                  Get.to(ConfirmData(
                    bookModel: widget.bookModel,
                    category: 'requestjoin',
                  ));
                },
                child:
                    //const Text('Permohonan masuk buku')
                    isjoined == false
                        ? const Text('Permohonan masuk buku')
                        : GestureDetector(
                            onTap: () {
                              Get.to(const DacBottomNavbar());
                            },
                            child: const Text('Dalam pengajuan'))),
          ],
        ));
  }

  Widget _bookcover() {
    return Container(
        color: Colors.red,
        height: Get.height * .3,
        width: Get.width * .5,
        child: Card(
          color: Colors.red,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  widget.bookModel.ismasterbook == true
                      ? const Text(
                          'Master book',
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.orange,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        )
                      : Container(),
                  Text(
                    widget.bookModel.bookname,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: CachedNetworkImage(
                  imageUrl: widget.bookModel.bookimage,
                  fit: BoxFit.fill,
                  height: 100,
                  width: 100,
                  // width: Get.width * 0.50,
                  placeholder: (context, url) {
                    return Image.network(
                      url,
                      fit: BoxFit.cover,
                    );
                  },
                  errorWidget: (context, url, error) {
                    return const Icon(Icons.error);
                  },
                ),
              ),
              Text(
                widget.bookModel.description,
                maxLines: 2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white.withOpacity(0.7),
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ));
  }

  Widget _body() {
    return Column(
      children: [
        _bookpreview(),
      ],
    );
  }

  Widget _bookpreview() {
    return SizedBox(
      width: Get.width,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.bookModel.bookname,
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                GestureDetector(
                    onTap: () async {
                      var link = await createDynamicLink(widget.bookModel.id);
                      debugPrint('link : ${link.shortUrl}');
                      await Clipboard.setData(
                          ClipboardData(text: link.shortUrl.toString()));
                      Get.snackbar('Tersalin', 'Buku tersalin');
                    },
                    child: const Icon(
                      Icons.copy,
                      size: 16,
                    )),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'dibuat pada ${widget.bookModel.date}',
              maxLines: 1,
              textAlign: TextAlign.start,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w200,
                fontSize: 14,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Jumlah alumni : ${widget.bookModel.bookmember.length}',
              maxLines: 1,
              textAlign: TextAlign.start,
              style: const TextStyle(
                color: ColorName.orangeGlow,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            const SizedBox(
              height: 25,
            ),
            /*
            SizedBox(
              width: Get.width,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        if (isloading == false) {
                          setState(() {
                            isloading = true;
                          });
                          var link =
                              await createDynamicLink(widget.bookModel.id);
                          linkshort = link.shortUrl.toString();
                          convertmessage(link.shortUrl.toString());
                          debugPrint('link : ${link.shortUrl}');
                        }
                      },
                      child: Row(
                        children: const [
                          Icon(Icons.share),
                          Text('Bagikan buku'),
                        ],
                      ),
                    )
                  ]),
            ),
            */
            Padding(
              padding: const EdgeInsets.only(top: 25.0),
              child: DefaultTabController(
                length: 2,
                child: Column(
                  children: [
                    Container(
                      color: Colors.white,
                      child: TabBar(
                        controller: tabController,
                        labelColor: ColorName.redprimary,
                        unselectedLabelColor: Colors.black,
                        indicatorColor: ColorName.redprimary,
                        indicatorPadding:
                            const EdgeInsets.symmetric(horizontal: 10),
                        labelStyle: const TextStyle(
                          fontSize: 14,
                          fontFamily: 'Roboto',
                        ),
                        tabs: const [
                          Tab(
                            child: Text('Deskripsi'),
                          ),
                          Tab(
                            child: Text("Alumni"),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 200,
                      child: TabBarView(
                        controller: tabController,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.bookModel.description,
                              style:
                                  const TextStyle(fontWeight: FontWeight.w300),
                            ),
                          ),
                          getbookmembers(),
                          //bookorganization()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 60,
            )
          ],
        ),
      ),
    );
  }

  Widget getbookmembers() {
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;

    return StreamBuilder(
      stream: fireStoreBookServices.getBookmember(widget.bookModel.id),
      builder: (context, snapshotsBookmember) {
        return (snapshotsBookmember.connectionState == ConnectionState.waiting)
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(10),
                  bottom: paddingBottomScreen == 0
                      ? ScreenUtil().setHeight(10)
                      : paddingBottomScreen,
                ),
                child: GridView.builder(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    padding: EdgeInsets.zero,
                    itemCount: snapshotsBookmember.data!.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      crossAxisSpacing: ScreenUtil().setWidth(5),
                      mainAxisSpacing: ScreenUtil().setHeight(5),
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      debugPrint(
                          'id book : ${widget.bookModel.id} len : ${snapshotsBookmember.data!.length}');
                      BookmemberModel data = snapshotsBookmember.data![index];
                      return GestureDetector(
                        onTap: () async {
                          // UserModel userdata = await fireStoreUserServices
                          //     .getCurrentFollower(data.uid);
                          // isrequestfollcurrentuser =
                          //     await profileMedsosController
                          //         .checkisrequestfollowcurrentuser(
                          //             uid: boxStorage.getUserId(),
                          //             followersid: userdata.uid);
                          // isrequestfoll = await profileMedsosController
                          //     .checkisrequestfollow(
                          //         uid: boxStorage.getUserId(),
                          //         followersid: userdata.uid);
                          // debugPrint(
                          //     'is status follow from current : $isrequestfollcurrentuser - is status follow : $isrequestfoll');

                          // Get.to(ProfileFollower(
                          //   usermodel: userdata,
                          //   isrequestfollowcurrentuser:
                          //       isrequestfollcurrentuser,
                          //   isrequestfollow: isrequestfoll,
                          // ));
                          // setState(() {});
                        },
                        child: Column(
                          children: [
                            CachedNetworkImage(
                              height: 60,
                              imageUrl: data.avatar,
                              fit: BoxFit.fitWidth,
                              imageBuilder: (context, imageProvider) =>
                                  CircleAvatar(
                                radius: 30,
                                backgroundImage: imageProvider,
                                backgroundColor: Colors.transparent,
                              ),
                              placeholder: (context, url) {
                                return Image.network(
                                  height: 20,
                                  data.avatar,
                                  fit: BoxFit.fitWidth,
                                );
                              },
                              errorWidget: (context, url, error) {
                                return const Icon(Icons.error);
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                data.username.toUpperCase(),
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      );
                    }),
              );
      },
    );
  }

  Widget bookorganization() {
    return StreamBuilder<QuerySnapshot>(
      stream: db
          .collection('book')
          .doc(widget.bookModel.id)
          .collection('bookmember')
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.data.docs.length == 0 ||
            snapshot.data.docs.length == null) {
          return SizedBox(
              height: Get.height * 0.6,
              width: Get.width * 0.6,
              child: const Center(
                  child: Text(
                'Struktur organisasi buku masih kosong,',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: ColorName.purplelow),
              )));
        }
        return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data!.docs.length,
            itemBuilder: (context, index) {
              var data =
                  snapshot.data!.docs[index].data() as Map<String, dynamic>;
              String posisi = data['positionlevel'].toString();
              return ListTile(
                leading: SizedBox(
                  height: 70,
                  width: 60,
                  child: Card(
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: data['avatar'] != ''
                          ? Image.network(data['avatar'])
                          : Assets.image.logoDigitalAlumni
                              .image(fit: BoxFit.fitWidth, height: 35),
                    ),
                  ),
                ),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    data['positionName'] != null
                        ? Text(
                            data['positionName'],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: ColorName.purplelow,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          )
                        : Container(),
                    Text(
                      data['username'],
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          color: Colors.black54,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                subtitle: GestureDetector(
                  onTap: () {},
                  child: Row(
                    children: [
                      data['positionlevel'] == null
                          ? Container()
                          : Text(
                              '${posisi.toUpperCase()} - ',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                  color: ColorName.blueprimary,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                      Text(
                        data['memberlevel'],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                // trailing: data['positionlevel'] == null
                //     ? Container(
                //         width: 10,
                //       )
                //     : GestureDetector(
                //         onTap: () {
                //           fireStoreBookServices.deletepositionmember(
                //               widget.bookModel.id, data['bookmember']);
                //         },
                //         child: const Icon(FontAwesomeIcons.arrowDownWideShort)),
              );
            });
      },
    );
  }

  String? value;
  String? message;

  showAlertDialogEnterBookPin(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
        child: const Text("Ok"),
        onPressed: () {
          final BoxStorage boxStorage = BoxStorage();
          if (widget.bookModel.pin == value) {
            String localuid = boxStorage.getUserId();
            String username = boxStorage.getUserName();
            String avatar = boxStorage.getUserAvatar();
            final time = DateFormat('hh:mm');
            final date = DateFormat('dd-MM-yyyy');
            db.collection('book').doc(widget.bookModel.id).update({
              'bookmember': FieldValue.arrayUnion([localuid]),
              'totalmember': FieldValue.increment(1)
            });
            db
                .collection('book')
                .doc(widget.bookModel.id)
                .collection('bookmember')
                .doc(localuid)
                .set({
              'bookmember': localuid,
              'date': date.format(DateTime.now()),
              'time': time.format(DateTime.now()),
              'username': username,
              'avatar': avatar,
              'uid': localuid,
              'memberlevel': 'anggota',
            });
            Get.snackbar('Informasi', "Selamat anda berhasil bergabung");

            Get.to(() => BookGroupAdministratorDetailView(
                  id: widget.bookModel.id,
                  doc: widget.bookModel,
                ));
          } else {
            Get.snackbar('Informasi', "Pin anda salah");
          }
          Navigator.pop(context);
        });

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Masukan pin"),
      content: SizedBox(
        width: MediaQuery.of(context).size.width * 0.9,
        child: TextField(
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            // Used a ternary operator to check if isUpdate is true then display
            // Update Todo.
            labelText: "masukan pin anda",
            hintText: 'Enter An PIN',
          ),
          onChanged: (String val) {
            // Storing the value of the text entered in the variable value.
            value = val;
          },
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialogRequestJoinBook(BuildContext context) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = TextButton(
        child: const Text("Ok"),
        onPressed: () {
          final BoxStorage boxStorage = BoxStorage();

          String localuid = boxStorage.getUserId();
          String username = boxStorage.getUserName();
          String avatar = boxStorage.getUserAvatar();
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');
          db.collection('book').doc(widget.bookModel.id).update({
            'bookmemberrequest': FieldValue.arrayUnion([localuid]),
            //'totalmember': FieldValue.increment(1)
          });
          db
              .collection('book')
              .doc(widget.bookModel.id)
              .collection('bookmemberrequest')
              .doc(localuid)
              .set({
            'bookmember': localuid,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
            'username': username,
            'avatar': avatar,
            'uid': localuid,
            'note': message,
            'memberlevel': 'anggota',
          });
          Get.snackbar('Informasi', "Sukses mengirimkan permohonan");

          // Get.to(() => BookGroupAdministratorDetailView(
          //       id: widget.bookModel.id,
          //       doc: widget.bookModel,
          //     ));

          // Navigator.of(context, rootNavigator: true).pop();
        });

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Masukan Pesan"),
      content: SizedBox(
        width: MediaQuery.of(context).size.width * 0.9,
        child: TextField(
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            // Used a ternary operator to check if isUpdate is true then display
            // Update Todo.
            labelText: "masukan pesan anda",
            hintText: 'Masukan sebuah Pesan',
          ),
          onChanged: (String val) {
            // Storing the value of the text entered in the variable value.
            value = val;
          },
        ),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class ConfirmData extends StatefulWidget {
  const ConfirmData(
      {super.key, required this.category, required this.bookModel});
  final String category;
  final BookModel bookModel;

  @override
  State<ConfirmData> createState() => _ConfirmDataState();
}

class _ConfirmDataState extends State<ConfirmData> {
  String? value;
  String? message;
  final BookGroupController bookGroupController = BookGroupController();

  Widget cancelButton() {
    return TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Widget btnjoinmemberbook() {
    return TextButton(
        child: const Text("Masukan pin"),
        onPressed: () async {
          final BoxStorage boxStorage = BoxStorage();
          if (widget.bookModel.pin == value) {
            String localuid = boxStorage.getUserId();
            String username = boxStorage.getUserName();
            String avatar = boxStorage.getUserAvatar();
            final time = DateFormat('hh:mm');
            final date = DateFormat('dd-MM-yyyy');

            db.collection('book').doc(widget.bookModel.id).update({
              'bookmember': FieldValue.arrayUnion([localuid]),
              'bookmemberrequest': FieldValue.arrayRemove([localuid]),
              'totalmember': FieldValue.increment(1)
            });
            db
                .collection('book')
                .doc(widget.bookModel.id)
                .collection('bookmember')
                .doc(localuid)
                .set({
              'bookmember': localuid,
              'date': date.format(DateTime.now()),
              'time': time.format(DateTime.now()),
              'username': username,
              'avatar': avatar,
              'uid': localuid,
              'memberlevel': 'anggota',
            });
            db
                .collection('book')
                .doc(widget.bookModel.id)
                .collection('bookmemberrequest')
                .doc(localuid)
                .delete();
            Get.snackbar('Informasi', "Selamat anda berhasil bergabung");
            if (widget.bookModel.bookadmin.isEmpty) {
              db.collection('book').doc(widget.bookModel.id).update({
                'bookadmin': FieldValue.arrayUnion([localuid]),
              });
              Navigator.pop(context);
Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BookGroupInfo(
                          uid: widget.bookModel.id,
                          documentSnapshot: widget.bookModel,
                          isadmin: true)));
             
            
                
            } else {
               Navigator.pop(context);
Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BookGroupInfo(
                          uid: widget.bookModel.id,
                          documentSnapshot: widget.bookModel,
                          isadmin: false)));
            }
          } else {
            Get.snackbar('Informasi', "Pin anda salah");
          }
        });
  }

  Widget btnRequestJoinbook() {
    return TextButton(
        child: const Text("Kirim Permintaan"),
        onPressed: () async {
          final BoxStorage boxStorage = BoxStorage();

          String localuid = boxStorage.getUserId();
          String username = boxStorage.getUserName();
          String avatar = boxStorage.getUserAvatar();
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');
          db.collection('book').doc(widget.bookModel.id).update({
            'bookmemberrequest': FieldValue.arrayUnion([localuid]),
            //'totalmember': FieldValue.increment(1)
          });
          db
              .collection('book')
              .doc(widget.bookModel.id)
              .collection('bookmemberrequest')
              .doc(localuid)
              .set({
            'bookmember': localuid,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
            'username': username,
            'avatar': avatar,
            'uid': localuid,
            'note': value,
            'memberlevel': 'anggota',
          });
          Get.snackbar('Informasi', "Sukses mengirimkan permohonan");
          Get.to(const DacBottomNavbar());

          // Navigator.of(context, rootNavigator: true).pop();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText: widget.category == 'requestjoin'
                    ? "masukan pesan anda"
                    : "masukan pin",
                hintText: widget.category == 'requestjoin'
                    ? "masukan sebuah pesan"
                    : "masukan pin",
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                value = val;
              },
            ),
            widget.category == 'requestjoin'
                ? btnRequestJoinbook()
                : btnjoinmemberbook()
          ],
        ),
      ),
    );
  }
}
