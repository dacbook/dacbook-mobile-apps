import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/remote/firestore/firestore_notification_services.dart';
import 'package:dac_apps/data/remote/firestore/firestore_user_services.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../data/model/user/user_model.dart';
import '../../data/remote/fcm/sending_fcm.dart';

final db = FirebaseFirestore.instance;
String? value;

class BookRequestView extends StatefulWidget {
  const BookRequestView({super.key, required this.bookid});
  final String bookid;
  @override
  State<BookRequestView> createState() => _BookRequestViewState();
}

class _BookRequestViewState extends State<BookRequestView> {
  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final FireStoreNotificationServices _fireStoreNotificationServices =
      FireStoreNotificationServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary),
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('book')
            .doc(widget.bookid)
            .collection('bookmemberrequest')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data?.docs.length,
            itemBuilder: (context, int index) {
              DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
              return ListTile(
                leading: Image.network(documentSnapshot['avatar']),
                title: Text(documentSnapshot['username']),
                subtitle: Row(
                  children: [
                    const Text('note : '),
                    Text(documentSnapshot['note'] ?? ''),
                  ],
                ),
                onTap: () async {
                  // Here We Will Add The Update Feature and passed the value 'true' to the is update
                  // feature.

                  Map<String, String> data = {
                    'avatar': documentSnapshot['avatar'],
                    'bookmember': documentSnapshot['bookmember'],
                    'date': documentSnapshot['date'],
                    'memberlevel': documentSnapshot['memberlevel'],
                    'note': documentSnapshot['note'] ?? '',
                    'time': documentSnapshot['time'],
                    'uid': documentSnapshot['uid'],
                    'username': documentSnapshot['username']
                  };
                  final collbook = db.collection('book');
                  final collbookmember = db
                      .collection('book')
                      .doc(widget.bookid)
                      .collection('bookmember');
                  final collrequestbookmember = db
                      .collection('book')
                      .doc(widget.bookid)
                      .collection('bookmemberrequest');

                  DocumentReference docrefbook = collbook.doc(widget.bookid);
                  DocumentReference docrefbookmember =
                      collbookmember.doc(documentSnapshot['uid']);
                  DocumentReference docrefbookrequestmember =
                      collrequestbookmember.doc(documentSnapshot['uid']);
                  final time = DateFormat('hh:mm');
                  final date = DateFormat('dd-MM-yyyy');

                  Get.dialog(
                      //  barrierColor: ColorName.redprimary,

                      AlertDialog(
                          content: Text(
                            'apakah kamu akan setuju ${documentSnapshot['username']} bergabung kedalam buku ?',
                            style: const TextStyle(fontSize: 20),
                          ),
                          actions: [
                        TextButton(
                            onPressed: () async {
                              String reply = 'kamu tidak diterima';
                              await sendingNotification(
                                  reply,
                                  documentSnapshot['username'],
                                  documentSnapshot['uid']);
                              docrefbookrequestmember.delete();
                              docrefbook.update({
                                'bookmemberrequest': FieldValue.arrayRemove(
                                    [documentSnapshot['uid']])
                              });
                              docrefbookmember.set({
                                'bookmember': documentSnapshot['uid'],
                                'date': date.format(DateTime.now()),
                                'time': time.format(DateTime.now()),
                                'username': documentSnapshot['username'],
                                'avatar': documentSnapshot['avatar'],
                                'uid': documentSnapshot['uid'],
                                'memberlevel': 'anggota',
                              });
                              Navigator.pop(context);
                            },
                            child: const Text('Reject')),
                        TextButton(
                            onPressed: () async {
                              String reply = 'selamat bergabung..';
                              await sendingNotification(
                                  reply,
                                  documentSnapshot['username'],
                                  documentSnapshot['uid']);
                              docrefbookmember.set(data);
                              docrefbookrequestmember.delete();
                              docrefbook.update({
                                'bookmember': FieldValue.arrayUnion(
                                    [documentSnapshot['uid']]),
                                'bookmemberrequest': FieldValue.arrayRemove(
                                    [documentSnapshot['uid']]),
                                'totalmember': FieldValue.increment(1)
                              });
                              Navigator.pop(context);
                            },
                            child: const Text('Approve'))
                      ]));
                },
                // trailing: IconButton(
                //   icon: const Icon(
                //     Icons.check,
                //   ),
                //   onPressed: () {
                //     // Here We Will Add The Delete Feature
                //      Map<String, String> data = {
                //     'avatar': documentSnapshot['avatar'],
                //     'bookmember': documentSnapshot['bookmember'],
                //     'date': documentSnapshot['date'],
                //     'memberlevel': documentSnapshot['memberlevel'],
                //     'note': documentSnapshot['note'] ?? '',
                //     'time': documentSnapshot['time'],
                //     'uid': documentSnapshot['uid'],
                //     'username': documentSnapshot['username']
                //   };

                //   final collbookmember = db
                //       .collection('book')
                //       .doc(widget.bookid)
                //       .collection('bookmember');
                //   final collbook = db
                //       .collection('book');
                //   final collrequestbookmember = db
                //       .collection('book')
                //       .doc(widget.bookid)
                //       .collection('bookmemberrequest');
                //   DocumentReference docrefbook =
                //       collbook.doc(widget.bookid);
                //   DocumentReference docrefbookmember =
                //       collbookmember.doc(documentSnapshot['uid']);
                //   DocumentReference docrefbookrequestmember =
                //       collrequestbookmember.doc(documentSnapshot['uid']);

                //   Get.dialog(
                //       //  barrierColor: ColorName.redprimary,

                //       AlertDialog(
                //           content: Text(
                //             'apakah kamu akan setuju ${documentSnapshot['username']} bergabung kedalam buku ?',
                //             style: const TextStyle(fontSize: 20),
                //           ),
                //           actions: [
                //         TextButton(
                //             onPressed: () {
                //               // docrefbookrequestmember.delete();
                //               // docrefbook.update({
                //               //   'bookmemberrequest' : FieldValue.arrayRemove([documentSnapshot['uid']])
                //               // });
                //               // Navigator.pop(context);
                //             },
                //             child: const Text('Reject')),
                //         TextButton(
                //             onPressed: () {
                //             //   // docrefbookmember.set(data);
                //             // //  docrefbookmember.collection(collectionPath).add(data);
                //             //   docrefbookrequestmember.delete();
                //             //    docrefbook.update({
                //             //     'bookmember': FieldValue.arrayUnion([documentSnapshot['uid']]),
                //             //     'bookmemberrequest' : FieldValue.arrayRemove([documentSnapshot['uid']])
                //             //   });
                //             //   Navigator.pop(context);
                //             },
                //             child: const Text('Approve'))
                //       ]));
                //   },
                // ),
              );
            },
          );
        },
      ),
    );
  }

  sendingNotification(String reply, String username, String uid) async {
    String bodydata = reply;
    String titledata = 'halo, $username';
    UserModel usermodel = await _fireStoreUserServices.loginCurrentUser(uid);
    debugPrint('fcm token : ${usermodel.fcmtoken}');
     sendingFcm(fcmtoken: usermodel.fcmtoken, bodydata: bodydata,titledata: titledata, feedid:  '', bookid: '', notificationCategory: 'followers');
    _fireStoreNotificationServices.addNotification(
      userid: usermodel.uid,
      body: 'Buku, $bodydata',
      title: titledata,
    );
  }
}
