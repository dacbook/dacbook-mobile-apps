import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import '_book_sub_view.dart';
import '_book_view.dart';

class BookSubView extends StatefulWidget {
  const BookSubView({super.key, required this.bookid});
  final String bookid;

  @override
  State<BookSubView> createState() => _BookSubViewState();
}

class _BookSubViewState extends State<BookSubView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary),
      body: SafeArea(child: ListView(children:  [
        Padding(
          padding:const EdgeInsets.only(top: 20.0),
          child: BookSubUserView(usertype: 'admin', bookid: widget.bookid,),
        )
      ],)),
    );
  }
}