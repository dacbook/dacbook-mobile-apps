import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../bookgroup/bookgroup_component/_bookgroup_info_view.dart';
import 'book_feeder/add_book_feeder_view.dart';
import 'book_feeder/book_feeder_view.dart';

class BookGroupUserDetailView extends StatelessWidget {
  const BookGroupUserDetailView(
      {super.key, required this.id, required this.doc,});
  final String id;
  final DocumentSnapshot<Object?> doc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          // automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox.shrink(),
                  Assets.image.logoDigitalAlumni
                      .image(fit: BoxFit.fitWidth, height: 50),
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: ListView(children: [
         Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: (() => Get.to(
                  () => AddBookFeederView(doc: doc))),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.book_sharp,
                      color: ColorName.purplelow,
                    ),
                    Text(
                      'Tambah Kegiatan',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
       // BookGroupInfo(documentSnapshot: doc),
        const BookFeederView(),
      // const BookUserView(usertype: 'admin'),
      ]),
    );
  }
}
