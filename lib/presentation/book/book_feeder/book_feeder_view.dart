import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
String? value;

class BookFeederView extends StatelessWidget {
  const BookFeederView({super.key});

  @override
  Widget build(BuildContext context) {
    int _current = 0;
    // final CarouselController _controller = CarouselController();
    // final LoginController loginController = Get.put(LoginController());
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db.collection('bookfeed').snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
         if (snapshot.data.docs.length == 0 ||
                      snapshot.data.docs.length == null) {
                    return SizedBox(
                        height: Get.height * 0.6,
                        width: Get.width * 0.6,
                        child: const Center(
                            child: Text(
                          'Tidak ada kegiatan pada buku ini',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: ColorName.purplelow),
                        )));
                  }

        return ListView.builder(
          // itemExtent: snapshot.data?.docs.length,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: snapshot.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
            //listItem = documentSnapshot['media'];

            debugPrint(documentSnapshot['mediaFeed'].toString());
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 15,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    SizedBox(
                      height: 40,
                      width: 40,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: CachedNetworkImage(
                          imageUrl: documentSnapshot['userimage'],
                          fit: BoxFit.fitWidth,
                          width: Get.width * 0.94,
                          placeholder: (context, url) {
                            return Image.asset(
                              'assets/image/example_album1.jpeg',
                              fit: BoxFit.cover,
                            );
                          },
                          errorWidget: (context, url, error) {
                            return Image.asset(
                              'assets/image/example_album1.jpeg',
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: [ Text(
                      documentSnapshot['username'],
                      style: const TextStyle(
                        fontSize: 18,
                      ),
                  ),
                 ],),
                    )
                   ],),
                 const  SizedBox(height: 5,),
                    Row(
                      children: [
                        Text(
                          documentSnapshot['category'],
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          documentSnapshot['jenisKegiatan'],
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                  ),
                  Text(
                    'Dibagikan pada ${documentSnapshot['date']} ',
                    style: const TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                      height: 250,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: CachedNetworkImage(
                          imageUrl: documentSnapshot['mediaFeed'],
                          fit: BoxFit.fitWidth,
                          width: Get.width * 0.94,
                          placeholder: (context, url) {
                            return Image.asset(
                              'assets/image/example_album1.jpeg',
                              fit: BoxFit.cover,
                            );
                          },
                          errorWidget: (context, url, error) {
                            return Image.asset(
                              'assets/image/example_album1.jpeg',
                              fit: BoxFit.cover,
                            );
                          },
                        ),
                      )),
                  const SizedBox(height: 5),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: const [
                            FaIcon(FontAwesomeIcons.heart,
                                color: ColorName.purplelow),
                            SizedBox(
                              width: 20,
                            ),
                            FaIcon(FontAwesomeIcons.comment,
                                color: ColorName.purplelow),
                          ],
                        ),
                        const FaIcon(FontAwesomeIcons.bookmark,
                            color: ColorName.purplelow),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    documentSnapshot['description'],
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}

class Photos {
  String key;
  String url;

  Photos(
    this.key,
    this.url,
  );

  @override
  String toString() {
    return '{ $key - $url}';
  }
}
