import 'dart:io';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../../gen/colors.gen.dart';
import '../../login/login_controller.dart';
import '../../profile_medsos/profile_medsos_view.dart';

final db = FirebaseFirestore.instance;
String? value;

class AddBookFeederView extends StatefulWidget {
  const AddBookFeederView({super.key, required this.doc});
  final DocumentSnapshot<Object?> doc;
  @override
  State<AddBookFeederView> createState() => _AddBookFeederViewState();
}

class _AddBookFeederViewState extends State<AddBookFeederView> {
  var txtControllerTitle = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerCategory = TextEditingController();
  var txtControllerJenisKegiatan = TextEditingController();

  String? bookname;

  uploadImagetFirebase() async {
    File file = File(imagePicked!.path);
    String? fileName = file.path.split('/').last;
    bookname = widget.doc['bookname'];
    await FirebaseStorage.instance
        .ref()
        .child('/bookgroup/')
        .child(bookname!)
        .child(fileName)
        .putFile(File(file.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${file.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child('/bookgroup/')
            .child(bookname!)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final f = DateFormat('hh:mm dd-MM-yyyy');

          Map<String, dynamic> data = {
            'author': loginController.uid.value,
            'bookid': widget.doc['id'],
            'bookname': widget.doc['bookname'],
            'category': txtControllerCategory.text,
            'jenisKegiatan': txtControllerJenisKegiatan.text,
            'username': loginController.username.value,
            'userimage': loginController.photoUrl.value,
            'description': txtControllerDescription.text,
            'mediaFeed': url,
            'favorites': 0,
            'date': f.format(DateTime.now()),
          };
          db.collection('bookfeed').add(data);
          return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
      Navigator.pop(context);
    });
  }

  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kegiatan'),
        centerTitle: true,
        backgroundColor: ColorName.purplelow,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 250,
                    width: 250,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Stack(
                        children: [
                          Container(
                            color: Colors.black.withOpacity(0.2),
                            child: image != null
                                ? SizedBox(
                                    height: 250,
                                    width: 250,
                                    child: Image.file(
                                      image!,
                                      fit: BoxFit.fitWidth,
                                    ),
                                  )
                                : SizedBox(
                                    height: 250,
                                    width: 250,
                                    child: Center(child: Container()),
                                  ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  getImage();
                                });
                              },
                              child: Container(
                                width: double.infinity,
                                padding:
                                    const EdgeInsets.symmetric(vertical: 5),
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.3),
                                ),
                                child: Icon(
                                  Icons.file_copy,
                                  color: Colors.white.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  _dropdownJenisKegiatan(),
                  _dropdownCategory(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerTitle,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerTitle,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "nama agenda cannot be empty";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Tambahkan Nama Agenda',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerDescription,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerDescription,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "description cannot be empty";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'tambahkan keterangan agenda',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
              child: ElevatedButton(
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(ColorName.purplelow),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: const BorderSide(
                                  color: ColorName.purplelow)))),
                  onPressed: () async {
                    uploadImagetFirebase();
                    //controller.getAuth(ischeckedValue);
                    //  controller.checkPin();
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Kirim Cerita".toUpperCase(),
                            style: const TextStyle(fontSize: 14)),
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  File? image;
  XFile? imagePicked;
  String? imageNamed;

  Future getImage() async {
    final ImagePicker picker = ImagePicker();
    imagePicked = await ImagePicker().pickImage(source: ImageSource.gallery);
    image = File(imagePicked!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    final kb = bytes / 1024;
    final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      if (ukuranPhoto <= 1.01) {
        setState(() {
          image = File(imagePicked!.path);
          imageNamed = fileName;
          // getting a directory path for saving
          final String path = image.toString();
        });
      } else {
        setState(() {
          imageNamed = 'Ukuran photo belum sesuai';
          image = null;
          Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
              snackPosition: SnackPosition.BOTTOM,
              backgroundColor: Colors.redAccent);
        });
      }
    } else {
      setState(() {
        image = null;
        imageNamed = 'Jenis photo belum sesuai';
        Get.snackbar('Gagal Upload', 'Jenis photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
  }

  final List<String> listCategory = [
    'kompetisi',
    'event',
    'program',
    'agenda',
  ];
  final List<String> listJenisKegiatan = [
    'event',
    'program',
    'agenda',
  ];

  Widget _dropdownCategory() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            'Ketegori',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'Kategori',
            items: listCategory,
            controller: txtControllerCategory,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                txtControllerCategory.text = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _dropdownJenisKegiatan() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            'Jenis Kegiatan',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'Jenis Kegiatan',
            items: listJenisKegiatan,
            controller: txtControllerJenisKegiatan,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                txtControllerJenisKegiatan.text = value;
              });
            },
          ),
        ),
      ],
    );
  }
}
