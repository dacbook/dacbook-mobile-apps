import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../data/model/book_position_rank/book_position_rank_model.dart';
import '../../../data/remote/firestore/firestore_book_services.dart';
import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '../../login/login_controller.dart';

final db = FirebaseFirestore.instance;

class BookOrganizationUserView extends StatefulWidget {
  const BookOrganizationUserView(
      {super.key, required this.usertype, required this.bookid, required this.bookmasterid});
  final bool usertype;
  final String bookid;
  final String bookmasterid;

  @override
  State<BookOrganizationUserView> createState() =>
      _BookOrganizationUserViewState();
}

class _BookOrganizationUserViewState extends State<BookOrganizationUserView>
    with SingleTickerProviderStateMixin {
  String name = "";
  bool isPositionRank = true;
  TabController? tabController;

  final _firestorebookservices = FireStoreBookServices();

  @override
  // ignore: unused_element
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  // ignore: unused_element
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }
  // @override
  // // ignore: unused_element
  // void initState() {
  //   var result = _firestorebookservices.getPositionRank(widget.bookid);
  //   debugPrint('res : $result');

  //   super.initState();
  // }

  // loadposition() async {
  //   if (bookPositionRank == []) {}
  // }

  @override
  Widget build(BuildContext context) {
    final LoginController loginController = Get.put(LoginController());

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(14.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 45,
                width: Get.width * 0.8,
                child: Card(
                  child: TextField(
                    decoration: const InputDecoration(
                        prefixIcon: Icon(Icons.search), hintText: 'Search...'),
                    onChanged: (val) {
                      setState(() {
                        name = val;
                      });
                    },
                  ),
                ),
              ),
            widget.usertype == true ?  GestureDetector(
                  onTap: () {
                    if (isPositionRank == false) {
                      setState(() {
                        isPositionRank = true;
                      });
                    } else {
                      setState(() {
                        isPositionRank = false;
                      });
                    }
                  },
                  child: const Icon(Icons.settings, size: 40,)) : Container()
            ],
          ),
        ),
        isPositionRank == false ?  Container(
          color: ColorName.redprimary,
          width: Get.width * 0.5,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                      context: context,
                      builder: (context) {
                        return showBottomSheet(context, false, null);
                      },
                    );
                  },
                  child: Row(
                    children: const [
                      Icon(
                        Icons.add_outlined,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Tambahkan posisi',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  )),
            ),
          ),
        ) : Container(),
       isPositionRank == false ? DefaultTabController(
          length: 2,
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: TabBar(
                  controller: tabController,
                  labelColor: ColorName.redprimary,
                  unselectedLabelColor: Colors.black,
                  indicatorColor: ColorName.redprimary,
                  indicatorPadding: const EdgeInsets.symmetric(horizontal: 10),
                  labelStyle: const TextStyle(
                    fontSize: 14,
                    fontFamily: 'Roboto',
                  ),
                  tabs: const [
                    Tab(
                      child: Text('Posisi Organisasi'),
                    ),
                    Tab(
                      child: Text("Alumni"),
                    ),
                   
                  ],
                ),
              ),
              SizedBox(
                height: Get.height,
                child: TabBarView(
                  controller: tabController,
                  children: [
                    positionRankOrganization(),
                    StreamBuilder<QuerySnapshot>(
                      stream: db
                          .collection('book')
                          .doc(widget.bookid)
                          .collection('bookmember')
                          .snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        if (snapshot.data.docs.length == 0 ||
                            snapshot.data.docs.length == null) {
                          return SizedBox(
                              height: Get.height * 0.6,
                              width: Get.width * 0.6,
                              child: const Center(
                                  child: Text(
                                'Struktur organisasi buku masih kosong, \n\n tambahkan anggota terlebih dahulu',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: ColorName.purplelow),
                              )));
                        }
                        return ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              var data = snapshot.data!.docs[index].data()
                                  as Map<String, dynamic>;
                              String posisi = data['positionlevel'].toString();
                              if (name.isEmpty) {
                                return ListTile(
                                  leading: SizedBox(
                                    height: 70,
                                    width: 60,
                                    child: Card(
                                      elevation: 5,
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: data['avatar'] != ''
                                            ? Image.network(data['avatar'])
                                            : Assets.image.logoDigitalAlumni
                                                .image(
                                                    fit: BoxFit.fitWidth,
                                                    height: 35),
                                      ),
                                    ),
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      data['positionName'] != null
                                          ? Text(
                                              data['positionName'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: ColorName.purplelow,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          : Container(),
                                      Text(
                                        data['username'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  subtitle: GestureDetector(
                                    onTap: () {
                                      debugPrint(
                                          'posisi data : $bookPositionRank');
                                      changePositionUser(context, data);
                                    },
                                    child: Row(
                                      children: [
                                        data['positionlevel'] == null
                                            ? Container()
                                            : Text(
                                                '${posisi.toUpperCase()} - ',
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    color:
                                                        ColorName.blueprimary,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                        Text(
                                          data['memberlevel'],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                  trailing: data['positionlevel'] == null
                                      ? Container(
                                          width: 10,
                                        )
                                      : GestureDetector(
                                          onTap: () {
                                            debugPrint(
                                                'posisi data : $bookPositionRank');
                                            _firestorebookservices
                                                .deletepositionmember(
                                                    widget.bookid,
                                                    data['bookmember']);
                                          },
                                          child: const Icon(FontAwesomeIcons
                                              .arrowDownWideShort)),
                                );
                              }
                              if (data['username']
                                      .toString()
                                      .toLowerCase()
                                      .startsWith(name.toLowerCase()) ||
                                  data['memberlevel']
                                      .toString()
                                      .toLowerCase()
                                      .startsWith(name.toLowerCase())) {
                                return ListTile(
                                  title: Text(
                                    data['username'],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Row(
                                    children: [
                                      data['positionlevel'] == null
                                          ? Container()
                                          : Text(
                                              data['positionlevel'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                      Text(
                                        data['memberlevel'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              return Container();
                            });
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ): StreamBuilder<QuerySnapshot>(
                      stream: db
                          .collection('book')
                          .doc(widget.bookid)
                          .collection('bookmember').where('positionlevel', isNull: false)
                          .snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        if (snapshot.data.docs.length == 0 ||
                            snapshot.data.docs.length == null) {
                          return SizedBox(
                              height: Get.height * 0.6,
                              width: Get.width * 0.6,
                              child: const Center(
                                  child: Text(
                                'Struktur organisasi buku masih kosong, \n\n tambahkan anggota terlebih dahulu',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: ColorName.purplelow),
                              )));
                        }
                        return ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              var data = snapshot.data!.docs[index].data()
                                  as Map<String, dynamic>;
                              String posisi = data['positionlevel'].toString();
                              if (name.isEmpty) {
                                return ListTile(
                                  leading: SizedBox(
                                    height: 70,
                                    width: 60,
                                    child: Card(
                                      elevation: 5,
                                      child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: data['avatar'] != ''
                                            ? Image.network(data['avatar'])
                                            : Assets.image.logoDigitalAlumni
                                                .image(
                                                    fit: BoxFit.fitWidth,
                                                    height: 35),
                                      ),
                                    ),
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      data['positionName'] != null
                                          ? Text(
                                              data['positionName'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: ColorName.purplelow,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          : Container(),
                                      Text(
                                        data['username'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  subtitle: GestureDetector(
                                    onTap: () {
                                      debugPrint(
                                          'posisi data : $bookPositionRank');
                                    //  changePositionUser(context, data);
                                    },
                                    child: Row(
                                      children: [
                                        data['positionlevel'] == null
                                            ? Container()
                                            : Text(
                                                '${posisi.toUpperCase()} - ',
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    color:
                                                        ColorName.blueprimary,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                        Text(
                                          data['memberlevel'],
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                  trailing:Container(
                                          width: 10,
                                        )
                                     
                                );
                              }
                              if (data['username']
                                      .toString()
                                      .toLowerCase()
                                      .startsWith(name.toLowerCase()) ||
                                  data['memberlevel']
                                      .toString()
                                      .toLowerCase()
                                      .startsWith(name.toLowerCase())) {
                                return ListTile(
                                  title: Text(
                                    data['username'],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  subtitle: Row(
                                    children: [
                                      data['positionlevel'] == null
                                          ? Container()
                                          : Text(
                                              data['positionlevel'],
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                      Text(
                                        data['memberlevel'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                            color: Colors.black54,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              return Container();
                            });
                      },
                    ) ,
      ],
    );
  }

  int positioncount = 0;
  List<BookPositionRank> bookPositionRank = [];

  Widget positionRankOrganization() {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: _firestorebookservices.getPositionRank(widget.bookid),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //  debugPrint('data snap : ${snapshot.data}');
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (snapshot.data.length == 0 || snapshot.data == null) {
          return SizedBox(
              height: Get.height * 0.6,
              width: Get.width * 0.6,
              child: const Center(
                  child: Text(
                'Posisi organisasi buku masih kosong \n\n',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: ColorName.purplelow),
              )));
        } else {
          return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data?.length,
            itemBuilder: (context, int index) {
              BookPositionRank positionRank = snapshot.data[index];
              bookPositionRank = snapshot.data;
              return ListTile(
                leading: Text('Lv. ${positionRank.positionLevel}'),
                title: Text(positionRank.positionName),
                onTap: () {
                  // Here We Will Add The Update Feature and passed the value 'true' to the is update
                  // feature.

                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return showBottomSheet(context, true, positionRank);
                    },
                  );
                },
                trailing: IconButton(
                  icon: const Icon(
                    Icons.delete_outline,
                  ),
                  onPressed: () {
                    // Here We Will Add The Delete Feature
                    db
                        .collection('book')
                        .doc(widget.bookid)
                        .collection('positionrank')
                        .doc(positionRank.id)
                        .delete();
                  },
                ),
              );
            },
          );
        }
      },
    );
  }

  String? positionName;
  String? positionLevel;
  showBottomSheet(
      BuildContext context, bool isUpdate, BookPositionRank? documentSnapshot) {
    // Added the isUpdate argument to check if our item has been updated
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter mystate) {
        return Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SizedBox(
            // height: Get.height * 0.3,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                // SizedBox(
                //   height: 30,
                //   child: Text('${documentSnapshot!['positionName']} Menjadi $positionName'),
                // ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: TextField(
                    keyboardType: TextInputType.number,
                    maxLength: 1,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      // Used a ternary operator to check if isUpdate is true then display
                      // Update Todo.
                      labelText: isUpdate ? 'Update level' : 'Tambah level',
                      hintText: 'Masukan level'
                           
                    ),
                    onChanged: (String val) {
                      // Storing the value of the text entered in the variable value.

                      positionLevel = val;
                    },
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                // SizedBox(
                //   height: 30,
                //   child: Text('${documentSnapshot['positionLevel']} Menjadi $positionLevel'),
                // ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: TextField(
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      // Used a ternary operator to check if isUpdate is true then display
                      // Update Todo.
                      labelText: isUpdate ? 'Update posisi' : 'Add posisi',
                      hintText: 'tambahkan posisi',
                    ),
                    onChanged: (String val) {
                      // Storing the value of the text entered in the variable value.
                      positionName = val;
                    },
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(ColorName.purplelow),
                    ),
                    onPressed: () {
                      // Check to see if isUpdate is true then update the value else add the value
                      if (isUpdate) {
                        _firestorebookservices.createPositionRank(
                            positionLevel: positionLevel == null
                                ? documentSnapshot!.positionLevel
                                : positionLevel!,
                            positionName: positionName == null
                                ? documentSnapshot!.positionName
                                : positionName!,
                            bookid: widget.bookid,
                            id: documentSnapshot!.id,
                            isupdate: isUpdate);
                      } else {
                        _firestorebookservices.createPositionRank(
                            positionLevel: positionLevel!,
                            positionName: positionName!,
                            bookid: widget.bookid,
                             id: widget.bookmasterid,
                            isupdate: isUpdate);
                      }
                      Navigator.pop(context);
                    },
                    child: isUpdate
                        ? const Text(
                            'Edit',
                            style: TextStyle(color: Colors.white),
                          )
                        : const Text('Tambahkan',
                            style: TextStyle(color: Colors.white))),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        );
      }
    );
  }

  String positionSelected = '';

  void changePositionUser(context, Map<String, dynamic> data) {
    showModalBottomSheet(
        //  isScrollControlled: true,
        context: context,
        builder: (context) {
          return StreamBuilder(
              stream: _firestorebookservices.getPositionRank(widget.bookid),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (snapshot.data.length == 0 || snapshot.data == null) {
                  return SizedBox(
                      height: Get.height * 0.6,
                      width: Get.width * 0.6,
                      child: const Center(
                          child: Text(
                        'Posisi organisasi buku masih kosong \n\n',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: ColorName.redprimary),
                      )));
                } else {
                  return Container(
                    padding: const EdgeInsets.all(8),
                    height: 200,
                    //  height: MediaQuery.of(context).viewInsets.bottom,
                    alignment: Alignment.center,
                    child: ListView.builder(
                        itemCount:
                            snapshot.data.isNotEmpty ? snapshot.data.length : 0,
                        itemBuilder: (context, index) {
                          BookPositionRank positionRanks = snapshot.data[index];
                          debugPrint('rank : ${snapshot.data.isNotEmpty}');
                          return SizedBox(
                            height: 50,
                            child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Lv. ${positionRanks.positionLevel} - ${positionRanks.positionName}'
                                          .toUpperCase(),
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    ElevatedButton(
                                        onPressed: () {
                                          _firestorebookservices
                                              .updatepositionmember(
                                                  widget.bookid,
                                                  data['bookmember'],
                                                  positionRanks.positionName, positionRanks.positionLevel);
                                          Navigator.pop(context);
                                        },
                                        child: const Text('Pilih posisi'))
                                  ],
                                ),
                                onTap: () {
                                  setState(() {
                                    positionSelected =
                                        positionRanks.positionName;
                                    debugPrint('posisi : $positionSelected');
                                  });
                                  Navigator.of(context).pop();
                                }),
                          );
                        }),
                  );
                }
              });
        });
  }
}
