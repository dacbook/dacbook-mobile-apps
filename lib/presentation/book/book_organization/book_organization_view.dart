import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:flutter/material.dart';

import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '_book_organization_user_view.dart';

class BookOrganizationView extends StatefulWidget {
  const BookOrganizationView({super.key, required this.bookid, required this.usertype, required this.bookmodel});
  final String bookid;
   final bool usertype;
   final BookModel bookmodel;
  @override
  State<BookOrganizationView> createState() => _BookOrganizationViewState();
}

class _BookOrganizationViewState extends State<BookOrganizationView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0, left: 20, right: 20),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: const Icon(Icons.arrow_back)),
                  Assets.image.logoDacLable
                      .image(fit: BoxFit.fitWidth, height: 50),
              
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: ListView(children:   [
        BookOrganizationUserView(usertype: widget.usertype, bookid: widget.bookid, bookmasterid: widget.bookmodel.bookmasterid, ),
      ]),
    );
  }
}