import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../gen/colors.gen.dart';
import '../bookgroup/bookgroup_component/_bookgroup_info_view.dart';
import '../login/login_controller.dart';
import '../bookgroup/_bookgroup_detail_view.dart';
import '../bookgroup/bookgroup_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';
import '_book_detail_view.dart';

final db = FirebaseFirestore.instance;
final boxStorage = BoxStorage();

class BookUserView extends StatefulWidget {
  const BookUserView({super.key, required this.usertype});
  final String usertype;

  @override
  State<BookUserView> createState() => _BookUserViewState();
}

class _BookUserViewState extends State<BookUserView> {
  String keyword = "";
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  @override
  Widget build(BuildContext context) {
    final LoginController loginController = Get.put(LoginController());
    final BookGroupController bookGroupController =
        Get.put(BookGroupController());

    return Column(
      children: [
        SizedBox(
          height: 45,
          width: Get.width * 0.95,
          child: Card(
            child: TextField(
              decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.search), hintText: 'Search...'),
              onChanged: (val) {
                setState(() {
                  keyword = val;
                });
              },
            ),
          ),
        ),
        StreamBuilder(
          // Reading Items form our Database Using the StreamBuilder widget
          stream: fireStoreBookServices.getBook(),

          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 1.0));
            }
            if (snapshot.connectionState == ConnectionState.none) {
              return const Text('Server not found - Error 500');
            }
            if (snapshot.connectionState == ConnectionState.done) {
              if (!snapshot.hasData || snapshot.data.length == 0) {
                return SizedBox(
                  height: Get.height * 0.4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      WidgetTextMont(
                        'Tidak ada data buku',
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                        textColor: Colors.grey[600]!,
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                );
              }
            }

            return ListView.builder(
                // itemExtent: snapshot.data?.docs.length,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: snapshot.data?.length,
                itemBuilder: (context, int index) {
                  BookModel documentSnapshot = snapshot.data[index];
                  //listItem = documentSnapshot['media'];

                  if (keyword.isEmpty) {
                    return GestureDetector(
                      onTap: () async {
                        bool isadmin = await bookGroupController
                            .checkIsAdminOrNot(documentSnapshot.bookadmin);
                        String uid = boxStorage.getUserId();
                        Get.to(() => BookGroupInfo(
                            uid: uid,
                            documentSnapshot: documentSnapshot,
                            isadmin: isadmin));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 2,
                          vertical: 2,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                            height: 70,
                                            width: 70,
                                            child: Card(
                                              color:
                                                  Colors.white.withOpacity(0.9),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: CachedNetworkImage(
                                                  imageUrl: documentSnapshot
                                                      .bookimage,
                                                  fit: BoxFit.fitWidth,
                                                  width: Get.width * 0.94,
                                                  placeholder: (context, url) {
                                                    return Image.network(
                                                      url,
                                                      fit: BoxFit.cover,
                                                    );
                                                  },
                                                  errorWidget:
                                                      (context, url, error) {
                                                    return const Icon(
                                                        Icons.error_rounded);
                                                  },
                                                ),
                                              ),
                                            )),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              width: 200,
                                              child: Text(
                                                documentSnapshot.bookname,
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Text(
                                              'dibuat pada ${documentSnapshot.date} ',
                                              style: const TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            SizedBox(
                                              width: 200,
                                              child: Text(
                                                overflow: TextOverflow.ellipsis,
                                                documentSnapshot.description,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    const FaIcon(FontAwesomeIcons.angleRight,
                                        color: ColorName.purplelow)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }

                  if (documentSnapshot.bookname
                      .toString()
                      .toLowerCase()
                      .startsWith(keyword.toLowerCase())) {
                    return GestureDetector(
                      onTap: () {
                        Get.to(() => BookGroupAdministratorDetailView(
                              id: documentSnapshot.id,
                              doc: documentSnapshot,
                            ));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 2,
                          vertical: 2,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        SizedBox(
                                            height: 70,
                                            width: 70,
                                            child: Card(
                                              color:
                                                  Colors.white.withOpacity(0.9),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: CachedNetworkImage(
                                                  imageUrl: documentSnapshot
                                                      .bookimage,
                                                  fit: BoxFit.fitWidth,
                                                  width: Get.width * 0.94,
                                                  placeholder: (context, url) {
                                                    return Image.asset(
                                                      'assets/image/example_album1.jpeg',
                                                      fit: BoxFit.cover,
                                                    );
                                                  },
                                                  errorWidget:
                                                      (context, url, error) {
                                                    return Image.asset(
                                                      'assets/image/example_album1.jpeg',
                                                      fit: BoxFit.cover,
                                                    );
                                                  },
                                                ),
                                              ),
                                            )),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              documentSnapshot.bookname,
                                              style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              'dibuat pada ${documentSnapshot.date} ',
                                              style: const TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w300,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              documentSnapshot.description,
                                              style: const TextStyle(
                                                fontSize: 14,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    const FaIcon(FontAwesomeIcons.angleRight,
                                        color: ColorName.purplelow)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }

                  return const SizedBox();
                });
          },
        ),
      ],
    );
  }
}
