import 'package:dac_apps/presentation/login/login_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../data/local/box/box_storage.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import 'login_view.dart';

class RegisterPinView extends StatefulWidget {
   const RegisterPinView({super.key,required this.googleUserCred});
  final User googleUserCred;
  @override
  State<RegisterPinView> createState() => _RegisterPinViewState();
}

class _RegisterPinViewState extends State<RegisterPinView> {
  final LoginController controller = Get.put(LoginController());
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final BoxStorage _boxStorage = BoxStorage();
   final Uri urlweb = Uri.parse('https://ilmci.com/privacy');

   Future<void> launchUrlWeb() async {
      if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urlweb';
      }
    }

    
  var txtEmail = TextEditingController();
  var txtPwd = TextEditingController();
  var txtUsername = TextEditingController();
  var txtPin = TextEditingController();
  var txtPhone = TextEditingController();

    final box = GetStorage();

     @override
  void initState() {
    loginWithGoogle();
    super.initState();

  }

    loginWithGoogle() async {
      User user = widget.googleUserCred;
    box.write('token', user.uid);
      txtUsername.text = user.displayName!;
      txtEmail.text = user.email!;
      txtPwd.text = '123456';
      txtPhone.text = '628';
      //photoUrl.value = user.photoURL!;
      //uid.value = user.uid;
      _boxStorage.setUserId(user.uid);
      _boxStorage.setUserEmail(user.email!);
      _boxStorage.setUserName(user.displayName!);
      _boxStorage.setUserAvatar(user.photoURL!);

      debugPrint(
          'tes gmail : email ${txtEmail.text} - username ${txtUsername.text}');
      
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(10.0),
            child: AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: ColorName.redprimary,
            )),
        body: SafeArea(
          child: ListView(
            children: [
              Assets.image.logoDac.image(fit: BoxFit.fitWidth),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20),
                child: SizedBox(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "DIGITAL",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.blueprimary),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          "ALUMNI",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.redprimary),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Row(
                        children: [
                          const Text(
                            "Halo,",
                            style: TextStyle(
                                fontWeight: FontWeight.w300, fontSize: 11),
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          Text(
                            controller.username.value,
                            style: const TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 11,
                                color: ColorName.redprimary),
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          const Text(
                            "Selesaikan pendaftaran sekarang",
                            style: TextStyle(
                                fontWeight: FontWeight.w300, fontSize: 11),
                          ),
                        ],
                      ),
                    ),
                     Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                            child: Row(
                              children: [
                                const Text(
                                  "Sudah memiliki akun ? ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 14),
                                ),
                                GestureDetector(
                                  onTap: () => Get.to(const LoginView()),
                                  child: const Text(
                                    "Masuk disini ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 14,
                                        color: ColorName.blueprimary),
                                  ),
                                ),
                              ],
                            ),
                          ),
                  ],
                )),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              //   child: TextFormField(
              //     controller: controller.txtControllerpassword.value,
              //     obscureText: controller.isPasswordVisible.value,
              //     keyboardType: TextInputType.text,
              //     onChanged: (value) => controller.txtControllerpassword.value,
              //     onSaved: (String? value) {},
              //     validator: (value) {
              //       if (value!.isEmpty) {
              //         return "password cannot be empty";
              //       }
              //       return null;
              //     },
              //     decoration: InputDecoration(
              //       border: const OutlineInputBorder(),
              //       labelText: 'Daftarkan Password',
              //       suffixIcon: IconButton(
              //         icon: Icon(
              //           controller.isPasswordVisible.value
              //               ? Icons.visibility_off_sharp
              //               : Icons.visibility,
              //           color: Colors.black,
              //         ),
              //         onPressed: () {
              //           controller.hidepassword();
              //         },
              //       ),
              //     ),
              //   ),
              // ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: TextFormField(
                  controller: txtPin,
                  keyboardType: TextInputType.text,
                  onSaved: (String? value) {},
                  onChanged: (value) => txtPin,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "pin cannot be empty";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    // hintText: 'cooper@example.com',
                    labelText: 'Masukan Pin',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorName.redprimary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.purplelow)))),
                    onPressed: () async {
                      //controller.getAuth(ischeckedValue);
                      if(txtPin.text == '' || txtPin.text.isEmpty){
                        Get.snackbar('Informasi', 'pin tidak boleh kosong');
                      }else{
                 
                     controller.checkPin(false, txtEmail.text,  txtPwd.text, txtPhone.text, txtUsername.text, txtPin.text);
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Registrasi Pin Kamu".toUpperCase(),
                              style: const TextStyle(fontSize: 14)),
                        ],
                      ),
                    )),
              ),
               Padding(
                            padding: const EdgeInsets.only(top: 15.0, left: 5, right: 5),
                            child: Row(
                              children: [
                                const Text(
                                  "Dengan mengklik data, anda menyetujui ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 14),
                                ),
                                GestureDetector(
                                  onTap: () => launchUrlWeb,
                                  child: const Text(
                                    "Persyaratan Layanan ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 14,
                                        color: ColorName.blueprimary),
                                  ),
                                ),
                              ],
                            ),
                          ),
            ],
          ),
        )));
  }

  bool ischeckedValue = false;
}
