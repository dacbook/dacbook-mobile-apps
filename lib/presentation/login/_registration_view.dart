import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import 'login_controller.dart';
import 'login_view.dart';

class RegistrationView extends StatefulWidget {
  const RegistrationView({super.key});

  @override
  State<RegistrationView> createState() => _RegistrationViewState();
}

class _RegistrationViewState extends State<RegistrationView> {
  final LoginController controller = Get.put(LoginController());
  bool isloading = false;
  final _formKey = GlobalKey<FormState>();

  final Uri urlweb = Uri.parse('https://ilmci.com/privacy');

   Future<void> launchUrlWeb() async {
      if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
        throw 'Could not launch $urlweb';
      }
    }

  var txtEmail = TextEditingController();
  var txtPwd = TextEditingController();
  var txtUsername = TextEditingController();
  var txtPin = TextEditingController();
  var txtPhone = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(10.0),
            child: AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: ColorName.redprimary,
            )),
        body: SafeArea(
          child: LoadingOverlay(
            isLoading: isloading,
            child: Form(
               autovalidateMode: AutovalidateMode.always,
              key: _formKey,
              child: AutofillGroup(
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Assets.image.logoDac.image(fit: BoxFit.fitWidth),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, left: 20),
                      child: SizedBox(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                "DIGITAL",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 30,
                                    color: ColorName.blueprimary),
                              ),
                              SizedBox(
                                width: 6,
                              ),
                              Text(
                                "ALUMNI",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 30,
                                    color: ColorName.redprimary),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                            child: Row(
                              children: [
                                const Text(
                                  "Sudah memiliki akun ? ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 14),
                                ),
                                GestureDetector(
                                  onTap: () => Get.to(const LoginView()),
                                  child: const Text(
                                    "Masuk disini ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 14,
                                        color: ColorName.blueprimary),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 15.0),
                            child: Text(
                              "Masukan Data Diri Anda",
                              style: TextStyle(
                                  fontWeight: FontWeight.w300, fontSize: 14),
                            ),
                          ),
                        ],
                      )),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtEmail,
                        keyboardType: TextInputType.text,
                        autofillHints: const [AutofillHints.email],
                        textInputAction: TextInputAction.next,
                        onSaved: (String? value) {},
                        onChanged: (value) =>
                            txtEmail,
                        validator: (value) => controller.validateEmail(value),
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'bagus@dac.com',
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtPwd,
                        obscureText: controller.isPasswordVisible.value,
                        keyboardType: TextInputType.text,
                        autofillHints: const [AutofillHints.password],
                        onEditingComplete: () =>
                            TextInput.finishAutofillContext(),
                        onChanged: (value) {
                           value = txtPwd.text ;
                         
                            debugPrint('password length ${value.length} == ${txtPwd.text}');
                        },
                        onSaved: (String? value) {},
                        validator: (value) {
                          if (value == null || value.isEmpty || value.length < 5) {
                            debugPrint('password length ${value!.length.toString()} == ${controller.txtControllerpassword.value.text.length}');
                            return "password belum lengkap atau minimal 6 karakter";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'Password',
                          suffixIcon: IconButton(
                            icon: Icon(
                              controller.isPasswordVisible.value
                                  ? Icons.visibility_off_sharp
                                  : Icons.visibility,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              controller.hidepassword();
                            },
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtUsername,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) =>
                            txtUsername.value,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "username cannot be empty";
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'benydac',
                          labelText: 'Username',
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtPin,
                        keyboardType: TextInputType.text,
                        onSaved: (String? value) {},
                        onChanged: (value) => txtPin.value,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "pin cannot be empty";
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'DAC123456',
                          labelText: 'PIN',
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtPhone,
                        keyboardType: TextInputType.number,
                        onSaved: (String? value) {},
                        onChanged: (value) =>
                            txtPhone.value,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "phone cannot be empty";
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: '6281192911433',
                          labelText: 'Phone',
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  ColorName.blueprimary),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: const BorderSide(
                                          color: ColorName.purplelow)))),
                          onPressed: () {
                            if (isloading == false && _formKey.currentState!.validate() ) {
                               setState(() {
                              isloading = true;
                            });
                              controller.checkPin(false, txtEmail.text,  txtPwd.text, txtPhone.text, txtUsername.text, txtPin.text);
                               setState(() {
                                  isloading = false;
                                });
                           
                            } else {
                              Get.snackbar('Registration Gagal',
                                  'Lengkapi data');
                            }
                            setState(() {
                              isloading = false;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Registrasi Akun DAC".toUpperCase(),
                                    style: const TextStyle(fontSize: 14)),
                              ],
                            ),
                          )),
                    ),
                    Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                            child: Row(
                              children: [
                                const Text(
                                  "Dengan mengklik data, anda menyetujui ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 14),
                                ),
                                GestureDetector(
                                  onTap: () => launchUrlWeb,
                                  child: const Text(
                                    "Persyaratan Layanan ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 14,
                                        color: ColorName.blueprimary),
                                  ),
                                ),
                              ],
                            ),
                          ),
                    const SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ),
            ),
          ),
        )));
  }
}
