import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/login_user/login_user_model.dart';
import 'package:dac_apps/presentation/splashscreen/splashscreen_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../app/constant/baseurl.dart';
import '../../data/local/box/box_storage.dart';
import '../../data/model/user/user_model.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../bottom_navbar/bottom_navbar_view.dart';
import '../introduction/introduction.dart';
import '_pin_register_view.dart';
import '_registration_view.dart';
import 'login_view.dart';

class LoginController extends GetxController {
  final box = GetStorage();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final db = FirebaseFirestore.instance;
  final FireStoreUserServices firestoreuserservices = FireStoreUserServices();

  String baseurl = MasterUri.baseurl.baseAuth;
  String pathLogin = MasterUri.pathAuth.authLogin;
  var isLoggedin = false.obs;
  var isLoggedinwithgoogleaccount = false.obs;
  var isRemember = false.obs;
  var isPasswordVisible = true.obs;
  var isStatusCodeTrue = false.obs;
  var isUserPinned = false.obs;
  var txtControllerusername = TextEditingController().obs;
  var txtControllerpin = TextEditingController().obs;
  var txtControllerpassword = TextEditingController().obs;
  var txtControlleremail = TextEditingController().obs;
  var txtControllerphone = TextEditingController().obs;
  var isTime = ''.obs;
  var isLocalTime = ''.obs;
  var isLocalDate = ''.obs;

  var isLogginProcess = false.obs;

  var token = ''.obs;

  var uid = ''.obs;
  var fcmtoken = ''.obs;
  var userPin = ''.obs;
  var username = ''.obs;
  var password = ''.obs;
  var photoUrl = ''.obs;
  var email = ''.obs;
  var package = ''.obs;
  var userStatus = ''.obs;
  var userRole = ''.obs;
  var whatssappNumber = ''.obs;
  var telegramNumber = ''.obs;

  String? value;

  late Map<String, dynamic> data;
  late Map<String, dynamic> dataIlmci;

  @override
  void onInit() {
    super.onInit();
    greetingLocalTimes();
    onLoad();
  }

  resetPasswordAuth(
      {required BuildContext context, required String email}) async {
    var response = await _fireStoreUserServices.resetPassword(email: email);
    if (response == true) {
      // ignore: use_build_context_synchronously
      Navigator.pop(context);
    }
  }

  loginGoogleCredentials() async {
    isLogginProcess.value = true;
    debugPrint(
        'is google account status : ${isLoggedinwithgoogleaccount.value}');
    greetingLocalTimes();
    try {
      FirebaseAuth.instance
          .authStateChanges()
          .listen((User? usercredetntials) async {
        if (usercredetntials != null) {
          debugPrint(
              'getting data google - google account : $usercredetntials');
          //_boxStorage.setUserGoogleCredential(usercredetntials);
          _boxStorage.setUserGoogleLogin();
          //final authResult = await _auth.signInWithCredential(credential);
          final User user = usercredetntials;
          debugPrint('user google account : $user');
          assert(!user.isAnonymous);
          // ignore: unnecessary_null_comparison
          assert(await user.getIdToken() != null);
          final User currentUser = _auth.currentUser!;
          assert(user.uid == currentUser.uid);
          box.write('token', user.uid);
          // txtControllerusername.value.text = user.displayName!;
          if (user.photoURL == null) {
            photoUrl.value =
                'https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525';
          } else {
            photoUrl.value = user.photoURL!;
          }
            uid.value = user.uid;
            _boxStorage.setUserId(user.uid);
            _boxStorage.setUserEmail(user.email!);
            _boxStorage.setUserAvatar(photoUrl.value);

          debugPrint(
              'tes gmails : username ${txtControlleremail.value.text} - email ${txtControlleremail.value.text}');
         
          uid.value = user.uid;
          email.value = user.email!;
          // convert data ilmci static

          //getUserDatafromMap();
          checkProfilePin(user);
          debugPrint(
              '${user.uid} ,${user.displayName}, ${user.email} , ${isUserPinned.value} , ${isStatusCodeTrue.value}');
          isLogginProcess.value = false;
        } else{
        firestoreuserservices.logout();
          _boxStorage.clearCache();
          //logincontroller.isLogginProcess.value = false;
          //await googleSignIn.signOut();
          Get.offAll(const LoginView());
        }
      });
    } on FirebaseAuthException catch (e) {
      debugPrint('$e');
      if (e.code == 'account-exists-with-different-credential') {
        Get.snackbar('Error', e.code);
        isLogginProcess.value = false;
        // handle the error here
      } else if (e.code == 'invalid-credential') {
        Get.snackbar('Error', 'invalid-credential');
        isLogginProcess.value = false;
        // handle the error here
      } else if (e.code == 'sign_in_canceled') {
        Get.snackbar('Error', e.code);
        isLogginProcess.value = false;
        // handle the error here
      }
    } catch (e) {
      Get.snackbar('Error', 'Client request not found');
      isLogginProcess.value = false;
      // handle the error here
    }
  }

  loginWithGoogle() async {
    await logoutGoogle();
    await googleSignIn.signOut();

    isLogginProcess.value = true;
    // isLoggedinwithgoogleaccount.value = true;
    debugPrint(
        'is google account status : ${isLoggedinwithgoogleaccount.value}');
    greetingLocalTimes();
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();
    debugPrint(
        'login process : ${isLogginProcess.value} - google account : $googleSignInAccount');
    if (googleSignInAccount == null) {
      return null;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    _boxStorage.setUserGoogleCredential(credential);
    _boxStorage.setUserGoogleLogin();
    try {
      debugPrint('getting data google - google account : $credential');
      final authResult = await _auth.signInWithCredential(credential);
      final User user = authResult.user!;
      debugPrint('user google account : $user');
      assert(!user.isAnonymous);
      // ignore: unnecessary_null_comparison
      assert(await user.getIdToken() != null);
      final User currentUser = _auth.currentUser!;
      assert(user.uid == currentUser.uid);
      box.write('token', user.uid);
      txtControllerusername.value.text = user.displayName!;
      photoUrl.value = user.photoURL!;
      uid.value = user.uid;
      _boxStorage.setUserId(user.uid);
      _boxStorage.setUserEmail(user.email!);
      _boxStorage.setUserName(user.displayName!);
      _boxStorage.setUserAvatar(user.photoURL!);

      debugPrint(
          'tes gmail : username ${txtControlleremail.value.text} - email ${txtControlleremail.value.text}');
      username.value = user.displayName!;
      photoUrl.value = user.photoURL!;
      uid.value = user.uid;
      email.value = user.email!;
      // convert data ilmci static

      //getUserDatafromMap();
      checkProfilePin(user);
      debugPrint(
          '${user.uid} ,${user.displayName}, ${user.email} , ${isUserPinned.value} , ${isStatusCodeTrue.value}');
      isLogginProcess.value = false;
    } on FirebaseAuthException catch (e) {
      debugPrint('$e');
      if (e.code == 'account-exists-with-different-credential') {
        Get.snackbar('Error', e.code);
        isLogginProcess.value = false;
        // handle the error here
      } else if (e.code == 'invalid-credential') {
        Get.snackbar('Error', 'invalid-credential');
        isLogginProcess.value = false;
        // handle the error here
      } else if (e.code == 'sign_in_canceled') {
        Get.snackbar('Error', e.code);
        isLogginProcess.value = false;
        // handle the error here
      }
    } catch (e) {
      Get.snackbar('Error', 'Client request not found');
      isLogginProcess.value = false;
      // handle the error here
    }
  }

  createUser() async {
    data = getUserDatafromMap();
    debugPrint('clog ==> $data');
    var uid = box.read('token');
    db.collection('user').doc(uid).set(data);
    Get.off(() => const OnBoardingPage());
  }

  checkProfilePin(User user) async {
    debugPrint('check user (${user.uid}) profile : $user');
    debugPrint(
        'is google account status : ${isLoggedinwithgoogleaccount.value}');
    bool exist = false;
    var collection = FirebaseFirestore.instance.collection('user');
    var docSnapshot = await collection.doc(user.uid).get().then((doc) {
      exist = doc.exists;
    });
    debugPrint('docSnapshot is : $exist}');

    if (exist == false) {
      Get.off(() =>  RegisterPinView(googleUserCred: user,));
    } else {
      Get.off(() => const DacBottomNavbar());
    }

    debugPrint(
        'clog ==> ${userPin.value} - isuserpinned  ${isUserPinned.value} - isusercodetrue  ${isStatusCodeTrue.value} ');
  }

  Map<String, dynamic> getUserDatafromMap() {
    package.value = "com.ilmci.digitalbook";
    userRole.value = "user";
    userStatus.value = "active";

    // userPin.value = txtControllerpin.value.text;
    // password.value = txtControllerpassword.value.text;

    data = {
      'uid': uid.value,
      'username': username.value,
      'password': txtControllerpassword.value.text,
      'email': email.value,
      'pin': txtControllerpin.value.text,
      'avatar': photoUrl.value,
      'package': package.value,
      'userStatus': userStatus.value,
      'userRole': userRole.value,
      'whastappNumber': whatssappNumber.value,
      'telegramNumber': telegramNumber.value,
    };
    return data;
  }

  Map<String, dynamic> getUserDataIlmcifromMap(String email, String pwd, String username, String phone, String pin) {
    package.value = "com.ilmci.digitalbook";
    //userRole.value = "user";
    //userStatus.value = "active";
    // if (userPin.value == '' || password.value == '') {
    //userPin.value = txtControllerpin.value.text;
    //password.value = txtControllerpassword.value.text;
    // }

    dataIlmci = {
      "email": email,
      "password": pwd,
      "nama": username,
      "phone": phone,
      "whatsapp": "089999999999",
      "pin": pin,
      "package": package.value
    };
    return dataIlmci;
  }

  String? validateEmail(String? value) {
  const pattern = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'"
      r'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-'
      r'\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*'
      r'[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4]'
      r'[0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9]'
      r'[0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\'
      r'x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';
  final regex = RegExp(pattern);

  return value!.isNotEmpty && !regex.hasMatch(value)
      ? 'Enter a valid email address'
      : null;
}

  Future<void> checkPin(bool isgoogleaccount, String email, String pwd, String phone, String username, String pin) async {
    //  registrasiDacUser(
    //           email: txtControlleremail.value.text,
    //           password: txtControllerpassword.value.text,
    //           phone: txtControllerphone.value.text,
    //           username: txtControllerusername.value.text,
    //           pin: txtControllerpin.value.text);
    try {
      dataIlmci = getUserDataIlmcifromMap(email, pwd, username, phone, pin);
      debugPrint('data : ${dataIlmci.toString()}');
      //const fullUrl = "https://admin.digital.ilmci.com/api/auth/register";
       String imagecloud =
              "https://firebasestorage.googleapis.com/v0/b/digital-alumni-ilmci.appspot.com/o/internal%2Fassets%2Flogo.png?alt=media&token=2375fed1-8208-44f0-84e6-86b8ea8a0525";
      String imageProfile = _boxStorage.getUserAvatar();
      if(imageProfile == ''|| imageProfile.isEmpty){
        imageProfile = imagecloud;
      }
       
      const fullUrl = "https://admin.digital.ilmci.com/api/auth/v2/register";
      var bodyRequestParam = jsonEncode(dataIlmci);
      final response = await http.Client().post(Uri.parse(fullUrl),
      
          headers: {
            "Content-Type": "application/json",
          },
          body: bodyRequestParam);
      final result = jsonDecode(response.body);
      debugPrint(response.statusCode.toString());
      switch (response.statusCode) {
        case 200:
          Get.snackbar('Register Success', 'Anda telah berhasil register');
        
          String userstatus = "active";
          String userrole = "user";
          registrasiDacUser(
              email: email,
              password: pwd,
              phone: phone,
              username: username,
              pin: pin,
              userRole: userrole,
              userStatus: userstatus,
              userCoverProfile: imageProfile,
              avatar: imageProfile,
              isgoogleaccount: isgoogleaccount);
          isLoggedinwithgoogleaccount.value = false;
          isRemember.value == true;
          isStatusCodeTrue.value = true;
          isUserPinned.value = true;

          debugPrint(response.body.toString());
          break;
        case 400:
          debugPrint(response.body.toString());

          if (result['message'] == 'Email already registered.') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                'Email already registered.');
            debugPrint(
                'is pinned : ${isUserPinned.value} - is status code : ${isUserPinned.value}');
          }
          if (result['message'] == 'PIN tidak terdaftar') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                'PIN tidak terdaftar.');
          }
          if (result['message'] ==
              '[500] Connection could not be established with host mail.ilmci.com [ #0]') {
            isStatusCodeTrue.value = false;
            isUserPinned.value = false;
            Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
                '[500] Connection could not be established with host mail.ilmci.com [ #0]');
          }
          //Email or Password is required.
          Get.snackbar('Registrasi ILMCI Failed [${response.statusCode}]',
              result['message']);

          break;
        case 500:
          Get.snackbar(
              'Login Failed [${response.statusCode}]', 'Data not found');
          isStatusCodeTrue.value = false;
          isUserPinned.value = false;
          break;
      }
    } on SocketException {
      isLoggedin.value = false;
      Get.snackbar('Login Failed', 'Data not found');
    }
    http.Client().close();
  }

  Future<void> logoutGoogle() async {
    //onSavedRememberMe(false);
    _fireStoreUserServices.logout();
    _boxStorage.clearCache();
    isLogginProcess.value = false;
    //await googleSignIn.signOut();
    Get.offAll(const LoginView());
    // Get.offAll(page)
  }

  greetingLocalTimes() {
    var now = DateTime.now().hour;
    isLocalDate.value =
        DateFormat('EEE, d/M/y').format(DateTime.now()).toString();
    isLocalTime.value =
        DateFormat('HH:mm:ss').format(DateTime.now()).toString();

    if (now >= 11 && now <= 14) {
      isTime.value = 'Selamat Siang!';
    }
    if (now >= 15 && now <= 16) {
      isTime.value = 'Selamat Sore!';
    }
    if (now >= 17 && now <= 18) {
      isTime.value = 'Selamat Petang';
    }
    if (now >= 19 && now <= 23) {
      isTime.value = 'Selamat Malam';
    }
    if (now >= 0 && now <= 10) {
      isTime.value = 'Selamat Pagi!';
    }
    debugPrint('is now : $now - is time : ${isTime.value}');
  }

  Future<void> getAuth(bool checked) async {
    try {
      final fullUrl = baseurl + pathLogin;
      Map data = {
        'username': txtControlleremail.value.text,
        'password': txtControllerpassword.value.text,
      };
      var bodyRequestParam = jsonEncode(data);
      final response = await http.Client().post(Uri.parse(fullUrl),
          headers: {
            "Content-Type": "application/json",
          },
          body: bodyRequestParam);
      final result = jsonDecode(response.body)['token'];
      debugPrint(response.statusCode.toString());
      switch (response.statusCode) {
        case 200:
          box.write('token', result);
          isLoggedin.value = true;
          token = result;
          onLoginUser();
          break;
        case 500:
          Get.snackbar('Login Failed', 'Data not found');
          break;
      }
    } on SocketException {
      isLoggedin.value = false;
      Get.snackbar('Login Failed', 'Data not found');
    }
    http.Client().close();
  }

  hidepassword() {
    isPasswordVisible.value
        ? isPasswordVisible.value = false
        : isPasswordVisible.value = true;
  }

  rememberMe() async {
    if (isRemember.value == false) {
      isRemember.value = true;
    } else {
      isRemember.value = false;
    }
   // onSavedRememberMe(isRemember.value);
  }

  onLoad() {
    isRemember.value = box.read('rememberMeCheck') ?? isRemember.value;
    if (isRemember.value == true) {
      txtControlleremail.value.text = _boxStorage.getUserEmail();
      txtControllerpassword.value.text = _boxStorage.getUserPassword();
    }
  }

  onSavedRememberMe(bool uncheck, String email, String pwd) async {
    //isRemember.value = uncheck;
    box.write('rememberMeCheck', uncheck);
    if (uncheck == false) {
      _boxStorage.deleteUserEmail();
      _boxStorage.deleteUserPassword();
      txtControlleremail.value.text = '';
      txtControllerpassword.value.text = '';
      isRemember.value == false;
      box.remove('rememberMeCheck');
      debugPrint('on saved');
    } else {
      _boxStorage.setUserEmail(email);
      _boxStorage.setUserPassword(pwd);
    }
    debugPrint(
        'saving email : $email - password : $pwd');
  }

  onLoginUser() {
    if (isLoggedin.value == true) {
      Get.snackbar('Login Success', 'Your Token : $token');
      Get.off(() => const DacBottomNavbar());
    } else {
      Get.off(() => const LoginView());
    }
  }

  onLogoutUser() {
    box.remove('token');
    debugPrint('token has been deleted');
    Get.off(() => const LoginView());
  }

  late Stream<String> _tokenStream;

  getFcmToken() {
    FirebaseMessaging.instance
        .getToken(
            vapidKey:
                'BGpdLRsMJKvFDD9odfPk92uBg-JbQbyoiZdah0XlUyrjG4SDgUsE1iC_kdRgt4Kn0CO7K3RTswPZt61NNuO0XoA')
        .then(setToken);
    _tokenStream = FirebaseMessaging.instance.onTokenRefresh;
    _tokenStream.listen(setToken);
  }

  void setToken(String? token) async {
    debugPrint('FCM Token: $token');
    _boxStorage.setUserFcmToken(token!);
  }

  final FireStoreUserServices _fireStoreUserServices = FireStoreUserServices();
  final BoxStorage _boxStorage = BoxStorage();
  final FirebaseFirestore _db = FirebaseFirestore.instance;

  loginFirestoreOrtu(String email,String pwd) async {
    getFcmToken();
    // String emaillocal = _boxStorage.getUserEmail();
    // String passwordlocal = _boxStorage.getUserPassword();
    // String uid = _boxStorage.getUserId();
    // bool isgooglelogin = _boxStorage.getUserGoogleLogin();
    debugPrint('load : $email - is googlelogin = $pwd');
    await _fireStoreUserServices.login(
        email: email,
        password: pwd,
      );
    // if (email != '' && email.isNotEmpty ||
    //     password != '' && password.isNotEmpty) {
    //   txtControllerusername.value.text = email;
    //   txtControllerpassword.value.text = password;
    //   await _fireStoreUserServices
    //       .login(
    //           email: txtControllerusername.value.text,
    //           password: txtControllerpassword.value.text)
    //       .then((value) {
    //     if (value != null) {
    //       debugPrint('run cloduser');
    //       clouduser();
    //     } else {
    //       return null;
    //     }

    //     debugPrint(' val : $value');
    //   });
    // } else {
    //   await _fireStoreUserServices
    //       .login(
    //           email: txtControllerusername.value.text,
    //           password: txtControllerpassword.value.text)
    //       .then((value) {
    //     if (value != null) {
    //       debugPrint('run cloduser 2');
    //       clouduser();
    //     } else {
    //       return null;
    //     }

    //     debugPrint(' val 2 : $value');
    //   });
    // }
  }

  clouduser() async {}

  registrasiDacUser(
      {required String email,
      required String password,
      required String username,
      required String avatar,
      required String pin,
      String? phone,
      required String userRole,
      required String userStatus,
      required String userCoverProfile,
      bool? isgoogleaccount}) async {
    debugPrint('is google account status $isgoogleaccount');
    String fcmtoken = _boxStorage.getUserFcmToken();

    String? result = await _fireStoreUserServices.registration(
        email: email,
        password: password,
        username: username,
        avatar: avatar,
        pin: pin,
        phone: phone,
        userRole: userRole,
        userCoverProfile: avatar,
        fcmtoken: fcmtoken,
        userStatus: userStatus,
        isgoogleaccount: isgoogleaccount);
    debugPrint('result registrasi : $result');
    if (result!.contains('Success')) {
      
     // onSavedRememberMe(isRemember.value);
      Future.delayed(const Duration(seconds: 3), () async {
        Get.off(() => const DacBottomNavbar());
        debugPrint('data $email - $username - $pin - $avatar');
      });
    }
    Get.snackbar('Informasi', result);
    isLogginProcess.value = true;
  }

  updateFcmToken(String uid) async {
    greetingLocalTimes();
    bool isupdated = await _fireStoreUserServices.updateFcmToken();
    if (isupdated == true) {
      return null;
    }
  }

  loginHardcode(String emailtext, String pwdtext) async {

    debugPrint(
        'data $emailtext - $pwdtext');
    if (emailtext == 'developer@dac.com' && pwdtext == '123456' ||
        emailtext == 'user@dac.com' && pwdtext == '123456' ||
        emailtext == 'user2@dac.com' && pwdtext == '123456') {
      switch (emailtext) {
        case 'developer@dac.com':
          uid.value = 'Xha4gwjMe5ZfR1SLfR2i';
          break;
        case 'user@dac.com':
          uid.value = 'I4OQbzZcl5Sm4s3AJ0Qp';
          break;
        case 'user2@dac.com':
          uid.value = 'r2xeX2M0qw36wWdtE7q4';
          break;

        default:
          return Get.snackbar('Informasi', 'mohon isi nisn dan password');
      }
      debugPrint('data $email - $password - ${uid.value}');

      // debugPrint('username : ${username.value} - token fcm : ${fcmtoken.value}');
    }
    greetingLocalTimes();
    //_boxStorage.setFirstInstall();
    loginFirestoreOrtu(emailtext, pwdtext);
    debugPrint(uid.value);
  }
}
