import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_controller.dart';

class GoogleSignInView extends StatefulWidget {
  const GoogleSignInView({super.key});

  @override
  State<GoogleSignInView> createState() => _GoogleSignInViewState();
}

class _GoogleSignInViewState extends State<GoogleSignInView> {
  @override
  Widget build(BuildContext context) {
    final LoginController controller = Get.put(LoginController());
    return Scaffold(
      appBar: AppBar(
        title: const Text('Logins'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  controller.loginWithGoogle();
                },
                child: const Center(
                    child: Text(
                  "Login with google",
                  style: TextStyle(color: Colors.white),
                )))
          ],
        ),
      ),
    );
  }
}
