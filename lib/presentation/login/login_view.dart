import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../profile_medsos/profile_medsos_view.dart';
import '../splashscreen/splashscreen_controller.dart';
import '_forgot_password.dart';
import '_registration_view.dart';
import 'login_controller.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final LoginController controller = Get.put(LoginController());
  final SplashScreenController splashcontroller =
      Get.put(SplashScreenController());
  bool isloading = false;
  bool isremember = false;
  bool ishide = false;
  final _formKey = GlobalKey<FormState>();

  var txtEmail = TextEditingController();
  var txtPassword = TextEditingController();

  void onhidePwd() {
    setState(() {
      if (ishide == false) {
        ishide = true;
      } else {
        ishide = false;
      }
    });
    debugPrint('is password hide status: $ishide');
  }

  void onRememberme() {
    setState(() {
      if (isremember == false) {
        isremember = true;
      } else {
        isremember = false;
      }
    });
    debugPrint('is remember status: $ishide');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(10.0),
            child: AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: ColorName.redprimary,
            )),
        body: SafeArea(
          child: ListView(
            children: [
              Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(),
                      _dropdownIsCountry(),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Assets.image.logoDac.image(fit: BoxFit.fitWidth),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: SizedBox(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "DIGITAL",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.blueprimary),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          "ALUMNI",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.redprimary),
                        ),
                      ],
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 20.0),
                      child: Text(
                        "jadilah alumni yang selalu terhubung",
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Row(
                        children: [
                          const Text(
                            "Belum memiliki akun ? ",
                            style: TextStyle(
                                fontWeight: FontWeight.w300, fontSize: 14),
                          ),
                          GestureDetector(
                            onTap: () => Get.to(const RegistrationView()),
                            child: const Text(
                              "Daftar disini ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  fontSize: 14,
                                  color: ColorName.blueprimary),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
              ),
              AutofillGroup(
                  child: Form(
                autovalidateMode: AutovalidateMode.always,
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtEmail,
                        keyboardType: TextInputType.text,
                        autofillHints: const [AutofillHints.email],
                        textInputAction: TextInputAction.next,
                        onSaved: (String? value) {},
                        onChanged: (value) => txtEmail,
                        validator: (value) => controller.validateEmail(value),
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'cooper@example.com',
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: TextFormField(
                        controller: txtPassword,
                        obscureText: ishide,
                        keyboardType: TextInputType.text,
                        autofillHints: const [AutofillHints.password],
                        onEditingComplete: () =>
                            TextInput.finishAutofillContext(),
                        onChanged: (value) => txtPassword,
                        onSaved: (String? value) {},
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "password cannot be empty";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'Password',
                          suffixIcon: IconButton(
                            icon: Icon(
                              ishide
                                  ? Icons.visibility_off_sharp
                                  : Icons.visibility,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              onhidePwd();
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, left: 25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        GestureDetector(
                            onTap: () {
                              onRememberme();
                            },
                            child: isremember == true
                                ? const Icon(Icons.check_box)
                                : const Icon(
                                    Icons.check_box_outline_blank_outlined)),
                        const Padding(
                          padding: EdgeInsets.only(left: 8.0),
                          child: Text(
                            "Ingat saya ",
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () => {Get.to(const ForgotPasswordView())},
                        child: const Text(
                          "Lupa password ?",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: Colors.black),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorName.blueprimary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.purplelow)))),
                    onPressed: () {
                      //controller.getAuth(ischeckedValue);

                      Future.delayed(const Duration(seconds: 3), () {
                        // <-- Delay here
                        if (_formKey.currentState!.validate() ||
                            isloading == false) {
                          setState(() {
                            isloading = true;
                          });
                          FocusManager.instance.primaryFocus?.unfocus();
                          setState(() {
                            debugPrint('isloading : $isloading');
                            isloading == true;
                          });
                          controller.loginHardcode(txtEmail.text, txtPassword.text);
                          if (isremember == true) {
                            controller.onSavedRememberMe(
                                isremember, txtEmail.text, txtPassword.text);
                          }

                          Future.delayed(const Duration(seconds: 3), () {
                            setState(() {
                              isloading = false;
                            });
                          });
                        } else {
                          Get.snackbar('Login Proses', 'login in progress');
                          debugPrint('login in progress..');
                          setState(() {
                            isloading = false;
                          });
                        }
                      });
                       FocusManager.instance.primaryFocus?.unfocus();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          isloading == true
                              ? const CircularProgressIndicator()
                              : Text("Masuk dengan akun dac".toUpperCase(),
                                  style: const TextStyle(fontSize: 14)),
                        ],
                      ),
                    )),
              ),
         
              Padding(
                padding:
                    const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(ColorName.raven),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.raven)))),
                    onPressed: () {
                      //controller.getAuth(ischeckedValue);
                      if (controller.isLogginProcess.value == false) {
                        controller.isLoggedinwithgoogleaccount.value = true;
                        debugPrint(
                            'is google account status : ${controller.isLoggedinwithgoogleaccount.value}');
                        controller.loginWithGoogle();
                      } else {
                        Get.snackbar(
                            'Login Proses', 'login with google in progress');
                        debugPrint('login with google in progress..');
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const FaIcon(FontAwesomeIcons.google),
                          const SizedBox(
                            width: 6,
                          ),
                          Text("Masuk dengan google".toUpperCase(),
                              style: const TextStyle(fontSize: 14)),
                        ],
                      ),
                    )),
              ),
         
              const SizedBox(
                height: 50,
              )
            ],
          ),
        ));
  }

  bool ischeckedValue = false;

  Widget _dropdownIsCountry() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width * 0.4,
          child: CustomDropdown(
            hintText: 'Bahasa',
            items: splashcontroller.splashKeyOptions,
            controller: splashcontroller.splashKey.value,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                splashcontroller.splashKey.value.text = value;
              });
              splashcontroller.selectSplashScreen(value);
            },
          ),
        ),
      ],
    );
  }
}
