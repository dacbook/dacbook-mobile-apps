

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import 'login_controller.dart';

class ForgotPasswordView extends StatefulWidget {
  const ForgotPasswordView({super.key});

  @override
  State<ForgotPasswordView> createState() => _ForgotPasswordViewState();
}

class _ForgotPasswordViewState extends State<ForgotPasswordView> {
    final TextEditingController _email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final LoginController loginController = Get.put(LoginController());
    return  Scaffold(
        appBar: PreferredSize(
            preferredSize: const Size.fromHeight(10.0),
            child: AppBar(
              elevation: 0,
              automaticallyImplyLeading: true,
              backgroundColor: ColorName.redprimary,
            )),
        body: SafeArea(
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Assets.image.logoDac.image(fit: BoxFit.fitWidth),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20),
                child: SizedBox(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "DIGITAL",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.redprimary),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          "ALUMNI",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 30,
                              color: ColorName.redprimary),
                        ),
                      ],
                    ),
                   
                  ],
                )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: TextFormField(
                  controller: _email,
                 // obscureText: controller.isPasswordVisible.value,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => _email,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "email cannot be empty";
                    }
                    return null;
                  },
                  decoration: const InputDecoration(
                    border:  OutlineInputBorder(),
                    labelText: 'Masukan E-mail',
                  
                  ),
                ),
              ),
             
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: ElevatedButton(
                    style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            ColorName.redprimary),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.purplelow)))),
                    onPressed: () async {
                     if(_email.text.isNotEmpty){
                      loginController.resetPasswordAuth(context: context, email: _email.text);
                     }else{
                      Get.snackbar('Informasi', 'Email masih kosong');
                     }
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Lupa Password".toUpperCase(),
                              style: const TextStyle(fontSize: 14)),
                        ],
                      ),
                    )),
              ),
            ],
          ),
        ));
  }
}