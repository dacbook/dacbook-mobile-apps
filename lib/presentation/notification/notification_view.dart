import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/data/model/notification/notification_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:readmore/readmore.dart';

import '../../data/remote/firestore/firestore_notification_services.dart';
import '../../gen/colors.gen.dart';
import '../profile/_profile_edit_avatar.dart';

class NotificationView extends StatefulWidget {
  const NotificationView({super.key});

  @override
  State<NotificationView> createState() => _NotificationViewState();
}

class _NotificationViewState extends State<NotificationView> {
  final FireStoreNotificationServices fireStoreNotificationServices =
      FireStoreNotificationServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        centerTitle: true,
        title: const Text('Notifikasi'),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              StreamBuilder(
                stream: fireStoreNotificationServices.getNotification(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  // debugPrint('lenght : ${snapshot.data!.length}');
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(strokeWidth: 1.0));
                  }
                  if (snapshot.connectionState == ConnectionState.none) {
                    return const Text('Server not found - Error 500');
                  }
                  if (snapshot.connectionState == ConnectionState.done ||
                      !snapshot.hasData ||
                      snapshot.data.length == 0 ||
                      snapshot.data == null) {
                    debugPrint('is snaps notif : ${snapshot.data.toString()}');

                    return SizedBox(
                      height: Get.height * 0.4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          Center(child: Text('Tidak ada notifikasi'))
                        ],
                      ),
                    );
                  } else {
                    return ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          NotificationModel feedCommentModel =
                              snapshot.data![index];
                          debugPrint('data => ${snapshot.data.length.toString()}');
                          var timeago = Jiffy(feedCommentModel.createdAt)
                              .fromNow()
                              .toString();
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ListTile(
                                leading: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(5),
                                      child: CachedNetworkImage(
                                        imageUrl: feedCommentModel.avatar,
                                        fit: BoxFit.fitHeight,
                                        width: Get.width * 0.94,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            url,
                                            fit: BoxFit.fitHeight,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const FaIcon(
                                              FontAwesomeIcons.camera);
                                        },
                                      ),
                                    )),
                                title: Row(
                                  children: [
                                    Text(
                                      feedCommentModel.username,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          color: Colors.black54,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      timeago,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          color: Colors.black54,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ReadMoreText(
                                      '${feedCommentModel.username} , ${feedCommentModel.body}',
                                      trimLines: 2,
                                      colorClickableText: ColorName.redprimary,
                                      trimMode: TrimMode.Line,
                                      trimCollapsedText: 'Selengkapnya',
                                      trimExpandedText: 'Lebih sedikit',
                                      moreStyle: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              const Divider(),
                            ],
                          );
                        });
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
