import 'dart:convert';

import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:dac_apps/gen/assets.gen.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../bottom_navbar/bottom_navbar_view.dart';
import '../login/login_controller.dart';
import '../login/login_view.dart';

class SplashScreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    getFcmToken();
    currentUserSplashScreenSelected();
  }

  @override
  void onClose() {
    super.onClose();
  }

  final BoxStorage _boxStorage = BoxStorage();
  final LoginController loginController = LoginController();

  var splashKeyOptions = <String>['indonesia', 'amerika'].obs;
  var splashKey = TextEditingController().obs;
  var splashScreenCountryLabel = ''.obs;
  var splashScreenCountryTheme = ''.obs;
  var splashScreenLoginValidation = ''.obs;

  currentUserSplashScreenSelected() {
    splashKey.value.text = _boxStorage.getSplashScreen();
    if (splashKey.value.text == '') {
      splashKey.value.text = 'indonesia';
      splashScreenCountryLabel.value = 'indonesia';
      splashScreenCountryTheme.value =
          Assets.image.ssbackgroundIndonesia.keyName;
    } else {
      splashKey.value = splashKey.value;
      splashScreenCountryLabel.value = splashKey.value.text;
      switch (splashKey.value.text) {
        case 'indonesia':
          splashScreenCountryTheme.value =
              Assets.image.ssbackgroundIndonesia.keyName;
          break;

        case 'amerika':
          splashScreenCountryTheme.value =
              Assets.image.ssbackgroundAmerika.keyName;
          break;
      }
      debugPrint('clog => ${splashScreenCountryTheme.value}');
    }
    isCurrentUserLoggedIn();
  }

  var fcmtoken = ''.obs;
  late Stream<String> _tokenStream;

  getFcmToken() {
    FirebaseMessaging.instance
        .getToken(
            vapidKey:
                'BGpdLRsMJKvFDD9odfPk92uBg-JbQbyoiZdah0XlUyrjG4SDgUsE1iC_kdRgt4Kn0CO7K3RTswPZt61NNuO0XoA')
        .then(setToken);
    _tokenStream = FirebaseMessaging.instance.onTokenRefresh;
    _tokenStream.listen(setToken);
  }

  void setToken(String? token) async {
    debugPrint('FCM Token: $token');
    _boxStorage.setUserFcmToken(token!);
  }

  selectSplashScreen(String splashKeyValue) {
    debugPrint(splashKeyValue);
    splashKey.value.text = splashKeyValue;
    _boxStorage.setSplashScreen(splashKey.value.text);
  }

  isCurrentUserLoggedIn() async {
    //sometime you need to clear local
    //_boxStorage.clearCache();

    String uid = _boxStorage.getUserId();
    String email = _boxStorage.getUserEmail();
    String password = _boxStorage.getUserPassword();
    String credetial = _boxStorage.getUserGoogleCredential();
    
    
    debugPrint('uid token in splash : $uid - $password - $email - $credetial');
    if (uid == ''|| uid.isEmpty) {
      
      Future.delayed(const Duration(seconds: 1), () async {
        Get.off(() => const LoginView());
      });
    } else {
      if(credetial != '' || credetial.isNotEmpty){
        debugPrint('is login google cres');
        splashScreenLoginValidation.value = 'Sedang memuat data';
        
       // AuthCredential credential = credetial as AuthCredential;
         Future.delayed(const Duration(seconds: 2), () async {
        splashScreenLoginValidation.value = 'mencoba masuk..';
        await loginController.loginGoogleCredentials();
        splashScreenLoginValidation.value = 'mendapatkan akun';
      });
      
      } else{
          debugPrint('is login ortu css');
           splashScreenLoginValidation.value = 'Sedang memuat data';
        Future.delayed(const Duration(seconds: 2), () async {
          splashScreenLoginValidation.value = 'mencoba masuk..';
        await loginController.loginFirestoreOrtu(email, password);
        splashScreenLoginValidation.value = 'mendapatkan akun';
      });
      }
      
      
    }
  }

loginVerificationLabel(String message) {
    debugPrint('message : $message');
    Get.showSnackbar(
  GetSnackBar(
  //  title: 'title',
    message: message,
    icon: const Icon(Icons.refresh),
    duration: const Duration(seconds: 3),
  ));
}
 
}
