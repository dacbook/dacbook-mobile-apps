import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/data/model/feed/feed_model.dart';
import 'package:dac_apps/presentation/bottom_navbar/bottom_navbar_view.dart';
import 'package:dac_apps/presentation/login/login_view.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/widgets/splash_screen.dart';
import '../../data/local/box/box_storage.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../main.dart';
import '../book_agenda/book_agenda_detail_view.dart';
import '../book_forum/book_forum_detail_view.dart';
import '../book_review/book_review_view.dart';
import '../bookgroup/bookgroup_controller.dart';
import '../feeder/feeder_detail_view.dart';
import 'splashscreen_controller.dart';

class SplashScreenView extends StatefulWidget {
  const SplashScreenView({super.key});

  @override
  State<SplashScreenView> createState() => _SplashScreenViewState();
}

class _SplashScreenViewState extends State<SplashScreenView> {
  String? initialMessage;
  bool resolved = false;
  final BoxStorage boxStorage = BoxStorage();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  final BookGroupController bookGroupController = BookGroupController();

  @override
  void initState() {
    super.initState();

    FirebaseMessaging.instance.getInitialMessage().then(
          (value) => setState(
            () {
              resolved = true;
              initialMessage = value?.data.toString();
            },
          ),
        );

    FirebaseMessaging.onMessage.listen(showFlutterNotification);

    // FirebaseMessaging.onBackgroundMessage((message) => null);

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      Get.snackbar('test', 'fcm on background');
      debugPrint(
          'clog ==> testing aplikasi notifikasi ${message.data['notificationCategory']}');
      String notificationCategory = message.data['notificationCategory'];

      switch (notificationCategory) {
        case 'bookforum':
          String bookid = message.data['bookid'];
          String feedid = message.data['feedid'];
          bool isadmin = false;

          //check single book & single feed
          BookModel bookModel =
              await fireStoreBookServices.getSingleBook(bookid);
          bool iscurrentadmin =
              await bookGroupController.checkIsAdminOrNot(bookModel.bookadmin);
          isadmin = iscurrentadmin;
          debugPrint('isadmin : $isadmin - $iscurrentadmin');
          FeedModel feedModel =
              await fireStoreBookServices.getSingleForum(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(BookForumDetailView(
            feedModel: feedModel,
            islikes: islikes,
            isadmin: isadmin,
          ));
          break;
        case 'bookagenda':
          String bookid = message.data['bookid'];
          String feedid = message.data['feedid'];
          bool isadmin = false;

          //check single book & single feed
          BookModel bookModel =
              await fireStoreBookServices.getSingleBook(bookid);
          bool iscurrentadmin =
              await bookGroupController.checkIsAdminOrNot(bookModel.bookadmin);
          isadmin = iscurrentadmin;
          debugPrint('isadmin : $isadmin - $iscurrentadmin');
          FeedModel feedModel =
              await fireStoreBookServices.getSingleAgendaBook(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(BookAgendaDetailView(
              feedModel: feedModel, islikes: islikes, isadmin: isadmin));
          break;
        case 'story':
          String feedid = message.data['feedid'];

          FeedModel feedModel =
              await fireStoreBookServices.getSingleStory(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(
            FeederDetailView(
              feedModel: feedModel,
              islikes: islikes,
            ),
          );
          break;

        default:
          Get.to(const SplashScreenView());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final SplashScreenController splashscreenController =
        Get.put(SplashScreenController());
    return Scaffold(
      body: SplashScreen(
        countryLabel: splashscreenController.splashScreenCountryLabel.value,
        countryName: splashscreenController.splashScreenCountryTheme.value,
      ),
    );
  }
}
