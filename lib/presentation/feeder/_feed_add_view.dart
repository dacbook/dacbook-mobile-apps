import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:dac_apps/presentation/feeder/feeder_controller.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../bottom_navbar/bottom_navbar_view.dart';
import '../login/login_controller.dart';
import 'package:loading_overlay/loading_overlay.dart';

final db = FirebaseFirestore.instance;
String? value;

class AddFeederView extends StatefulWidget {
  const AddFeederView({super.key, required this.category});
  final String category;
  @override
  State<AddFeederView> createState() => _AddFeederViewState();
}

class _AddFeederViewState extends State<AddFeederView> {
  var txtControllerTitle = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerCategory = TextEditingController();
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  bool iscameraStatusActive = false;
  bool isloading = false;
  String category = '';

  uploadImagetFirebase() async {
    iscameraStatusActive = true;
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    List<dynamic> followerList =
        await fireStoreUserServices.getFollowerData(uid);
    if (followerList == [] || followerList.isEmpty) {
      followerList = [uid];
    }
    File file = File(imageFromCamera!.path);
    String? fileName = file.path.split('/').last;
    await FirebaseStorage.instance
        .ref()
        .child('/feedPhotos/')
        .child(uid)
        .child(fileName)
        .putFile(File(file.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${file.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child('/feedPhotos/')
            .child(uid)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');

          final collRef = db.collection('feed').doc(uid).collection('story');

          final collRef2 = db.collection('story');
          final collRef3 = db.collection('user');
          DocumentReference docReference = collRef.doc();
          DocumentReference docReference2 = collRef2.doc(docReference.id);
          DocumentReference docReference3 = collRef3.doc(uid);

          Map<String, dynamic> data = {
            'id': docReference.id,
            'author': uid,
            'category': [widget.category, 'all'],
            'mediacategory': category,
            'username': username,
            'userimage': avatar,
            'thumbnail': thumbnailName,
            'description': txtControllerDescription.text,
            'mediaFeed': url,
            'followers': followerList,
            'favorites': 0,
            'createdAt': FieldValue.serverTimestamp(),
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
          };

          docReference.set(data);
          docReference2.set(data);
          docReference3.update({'postlength': FieldValue.increment(1)});

          return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
    });
  }

  List<String> imageUrls = [];

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    debugPrint(imageUrls.toString());
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    Reference storageReference =
        FirebaseStorage.instance.ref().child('feedPhotos/${imageData.path}');
    if (isImage == true) {
      UploadTask uploadTask = storageReference.putFile(imageData);
      await uploadTask.whenComplete(() => null);
    }
    if (isVideo == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'video/mp4'));
      await uploadTask.whenComplete(() => null);
    }
    if (isPdf == true) {
      UploadTask uploadTask = storageReference.putFile(
          imageData, SettableMetadata(contentType: 'application/pdf'));
      await uploadTask.whenComplete(() => null);
    }

    return await storageReference.getDownloadURL();
  }

  uploadCloudFirestore() async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    debugPrint('process upload  $isPdf');
    String pdflink = '';
    try {
      if (isPdf == true) {
        pdflink = imageUrls[0];
      }
      List<dynamic> followerList =
          await fireStoreUserServices.getFollowerData(uid);
      final time = DateFormat('hh:mm');
      final date = DateFormat('dd-MM-yyyy');

      final collRef = db.collection('feed').doc(uid).collection('story');

      final collRef2 = db.collection('story');
      DocumentReference docReference = collRef.doc();
      DocumentReference docReference2 = collRef2.doc(docReference.id);

      Map<String, dynamic> data = {
        'id': docReference.id,
        'author': uid,
        'category': [widget.category, 'all'],
        'mediacategory': category,
        'thumbnail': thumbnailName,
        'username': username,
        'userimage': avatar,
        'description': txtControllerDescription.text,
        'mediaFeedList': imageUrls,
        'followers': followerList,
        'favorites': 0,
        'createdAt': FieldValue.serverTimestamp(),
        'date': date.format(DateTime.now()),
        'time': time.format(DateTime.now()),
      };
      Map<String, dynamic> data2 = {
        'id': docReference.id,
        'author': uid,
        'category': [widget.category, 'all'],
        'mediacategory': category,
        'thumbnail': thumbnailName,
        'username': username,
        'userimage': avatar,
        'description': txtControllerDescription.text,
        'pdfdownload': pdflink,
        'followers': followerList,
        'favorites': 0,
        'createdAt': FieldValue.serverTimestamp(),
        'date': date.format(DateTime.now()),
        'time': time.format(DateTime.now()),
      };
      if (isPdf == true) {
        docReference.set(data2);
        docReference2.set(data2);
        debugPrint('sending => $data2');
      } else {
        docReference.set(data);
        docReference2.set(data);
        debugPrint('sending => $data');
      }
      final collRef3 = db.collection('user');
      DocumentReference docReference3 = collRef3.doc(uid);
      docReference3.update({'postlength': FieldValue.increment(1)});
    } catch (e) {
      debugPrint('firestore error : $e');
      return e.toString();
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
    return 'Success';
  }

  uploadMultiBucketFiles() async {
    debugPrint(imagefiles.length.toString());
    try {
      for (int i = 0; i < imagefiles.length; i++) {
        //  File file = File(imageFromCamera!.path);
        //  String? fileName = file.path.split('/').last;
        debugPrint("task done ==> ${imagefiles[i].path}");
        await FirebaseStorage.instance
            .ref()
            .child('/feedPhotos/$i')
            .putFile(File(imagefiles[i].path))
            .then((taskSnapshot) {
          // download url when it is uploaded
          if (taskSnapshot.state == TaskState.success) {
            FirebaseStorage.instance
                .ref()
                .child('/feedPhotos/$i')
                .child(imagefiles[i].path)
                .getDownloadURL()
                .then((url) {
              imageUrls.add(url);
              debugPrint("Here is the URL of Image $url");

              return url;
            }).catchError((onError) {
              debugPrint("Got Error $onError");
            });
          }
          Navigator.pop(context);
        });
      }
      //upload the list of imageUrls to firebase as an array

    } catch (e) {
      debugPrint(e.toString());
    }
  }

  final FeederController feederController = Get.put(FeederController());
  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
        isLoading: isloading,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.category),
          centerTitle: true,
          backgroundColor: ColorName.redprimary,
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: ListView(
            children: [
              isImage == true
                  ? Wrap(
                      children: imagefiles.map((imageone) {
                        return SizedBox(
                            child: Card(
                          child: SizedBox(
                            height: 100,
                            width: 100,
                            child: Image.file(File(imageone.path)),
                          ),
                        ));
                      }).toList(),
                    )
                  : Container(),
              if (_video != null)
                _videoPlayerController!.value.isInitialized
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipRect(
                          child: AspectRatio(
                            aspectRatio:
                                _videoPlayerController!.value.aspectRatio *
                                    16 /
                                    9,
                            child: VideoPlayer(_videoPlayerController!),
                          ),
                        ),
                      )
                    : Container(),
              // image == null
              //     ? Container()
              //     : Padding(
              //         padding: const EdgeInsets.all(4.0),
              //         child: Image.file(image!),
              //       ),
              thumbnailName == null
                  ? Container()
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(thumbnailName!),
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                child: SizedBox(
                  child: TextFormField(
                    controller: txtControllerDescription,
                    // obscureText: txtControllerDescription,
                    keyboardType: TextInputType.text,
                    onChanged: (value) => txtControllerDescription,
                    onSaved: (String? value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "description cannot be empty";
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Masukan Cerita',
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                              onTap: () => showModalBottomSheet(
                                    shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20.0),
                                          topRight: Radius.circular(20.0)),
                                    ),
                                    context: context,
                                    builder: (BuildContext context) {
                                      return showBottomSheet(context);
                                    },
                                  ),
                              child: const Icon(
                                Icons.attachment,
                                color: ColorName.blackgrey,
                                size: 26,
                              )),
                          GestureDetector(
                              onTap: () => getCamera(),
                              child: const Icon(
                                Icons.camera_alt,
                                color: ColorName.blackgrey,
                                size: 26,
                              )),
                        ],
                      ),
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            foregroundColor:
                                MaterialStateProperty.all<Color>(Colors.white),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                ColorName.redprimary),
                            shape: MaterialStateProperty.all<
                                    RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                    side: const BorderSide(
                                        color: ColorName.redprimary)))),
                        onPressed: () async {
                          if (txtControllerDescription.text != '' &&
                              isloading != true) {
                            setState(() {
                              isloading = true;
                            });
                            if (iscameraStatusActive == true) {
                              uploadImagetFirebase();
                            } else {
                              if (imagefiles.length > 5) {
                                Get.snackbar(
                                    'Informasi', 'Batas maksimal foto hanya 5');
                              } else {
                                List<File> imageListData = [];
                                if (isPdf == true) {
                                  imageListData.add(_pdf!);
                                } else {
                                  imageListData = imagefiles
                                      .map<File>((xfile) => File(xfile.path))
                                      .toList();
                                }
                                if (imagefiles == []) {
                                  await uploadCloudFirestore();
                                  setState(() {
                                    isloading = false;
                                  });
                                } else {
                                  imageUrls = await uploadFiles(imageListData);
                                  debugPrint(
                                      'image file $imagefiles image url : ${imageUrls.toString()}');
                                  await uploadCloudFirestore();
                                  setState(() {
                                    isloading = false;
                                  });
                                }
                              }
                            }
    
                            // ignore: use_build_context_synchronously
                            Navigator.pop(context);
                          } else {}
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Kirim Cerita".toUpperCase(),
                                  style: const TextStyle(fontSize: 14)),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final ImagePicker imgpicker = ImagePicker();
  VideoPlayerController? _videoPlayerController;
  List<XFile> imagefiles = [];
  File? _video;
  File? _pdf;
  String? thumnail;
  String? thumbnailName;
  bool isImage = false;
  bool isVideo = false;
  bool isPdf = false;

  getFileImages() async {
    isImage = true;
    isVideo = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickMultiImage();
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != [] || pickedfiles.isNotEmpty) {
        imagefiles = pickedfiles;
        thumnail = pickedfiles[0].path;
        setState(() {
          category = 'image';
        });
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFilePdf() async {
    isImage = false;
    isVideo = false;
    isPdf = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['doc', 'pdf', 'xls', 'xlsx'],
    );
    try {
      if (result != null) {
        _pdf = File(result.files.single.path!);
        int sizeInBytes = _pdf!.lengthSync();
        double sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb < 10) {
          PlatformFile file = result.files.first;
          thumbnailName = file.name;
          setState(() {
            category = 'pdf';
          });
          // This file is Longer the
        } else {
          debugPrint('file too big');
          isPdf = false;
          Get.snackbar('File terlalu besar', 'ukuran file harus dibawah 10mb');
        }
      } else {
        // User canceled the picker
      }
      setState(() {});
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  getFileVideo() async {
    isVideo = true;
    isImage = false;
    isPdf = false;
    try {
      var pickedfiles = await imgpicker.pickVideo(source: ImageSource.gallery);

      // thumnail
      final String? _path = await VideoThumbnail.thumbnailFile(
        video: pickedfiles!.path,
        thumbnailPath: (await getTemporaryDirectory()).path,

        /// path_provider
        imageFormat: ImageFormat.PNG,
        //maxHeight: 120,
        quality: 10,
      );
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != null) {
        _video = File(pickedfiles.path);
        _videoPlayerController = VideoPlayerController.file(_video!)
          ..initialize().then((_) {
            setState(() {});
            _videoPlayerController!.play();
          });
        thumnail = _path;
        imagefiles.add(pickedfiles);
        setState(() {
          category = 'video';
        });
      } else {
        debugPrint("No image is selected.");
      }
    } catch (e) {
      debugPrint("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
  }

  File? image;
  XFile? imageFromCamera;

  Future getCamera() async {
    imagefiles = [];
    final box = GetStorage();
    //final ImagePicker picker = ImagePicker();
    imageFromCamera = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 20);

    image = File(imageFromCamera!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    //final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        isImage = true;
        isVideo = false;
        isPdf = false;
        imagefiles.add(imageFromCamera!);
        image = File(imageFromCamera!.path);
        feederController.phoneCtrl.value.text = fileName;
        // getting a directory path for saving
        final String path = image.toString();
        box.write('photosstory', imageFromCamera.toString());
        debugPrint('clog ==> ${path.toString()}');
        category = 'image';
      });
    } else {
      setState(() {
        feederController.phoneCtrl.value.text = 'Ukuran photo belum sesuai';
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
    // ignore: use_build_context_synchronously
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () {
              getCamera();
              Navigator.pop(context);
            },
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt,
                    size: 60,
                    semanticLabel: 'kamera',
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () => getFileImages(),
            child: const Card(
                color: ColorName.carnationPink,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.photo_album,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () => getFilePdf(),
            child: const Card(
                color: ColorName.downy,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.file_copy,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
