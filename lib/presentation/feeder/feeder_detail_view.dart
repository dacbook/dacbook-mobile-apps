import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_plus/flutter_swiper_plus.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/feed/feed_model.dart';
import '../../data/remote/firestore/firestore_feed_services.dart';
import '../../gen/colors.gen.dart';
import 'feeder_comment_view.dart';
import 'feeder_controller.dart';
import 'feeder_view.dart';

class FeederDetailView extends StatefulWidget {
  const FeederDetailView(
      {super.key, required this.feedModel, required this.islikes});
  final FeedModel feedModel;
  final bool islikes;
  @override
  State<FeederDetailView> createState() => _FeederDetailViewState();
}

class _FeederDetailViewState extends State<FeederDetailView> {
  var txtControllerComment = TextEditingController();
  final FireStoreFeedServices fireStoreFeedServices = FireStoreFeedServices();
  final boxStorage = BoxStorage();
  final FeederController feedController = Get.put(FeederController());
  bool islikes = false;
  int countKeyboardComment = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorName.redprimary,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
            GestureDetector(
              onTap: (){
                fireStoreFeedServices.deleteFeed(widget.feedModel);
                Navigator.pop(context);
              },
              child: const Icon(Icons.delete, color: ColorName.whiteprimary,))
          ],)
        ),
        body: StreamBuilder(
            // Reading Items form our Database Using the StreamBuilder widget
            stream:
                fireStoreFeedServices.getFeedStoriesDetail(widget.feedModel.id),
            builder: (context, snapshot) {
              debugPrint('detail feed snap : ${snapshot.data}');
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const SizedBox(
                    height: 30, width: 30, child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.none) {
                return const Text('Server not found - Error 500');
              }

              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasData) {
                FeedModel feedData = snapshot.data!;
                islikes = feedData.followersfavorites
                    .contains(boxStorage.getUserId());
                     String jumlahkomentar = '';
              if(feedData.commentcount < 1000){
                jumlahkomentar = feedData.commentcount.toString();
              } else if(feedData.commentcount >= 5000){
                jumlahkomentar = '5rb';
              } 
               else if(feedData.commentcount > 4000){
                jumlahkomentar = '4rb';
              } else if(feedData.commentcount > 3000){
                jumlahkomentar = '3rb';
              } else if(feedData.commentcount > 2000){
                jumlahkomentar = '2rb';
              } else if(feedData.commentcount > 1000){
                jumlahkomentar = '1rb';
              }
                debugPrint(
                    'is like detail feed : $islikes - ${feedData.favorites}');
                return ListView(children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 15,
                    ),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                  height: 50,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: CachedNetworkImage(
                                      imageUrl: feedData.userimage,
                                      fit: BoxFit.fitHeight,
                                      width: 50,
                                      placeholder: (context, url) {
                                        return Image.network(
                                          url,
                                          fit: BoxFit.fitHeight,
                                        );
                                      },
                                      errorWidget: (context, url, error) {
                                        return const FaIcon(
                                            FontAwesomeIcons.camera);
                                      },
                                    ),
                                  )),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      feedData.username,
                                      style: const TextStyle(
                                        fontSize: 18,
                                      ),
                                    ),
                                    Text(
                                      'Dibagikan pada ${feedData.date} ',
                                      style: const TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          feedData.mediacategory == 'pdf'
                              ? GestureDetector(
                                  onTap: () {
                                    launchUrlWeb(feedData.pdfdownload);
                                  },
                                  child: Column(children: [
                                    Container(
                                      height: 60,
                                      color:
                                          ColorName.redprimary.withOpacity(0.6),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              feedData.thumbnail,
                                              style: const TextStyle(
                                                  color:
                                                      ColorName.whiteprimary),
                                            ),
                                            const Icon(Icons.download)
                                          ],
                                        ),
                                      ),
                                    )
                                  ]),
                                )
                              : Container(),
                          feedData.mediaFeedList.isNotEmpty
                              ? SizedBox(
                                  height: 250,
                                  child: feedData.mediaFeedList.length > 1
                                      ? Swiper(
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: CachedNetworkImage(
                                                imageUrl: feedData
                                                    .mediaFeedList[index],
                                                fit: BoxFit.fitHeight,
                                                width: Get.width * 0.94,
                                                placeholder: (context, url) {
                                                  return Image.network(
                                                    url,
                                                    fit: BoxFit.fitHeight,
                                                  );
                                                },
                                                errorWidget:
                                                    (context, url, error) {
                                                  return const FaIcon(
                                                      FontAwesomeIcons.camera);
                                                },
                                              ),
                                            );
                                          },
                                          indicatorLayout:
                                              PageIndicatorLayout.COLOR,
                                          autoplay: false,
                                          itemCount:
                                              feedData.mediaFeedList.length,
                                          pagination: const SwiperPagination(),
                                          //control: const SwiperControl(),
                                        )
                                      : ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: CachedNetworkImage(
                                            imageUrl: feedData.mediaFeedList[0],
                                            fit: BoxFit.cover,
                                            width: Get.width * 0.94,
                                            placeholder: (context, url) {
                                              return Image.network(
                                                url,
                                                fit: BoxFit.cover,
                                              );
                                            },
                                            errorWidget: (context, url, error) {
                                              return const FaIcon(
                                                  FontAwesomeIcons.camera);
                                            },
                                          ),
                                        ),
                                )
                              : Container(),
                          const SizedBox(height: 5),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                feedData.mediaFeedList.isEmpty
                                    ? ReadMoreText(
                                        '${feedData.username} ${feedData.description}',
                                        trimLines: 2,
                                        colorClickableText:
                                            ColorName.redprimary,
                                        trimMode: TrimMode.Line,
                                        trimCollapsedText: 'Selengkapnya',
                                        trimExpandedText: 'Lebih sedikit',
                                        moreStyle: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      )
                                    : Container(),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            feedController
                                                .addLikefeeder(feedData);

                                            debugPrint('is likess : $islikes');
                                          },
                                          child: islikes == true
                                              ? const FaIcon(
                                                  FontAwesomeIcons
                                                      .heartCircleCheck,
                                                  color: ColorName.redprimary)
                                              : const FaIcon(
                                                  FontAwesomeIcons.heart,
                                                  color: ColorName.redprimary),
                                        ),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        Row(
                                          children: [
                                            GestureDetector(
                                              onTap: () {},
                                              child: const FaIcon(
                                                  FontAwesomeIcons.comment,
                                                  color: ColorName.redprimary),
                                            ),
                                             Padding(
                                        padding: const EdgeInsets.only(left:8.0),
                                        child: feedData.commentcount == 0 ? Container(): Text('$jumlahkomentar menanggapi'),
                                      ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    // const FaIcon(FontAwesomeIcons.bookmark,
                                    //     color: ColorName.redprimary),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text('${feedData.favorites} Suka'),
                              ],
                            ),
                          ),
                          const SizedBox(height: 16),
                          feedData.mediaFeedList.isEmpty
                              ? const SizedBox()
                              : ReadMoreText(
                                  '${feedData.username} ${feedData.description}',
                                  trimLines: 2,
                                  colorClickableText: ColorName.redprimary,
                                  trimMode: TrimMode.Line,
                                  trimCollapsedText: 'Selengkapnya',
                                  trimExpandedText: 'Lebih sedikit',
                                  moreStyle: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                          Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 40.0, left: 10, right: 10),
                                child: SizedBox(
                                  child: TextFormField(
                                    controller: txtControllerComment,
                                    keyboardType: TextInputType.multiline,
                                    onChanged: (value) {
                                      value.length;
                                      debugPrint(value.length.toString());
                                      countKeyboardComment = value.length;
                                      txtControllerComment;
                                    },
                                    maxLines: null,
                                    maxLength: 1000,
                                    onSaved: (String? value) {},
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "comment cannot be empty";
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Berikan tanggapan kamu',
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    ElevatedButton(
                                        style: ButtonStyle(
                                            foregroundColor:
                                                MaterialStateProperty.all<
                                                    Color>(Colors.white),
                                            backgroundColor:
                                                MaterialStateProperty.all<Color>(
                                                    ColorName.redprimary),
                                            shape: MaterialStateProperty.all<
                                                    RoundedRectangleBorder>(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(8),
                                                    side: const BorderSide(color: ColorName.redprimary)))),
                                        onPressed: () async {
                                          if (countKeyboardComment == 0) {
                                            Get.snackbar('Informasi',
                                                'Komentar masih kosong');
                                          } else {
                                            fireStoreFeedServices
                                                .addCommentFeed(
                                              '${txtControllerComment.text}  ',
                                              widget.feedModel,
                                            );
                                            txtControllerComment.clear();
                                            FocusManager.instance.primaryFocus?.unfocus();
                                          }
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text("Kirim".toUpperCase(),
                                                  style: const TextStyle(
                                                      fontSize: 12)),
                                            ],
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          FeederCommentView(
                            feedId: widget.feedModel.id,
                          )
                        ]),
                  )
                ]);
              }
              return const SizedBox();
            }));
  }
}
