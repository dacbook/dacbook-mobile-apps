import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/feed_comment/feed_comment_model.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:dac_apps/presentation/profile/_profile_edit_avatar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';
import 'package:jiffy/jiffy.dart';

import '../../data/remote/firestore/firestore_feed_services.dart';
import 'feeder_comment_reply_view.dart';

class FeederCommentView extends StatelessWidget {
  const FeederCommentView({super.key, required this.feedId});
  final String feedId;

  @override
  Widget build(BuildContext context) {
    final FireStoreFeedServices fireStoreFeedServices = FireStoreFeedServices();

    return StreamBuilder(
      stream: fireStoreFeedServices.getFeedComment(feedId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        // debugPrint('lenght : ${snapshot.data!.length}');
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(strokeWidth: 1.0));
        }
        if (snapshot.connectionState == ConnectionState.none) {
          return const Text('Server not found - Error 500');
        }
        if (snapshot.connectionState == ConnectionState.done) {
          if (!snapshot.hasData ||
              snapshot.data.length == 0 ||
              snapshot.data == null) {
            return SizedBox(
              height: Get.height * 0.4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[Text('Tidak ada data')],
              ),
            );
          }
        }
        return ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              FeedCommentModel feedCommentModel = snapshot.data![index];
              debugPrint('followers => ${feedCommentModel.id.toString()}');
              String uid = boxStorage.getUserId();
              var timeago =
                  Jiffy(feedCommentModel.createdAt).fromNow().toString();
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    leading: SizedBox(
                        height: 50,
                        width: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: CachedNetworkImage(
                            imageUrl: feedCommentModel.avatar,
                            fit: BoxFit.fitHeight,
                            width: Get.width * 0.94,
                            placeholder: (context, url) {
                              return Image.network(
                                url,
                                fit: BoxFit.fitHeight,
                              );
                            },
                            errorWidget: (context, url, error) {
                              return const FaIcon(FontAwesomeIcons.camera);
                            },
                          ),
                        )),
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          timeago,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              color: Colors.black54,
                              fontSize: 12,
                              fontWeight: FontWeight.w400),
                        ),
                         const SizedBox(
                          height: 5,
                        ),
                        
                        Text(
                          feedCommentModel.username,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        ),
                      const SizedBox(height: 10,),
                      ],
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ReadMoreText(
                          feedCommentModel.comment,
                          trimLines: 2,
                          colorClickableText: ColorName.redprimary,
                          trimMode: TrimMode.Line,
                          trimCollapsedText: 'Selengkapnya',
                          trimExpandedText: 'Lebih sedikit',
                          moreStyle: const TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                       feedCommentModel.countComment == 0 ? GestureDetector(
                           onTap: () => showModalBottomSheet(
                            isScrollControlled: true,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(20.0),
                                        topRight: Radius.circular(20.0)),
                                  ),
                                  context: context,
                                  builder: (BuildContext context) {
                                    return showBottomSheet(context, feedId, feedCommentModel);
                                  },
                                ),
                          child: const Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Text(
                              'Balas',
                              style: TextStyle(color: Colors.blue),
                            ),
                          ),
                        ) : GestureDetector(
                           onTap: () => Get.to(FeederCommentReplyView(feedId: feedId, commentId: feedCommentModel.commentid,  feedcommentmodel: feedCommentModel,)),
                          child:  Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              '${feedCommentModel.countComment.toString()} Balasan ',
                              style: const TextStyle(color: Colors.blue),
                            ),
                          ),
                        ) , const SizedBox(height: 10,),
                      ],
                    ),
                    trailing: feedCommentModel.uid == uid ? GestureDetector(
                      onTap: (){
                       showAlertDialog(context, feedCommentModel);
                      },
                      child: const Icon(Icons.delete)) : const SizedBox(),
                  ),
                  const Divider(),
                ],
              );
            });
      },
    );
  }
}

 showAlertDialog(BuildContext context, FeedCommentModel feedCommentModel) {
    // set up the button
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () {
         firestorefeedservices.deleteComment(feedCommentModel);
         Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Informasi"),
      content: const Text("Apakah ingin menghapus komentar ini ?"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
     // barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return  alert;
      },
    );
  }

final db = FirebaseFirestore.instance;
final firestorefeedservices = FireStoreFeedServices();
String? reply;
showBottomSheet(
    BuildContext context, String feedId, FeedCommentModel feedCommentModel) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: MediaQuery.of(context).viewInsets,
    child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.7,
            child: TextField(
              keyboardType: TextInputType.text,
              maxLength: 100,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText: 'balas komentar',
                hintText: 'Masukan balasan',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                reply = val;
              },
            ),
          ),
          TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ColorName.redprimary),
              ),
              onPressed: () {
                if (reply == null && reply == '') {
                  Navigator.pop(context);
                } else {
                  firestorefeedservices.addCommentFeedReply(feedId, feedCommentModel.id, reply!, feedCommentModel);
                  reply = null;
                   Navigator.pop(context);
                }
              },
              child: const Text('Balas', style: TextStyle(color: Colors.white))),
        ],
      ),
    ),
  );
}
