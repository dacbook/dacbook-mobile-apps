import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/feed/feed_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_plus/flutter_swiper_plus.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
import 'package:readmore/readmore.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/remote/firestore/firestore_feed_services.dart';
import '../../gen/colors.gen.dart';
import '../login/login_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';
import 'feeder_controller.dart';
import 'feeder_detail_view.dart';

Future<void> launchUrlWeb(String url) async {
  debugPrint('url launch web : $url');
  final Uri urlweb = Uri.parse(url);
  if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
    throw 'Could not launch $url';
  }
}

class FeederView extends StatelessWidget {
  const FeederView({super.key});

  @override
  Widget build(BuildContext context) {
    final BoxStorage boxStorage = BoxStorage();
    final LoginController loginController = Get.put(LoginController());
    final FeederController feedController = Get.put(FeederController());
    final FireStoreFeedServices fireStoreFeedServices = FireStoreFeedServices();
    bool islikes = false;

     List<String> masterReport = [
    '1. Blokir konten yang berpotensi melanggar',
    '2. Blokir pengguna untuk potensi pelanggaran',
    '3. Melaporkan konten pengguna yang melanggar',
    '4. Melaporkan konten yang melanggar',
    '5. Ini adalah spam',
    '6. Saya hanya tidak menyukainya',
    '7. Kekerasan atau organisasi berbahaya',
    '8. Informasi palsu',
  ];

  void reportFeed(context) {
    showModalBottomSheet(
        //  isScrollControlled: true,
        context: context,
        builder: (context) {
           
                  return Container(
                    padding: const EdgeInsets.all(8),
                    height: 300,
                    //  height: MediaQuery.of(context).viewInsets.bottom,
                    alignment: Alignment.center,
                    child: ListView.builder(
                        itemCount:
                            masterReport.length,
                        itemBuilder: (context, index) {
                         
                          return SizedBox(
                            height: 50,
                            child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      masterReport[index]
                                          .toUpperCase(),
                                      style: const TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                   
                                  ],
                                ),
                                onTap: () {
                                 Get.snackbar('Laporan diterima', 'Laporan anda sedang diproses');
                                  Navigator.of(context).pop();
                                }),
                          );
                        }),
                  );
                });
            
  }

    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: fireStoreFeedServices.getRelateFeedfromfollower(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(strokeWidth: 1.0));
        }
        if (snapshot.connectionState == ConnectionState.none) {
          return const Text('Server not found - Error 500');
        }
        if (snapshot.connectionState == ConnectionState.done ||
            !snapshot.hasData ||
            snapshot.data.length == 0 ||
            snapshot.data == []) {
          debugPrint('this is stories = ${snapshot.data}');

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: Get.height * 0.4,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  WidgetTextMont(
                    'Tidak ada cerita',
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                    textColor: Colors.grey[600]!,
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  WidgetTextMont(
                    'Buatlah sebuah cerita agar rekan alumni dapat mengetahuinya',
                    fontSize: 14,
                    textAlign: TextAlign.center,
                    textColor: Colors.grey[700]!,
                  ),
                ],
              ),
            ),
          );
        } else {
          return ListView.builder(
            // itemExtent: snapshot.data?.docs.length,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: snapshot.data?.length,
            itemBuilder: (context, int index) {
              String localuid = boxStorage.getUserId();
              FeedModel feedModel = snapshot.data[index];
              islikes = feedModel.followersfavorites.contains(localuid);
              String jumlahkomentar = '';
              if(feedModel.commentcount < 1000){
                jumlahkomentar = feedModel.commentcount.toString();
              } else if(feedModel.commentcount >= 5000){
                jumlahkomentar = '5rb';
              } 
               else if(feedModel.commentcount > 4000){
                jumlahkomentar = '4rb';
              } else if(feedModel.commentcount > 3000){
                jumlahkomentar = '3rb';
              } else if(feedModel.commentcount > 2000){
                jumlahkomentar = '2rb';
              } else if(feedModel.commentcount > 1000){
                jumlahkomentar = '1rb';
              }
              debugPrint('clog => ${feedModel.id}');
              //listItem = documentSnapshot['media'];

              return Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                SizedBox(
                                    height: 50,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(5),
                                      child: CachedNetworkImage(
                                        imageUrl: feedModel.userimage,
                                        fit: BoxFit.fitHeight,
                                        width: 50,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            url,
                                            fit: BoxFit.fitHeight,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const FaIcon(
                                              FontAwesomeIcons.camera);
                                        },
                                      ),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                            width: Get.width * .6,
                                            child: Text(
                                              feedModel.username,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(color: ColorName.blackprimary),
                                            ),
                                          ),
                                      Text(
                                        'Dibagikan pada ${feedModel.date} ',
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                           GestureDetector(
                            onTap: () => reportFeed(context),
                            child:const Padding(
                              padding:  EdgeInsets.only(right: 10.0),
                              child:  Text('Laporkan', style: TextStyle(fontWeight: FontWeight.bold),),
                            ))
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        feedModel.mediacategory == 'pdf'
                            ? GestureDetector(
                                onTap: () {
                                    islikes = feedModel.followersfavorites
                                      .contains(localuid);
                                  Get.to(
                                    FeederDetailView(
                                      feedModel: feedModel,
                                      islikes: islikes,
                                    ),
                                  );
                                   launchUrlWeb(
                                                feedModel.pdfdownload);
                                },
                                child: Column(children: [
                                  Container(
                                    height: 60,
                                    color: ColorName.redprimary.withOpacity(0.6),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          SizedBox(
                                            width: Get.width * .8,
                                            child: Text(
                                              feedModel.thumbnail,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(color: ColorName.whiteprimary),
                                            ),
                                          ),
                                          const Icon(Icons.download)
                                        ],
                                      ),
                                    ),
                                  )
                                ]),
                              )
                            : Container(),
                        feedModel.mediacategory == 'image'
                            ? GestureDetector(
                                onTap: (() {
                                  islikes = feedModel.followersfavorites
                                      .contains(localuid);
                                  Get.to(
                                    FeederDetailView(
                                      feedModel: feedModel,
                                      islikes: islikes,
                                    ),
                                  );
                                }),
                                child: feedModel.mediaFeedList.isNotEmpty
                                    ? SizedBox(
                                        height: 250,
                                        child: feedModel.mediaFeedList.length >
                                                1
                                            ? Swiper(
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    child: CachedNetworkImage(
                                                      imageUrl: feedModel
                                                          .mediaFeedList[index],
                                                      fit: BoxFit.fitHeight,
                                                      width: Get.width * 0.94,
                                                      placeholder:
                                                          (context, url) {
                                                        return Image.network(
                                                          url,
                                                          fit: BoxFit.fitHeight,
                                                        );
                                                      },
                                                      errorWidget: (context,
                                                          url, error) {
                                                        return const FaIcon(
                                                            FontAwesomeIcons
                                                                .camera);
                                                      },
                                                    ),
                                                  );
                                                },
                                                indicatorLayout:
                                                    PageIndicatorLayout.COLOR,
                                                autoplay: false,
                                                itemCount: feedModel
                                                    .mediaFeedList.length,
                                                pagination:
                                                    const SwiperPagination(),
                                                //control: const SwiperControl(),
                                              )
                                            : ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                child: CachedNetworkImage(
                                                  imageUrl: feedModel
                                                      .mediaFeedList[0],
                                                  fit: BoxFit.cover,
                                                  width: Get.width * 0.94,
                                                  placeholder: (context, url) {
                                                    return Image.network(
                                                      url,
                                                      fit: BoxFit.cover,
                                                    );
                                                  },
                                                  errorWidget:
                                                      (context, url, error) {
                                                    return const FaIcon(
                                                        FontAwesomeIcons
                                                            .camera);
                                                  },
                                                ),
                                              ),
                                      )
                                    : Container(),
                              )
                            : Container(),
                        const SizedBox(height: 5),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          feedModel.mediaFeedList.isEmpty
                              ? ReadMoreText(
                                  ' ${feedModel.description}',
                                  trimLines: 2,
                                  colorClickableText: ColorName.redprimary,
                                  trimMode: TrimMode.Line,
                                  trimCollapsedText: 'Selengkapnya',
                                  trimExpandedText: 'Lebih sedikit',
                                  moreStyle: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                )
                              : Container(),
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: (() async {
                                      islikes = await feedController
                                          .addLikefeeder(feedModel);
                                      debugPrint('is likes = $islikes');
                                    }),
                                    child: islikes == true
                                        ? const FaIcon(
                                            FontAwesomeIcons.heartCircleCheck,
                                            color: ColorName.redprimary)
                                        : const FaIcon(FontAwesomeIcons.heart,
                                            color: ColorName.redprimary),
                                  ),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Row(
                                    children: [
                                      GestureDetector(
                                        onTap: (() {
                                          String uid = boxStorage.getUserId();
                                          bool islikes = feedModel
                                              .followersfavorites
                                              .contains(uid);
                                          Get.to(FeederDetailView(
                                            feedModel: feedModel,
                                            islikes: islikes,
                                          ));
                                        }),
                                        child: const FaIcon(
                                            FontAwesomeIcons.comment,
                                            color: ColorName.redprimary),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left:8.0),
                                        child: feedModel.commentcount == 0 ? Container(): Text('$jumlahkomentar menanggapi'),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                             const SizedBox.shrink()
                              // const FaIcon(FontAwesomeIcons.bookmark,
                              //     color: ColorName.redprimary),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text('${feedModel.favorites.toString()} Suka'),
                        ],
                      ),
                    ),
                    const SizedBox(height: 16),
                    feedModel.mediaFeedList.isEmpty
                        ? const SizedBox()
                        : ReadMoreText(
                            '${feedModel.username} ${feedModel.description}',
                            trimLines: 2,
                            colorClickableText: ColorName.redprimary,
                            trimMode: TrimMode.Line,
                            trimCollapsedText: 'Selengkapnya',
                            trimExpandedText: 'Lebih sedikit',
                            moreStyle: const TextStyle(
                                fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                  ],
                ),
              );
            },
          );
        }
      },
    );

    
  }

 
}

class Photos {
  String key;
  String url;

  Photos(
    this.key,
    this.url,
  );

  @override
  String toString() {
    return '{ $key - $url}';
  }
}
