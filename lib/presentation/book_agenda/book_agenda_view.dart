import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/feed/feed_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../gen/colors.gen.dart';
import '../feeder/feeder_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';
import 'book_agenda_detail_view.dart';

final db = FirebaseFirestore.instance;

class BookAgendaView extends StatelessWidget {
  const BookAgendaView(
      {super.key, required this.bookid, required this.categoryKegiatan, required this.isadmin});
  final String bookid;
  final String categoryKegiatan;
  final bool isadmin;
  @override
  Widget build(BuildContext context) {
    final BoxStorage boxStorage = BoxStorage();
    final FeederController feedController = Get.put(FeederController());
    final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
    bool islikes = false;
    return Scaffold(
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('bookagenda')
            .where('bookid', isEqualTo: bookid)
            .where('categoryKegiatan', isEqualTo: categoryKegiatan)
            .orderBy('timestamp', descending: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const SizedBox(
                width: 20,
                height: 20,
                child: CircularProgressIndicator(strokeWidth: 1.0));
          }
          if (snapshot.connectionState == ConnectionState.none) {
            return const Text('Server not found - Error 500');
          }
          if (snapshot.connectionState == ConnectionState.done ||
              !snapshot.hasData ||
              snapshot.data.docs.length == 0 ||
              snapshot.data == [] ||
              snapshot.data.docs == null || snapshot.data.docs == []) {
            debugPrint('agenda = ${snapshot.data.docs.length}');

            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: Get.height * 0.3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    WidgetTextMont(
                      'Tidak ada $categoryKegiatan',
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      textColor: Colors.grey[600]!,
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    WidgetTextMont(
                      'Buatlah sebuah $categoryKegiatan agar rekan alumni dapat mengetahui',
                      fontSize: 14,
                      textAlign: TextAlign.center,
                      textColor: Colors.grey[700]!,
                    ),
                  ],
                ),
              ),
            );
          } else {
            var feedlist = snapshot.data.docs
                .map((e) => FeedModel.fromJson(e.data(), e.id))
                .toList();
              debugPrint('data agenda : ${feedlist.length}');
            return SizedBox(
              height: 400,
              child: ListView.builder(
                // itemExtent: 200,
                       //  physics: const NeverScrollableScrollPhysics(),
                       physics: const ClampingScrollPhysics(),
                shrinkWrap: false,
                itemCount: feedlist.length,
                itemBuilder: (context, int index) {
                  String localuid = boxStorage.getUserId();
                  FeedModel feedModel = feedlist[index];
            
                  islikes = feedModel.followersfavorites.contains(localuid);
                  debugPrint('clog => ${feedlist.length} -- ');
                  //listItem = documentSnapshot['media'];
            
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 15,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                              feedModel.mediaFeedList.isEmpty ? const SizedBox.shrink() :  SizedBox(
                                    height: 90,
                                    child: GestureDetector(
                                      onTap: () => showImageViewer(context,
                                  Image.network(feedModel.mediaFeedList[0]).image,
                                  useSafeArea: true,
                                  swipeDismissible: true,
                                  doubleTapZoomable: true),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: CachedNetworkImage(
                                          imageUrl: feedModel.mediaFeedList[0],
                                          fit: BoxFit.fitWidth,
                                          width: 120,
                                          placeholder: (context, url) {
                                            return Image.network(
                                              url,
                                              fit: BoxFit.fitHeight,
                                            );
                                          },
                                          errorWidget: (context, url, error) {
                                            return const FaIcon(
                                                FontAwesomeIcons.camera);
                                          },
                                        ),
                                      ),
                                    )),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: SizedBox(
                                    width: 200,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                      const  Text(
                                          'Diselenggarakan pada',
                                          overflow: TextOverflow.ellipsis,
                                          style:  TextStyle(
                                            fontWeight: FontWeight.w300,
                                          ),
                                        ),
                                        Text(
                                          feedModel.tanggalkegiatan,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontWeight: FontWeight.w300,
                                          ),
                                        ),
                                        Text(
                                          feedModel.title,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                        Text(
                                          feedModel.description,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                            fontSize: 18,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      GestureDetector(
                                        onTap: (() async {
                                          islikes = await fireStoreBookServices
                                              .addLikeBookagendaCheck(feedModel);
                                          debugPrint('is likes = $islikes');
                                        }),
                                        child: islikes == true
                                            ? const FaIcon(
                                                FontAwesomeIcons.heartCircleCheck,
                                                color: ColorName.redprimary)
                                            : const FaIcon(FontAwesomeIcons.heart,
                                                color: ColorName.redprimary),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                          '${feedModel.favorites.toString()} Suka'),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      GestureDetector(
                                        onTap: (() {
                                          String uid = boxStorage.getUserId();
                                          bool islikes = feedModel
                                              .followersfavorites
                                              .contains(uid);
                                          Get.to(BookAgendaDetailView(
                                            feedModel: feedModel,
                                            islikes: islikes,
                                            isadmin: isadmin
                                          ));
                                        }),
                                        child: const FaIcon(
                                            FontAwesomeIcons.comment,
                                            color: ColorName.redprimary),
                                      ),
                                    ],
                                  ),
                                  // const FaIcon(FontAwesomeIcons.bookmark,
                                  //     color: ColorName.redprimary),
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                        const Divider(
                          color: ColorName.redprimary,
                          thickness: 2,
                        )
                      ],
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }
}
