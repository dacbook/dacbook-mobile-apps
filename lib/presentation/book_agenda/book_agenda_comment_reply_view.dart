import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:readmore/readmore.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/feed_comment/feed_comment_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../gen/colors.gen.dart';

class BookAgendaCommentReplyView extends StatefulWidget {
  const BookAgendaCommentReplyView(
      {super.key, required this.feedId, required this.feedcommentmodel, required this.commentId,  required this.bookid,});
  final String feedId;
  final String commentId;
    final String bookid;
  final FeedCommentModel feedcommentmodel;

  @override
  State<BookAgendaCommentReplyView> createState() => _BookAgendaCommentReplyViewState();
}

class _BookAgendaCommentReplyViewState extends State<BookAgendaCommentReplyView> {
  
  final boxStorage = BoxStorage();
  
  @override
  Widget build(BuildContext context) {
    final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
    //var timeago = Jiffy(widget.feedcommentmodel.createdAt).fromNow().toString();
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: ListView(
          children: [
            ListTile(
              leading: SizedBox(
                  height: 50,
                  width: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: CachedNetworkImage(
                      imageUrl: widget.feedcommentmodel.avatar,
                      fit: BoxFit.fitHeight,
                      width: Get.width * 0.94,
                      placeholder: (context, url) {
                        return Image.network(
                          url,
                          fit: BoxFit.fitHeight,
                        );
                      },
                      errorWidget: (context, url, error) {
                        return const FaIcon(FontAwesomeIcons.camera);
                      },
                    ),
                  )),
              title: Row(
                children: [
                  Text(
                    widget.feedcommentmodel.username,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  // Text(
                  //   timeago,
                  //   maxLines: 1,
                  //   overflow: TextOverflow.ellipsis,
                  //   style: const TextStyle(
                  //       color: Colors.black54,
                  //       fontSize: 12,
                  //       fontWeight: FontWeight.w400),
                  // ),
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ReadMoreText(
                    widget.feedcommentmodel.comment,
                    trimLines: 2,
                    colorClickableText: ColorName.redprimary,
                    trimMode: TrimMode.Line,
                    trimCollapsedText: 'Selengkapnya',
                    trimExpandedText: 'Lebih sedikit',
                    moreStyle: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                    onTap: () => showModalBottomSheet(
                      isScrollControlled: true,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20.0),
                            topRight: Radius.circular(20.0)),
                      ),
                      context: context,
                      builder: (BuildContext context) {
                        return showBottomSheet(
                            context, widget.feedId, widget.commentId, widget.feedcommentmodel, widget.bookid);
                      },
                    ),
                    child: const Padding(
                      padding: EdgeInsets.only(top: 4.0),
                      child: Text(
                        'Balas',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  )
                ],
              ),
            ),
            const Divider(),
            const Padding(
              padding: EdgeInsets.only(top: 4.0, left: 20),
              child: Text(
                'Balasan',
                style: TextStyle(
                    color: Colors.blueGrey, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: StreamBuilder(
                stream: fireStoreBookServices.getBookAgendaCommentReply(
                    widget.feedId, widget.feedcommentmodel.id),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  // debugPrint('lenght : ${snapshot.data!.length}');
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(strokeWidth: 1.0));
                  }
                  if (snapshot.connectionState == ConnectionState.none) {
                    return const Text('Server not found - Error 500');
                  }
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (!snapshot.hasData ||
                        snapshot.data.length == 0 ||
                        snapshot.data == null) {
                      return SizedBox(
                        height: Get.height * 0.4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const <Widget>[Text('Tidak ada data')],
                        ),
                      );
                    }
                  }
                  return ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        FeedCommentModel feedCommentModel = snapshot.data![index];
                        debugPrint(
                            'followers => ${feedCommentModel.id.toString()}');
                             String uid = boxStorage.getUserId();
                        // var timeago = Jiffy(feedCommentModel.createdAt)
                        //     .fromNow()
                        //     .toString();
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListTile(
                              leading: SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: CachedNetworkImage(
                                      imageUrl: feedCommentModel.avatar,
                                      fit: BoxFit.fitHeight,
                                      width: Get.width * 0.94,
                                      placeholder: (context, url) {
                                        return Image.network(
                                          url,
                                          fit: BoxFit.fitHeight,
                                        );
                                      },
                                      errorWidget: (context, url, error) {
                                        return const FaIcon(
                                            FontAwesomeIcons.camera);
                                      },
                                    ),
                                  )),
                              title: Row(
                                children: [
                                  Text(
                                    feedCommentModel.username,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  // Text(
                                  //   timeago,
                                  //   maxLines: 1,
                                  //   overflow: TextOverflow.ellipsis,
                                  //   style: const TextStyle(
                                  //       color: Colors.black54,
                                  //       fontSize: 12,
                                  //       fontWeight: FontWeight.w400),
                                  // ),
                                ],
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ReadMoreText(
                                    '${feedCommentModel.authorName} , ${feedCommentModel.reply}',
                                    trimLines: 2,
                                    colorClickableText: ColorName.redprimary,
                                    trimMode: TrimMode.Line,
                                    trimCollapsedText: 'Selengkapnya',
                                    trimExpandedText: 'Lebih sedikit',
                                    moreStyle: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  GestureDetector(
                                    onTap: () => showModalBottomSheet(
                                      isScrollControlled: true,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20.0),
                                            topRight: Radius.circular(20.0)),
                                      ),
                                      context: context,
                                      builder: (BuildContext context) {
                                        return showBottomSheet(context,
                                            feedCommentModel.feedid, feedCommentModel.commentid, feedCommentModel, widget.bookid);
                                      },
                                    ),
                                    child: const Padding(
                                      padding: EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        'Balas',
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              trailing: feedCommentModel.uid == uid ? GestureDetector(
                      onTap: (){
                        fireStoreBookServices.deleteCommentaReplyBookAgenda(feedCommentModel);
                      },
                      child: const Icon(Icons.delete)) : const SizedBox(),
                  
                            ),
                            const Divider(),
                          ],
                        );
                      });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final db = FirebaseFirestore.instance;
final fireStoreBookServices = FireStoreBookServices();
String? reply;
showBottomSheet(
    BuildContext context, String feedId, String commentid, FeedCommentModel feedCommentModel, String bookid) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: MediaQuery.of(context).viewInsets,
    child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.7,
            child: TextField(
              keyboardType: TextInputType.text,
              maxLength: 100,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText: 'balas komentar',
                hintText: 'Masukan balasan',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                reply = val;
              },
            ),
          ),
          TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ColorName.redprimary),
              ),
              onPressed: () {
                if (reply == null && reply == '') {
                  Navigator.pop(context);
                } else {
                  fireStoreBookServices.addCommentBookAgendaReply(
                      feedId, commentid, reply!, feedCommentModel, bookid);
                  reply = null;
                  Navigator.pop(context);
                }
              },
              child:
                  const Text('Balas', style: TextStyle(color: Colors.white))),
        ],
      ),
    ),
  );
}
