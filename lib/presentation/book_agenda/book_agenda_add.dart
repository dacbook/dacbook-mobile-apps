import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
String? value;
String? seletectedCategory;

class BookAgendaAddView extends StatefulWidget {
  const BookAgendaAddView(
      {super.key, required this.bookmodel, required this.categoryKegiatan});
  final BookModel bookmodel;
  final String categoryKegiatan;
  @override
  State<BookAgendaAddView> createState() => _BookAgendaAddViewState();
}

class _BookAgendaAddViewState extends State<BookAgendaAddView> {
  var txtControllerTitle = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerCategory = TextEditingController();
  var txttanggalkegiatan = TextEditingController();
  var txtketerangankegiatan = TextEditingController();
  final BoxStorage _boxStorage = BoxStorage();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  bool iscameraStatusActive = false;
  bool isloading = false;

  @override
  void initState() {
    super.initState();
    imagefiles = [];
    debugPrint('image files: $imagefiles');
  }

  uploadImagetFirebase() async {
    iscameraStatusActive = true;
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    List<dynamic> followerList =
        await fireStoreBookServices.getBookmemberBuku(widget.bookmodel.id);
    File file = File(imageFromCamera!.path);
    String? fileName = file.path.split('/').last;
    await FirebaseStorage.instance
        .ref()
        .child('/bookagendaphotos/')
        .child(uid)
        .child(fileName)
        .putFile(File(file.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${file.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child('/bookagendaphotos/')
            .child(uid)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final time = DateFormat('hh:mm');
          final date = DateFormat('dd-MM-yyyy');

          final collRef =
              db.collection('bookforumuser').doc(uid).collection('agenda');

          final collRef2 = db.collection('bookagenda');
          DocumentReference docReference = collRef.doc();
          DocumentReference docReference2 = collRef2.doc(docReference.id);
          debugPrint('book id : ${widget.bookmodel.id}');
          debugPrint('upload doc id : ${docReference.id}');
          Map<String, dynamic> data = {
            'id': docReference.id,
            'bookid': widget.bookmodel.id,
            'booklogo': widget.bookmodel.bookimage,
            'author': uid,
            'category': widget.categoryKegiatan,
            'tanggalkegiatan': txttanggalkegiatan.text,
            'keterangankegiatan': txtketerangankegiatan.text,
            'categoryKegiatan': widget.categoryKegiatan,
            'username': username,
            'userimage': avatar,
            'description': txtControllerDescription.text,
            'mediaFeedList': imageUrls,
            'followers': followerList,
            'title': txtControllerTitle.text,
            'bookmasterid': widget.bookmodel.bookmasterid,
            'favorites': 0,
            'timestamp': FieldValue.serverTimestamp(),
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
          };

          docReference.set(data);
          docReference2.set(data);

          return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
      Navigator.pop(context);
    });
  }

  List<String> imageUrls = [];
  String textUploadValidation = '';

  Future<List<String>> uploadFiles(List<File> imagesList) async {
    var imageUrls = await Future.wait(
        imagesList.map((imagesList) => uploadFile(imagesList)));
    print(imageUrls);
    return imageUrls;
  }

  Future<String> uploadFile(File imageData) async {
    Reference storageReference =
        FirebaseStorage.instance.ref().child('bookagenda/${imageData.path}');
    UploadTask uploadTask = storageReference.putFile(imageData);
    await uploadTask.whenComplete(() => null);

    return await storageReference.getDownloadURL();
  }

  uploadCloudFirestore() async {
    iscameraStatusActive = true;
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();

    try {
      List<dynamic> followerList =
          await fireStoreBookServices.getBookmemberBuku(widget.bookmodel.id);
      final time = DateFormat('hh:mm');
      final date = DateFormat('dd-MM-yyyy');

      final collRef =
          db.collection('bookforumuser').doc(uid).collection('agenda');

      final collRef2 = db.collection('bookagenda');
      DocumentReference docReference = collRef.doc();
      DocumentReference docReference2 = collRef2.doc(docReference.id);
      debugPrint('book id : ${widget.bookmodel.id}');
      setState(() {
        textUploadValidation = 'mendistribusikan data buku';
      });
      Map<String, dynamic> data = {
        'id': docReference.id,
        'bookid': widget.bookmodel.id,
        'booklogo': widget.bookmodel.bookimage,
        'author': uid,
        'category': [widget.categoryKegiatan],
        'tanggalkegiatan': txttanggalkegiatan.text,
        'keterangankegiatan': txtketerangankegiatan.text,
        'categoryKegiatan': widget.categoryKegiatan,
        'username': username,
        'userimage': avatar,
        'description': txtControllerDescription.text,
        'mediaFeedList': imageUrls,
        'followers': followerList,
        'title': txtControllerTitle.text,
        'bookmasterid': widget.bookmodel.bookmasterid,
        'favorites': 0,
        'date': date.format(DateTime.now()),
        'timestamp': FieldValue.serverTimestamp(),
        'time': time.format(DateTime.now()),
      };
      debugPrint('upload doc id : ${docReference.id}');

      docReference.set(data);
      docReference2.set(data);
    } catch (e) {
      debugPrint(e.toString());
      return e.toString();
    }
    debugPrint('proses upload selesai');
    // ignore: use_build_context_synchronously
    Navigator.pop(context);
    return 'Success';
  }

  uploadMultiBucketFiles() async {
    debugPrint(imagefiles.length.toString());
    try {
      for (int i = 0; i < imagefiles.length; i++) {
        //  File file = File(imageFromCamera!.path);
        //  String? fileName = file.path.split('/').last;
        debugPrint("task done ==> ${imagefiles[i].path}");
        await FirebaseStorage.instance
            .ref()
            .child('/bookagendaphotos/$i')
            .putFile(File(imagefiles[i].path))
            .then((taskSnapshot) {
          // download url when it is uploaded
          if (taskSnapshot.state == TaskState.success) {
            FirebaseStorage.instance
                .ref()
                .child('/bookagendaphotos/$i')
                .child(imagefiles[i].path)
                .getDownloadURL()
                .then((url) {
              imageUrls.add(url);
              debugPrint("Here is the URL of Image $url");

              return url;
            }).catchError((onError) {
              debugPrint("Got Error $onError");
            });
          }
          Navigator.pop(context);
        });
      }
      //upload the list of imageUrls to firebase as an array
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return isloading == true
        ? Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: ColorName.blackgrey,
                            width: 5.0,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(20),
                        color: ColorName.blackgrey,
                      ),
                      height: 30,
                      width: 200,
                      child: Center(
                          child: Text(
                        textUploadValidation,
                        style: const TextStyle(color: ColorName.whiteprimary),
                      )),
                    ),
                  ),
                  const CircularProgressIndicator(),
                ],
              ),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text(
                  'TAMBAH KEGIATAN ${widget.categoryKegiatan.toUpperCase()}'),
              centerTitle: true,
              backgroundColor: ColorName.redprimary,
            ),
            body: LoadingOverlay(
              isLoading: isloading,
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: ListView(
                  children: [
                    imagefiles == [] || imagefiles.isEmpty
                        ? GestureDetector(
                            onTap: () => getFileImages(),
                            child: const Icon(
                              Icons.camera_alt,
                              size: 200,
                              color: ColorName.blackgrey,
                            ),
                          )
                        : Center(
                            child: Wrap(
                              children: imagefiles.map((imageone) {
                                return SizedBox(
                                    child: Card(
                                  child: SizedBox(
                                    height: 100,
                                    width: 100,
                                    child: Image.file(File(imageone.path)),
                                  ),
                                ));
                              }).toList(),
                            ),
                          ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Tanggal kegiatan ${widget.categoryKegiatan}',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: ColorName.blackprimary
                                        .withOpacity(0.7)),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime(2099),
                                    ).then((date) {
                                      //tambahkan setState dan panggil variabel _dateTime.
                                      setState(() {
                                        txttanggalkegiatan.text =
                                            DateFormat('yyyy-MM-dd')
                                                .format(date!);
                                      });
                                    });
                                  },
                                  child: const Icon(Icons.calendar_month)),
                            ],
                          ),
                          // ignore: unnecessary_null_comparison
                          txttanggalkegiatan != null ||
                                  txttanggalkegiatan.text != ''
                              ? Text(
                                  txttanggalkegiatan.text,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: ColorName.blueprimary
                                          .withOpacity(0.7)),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: SizedBox(
                        child: TextFormField(
                          controller: txtControllerTitle,
                          // obscureText: txtControllerDescription,
                          keyboardType: TextInputType.text,
                          onChanged: (value) => txtControllerTitle,
                          onSaved: (String? value) {},
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "title cannot be empty";
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Masukan Kegiatan',
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: SizedBox(
                        child: TextFormField(
                          controller: txtControllerDescription,
                          // obscureText: txtControllerDescription,
                          keyboardType: TextInputType.text,
                          onChanged: (value) => txtControllerDescription,
                          onSaved: (String? value) {},
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "description cannot be empty";
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Masukan deskripsi kegiatan',
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 20, right: 20),
                      child: SizedBox(
                        child: TextFormField(
                          controller: txtketerangankegiatan,
                          // obscureText: txtControllerDescription,
                          keyboardType: TextInputType.text,
                          onChanged: (value) => txtketerangankegiatan,
                          onSaved: (String? value) {},
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "keterangan cannot be empty";
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Masukan keterangan kegiatan',
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, right: 20, left: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const SizedBox(
                            width: 100,
                          ),
                          ElevatedButton(
                              style: ButtonStyle(
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          ColorName.redprimary),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          side: const BorderSide(
                                              color: ColorName.redprimary)))),
                              onPressed: () async {
                                if (txtControllerDescription.text != '' &&
                                        txtControllerTitle.text != '' ||
                                    seletectedCategory != null &&
                                        isloading != true) {
                                  setState(() {
                                    textUploadValidation = 'mempersiapkan data';
                                    isloading = true;
                                    textUploadValidation = 'membungkus media';
                                  });

                                  debugPrint('proses upload');
                                  List<File> imageListData = imagefiles
                                      .map<File>((xfile) => File(xfile.path))
                                      .toList();
                                  setState(() {
                                    textUploadValidation =
                                        'proses pengiriman media';
                                  });
                                  imageUrls = await uploadFiles(imageListData);

                                  debugPrint(imageUrls.toString());
                                  await uploadCloudFirestore();

                                  //}
                                  setState(() {
                                    textUploadValidation =
                                        'file berhasil terkirim';
                                    isloading = false;
                                  });
                                } else {
                                  Get.snackbar('Information', 'Lengkapi data');
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "Tambah Kegiatan ${widget.categoryKegiatan}"
                                            .toUpperCase(),
                                        style: const TextStyle(fontSize: 12)),
                                  ],
                                ),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  final ImagePicker imgpicker = ImagePicker();
  List<XFile> imagefiles = [];

  getFileImages() async {
    image = null;
    try {
      var pickedfiles = await imgpicker.pickMultiImage();
      //you can use ImageCourse.camera for Camera capture
      if (pickedfiles != null) {
        imagefiles = pickedfiles;
        setState(() {});
      } else {
        print("No image is selected.");
      }
    } catch (e) {
      print("error while picking file.");
    }
    // ignore: use_build_context_synchronously
    //Navigator.pop(context);
  }

  File? image;
  XFile? imageFromCamera;

  Future getGallery2() async {
    imagefiles = [];
    final box = GetStorage();
    //final ImagePicker picker = ImagePicker();
    imageFromCamera =
        await ImagePicker().pickImage(source: ImageSource.gallery);

    image = File(imageFromCamera!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        image = File(imageFromCamera!.path);
        //feederController.phoneCtrl.value.text = fileName;
        // getting a directory path for saving
        final String path = image.toString();
        box.write('photosstory', imageFromCamera.toString());
        debugPrint('clog ==> ${path.toString()}');
      });
    } else {
      setState(() {
        //   feederController.phoneCtrl.value.text = 'Ukuran photo belum sesuai';
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
    // ignore: use_build_context_synchronously
  }

  Future getGallery() async {
    imagefiles = [];
    final box = GetStorage();
    //final ImagePicker picker = ImagePicker();
    imageFromCamera =
        await ImagePicker().pickImage(source: ImageSource.gallery);

    image = File(imageFromCamera!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    // final kb = bytes / 1024;
    // final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      setState(() {
        image = File(imageFromCamera!.path);
        imagefiles.add(imageFromCamera!);
        // feederController.phoneCtrl.value.text = fileName;
        // getting a directory path for saving
        final String path = image.toString();
        box.write('photosstory', imageFromCamera.toString());
        debugPrint('clog ==> ${path.toString()}');
      });
    } else {
      setState(() {
        // feederController.phoneCtrl.value.text = 'Ukuran photo belum sesuai';
        image = null;
        Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.redAccent);
      });
    }
    // ignore: use_build_context_synchronously
    //Navigator.pop(context);
  }

  showBottomSheet(BuildContext context) {
    // Added the isUpdate argument to check if our item has been updated
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          GestureDetector(
            onTap: () => getGallery(),
            child: const Card(
                color: ColorName.bilobaFlower,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.camera_alt,
                    size: 60,
                    semanticLabel: 'kamera',
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            onTap: () => getFileImages(),
            child: const Card(
                color: ColorName.carnationPink,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.file_copy,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
          GestureDetector(
            child: const Card(
                color: ColorName.downy,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.audio_file,
                    size: 60,
                    color: Colors.white,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
