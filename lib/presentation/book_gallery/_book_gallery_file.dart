import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../gen/colors.gen.dart';
import '../profile_medsos/profile_medsos_view.dart';

import 'package:url_launcher/url_launcher.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

Future<void> launchUrlWeb(String url) async {
  final Uri urlweb = Uri.parse(url);
  if (!await launchUrl(urlweb, mode: LaunchMode.externalApplication)) {
    throw 'Could not launch $url';
  }
}

class BookGalleryFile extends StatelessWidget {
  const BookGalleryFile({super.key, required this.bookModel});
  final BookModel bookModel;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db
          .collection('book')
          .doc(bookModel.id)
          .collection('galerycategory')
          .snapshots(),

      builder: (BuildContext context, AsyncSnapshot snapshotCategory) {
        if (!snapshotCategory.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        debugPrint('print => ${bookModel.id} ${snapshotCategory.data.docs}');
        return ListView.builder(
          itemCount: snapshotCategory.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshotCategory =
                snapshotCategory.data.docs[index];
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [ColorName.redprimary, ColorName.redprimary.withOpacity(0.6)])),
                    child: ListTile(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 2.0, vertical: 0.0),
                        title: Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: Text(
                            documentSnapshotCategory['category'],
                            style: const TextStyle(fontWeight: FontWeight.bold, color: ColorName.whiteprimary),
                          ),
                        ),
                        trailing: const Icon(Icons.arrow_forward, color: ColorName.whiteprimary,),
                        onTap: () {
                          Get.to(BookGalleryFilePerCategory(
                            bookModel: bookModel,
                            category: documentSnapshotCategory['category'],
                          ));
                          // Here We Will Add The Update Feature and passed the value 'true' to the is update
                          // feature.
                        }),
                  ),
                  StreamBuilder(
                      // Reading Items form our Database Using the StreamBuilder widget
                      stream: db
                          .collection('bookgallery')
                          .orderBy('date', descending: true)
                          .where('bookid', isEqualTo: bookModel.id)
                          .where('subscription',
                              isEqualTo: documentSnapshotCategory['category'])
                          .where('category',
                              arrayContainsAny: ['file']).snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasError) {
                          return const Center(
                            child: Icon(Icons.error),
                          );
                        }
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }

                        if (snapshot.connectionState == ConnectionState.done ||
                            snapshot.data.docs.length == 0 ||
                            snapshot.data == null ||
                            snapshot.data.docs == null) {
                          return SizedBox(
                            height: 100,
                            child: Center(
                              child: WidgetTextMont(
                                'Tidak ada dokumen kegiatan tersimpan',
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                textColor: Colors.grey[600]!,
                              ),
                            ),
                          );
                        } else {
                          return Padding(
                              padding: EdgeInsets.only(
                                top: ScreenUtil().setHeight(10),
                              ),
                              child: SizedBox(
                                height: 100,
                                child: ListView.builder(
                                    itemCount: snapshot.data.docs.length > 4
                                        ? 4
                                        : snapshot.data.docs.length,
                                    itemBuilder: (context, int index) {
                                      DocumentSnapshot doc =
                                          snapshot.data.docs[index];
                                      return ListTile(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 2.0,
                                                  vertical: 0.0),
                                          title: Text(
                                            doc['thumbnail'],
                                            style: const TextStyle(
                                                fontWeight: FontWeight.w400),
                                          ),
                                          trailing: const Icon(Icons.download),
                                          onTap: () {
                                            launchUrlWeb(
                                                doc['mediaFeedList'][0]);
                                            // Here We Will Add The Update Feature and passed the value 'true' to the is update
                                            // feature.
                                          });
                                    }),
                              ));
                        }
                      })
                ],
              ),
            );
          },
        );
      },
    );
  }
}

class BookGalleryFilePerCategory extends StatelessWidget {
  const BookGalleryFilePerCategory(
      {super.key, required this.bookModel, required this.category});
  final BookModel bookModel;
  final String category;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(category.toUpperCase()),
          backgroundColor: ColorName.redprimary,
        ),
        body: StreamBuilder(
            // Reading Items form our Database Using the StreamBuilder widget
            stream: db
                .collection('bookgallery')
                .orderBy('date', descending: true)
                .where('bookid', isEqualTo: bookModel.id)
                .where('subscription', isEqualTo: category)
                .where('category', arrayContainsAny: ['file']).snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasError) {
                return const Center(
                  child: Icon(Icons.error),
                );
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.data.docs.length == 0 ||
                  snapshot.data == null ||
                  snapshot.data.docs == null) {
                return SizedBox(
                  height: 100,
                  child: Center(
                    child: WidgetTextMont(
                      'Tidak ada dokumen kegiatan tersimpan',
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      textColor: Colors.grey[600]!,
                    ),
                  ),
                );
              } else {
                return Padding(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(10),
                    ),
                    child: SizedBox(
                      height: Get.height,
                      child: ListView.builder(
                          itemCount: snapshot.data.docs.length,
                          itemBuilder: (context, int index) {
                            DocumentSnapshot doc = snapshot.data.docs[index];
                            return Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: ListTile(
                                  contentPadding: const EdgeInsets.symmetric(
                                      horizontal: 2.0, vertical: 0.0),
                                  title: Text(
                                    doc['thumbnail'],
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w400),
                                  ),
                                  trailing: const Icon(Icons.download),
                                  onTap: () {
                                    // Here We Will Add The Update Feature and passed the value 'true' to the is update
                                    // feature.
                                  }),
                            );
                          }),
                    ));
              }
            }));
  }
}
