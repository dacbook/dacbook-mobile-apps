import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/widgets/video_player.dart';
import '../../data/local/box/box_storage.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class BookGalleryVideo extends StatefulWidget {
    const BookGalleryVideo({super.key, required this.bookModel});
  final BookModel bookModel;

  @override
  State<BookGalleryVideo> createState() => _BookGalleryVideoState();
}

class _BookGalleryVideoState extends State<BookGalleryVideo> {
  String? seletectedCategory;
    final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  
  @override
  Widget build(BuildContext context) {
    
    return ListView(
      children: [
        Container(
            height: 60,
            width: Get.width,
            padding: const EdgeInsets.all(5),
            child: StreamBuilder<QuerySnapshot>(
                stream: db
                    .collection('book')
                    .doc(widget.bookModel.id)
                    .collection('galerycategory')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  return Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text('Kategori Video', style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: const EdgeInsets.all(5),
                          child: DropdownButton(
                            value: seletectedCategory,
                            isDense: true,
                            items:
                                snapshot.data!.docs.map((DocumentSnapshot doc) {
                              return DropdownMenuItem<String>(
                                  value: doc['category'],
                                  child: Text(doc['category']));
                            }).toList(),
                            hint: const Text("Tentukan kategori"),
                            onChanged: (value) {
                             setState(() {
                                seletectedCategory = value;
                                debugPrint('selected kategori : $seletectedCategory');
                             });
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                })),
        SizedBox(
          height: Get.height * .7,
          width: Get.width,
          child: StreamBuilder(
            // Reading Items form our Database Using the StreamBuilder widget
            stream: db
                .collection('bookgallery')
                .orderBy('date', descending: true)
                .where('bookid', isEqualTo: widget.bookModel.id)
                .where('subscription', isEqualTo: seletectedCategory)
                .where('category', arrayContainsAny: ['video']).snapshots(),

            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              debugPrint('print => ${snapshot.data.docs}');

              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: StaggeredGridView.countBuilder(
                  padding: EdgeInsets.zero,
                  crossAxisCount: 4,
                  itemCount: snapshot.data.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
                    return GestureDetector(
                      onTap: () => Get.to(VideoPlayerWidget(
                        url: documentSnapshot['mediaFeedList'][0],
                        time: documentSnapshot['time'],
                        title: documentSnapshot['title'],
                        category: documentSnapshot['subscription'],
                        date: documentSnapshot['date'],
                        deskripsi: documentSnapshot['description'],
                      )),
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5.0)),
                        child: Container(
                            decoration: BoxDecoration(
                              gradient: RadialGradient(
                                  colors: [
                                    Colors.grey.withOpacity(0.3),
                                    Colors.grey.withOpacity(0.7),
                                  ],
                                  center: const Alignment(0, 0),
                                  radius: 0.8,
                                  focal: const Alignment(0, 0),
                                  focalRadius: 0.1),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Image.network(
                                documentSnapshot['thumbnail'],
                                fit: BoxFit.cover,
                              ),
                            )),
                      ),
                    );
                  },
                  staggeredTileBuilder: (int index) =>
                      StaggeredTile.count(2, index.isEven ? 3 : 2),
                  mainAxisSpacing: 4.0,
                  crossAxisSpacing: 4.0,
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
