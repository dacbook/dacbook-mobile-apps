import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../profile_medsos/profile_medsos_view.dart';
import '_book_gallery_foto_detail.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class BookGalleryFoto extends StatelessWidget {
  const BookGalleryFoto({super.key, required this.bookModel});
  final BookModel bookModel;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db
          .collection('book')
          .doc(bookModel.id)
          .collection('galerycategory')
          .snapshots(),

      builder: (BuildContext context, AsyncSnapshot snapshotCategory) {
        if (!snapshotCategory.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        debugPrint('print => ${bookModel.id} ${snapshotCategory.data.docs}');
        return ListView.builder(
          itemCount: snapshotCategory.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshotCategory =
                snapshotCategory.data.docs[index];
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            // top: BorderSide(
                            //   width: 2,
                            //   color: ColorName.redprimary,
                            // ),

                            bottom: BorderSide(
                      width: 2,
                      color: ColorName.redprimary,
                    ))),
                    child: ListTile(
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 2.0, vertical: 0.0),
                        title: Text(
                          documentSnapshotCategory['category'],
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        trailing: const Icon(Icons.arrow_forward),
                        onTap: () {
                          Get.to(BookGalleryFotoPerCategory(
                            bookModel: bookModel,
                            category: documentSnapshotCategory['category'],
                          ));
                          // Here We Will Add The Update Feature and passed the value 'true' to the is update
                          // feature.
                        }),
                  ),
                  StreamBuilder(
                      // Reading Items form our Database Using the StreamBuilder widget
                      stream: db
                          .collection('bookgallery')
                          .orderBy('date', descending: true)
                          .where('bookid', isEqualTo: bookModel.id)
                          .where('subscription',
                              isEqualTo: documentSnapshotCategory['category'])
                          .where('category',
                              arrayContainsAny: ['foto']).snapshots(),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasError) {
                          return const Center(
                            child: Icon(Icons.error),
                          );
                        }
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }

                        if (snapshot.connectionState == ConnectionState.done ||
                            snapshot.data.docs.length == 0 ||
                            snapshot.data == null ||
                            snapshot.data.docs == null) {
                          return WidgetTextMont(
                            'Tidak ada foto kegiatan tersimpan',
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            textColor: Colors.grey[600]!,
                          );
                        } else {
                          return Padding(
                            padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(10),
                            ),
                            child: GridView.builder(
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                padding: EdgeInsets.zero,
                                semanticChildCount: 1,
                                itemCount: snapshot.data.docs.length > 3
                                    ? 3
                                    : snapshot.data.docs.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: ScreenUtil().setWidth(5),
                                  mainAxisSpacing: ScreenUtil().setHeight(5),
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  DocumentSnapshot documentSnapshot =
                                      snapshot.data.docs[index];
                                  return GestureDetector(
                                    onTap: () {
                                      Get.to(BookGalleryFotoDetail(
                                        imgurl:
                                            documentSnapshot['mediaFeedList'],
                                        title: documentSnapshot['title'],
                                        description:
                                            documentSnapshot['description'],
                                      ));
                                    },
                                    child: ClipRRect(
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(5),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: documentSnapshot['thumbnail'],
                                        fit: BoxFit.cover,
                                        placeholder: (context, url) {
                                          return Image.network(
                                            url,
                                            fit: BoxFit.cover,
                                          );
                                        },
                                        errorWidget: (context, url, error) {
                                          return const Icon(Icons.error);
                                        },
                                      ),
                                    ),
                                  );
                                }),
                          );
                        }
                      })
                ],
              ),
            );
          },
        );
      },
    );
  }
}

class BookGalleryFotoPerCategory extends StatelessWidget {
  const BookGalleryFotoPerCategory(
      {super.key, required this.bookModel, required this.category});
  final BookModel bookModel;
  final String category;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(category.toUpperCase()),
        backgroundColor: ColorName.redprimary,
      ),
      body: StreamBuilder(
          // Reading Items form our Database Using the StreamBuilder widget
          stream: db
              .collection('bookgallery')
              .orderBy('date', descending: true)
              .where('bookid', isEqualTo: bookModel.id)
              .where('subscription', isEqualTo: category)
              .where('category', arrayContainsAny: ['foto']).snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Icon(Icons.error),
              );
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (snapshot.connectionState == ConnectionState.done ||
                snapshot.data.docs.length == 0 ||
                snapshot.data == null ||
                snapshot.data.docs == null) {
              return WidgetTextMont(
                'Tidak ada foto kegiatan tersimpan',
                fontWeight: FontWeight.bold,
                fontSize: 14,
                textColor: Colors.grey[600]!,
              );
            } else {
              return Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(10),
                ),
                child: GridView.builder(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    padding: EdgeInsets.zero,
                    semanticChildCount: 1,
                    itemCount: snapshot.data.docs.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: ScreenUtil().setWidth(5),
                      mainAxisSpacing: ScreenUtil().setHeight(5),
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      DocumentSnapshot documentSnapshot =
                          snapshot.data.docs[index];
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            Get.to(BookGalleryFotoDetail(
                              imgurl: documentSnapshot['mediaFeedList'],
                              title: documentSnapshot['title'],
                              description: documentSnapshot['description'],
                            ));
                          },
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(5),
                            ),
                            child: CachedNetworkImage(
                              imageUrl: documentSnapshot['thumbnail'],
                              fit: BoxFit.cover,
                              placeholder: (context, url) {
                                return Image.network(
                                  url,
                                  fit: BoxFit.cover,
                                );
                              },
                              errorWidget: (context, url, error) {
                                return const Icon(Icons.error);
                              },
                            ),
                          ),
                        ),
                      );
                    }),
              );
            }
          }),
    );
  }
}
