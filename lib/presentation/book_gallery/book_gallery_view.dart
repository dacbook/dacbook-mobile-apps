import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/presentation/book_gallery/_book_gallery_file.dart';
import 'package:dac_apps/presentation/book_gallery/_book_gallery_foto.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../gen/colors.gen.dart';
import '_book_gallery_video.dart';
import 'book_gallery_add_view.dart';

 final BoxStorage _boxStorage = BoxStorage();

class BookGalleryView extends StatefulWidget {
  const BookGalleryView({super.key, required this.bookModel});
  final BookModel bookModel;
  @override
  State<BookGalleryView> createState() => _BookGalleryViewState();
}

class _BookGalleryViewState extends State<BookGalleryView>
    with SingleTickerProviderStateMixin {
  TabController? tabController;
  String title = '';

  List<String> checkisadmin = [];
  bool isadmin = false;

  @override
  // ignore: unused_element
  void initState() {
    super.initState();
    title = 'foto';
    checkadminstatus();
    
    tabController = TabController(length: 3, vsync: this);
    debugPrint('index : ${tabController!.length}');
    tabController?.addListener(_handleTabSelection);
  }

  checkadminstatus(){
    checkisadmin = widget.bookModel.bookadmin;
    setState(() {
      isadmin = checkisadmin.contains(_boxStorage.getUserId());
      debugPrint('is admin status : $isadmin');
    });
  }

  @override
  // ignore: unused_element
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

   void _handleTabSelection() {
    if (tabController!.indexIsChanging) {
      switch (tabController!.index) {
        case 0:
         debugPrint('index : 0');
         setState(() {
           title = 'foto';
         });
          break;
        case 1:
          debugPrint('index : 1');
          setState(() {
           title = 'video';
         });
          break;
        case 2:
          debugPrint('index : 2');
          setState(() {
           title = 'file';
         });
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary, actions:  [Padding(
        padding: const EdgeInsets.all(8.0),
        child: isadmin == false ? const SizedBox.shrink(): GestureDetector(
          onTap: (){
              Get.to(() => AddBookGalleryView(
                  bookModel: widget.bookModel,
                ));
          },
          child: SizedBox( 
            height: 50,
            width: 150,
            child: Center(child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text('tambah $title', textAlign: TextAlign.center,),
                const Icon(Icons.add),
              ],
            ))),
        ),
      )],),
      body: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: [
        DefaultTabController(
          length: 3,
          child: Column(
            children: [
              Container(
                color: Colors.white,
                child: TabBar(
                  controller: tabController,
                  labelColor: ColorName.redprimary,
                  unselectedLabelColor: Colors.black,
                  indicatorColor: ColorName.redprimary,
                  indicatorPadding: const EdgeInsets.symmetric(horizontal: 10),
                  labelStyle: const TextStyle(
                    fontSize: 14,
                    fontFamily: 'Roboto',
                  ),
                  tabs: const [
                    Tab(
                      child: Text('Foto'),
                    ),
                    Tab(
                      child: Text("Video"),
                    ),
                    Tab(
                      child: Text("File"),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: Get.height * .8,
                child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  controller: tabController,
                  children: [
                    //KarirUserView(uid: 'Xha4gwjMe5ZfR1SLfR2i'),
                    BookGalleryFoto(
                      bookModel: widget.bookModel,
                    ),
                    BookGalleryVideo(
                      bookModel: widget.bookModel,
                    ),
                    BookGalleryFile(
                      bookModel: widget.bookModel,
                    ),
                   
                  ],
                ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 40,
        )
      ]),
    );
  }
}
