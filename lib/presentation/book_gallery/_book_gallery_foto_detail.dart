import 'package:cached_network_image/cached_network_image.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class BookGalleryFotoDetail extends StatefulWidget {
  const BookGalleryFotoDetail(
      {super.key,
      required this.imgurl,
      required this.title,
      required this.description});
  final List<dynamic> imgurl;
  final String title;
  final String description;
  @override
  State<BookGalleryFotoDetail> createState() => _BookGalleryFotoDetailState();
}

class _BookGalleryFotoDetailState extends State<BookGalleryFotoDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
      ),
      body: ListView(
        children: [
          SizedBox(
            width: 250,
            child: Text(
              widget.title,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.grey[600]!,),
            ),
          ),
          SizedBox(
            width: 250,
            child: Text(
              widget.description,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 16,
              color: Colors.grey[500]!,),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
            ),
            child: GridView.builder(
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                padding: EdgeInsets.zero,
                itemCount: widget.imgurl.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: ScreenUtil().setWidth(5),
                  mainAxisSpacing: ScreenUtil().setHeight(5),
                ),
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () => showImageViewer(context,
                                Image.network(widget.imgurl[index]).image,
                                useSafeArea: true,
                                swipeDismissible: true,
                                doubleTapZoomable: true),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(5),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: widget.imgurl[index],
                        fit: BoxFit.cover,
                        placeholder: (context, url) {
                          return Image.network(
                            url,
                            fit: BoxFit.cover,
                          );
                        },
                        errorWidget: (context, url, error) {
                          return const Icon(Icons.error);
                        },
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
