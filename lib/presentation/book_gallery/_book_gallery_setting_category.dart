import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();
String? category;

class BookGallerySettingCategory extends StatelessWidget {
  const BookGallerySettingCategory({super.key, required this.bookid});
  final String bookid;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Tambahkan Kategori Galeri",
              style: TextStyle(color: ColorName.whiteprimary),
            ),
            GestureDetector(
              onTap: () => showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (context) {
                  return showBottomSheet(context, false, null);
                },
              ),
              child: const Icon(Icons.add),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('book')
            .doc(bookid)
            .collection('galerycategory')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data?.docs.length,
            itemBuilder: (context, int index) {
              DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
              return ListTile(
                // leading: Text(documentSnapshot['tahun']),
                title: Text(documentSnapshot['category']),
                // subtitle: Row(
                //   children: [
                //     Text(documentSnapshot['penghargaan']),
                //   ],
                // ),
                onTap: () {
                  // Here We Will Add The Update Feature and passed the value 'true' to the is update
                  // feature.
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return showBottomSheet(context, true, documentSnapshot);
                    },
                  );
                },
                trailing: IconButton(
                  icon: const Icon(
                    Icons.delete_outline,
                  ),
                  onPressed: () {
                    // Here We Will Add The Delete Feature
                    db
                        .collection('book')
                        .doc(bookid)
                        .collection('galerycategory')
                        .doc(documentSnapshot.id)
                        .delete();
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
  showBottomSheet(
    BuildContext context, bool isUpdate, DocumentSnapshot? documentSnapshot) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: TextField(
              keyboardType: TextInputType.text,
              maxLength: 100,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText: isUpdate ? 'Update kategori' : 'Tambahkan kategori',
                hintText: 'Masukan kategori',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                category = val;
              },
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: Get.width * .7,
            child: TextButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(ColorName.redprimary),
                ),
                onPressed: () {
                  if (category == null) {
                    Get.snackbar('Peringatan', 'Lengkapi data');
                  } else {
                    Map<String, Object> data = {
                      'category': category!,
                    };
                    // Check to see if isUpdate is true then update the value else add the value
                    if (isUpdate) {
                      db
                          .collection('book')
                          .doc(bookid)
                          .collection('galerycategory')
                          .doc(documentSnapshot?.id)
                          .update(data);
                    } else {
                      db
                          .collection('book')
                          .doc(bookid)
                          .collection('galerycategory')
                          .add(data);
                    }
                    Navigator.pop(context);
                  }
                },
                child: isUpdate
                    ? const Text(
                        'UPDATE',
                        style: TextStyle(color: Colors.white),
                      )
                    : const Text('ADD', style: TextStyle(color: Colors.white))),
          ),
          const SizedBox(height: 20),
        ],
      ),
    ),
  );
}
}


