import 'package:dac_apps/presentation/account_menu/privasi_menu_view.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../gen/colors.gen.dart';
import '../account_menu/callcenter_menu_view.dart';
import '../account_menu/daftar_riwayat_hidup_view.dart';
import '../account_menu/umum_menu_view.dart';
import '../profile_account/profile_account_view.dart';

class AccountView extends StatefulWidget {
  const AccountView({super.key});

  @override
  State<AccountView> createState() => _AccountViewState();
}

class _AccountViewState extends State<AccountView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorName.redprimary,
          //automaticallyImplyLeading: false,
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            //ProfileAccountView(),
            Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  //PrivasiMenuView(),
                //  DaftarRiwayatHidupMenuView(),
                  UmumMenuView(),
                  CallcenterMenuView(),
                ])
          ],
        ));
  }
}
