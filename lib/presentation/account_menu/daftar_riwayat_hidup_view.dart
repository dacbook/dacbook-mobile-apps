import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/model/account_menu/account_menu_model.dart';
import '../../gen/colors.gen.dart';
import '../faq/faq_view.dart';
import 'riwayatHidup/_archivment_view.dart';
import 'riwayatHidup/_jabatan_view.dart';
import 'riwayatHidup/_karir_view.dart';
import 'riwayatHidup/_keluarga_view.dart';
import 'riwayatHidup/_pendidikan_view.dart';

class DaftarRiwayatHidupMenuView extends StatefulWidget {
  const DaftarRiwayatHidupMenuView({super.key});

  @override
  State<DaftarRiwayatHidupMenuView> createState() => _DaftarRiwayatHidupMenuViewState();
}

class _DaftarRiwayatHidupMenuViewState extends State<DaftarRiwayatHidupMenuView> {
  List<dynamic> icondata = const [
    FaIcon(
      FontAwesomeIcons.usersBetweenLines,
      color: ColorName.redprimary,
    ),
   
    FaIcon(
      FontAwesomeIcons.usersBetweenLines,
      color: ColorName.redprimary,
    ),
    FaIcon(
      FontAwesomeIcons.usersBetweenLines,
      color: ColorName.redprimary,
    ),
    FaIcon(
      FontAwesomeIcons.usersBetweenLines,
      color: ColorName.redprimary,
    ),
  ];

  List<AccountMenuModel> ctx = const [
    AccountMenuModel(
      id: 1,
      menu: 'Karir',
      label: 'Karir',
      colors: ColorName.redprimary,
    ),
   
    AccountMenuModel(
        id: 3,
        menu: 'Penghargaan',
        label: 'Penghargaan',
        colors: ColorName.redprimary),
    AccountMenuModel(
        id: 4,
        menu: 'Pendidikan',
        label: 'Pendidikan',
        colors: ColorName.redprimary),
    AccountMenuModel(
        id: 5,
        menu: 'Keluarga',
        label: 'Keluarga',
        colors: ColorName.redprimary),
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 18.0, top: 14),
            child: Text(
              'Daftar Riwayat Hidup',
              style: TextStyle(
                  color: ColorName.redprimary,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: ctx.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return SizedBox(
                  width: Get.width,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 60,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0.0),
                            foregroundColor: MaterialStateProperty.all<Color>(
                                Colors.transparent),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.transparent),
                          ),
                          onPressed: () => {
                            if (ctx[index].id == 1)
                              {Get.to(const KarirView())},
                            if (ctx[index].id == 3)
                              {Get.to(const ArchivmentView())},
                            if (ctx[index].id == 4)
                              {Get.to(const PendidikanView())},
                            if (ctx[index].id == 5)
                              {Get.to(const KeluargaView())},
                            
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 60,
                                    child: ElevatedButton(
                                        child: icondata[index],
                                        style: ButtonStyle(
                                          elevation:
                                              MaterialStateProperty.all(0.0),
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.transparent),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.transparent),
                                        ),
                                        onPressed: () => {}),
                                  ),
                                  const SizedBox(
                                    height: 15,
                                  ),
                                  Text(ctx[index].menu,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                          fontSize: 12, color: ColorName.raven))
                                ],
                              ),
                              const FaIcon(
                                FontAwesomeIcons.angleRight,
                                color: ColorName.raven,
                                size: 16,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }
}
