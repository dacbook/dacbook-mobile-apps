import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../data/model/contact/contact_model.dart';
import '../../data/remote/firestore/firestore_contact_services.dart';

class AccountMenuController extends GetxController {
  final FireStoreContactServices fireStoreContactServices =
      FireStoreContactServices();

  var phoneNumber = '081219056496'.obs;
  var email = 'dacilmci@gmail.com'.obs;

  Future<void> callphone(String phone) async {
    final Uri launchUri = Uri.parse('tel:$phone');
    await launchUrl(launchUri);
  }

  Future<void> msgphone(String phone) async {
    final Uri launchUri = Uri.parse('sms:$phone');
    await launchUrl(launchUri);
  }

  Future<void> msgwhatsapp(String whatsapp) async {
    final Uri urlWhatsapp =
        Uri.parse('https://api.whatsapp.com/send?phone=$whatsapp&text=digital alumni');
    if (!await launchUrl(urlWhatsapp, mode: LaunchMode.externalApplication)) {
      throw 'Could not launch $urlWhatsapp';
    }
  }

  Future<void> msgemail(String email) async {
    final Uri launchUri = Uri(
      scheme: 'mailto',
      path: email,
      query: 'subject=Halo Digital Alumni&body=Digital alumni',
    );
    await launchUrl(launchUri);
  }

  Future<void> faq(String email) async {
    
  }
  Future<void> rules(String email) async {
    
  }
  Future<void> term(String email) async {
    
  }
  Future<void> feedback(String email) async {
    
  }

  navCallCenterMenu(int id) async {
    ContactModel contactModel = await fireStoreContactServices.getMcontact();
    switch (id) {
      case 1:
        await callphone(contactModel.phone);
        break;
      case 2:
        await msgphone(contactModel.phone);
        break;
      case 3:
        await msgemail(contactModel.email);
        break;
      case 4:
        await msgwhatsapp(contactModel.whatsapp);
        break;
    }
  }

  navUmumMenu(int id) async {
    ContactModel contactModel = await fireStoreContactServices.getMcontact();
    switch (id) {
      case 1:
        await faq(contactModel.phone);
        break;
      case 2:
        await rules(contactModel.phone);
        break;
      case 3:
        await term(contactModel.email);
        break;
      case 4:
        await feedback(contactModel.whatsapp);
        break;
    }
  }
}
