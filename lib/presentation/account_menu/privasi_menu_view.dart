import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/model/account_menu/account_menu_model.dart';
import '../../gen/colors.gen.dart';
import '../faq/faq_view.dart';
import 'privasi/_address_view.dart';
import 'privasi/_family_view.dart';

class PrivasiMenuView extends StatefulWidget {
  const PrivasiMenuView({super.key});

  @override
  State<PrivasiMenuView> createState() => _PrivasiMenuViewState();
}

class _PrivasiMenuViewState extends State<PrivasiMenuView> {
  List<dynamic> icondata = const [
    FaIcon(
      FontAwesomeIcons.addressBook,
      color: ColorName.purplelow,
    ),
    FaIcon(
      FontAwesomeIcons.bookOpen,
      color: ColorName.purplelow,
    ),
    FaIcon(
      FontAwesomeIcons.fileCircleCheck,
      color: ColorName.purplelow,
    ),
    // FaIcon(
    //   FontAwesomeIcons.envelopeOpenText,
    //   color: ColorName.purplelow,
    // ),
  ];

  List<AccountMenuModel> ctx = const [
    AccountMenuModel(
      id: 1,
      menu: 'Alamat',
      label: 'Alamat',
      colors: ColorName.purplelow,
    ),
    AccountMenuModel(
      id: 2,
      menu: 'Keluarga',
      label: 'Keluarga',
      colors: ColorName.purplelow,
    ),
    AccountMenuModel(
        id: 3,
        menu: 'File Saya',
        label: 'File Saya',
        colors: ColorName.purplelow),
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(left: 18.0, top: 14),
            child: Text(
              'Privasi',
              style: TextStyle(
                  color: ColorName.purplelow,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
          ),
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: ctx.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return SizedBox(
                  width: Get.width,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 60,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0.0),
                            foregroundColor: MaterialStateProperty.all<Color>(
                                Colors.transparent),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.transparent),
                          ),
                          onPressed: () => {
                            
                            if (ctx[index].id == 1)
                              {Get.to(const AddressView())},
                            if (ctx[index].id == 2)
                              {Get.to(const FamilyView())}
                            
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 60,
                                    child: ElevatedButton(
                                        child: icondata[index],
                                        style: ButtonStyle(
                                          elevation:
                                              MaterialStateProperty.all(0.0),
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.transparent),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.transparent),
                                        ),
                                        onPressed: () => {}),
                                  ),
                                  const SizedBox(
                                    height: 15,
                                  ),
                                  Text(ctx[index].menu,
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                          fontSize: 12, color: ColorName.raven))
                                ],
                              ),
                              const FaIcon(
                                FontAwesomeIcons.angleRight,
                                color: ColorName.raven,
                                size: 16,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Divider(
                          thickness: 2,
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }
}
