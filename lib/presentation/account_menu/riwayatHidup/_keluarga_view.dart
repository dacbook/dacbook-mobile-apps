import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../gen/colors.gen.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();
String? tanggallahir;
String? statusdalamkeluarga;
String? nama;
String? deskripsi;
String? pendidikan;
String? pekerjaan;
String? nohandphone;
String? alamat;

class KeluargaView extends StatelessWidget {
  const KeluargaView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Tambahkan Keluarga",
              style: TextStyle(color: ColorName.whiteprimary),
            ),
            GestureDetector(
              onTap: () => showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (context) {
                  return showBottomSheet(context, false, null);
                },
              ),
              child: const Icon(Icons.add),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('user')
            .doc(localdata.getUserId())
            .collection('keluarga')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data?.docs.length,
            itemBuilder: (context, int index) {
              DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListTile(
                  //  leading: Text(documentSnapshot['tahun']),
                  title: Text(documentSnapshot['nama']),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                     const Divider(thickness: 4,),
                      Text('No HP : ${documentSnapshot['nohandphone']}'),
                      // const Text(' s/d '),
                       const SizedBox(height: 10,),
                      Text(
                          'Status keluarga : ${documentSnapshot['statusdalamkeluarga']}'),
                       const SizedBox(height: 10,),
                      Text('Tanggal Lahir : ${documentSnapshot['tanggallahir']}'),
                      const SizedBox(height: 10,),
                      Text('Pendidikan ${documentSnapshot['pendidikan']}'),
                     const SizedBox(height: 10,),
                      Text('Pekerjaan : ${documentSnapshot['pekerjaan']}'),
                    ],
                  ),
                  onTap: () {
                    // Here We Will Add The Update Feature and passed the value 'true' to the is update
                    // feature.
                    showModalBottomSheet(
                      isScrollControlled: true,
                      context: context,
                      builder: (BuildContext context) {
                        return showBottomSheet(context, true, documentSnapshot);
                      },
                    );
                  },
                  trailing: IconButton(
                    icon: const Icon(
                      Icons.delete_outline,
                    ),
                    onPressed: () {
                      // Here We Will Add The Delete Feature
                      db
                          .collection('user')
                          .doc(localdata.getUserId())
                          .collection('keluarga')
                          .doc(documentSnapshot.id)
                          .delete();
                    },
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}

showBottomSheet(
    BuildContext context, bool isUpdate, DocumentSnapshot? documentSnapshot) {
  // Added the isUpdate argument to check if our item has been updated
  return SingleChildScrollView(
    child: Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                keyboardType: TextInputType.text,
                //  maxLength: 4,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText: isUpdate ? 'Update nama' : 'nama',
                  hintText: 'Masukan nama',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  nama = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                keyboardType: TextInputType.text,
                //  maxLength: 4,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText: isUpdate ? 'Update tanggallahir' : 'tanggallahir',
                  hintText: 'Masukan tanggal lahir',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  tanggallahir = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                keyboardType: TextInputType.text,
                // maxLength: 4,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText: isUpdate
                      ? 'Update status dalam keluarga'
                      : 'status dalam keluarga',
                  hintText: 'Masukan status dalam keluarga',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  statusdalamkeluarga = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText:
                      isUpdate ? 'Update deskripsi' : 'Tambahkan deskripsi',
                  hintText: 'Masukan deskripsi',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  deskripsi = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText:
                      isUpdate ? 'Update pendidikan' : 'Tambahkan Pendidikan',
                  hintText: 'Masukan Pendidikan',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  pendidikan = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText:
                      isUpdate ? 'Update pekerjaan' : 'Tambahkan pekerjaan',
                  hintText: 'Masukan pekerjaan',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  pekerjaan = val;
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              child: TextField(
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  // Used a ternary operator to check if isUpdate is true then display
                  // Update Todo.
                  labelText: isUpdate ? 'Update no hp' : 'Tambahkan no hp',
                  hintText: 'Masukan no hp',
                ),
                onChanged: (String val) {
                  // Storing the value of the text entered in the variable value.
                  nohandphone = val;
                },
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            TextButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(ColorName.redprimary),
                ),
                onPressed: () {
                  if (tanggallahir == null &&
                      statusdalamkeluarga == null &&
                      nama == null &&
                      deskripsi == null &&
                      pendidikan == null &&
                      pekerjaan == null &&
                      nohandphone == null) {
                    Get.snackbar('Peringatan', 'Lengkapi data');
                  } else {
                    Map<String, Object> data = {
                      'nama': nama!,
                      'statusdalamkeluarga': statusdalamkeluarga!,
                      'deskripsi': deskripsi!,
                      'pendidikan': pendidikan!,
                      'pekerjaan': pekerjaan!,
                      'nohandphone': nohandphone!,
                      'tanggallahir': tanggallahir!,
                    };
                    // Check to see if isUpdate is true then update the value else add the value
                    if (isUpdate) {
                      db
                          .collection('user')
                          .doc(localdata.getUserId())
                          .collection('keluarga')
                          .doc(documentSnapshot?.id)
                          .update(data);
                    } else {
                      db
                          .collection('user')
                          .doc(localdata.getUserId())
                          .collection('keluarga')
                          .add(data);
                    }
                    Navigator.pop(context);
                  }
                },
                child: isUpdate
                    ? const Text(
                        'UPDATE',
                        style: TextStyle(color: Colors.white),
                      )
                    : const Text('ADD', style: TextStyle(color: Colors.white))),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    ),
  );
}
