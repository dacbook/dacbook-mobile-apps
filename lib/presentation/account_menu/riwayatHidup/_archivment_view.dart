import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/local/box/box_storage.dart';
import '../../login/login_controller.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();
String? tahun;
String? totahun;
String? penghargaan;
String? instansi;
String? token;

class ArchivmentView extends StatelessWidget {
  const ArchivmentView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Tambahkan Penghargaan",
              style: TextStyle(color: ColorName.whiteprimary),
            ),
            GestureDetector(
              onTap: () => showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (context) {
                  return showBottomSheet(context, false, null);
                },
              ),
              child: const Icon(Icons.add),
            )
          ],
        ),
        centerTitle: true,
      ),
      body: StreamBuilder(
        // Reading Items form our Database Using the StreamBuilder widget
        stream: db
            .collection('user')
            .doc(localdata.getUserId())
            .collection('penghargaan')
            .orderBy('tahun', descending: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView.builder(
            itemCount: snapshot.data?.docs.length,
            itemBuilder: (context, int index) {
              DocumentSnapshot documentSnapshot = snapshot.data.docs[index];
              return ListTile(
               // leading: Text(documentSnapshot['tahun']),
                title: Text(documentSnapshot['instansi']),
                subtitle: Column(
                  children: [
                     Row(
                      children: [
                        Text(documentSnapshot['tahun']),
                        const Text(' s/d '),
                        Text(documentSnapshot['totahun']),
                      ],
                    ),
                    Row(
                      children: [
                        Text(documentSnapshot['penghargaan']),
                      ],
                    ),
                  ],
                ),
                onTap: () {
                  // Here We Will Add The Update Feature and passed the value 'true' to the is update
                  // feature.
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) {
                      return showBottomSheet(context, true, documentSnapshot);
                    },
                  );
                },
                trailing: IconButton(
                  icon: const Icon(
                    Icons.delete_outline,
                  ),
                  onPressed: () {
                    // Here We Will Add The Delete Feature
                    db
                        .collection('user')
                        .doc(localdata.getUserId())
                        .collection('penghargaan')
                        .doc(documentSnapshot.id)
                        .delete();
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}

showBottomSheet(
    BuildContext context, bool isUpdate, DocumentSnapshot? documentSnapshot) {
  // Added the isUpdate argument to check if our item has been updated
  return Padding(
    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
         Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.4,
                child: TextField(
                  keyboardType: TextInputType.number,
                  maxLength: 4,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    // Used a ternary operator to check if isUpdate is true then display
                    // Update Todo.
                    labelText: isUpdate ? 'Update mulai tahun' : 'tahun mulai',
                    hintText: 'Masukan tahun mulai',
                  ),
                  onChanged: (String val) {
                    // Storing the value of the text entered in the variable value.
                    tahun = val;
                  },
                ),
              ),
              SizedBox(
            width: MediaQuery.of(context).size.width * 0.4,
            child: TextField(
              keyboardType: TextInputType.number,
              maxLength: 4,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText: isUpdate ? 'Update sampai tahun' : 'tahun selesai',
                hintText: 'Masukan tahun sampai',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                totahun = val;
              },
            ),
          ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: TextField(
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText:
                    isUpdate ? 'Update Penghargaan' : 'Tambahkan Penghargaan',
                hintText: 'Masukan Penghargaan',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                penghargaan = val;
              },
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: TextField(
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                // Used a ternary operator to check if isUpdate is true then display
                // Update Todo.
                labelText:
                    isUpdate ? 'Update penghargaan' : 'Tambahkan Instansi',
                hintText: 'Masukan Instansi',
              ),
              onChanged: (String val) {
                // Storing the value of the text entered in the variable value.
                instansi = val;
              },
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(ColorName.redprimary),
              ),
              onPressed: () {
                if (tahun == null && instansi == null && penghargaan == null) {
                  Get.snackbar('Peringatan', 'Lengkapi data');
                } else {
                  Map<String, Object> data = {
                    'tahun': tahun!,
                    'totahun': totahun!,
                    'instansi': instansi!,
                    'penghargaan': penghargaan!,
                  };
                  // Check to see if isUpdate is true then update the value else add the value
                  if (isUpdate) {
                    db
                        .collection('user')
                        .doc(localdata.getUserId())
                        .collection('penghargaan')
                        .doc(documentSnapshot?.id)
                        .update(data);
                  } else {
                    db
                        .collection('user')
                        .doc(localdata.getUserId())
                        .collection('penghargaan')
                        .add(data);
                  }
                  Navigator.pop(context);
                }
              },
              child: isUpdate
                  ? const Text(
                      'UPDATE',
                      style: TextStyle(color: Colors.white),
                    )
                  : const Text('ADD', style: TextStyle(color: Colors.white))),
          const SizedBox(height: 50),
        ],
      ),
    ),
  );
}
