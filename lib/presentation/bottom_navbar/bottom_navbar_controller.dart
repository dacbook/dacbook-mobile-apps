import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

//import '../account/account_view.dart';
import '../../data/remote/firestore/firestore_user_services.dart';
import '../account/account_view.dart';
import '../book/book_view.dart';
import '../bookgroup/_bookgroup_administrator_view.dart';
import '../bookgroup/bookgroup_view.dart';
import '../search/search_view.dart';
import '../home/home_view.dart';
//import '../login/login_view.dart';
import '../login/login_controller.dart';
import '../logout/logout_view.dart';
import '../profile_medsos/profile_medsos_view.dart';
//import '../message_notification/message_notification_view.dart';

class DacBottomNavbarController extends GetxController {
  final LoginController controller = Get.put(LoginController());
  final FireStoreUserServices firestoreuserservices = FireStoreUserServices();

  var selectedIndex = 0.obs;
  var selectedPage = <Widget>[].obs;
  final box = GetStorage();
  final _boxStorage = BoxStorage();

  var selectedPages = <Widget>[
    const HomeView(),
    const SearchView(),
    const BookView(),
    const ProfileMedsos(),
    const LogoutView(),
  ].obs;

  int get index => selectedIndex.value;
  List<Widget> get pages => selectedPages;

 @override
  void onInit() {
    super.onInit();
    firestoreuserservices.updatealluserfollowingandfollower();
  }

  onTapBottomMenu(int currentIndex, BuildContext context) => {
        selectedIndex.value = currentIndex,
        selectedPage.value = selectedPages,
        if (currentIndex == 4) {onBasicAlertPressed(context)},
      };

  onBasicAlertPressed(context) {
    Alert(
        context: context,
        title: "Logout",
        desc: "Apakah anda yakin ingin keluar dari aplikasi digital alumni?",
        alertAnimation: fadeAlertAnimation,
        closeFunction: onCloseLogoutMsg(context),
        buttons: [
          DialogButton(
            color: Colors.red,
            onPressed: () {
              controller.logoutGoogle();
              onLogoutApps();
            },
            child: const Text(
              "Logout",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
          DialogButton(
            onPressed: () => Navigator.of(context, rootNavigator: true).pop(),
            child: const Text(
              "Tidak",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          )
        ]).show();
  }

  onLogoutApps() {
    box.erase();
    _boxStorage.deleteUserId();
    _boxStorage.deleteUserFcmToken();
    // Get.off(() => const LoginView());
  }

  onCloseLogoutMsg(context) {
    onTapBottomMenu(0, context);
  }

  Widget fadeAlertAnimation(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }
}
