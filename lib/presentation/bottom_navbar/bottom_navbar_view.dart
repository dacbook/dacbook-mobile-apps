// ignore_for_file: depend_on_referenced_packages

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/model/feed/feed_model.dart';
import '../../data/model/user/user_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../main.dart';
import '../book_agenda/book_agenda_detail_view.dart';
import '../book_forum/book_forum_detail_view.dart';
import '../bookgroup/bookgroup_controller.dart';
import '../feeder/feeder_detail_view.dart';
import '../splashscreen/splashscreen_view.dart';
import 'bottom_navbar_controller.dart';

class DacBottomNavbar extends StatefulWidget {
  const DacBottomNavbar({super.key});

  @override
  State<DacBottomNavbar> createState() => _DacBottomNavbarState();
}

class _DacBottomNavbarState extends State<DacBottomNavbar> {
    String? initialMessage;
  bool resolved = false;
  final BoxStorage boxStorage = BoxStorage();
  final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
  final BookGroupController bookGroupController = BookGroupController();

  

   @override
  void initState() {
    super.initState();

    fetchProfile();

    FirebaseMessaging.instance.getInitialMessage().then(
          (value) => setState(
            () {
              resolved = true;
              initialMessage = value?.data.toString();
            },
          ),
        );

    FirebaseMessaging.onMessage.listen(showFlutterNotification);

    // FirebaseMessaging.onBackgroundMessage((message) => null);

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      Get.snackbar('test', 'fcm on background');
      debugPrint(
          'clog ==> testing aplikasi notifikasi ${message.data['notificationCategory']}');
      String notificationCategory = message.data['notificationCategory'];

      switch (notificationCategory) {
        case 'bookforum':
         debugPrint('run book forum');
          String bookid = message.data['bookid'];
          String feedid = message.data['feedid'];
          bool isadmin = false;

          //check single book & single feed
          BookModel bookModel =
              await fireStoreBookServices.getSingleBook(bookid);
          bool iscurrentadmin =
              await bookGroupController.checkIsAdminOrNot(bookModel.bookadmin);
          isadmin = iscurrentadmin;
          debugPrint('isadmin : $isadmin - $iscurrentadmin');
          FeedModel feedModel =
              await fireStoreBookServices.getSingleForum(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(BookForumDetailView(
            feedModel: feedModel,
            islikes: islikes,
            isadmin: isadmin,
          ));
          break;
        case 'bookagenda':
         debugPrint('run bookagenda');
          String bookid = message.data['bookid'];
          String feedid = message.data['feedid'];
          bool isadmin = false;

          //check single book & single feed
          BookModel bookModel =
              await fireStoreBookServices.getSingleBook(bookid);
          bool iscurrentadmin =
              await bookGroupController.checkIsAdminOrNot(bookModel.bookadmin);
          isadmin = iscurrentadmin;
          debugPrint('isadmin : $isadmin - $iscurrentadmin');
          FeedModel feedModel =
              await fireStoreBookServices.getSingleAgendaBook(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(BookAgendaDetailView(
              feedModel: feedModel, islikes: islikes, isadmin: isadmin));
          break;
        case 'story':
         debugPrint('run story');
          String feedid = message.data['feedid'];

          FeedModel feedModel =
              await fireStoreBookServices.getSingleStory(feedid);
          String uid = boxStorage.getUserId();
          bool islikes = feedModel.followersfavorites.contains(uid);
          Get.to(
            FeederDetailView(
              feedModel: feedModel,
              islikes: islikes,
            ),
          );
          break;

        default:
        debugPrint('run default');
          Get.to(const SplashScreenView());
      }
    });

  

  }

    final FirebaseFirestore _db = FirebaseFirestore.instance;

  Future<void> fetchProfile() async {
   UserModel user = await loginCurrentUser(boxStorage.getUserId());
    // debugPrint('$listbookdata');
    setState(() {
      user;
    });
  }

    Future<UserModel> loginCurrentUser(String uid) async {
    // final prefs = await SharedPreferences.getInstance();
    debugPrint('current UserModel : $uid');
    return _db
        .collection('user')
        .doc(uid)
        .get()
        .then((value) => UserModel.fromJson(value.data()!, value.id));
  }

  @override
  Widget build(BuildContext context) {
    final DacBottomNavbarController dacBottomNavbarController =
        Get.put(DacBottomNavbarController());
    return Scaffold(
      
        bottomNavigationBar: Obx(() => (Theme(
              data: Theme.of(context).copyWith(
                canvasColor: ColorName.whiteprimary,
                primaryColor: ColorName.whiteprimary.withOpacity(0.2),
              ),
              child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                selectedIconTheme: const IconThemeData(
                    color: ColorName.redprimary),
                selectedItemColor: ColorName.redprimary,
                unselectedItemColor: ColorName.raven,
                selectedLabelStyle:
                    const TextStyle(fontWeight: FontWeight.bold),
                showSelectedLabels: true,
                showUnselectedLabels: true,
                currentIndex: dacBottomNavbarController.index,
                iconSize: 20.0,
                unselectedFontSize: 11.0,
                selectedFontSize: 12.0,
                onTap: (_) {
                  dacBottomNavbarController.onTapBottomMenu(_, context);
                  setState(() {});
                },
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: FaIcon(FontAwesomeIcons.house),
                    label: 'Beranda',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.search),
                    label: 'Cari Buku',
                  ),
                  BottomNavigationBarItem(
                    icon: FaIcon(FontAwesomeIcons.book),
                    label: 'Buku Digital',
                  ),
                  // BottomNavigationBarItem(
                  //   icon: FaIcon(FontAwesomeIcons.peopleGroup),
                  //   label: 'Buku Digital',
                  // ),
                  BottomNavigationBarItem(
                     icon: FaIcon(FontAwesomeIcons.user),
                    label: 'Akun',
                  ),
                  BottomNavigationBarItem(
                     icon: FaIcon(FontAwesomeIcons.arrowRightFromBracket),
                    label: 'Keluar',
                  ),
                ],
              ),
            ))),
        body:  DoubleBackToCloseApp(
          snackBar: const SnackBar(
            backgroundColor: ColorName.redprimary,
            content: Text('Tekan lagi untuk keluar aplikasi', style: TextStyle(color: ColorName.whiteprimary),),
          ),
          child: IndexedStack(
              index: dacBottomNavbarController.index,
              children: dacBottomNavbarController.pages),
        ));
  }
}
