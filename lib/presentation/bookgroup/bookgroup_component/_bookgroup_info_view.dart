import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:dac_apps/presentation/book_forum/book_forum_comment_reply_view.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../data/model/bookmember/book_member_model.dart';
import '../../../data/remote/firestore/firestore_book_services.dart';
import '../../../data/remote/firestore/firestore_user_services.dart';
import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '../../book/_book_view.dart';
import '../../book/book_organization/book_organization_view.dart';
import '../../book/book_sub.dart';
import '../../book_agenda/book_agenda_add.dart';
import '../../book_agenda/book_agenda_view.dart';
import '../../book_forum/book_forum_add.dart';
import '../../book_forum/book_forum_view.dart';
import '../../book_gallery/book_gallery_add_view.dart';
import '../../book_gallery/book_gallery_view.dart';
import '../../book_request/book_request_view.dart';
import '../../bottom_navbar/bottom_navbar_view.dart';
import '../../profile_medsos/profile_follower_current_user_view.dart';
import '../../profile_medsos/profile_medsos_controller.dart';
import '../_add_book_view.dart';
import '../bookgroup_controller.dart';
import '_book_group_member_view.dart';

final db = FirebaseFirestore.instance;
final localdata = BoxStorage();

class BookGroupInfo extends StatefulWidget {
  const BookGroupInfo(
      {super.key,
      required this.documentSnapshot,
      required this.uid,
      required this.isadmin});
  final BookModel documentSnapshot;
  final String uid;
  final bool isadmin;

  @override
  State<BookGroupInfo> createState() => _BookGroupInfoState();
}

class _BookGroupInfoState extends State<BookGroupInfo>
    with SingleTickerProviderStateMixin {
  final boxStorage = BoxStorage();

  String name = "";
  String bookid = "";

  bool isGridView = false;

  bool isrequestfollcurrentuser = false;
  bool isrequestfoll = false;
  bool isneedfollback = false;
  bool isdontneedfollback = false;

  bool isadmin = false;

  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();

  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  final BookGroupController bookGroupController = BookGroupController();

  TabController? tabController;

  List<String> isadminbooklist = [];

  @override
  // ignore: unused_element
  void initState() {
    checkadminbook(widget.documentSnapshot.bookadmin);
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  // ignore: unused_element
  void dispose() {
    tabController?.dispose();
    super.dispose();
  }

  checkadminbook(List<String> listadmin) async {
    debugPrint('list admin : $listadmin');
    bool iscurrentadmin =
        await bookGroupController.checkIsAdminOrNot(listadmin);
    setState(() {
      isadmin = iscurrentadmin;
      debugPrint('isadmin : $isadmin - $iscurrentadmin');
    });
  }

  // youareAdmin(String bookid) {
  //   final collRef =
  //       db.collection('book').doc(bookid);
  //   DocumentReference docReference = collRef.doc(doc['id']);
  // }

  @override
  Widget build(BuildContext context) {
    final FireStoreBookServices fireStoreBookServices =
        Get.put(FireStoreBookServices());
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(Icons.arrow_back)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: Assets.image.logoDacLable
                        .image(fit: BoxFit.fitWidth, height: 50),
                  ),
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: SizedBox(
          child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 2,
          vertical: 10,
        ),
        child: StreamBuilder(
            stream: fireStoreBookServices
                .getBookDetailStream(widget.documentSnapshot.id),
            builder: (context, snapshot) {
              debugPrint('group : ${snapshot.data}');

              if (snapshot.connectionState == ConnectionState.waiting) {
                return const SizedBox(
                    height: 30, width: 30, child: CircularProgressIndicator());
              }
              if (snapshot.connectionState == ConnectionState.none) {
                return const Text('Server not found - Error 500');
              }
              if (snapshot.connectionState == ConnectionState.done ||
                  snapshot.hasData) {
                BookModel bookModel = snapshot.data!;
                isadminbooklist = bookModel.bookadmin;
                isadmin = isadminbooklist.contains(localdata.getUserId());

                //checkadminbook(bookModel.bookadmin);
                debugPrint(
                    'admin status : $isadmin');
                debugPrint(
                    'request member : ${bookModel.bookmemberrequest.isNotEmpty}');

                return ListView(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            isadmin
                                ? bookModel.bookadmin.length == 1
                                    ? showAlertDialogPanelLastAdmin(
                                        context, bookModel, widget.uid)
                                    : showAlertDialogPanelAdmin(
                                        context, bookModel, widget.uid)
                                : showAlertDialogPanelUser(context, bookModel);
                            setState(() {});
                            //  Get.to(
                            //   () => const AddBookgroupView(category: 'Buku', id: ''));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.book_sharp,
                                  color: ColorName.redprimary,
                                ),
                                Text(
                                  isadmin ? 'Admin Menu' : 'Menu',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    Center(
                      child: SizedBox(
                          height: 150,
                          width: 150,
                          child: Card(
                            color: Colors.white.withOpacity(0.9),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: CachedNetworkImage(
                                imageUrl: bookModel.bookimage,
                                fit: BoxFit.fitWidth,
                                width: Get.width * 0.94,
                                placeholder: (context, url) {
                                  return Image.network(
                                    url,
                                    fit: BoxFit.cover,
                                  );
                                },
                                errorWidget: (context, url, error) {
                                  return Image.asset(
                                    'assets/image/example_album1.jpeg',
                                    fit: BoxFit.cover,
                                  );
                                },
                              ),
                            ),
                          )),
                    ),
                    SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                            mainAxisAlignment: isadmin
                                ? MainAxisAlignment.spaceBetween
                                : MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              isadmin
                                  ? Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Text('Pin buku'),
                                        GestureDetector(
                                          child: Row(
                                            children: [
                                              Text(
                                                bookModel.pin.toString(),
                                                style: const TextStyle(
                                                    color:
                                                        ColorName.blueprimary,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              const Icon(
                                                Icons.copy,
                                                size: 14,
                                              )
                                            ],
                                          ),
                                          onTap: () async {
                                            await Clipboard.setData(
                                                ClipboardData(
                                                    text: bookModel.pin
                                                        .toString()));
                                            Get.snackbar(
                                                'Tersalin', 'Buku tersalin');
                                          },
                                        ),
                                      ],
                                    )
                                  : const SizedBox.shrink(),
                              GestureDetector(
                                onTap: () async {
                                  if (isloading == false) {
                                    setState(() {
                                      isloading = true;
                                    });
                                    var link =
                                        await createDynamicLink(bookModel.id);
                                    linkshort = link.shortUrl.toString();
                                    convertmessage(
                                        link.shortUrl.toString(), bookModel);
                                    debugPrint('link : ${link.shortUrl}');
                                  }
                                },
                                child: Row(
                                  children: const [
                                    Icon(Icons.share),
                                    Text('Bagikan buku'),
                                  ],
                                ),
                              )
                            ]),
                      ),
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      bookModel.bookname,
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      'dibuat pada ${bookModel.date} ',
                                      style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    SizedBox(
                                      width: 300,
                                      child: Text(
                                        bookModel.description,
                                        overflow: TextOverflow.ellipsis,
                                        style: const TextStyle(
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            const FaIcon(FontAwesomeIcons.check,
                                color: ColorName.redprimary)
                          ],
                        ),
                      ),
                    ),
                    isadmin == true
                        ? GestureDetector(
                            onTap: () =>
                                Get.to(BookRequestView(bookid: bookModel.id)),
                            child: Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 10.0, bottom: 10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              const Text(
                                                "Permintaan masuk buku",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              bookModel.bookmemberrequest
                                                      .isNotEmpty
                                                  ? const Text(
                                                      " !",
                                                      style: TextStyle(
                                                          color: Colors.red,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )
                                                  : const SizedBox.shrink(),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    const FaIcon(FontAwesomeIcons.angleRight,
                                        color: ColorName.redprimary)
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    bookModel.ismasterbook == true
                        ? GestureDetector(
                            onTap: () {
                              if (isadmin == true) {
                                String uid = boxStorage.getUserId();
                                db
                                    .collection('book')
                                    .doc(bookModel.id)
                                    .collection('bookmember')
                                    .doc(uid)
                                    .update({'memberlevel': 'admin'});
                              }
                              Get.to(BookOrganizationView(
                                bookid: bookModel.id,
                                usertype: isadmin,
                                bookmodel: bookModel,
                              ));
                            },
                            child: Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 10.0, bottom: 10.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: const [
                                          Text(
                                            "Struktur Organisasi",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const FaIcon(FontAwesomeIcons.angleRight,
                                        color: ColorName.redprimary)
                                  ],
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    GestureDetector(
                      onTap: () => Get.to(BookGroupMemberView(
                        documentSnapshot: bookModel,
                        isadmin: isadmin,
                      )),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Alumni (${bookModel.bookmember.length.toString()})",
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              const FaIcon(FontAwesomeIcons.angleRight,
                                  color: ColorName.redprimary)
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Get.to(BookGalleryView(
                          bookModel: bookModel,
                        ));
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text(
                                      "Galeri",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              const FaIcon(FontAwesomeIcons.angleRight,
                                  color: ColorName.redprimary)
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        String localuid = boxStorage.getUserId();
                        fireStoreBookServices.updateforumcurrentuser(
                            widget.documentSnapshot.id, localuid);
                        Get.to(BookForumView(
                            bookid: widget.documentSnapshot.id,
                            isforumku: false,
                            isadmin: isadmin, bookModel: bookModel,));
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text(
                                      "Forum",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              const FaIcon(FontAwesomeIcons.angleRight,
                                  color: ColorName.redprimary)
                            ],
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Get.to(
                          BookSubView(
                            bookid: widget.documentSnapshot.id,
                          ),
                        );
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text(
                                      "Sub buku",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              const FaIcon(FontAwesomeIcons.angleRight,
                                  color: ColorName.redprimary)
                            ],
                          ),
                        ),
                      ),
                    ),
                    DefaultTabController(
                      length: 3,
                      child: Column(
                        children: [
                          Container(
                            color: Colors.white,
                            child: TabBar(
                              controller: tabController,
                              labelColor: ColorName.redprimary,
                              unselectedLabelColor: Colors.black,
                              indicatorColor: ColorName.redprimary,
                              indicatorPadding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              labelStyle: const TextStyle(
                                fontSize: 14,
                                fontFamily: 'Roboto',
                              ),
                              tabs: const [
                                Tab(
                                  child: Text('agenda'),
                                ),
                                Tab(
                                  child: Text("event"),
                                ),
                                Tab(
                                  child: Text("program"),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 420,
                            child: TabBarView(
                              controller: tabController,
                              children: [
                                BookAgendaView(
                                  bookid: widget.documentSnapshot.id,
                                  categoryKegiatan: 'agenda',
                                  isadmin: isadmin,
                                ),
                                BookAgendaView(
                                  bookid: widget.documentSnapshot.id,
                                  categoryKegiatan: 'event',
                                  isadmin: isadmin,
                                ),
                                BookAgendaView(
                                  bookid: widget.documentSnapshot.id,
                                  categoryKegiatan: 'program',
                                  isadmin: isadmin,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    )
                  ],
                );
              }
              return const SizedBox();
            }),
      )),
    );
  }

  late String linkshort;
  String ilmcisharemessage = '';
  bool isloading = false;

  createDynamicLink(String id) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://ilmci.page.link/',
      link: Uri.parse('https://development.dac.com/book?$id'),
      androidParameters: const AndroidParameters(
        packageName: 'com.ilmci.dac.android.development',
        minimumVersion: 1,
      ),
    );
    var dynamicUrl =
        await FirebaseDynamicLinks.instance.buildShortLink(parameters);
    dynamicUrl.shortUrl;
    debugPrint('dynamic uri : ${dynamicUrl.shortUrl}');

    return dynamicUrl;
  }

  convertmessage(String urishort, BookModel bookModel) {
    String link = urishort;
    String bookname = bookModel.bookname;
    String bookpin = bookModel.pin;
    ilmcisharemessage = ''' Kepada semua $bookname, 
 Mari kita bergabung di Digital Alumni & Community Book (DAC Book), karya anak bangsa bertema perdamaian dunia n Aksi belneg (setia kpd Pancasila) serta nikmati berbagai manfaat lainnya 
 
 Daftar Baru (Register): 
 1. Buka link : $link
 2. Install 
 3. Register 
 
 Cari Buku (Sign In):
 1. klik menu buku, Cari $bookname/ Klik link buku yang di lampirkan ( $link ). 
 2. Masukkan PIN ($bookpin) / klik permintaan bergabung - admin 
 Mari bersatu dlm kerangka NKRI (PANCASILA) & buktikan Merah Putihmu 🇮🇩 
 (tiktok: ac book) ''';
    setState(() {
      isloading = false;
    });
    Share.share(ilmcisharemessage);
  }
}

final boxStorage = BoxStorage();

showAlertDialogAddAdmin(
    BuildContext context, bool isadmin, BookmemberModel bookmemberModel) {
  // set up the button
  Widget okButton = TextButton(
    child: const Text("Setujui"),
    onPressed: () {
      String uid = boxStorage.getUserId();
      if (isadmin == true) {
        fireStoreBookServices.removeAdminbook(uid, bookmemberModel.uid);
      } else {
        fireStoreBookServices.addAdminbook(uid, bookmemberModel.uid);
      }
    },
  );

  // set up the AlertDialog
  AlertDialog alertUpAdmin = AlertDialog(
    title: const Text("Pemberitahuan"),
    content: const Text("Apakah anda ingin menjadikan sebagai admin ?"),
    actions: [
      okButton,
    ],
  );

  // set up the AlertDialog
  AlertDialog alertDownAdmin = AlertDialog(
    title: const Text("Pemberitahuan"),
    content: const Text("Apakah anda ingin menghapus posisi admin ?"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return isadmin == false ? alertUpAdmin : alertDownAdmin;
    },
  );
}

showAlertDialogPanelAdmin(BuildContext context, BookModel doc, String uid) {
  // set up the buttons
  Widget menubutton = SizedBox(
    height: 360,
    child: Column(
      children: [
        TextButton(
          child: Text(
            "tambah buku".toUpperCase(),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          onPressed: () {
            Get.to(() => const AddBookgroupView(category: 'Buku', id: ''));
          },
        ),
        TextButton(
          child: Text("tambah forum".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            String localuid = boxStorage.getUserId();
            fireStoreBookServices.updateforumcurrentuser(doc.id, localuid);
            Get.to(() => BookForumAddView(
                  bookmodel: doc,
                ));
          },
        ),
        TextButton(
          child: Text("tambah galeri".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => AddBookGalleryView(
                  bookModel: doc,
                ));
          },
        ),
        TextButton(
          child: Text("tambah agenda".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            String localuid = boxStorage.getUserId();
            fireStoreBookServices.updateforumcurrentuser(doc.id, localuid);
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'agenda',
                ));
          },
        ),
        TextButton(
          child: Text("tambah event".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'event',
                ));
          },
        ),
        TextButton(
          child: Text("tambah program".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'program',
                ));
          },
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(ColorName.redprimary),
                      textStyle: MaterialStateProperty.all(const TextStyle(
                        fontFamily: 'Mont',
                        color: ColorName.whiteprimary,
                      ))),
                  onPressed: () {
                    String uid = boxStorage.getUserId();
                     List<String> isadminlist = doc.bookadmin;
                      bool isadmin = isadminlist.contains(uid);
                    fireStoreBookServices.deleteMemberbook(doc.id, uid, isadmin);
                    Get.to(const DacBottomNavbar());
                  },
                  child: Text(
                    'Keluar dari grup buku'.toUpperCase(),
                  ),
                ),
              ),
            ],
          ),
        ),
        //  TextButton(
        //   child: const Text("Cancel"),
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
      ],
    ),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text("Menu admin"),
    content: menubutton,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showAlertDialogPanelLastAdmin(BuildContext context, BookModel doc, String uid) {
  // set up the buttons
  Widget menubutton = SizedBox(
    height: 360,
    child: Column(
      children: [
        TextButton(
          child: Text(
            "tambah buku".toUpperCase(),
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          onPressed: () {
            Get.to(() => const AddBookgroupView(category: 'Buku', id: ''));
          },
        ),
        TextButton(
          child: Text("tambah forum".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            String localuid = boxStorage.getUserId();
            fireStoreBookServices.updateforumcurrentuser(doc.id, localuid);
            Get.to(() => BookForumAddView(
                  bookmodel: doc,
                ));
          },
        ),
        TextButton(
          child: Text("tambah galeri".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => AddBookGalleryView(
                  bookModel: doc,
                ));
          },
        ),
        TextButton(
          child: Text("tambah agenda".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            String localuid = boxStorage.getUserId();
            fireStoreBookServices.updateforumcurrentuser(doc.id, localuid);
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'agenda',
                ));
          },
        ),
        TextButton(
          child: Text("tambah event".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'event',
                ));
          },
        ),
        TextButton(
          child: Text("tambah program".toUpperCase(),
              style: const TextStyle(fontWeight: FontWeight.bold)),
          onPressed: () {
            Get.to(() => BookAgendaAddView(
                  bookmodel: doc,
                  categoryKegiatan: 'program',
                ));
          },
        ),

        // Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Row(
        //     children: [
        //       Expanded(
        //         child: ElevatedButton(
        //           style: ButtonStyle(
        //               backgroundColor:
        //                   MaterialStateProperty.all(ColorName.redprimary),
        //               textStyle: MaterialStateProperty.all(const TextStyle(
        //                 fontFamily: 'Mont',
        //                 color: ColorName.whiteprimary,
        //               ))),
        //           onPressed: () {
        //             String uid = boxStorage.getUserId();
        //             fireStoreBookServices.deleteMemberbook(doc.id, uid);
        //             Get.to(const DacBottomNavbar());
        //           },
        //           child: Text(
        //             ''.toUpperCase(),
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        //  TextButton(
        //   child: const Text("Cancel"),
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
      ],
    ),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text("Menu admin"),
    content: menubutton,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showAlertDialogPanelUser(BuildContext context, BookModel doc) {
  // set up the buttons
  Widget menubutton = SizedBox(
    height: 100,
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(ColorName.redprimary),
                      textStyle: MaterialStateProperty.all(const TextStyle(
                        fontFamily: 'Mont',
                        color: ColorName.whiteprimary,
                      ))),
                  onPressed: () {
                    String uid = boxStorage.getUserId();
                     List<String> isadminlist = doc.bookadmin;
                      bool isadmin = isadminlist.contains(uid);
                    fireStoreBookServices.deleteMemberbook(doc.id, uid, isadmin);
                    Get.to(const DacBottomNavbar());
                  },
                  child: Text(
                    'Keluar dari grup buku'.toUpperCase(),
                  ),
                ),
              ),
            ],
          ),
        ),
        //  TextButton(
        //   child: const Text("Cancel"),
        //   onPressed: () {
        //     Navigator.pop(context);
        //   },
        // ),
      ],
    ),
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text("Menu"),
    content: menubutton,
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
