// ignore_for_file: deprecated_member_use

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:dac_apps/data/model/user/user_model.dart';
import 'package:dac_apps/presentation/bookgroup/_bookgroup_detail_view.dart';
import 'package:easy_image_viewer/easy_image_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/local/box/box_storage.dart';
import '../../../data/model/bookmember/book_member_model.dart';
import '../../../data/remote/firestore/firestore_book_services.dart';
import '../../../data/remote/firestore/firestore_user_services.dart';
import '../../../gen/assets.gen.dart';
import '../../../gen/colors.gen.dart';
import '../../profile_medsos/profile_follower_current_user_view.dart';
import '../../profile_medsos/profile_medsos_controller.dart';

class BookGroupMemberView extends StatefulWidget {
  const BookGroupMemberView(
      {super.key, required this.documentSnapshot, required this.isadmin});
  final BookModel documentSnapshot;
  final bool isadmin;

  @override
  State<BookGroupMemberView> createState() => _BookGroupMemberViewState();
}

class _BookGroupMemberViewState extends State<BookGroupMemberView> {
  final box = GetStorage();

  String name = "";
  String bookid = "";

  bool isGridView = false;

  bool isrequestfollowcurrentuser = false;
  bool isrequestfollow = false;
  bool isfollowing = false;

  final ProfileMedsosController profileMedsosController =
      ProfileMedsosController();
  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  final BoxStorage _boxStorage = BoxStorage();
  bool isuserloggedin = false;

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Never called
    print("Disposing first route");
    super.dispose();
  }

  final FireStoreBookServices _fireStoreBookServices = FireStoreBookServices();

  @override
  Widget build(BuildContext context) {
    double paddingBottomScreen = MediaQuery.of(context).padding.bottom;
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: AppBar(
            automaticallyImplyLeading: false,
            flexibleSpace: Padding(
              padding: const EdgeInsets.only(top: 35.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Assets.image.logoDacLable
                        .image(fit: BoxFit.fitWidth, height: 35),
                    SizedBox(
                      height: 45,
                      width: Get.width * 0.7,
                      child: Card(
                        child: TextField(
                          decoration: const InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              hintText: 'Search...'),
                          onChanged: (val) {
                            setState(() {
                              name = val;
                              isGridView = true;
                            });
                          },
                        ),
                      ),
                    ),
                  ]),
            ),
            backgroundColor: ColorName.whiteprimary,
          ),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                width: Get.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 100,
                      width: 100,
                      child: Image.network(widget.documentSnapshot.bookimage),
                    ),
                    SizedBox(
                      width: 300,
                      child: Text(
                        widget.documentSnapshot.bookname.toUpperCase(),
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 20,
                            color: ColorName.redprimary,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      width: 300,
                      child: Text(
                        widget.documentSnapshot.description.toUpperCase(),
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                            fontSize: 14,
                            color: ColorName.blackgrey,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  width: 150,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: isGridView == true
                          ? ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  isGridView = false;
                                });
                              },
                              child: Text('tampil grid'.toUpperCase()),
                            )
                          : ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  isGridView = true;
                                });
                              },
                              child: Text('tampil list'.toUpperCase()),
                            )),
                ),
              ],
            ),
            StreamBuilder(
              stream: _fireStoreBookServices
                  .getBookmember(widget.documentSnapshot.id),
              builder: (context, snapshots) {
                return (snapshots.connectionState == ConnectionState.waiting)
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : isGridView
                        ? ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: snapshots.data!.length,
                            itemBuilder: (context, index) {
                              BookmemberModel data = snapshots.data![index];
                             
                              
                              if (name.isEmpty) {
                                return GestureDetector(
                                  onTap: () {
                                    bool isadmin = false;
                                    if (data.memberlevel == 'admin') {
                                      setState(() {
                                        isadmin = true;
                                      });
                                    }
                                    debugPrint('$isadmin');
                                    showAlertDialog(context, isadmin, data);
                                    Navigator.pop(context);
                                  },
                                  child: ListTile(
                                    leading: SizedBox(
                                      height: 70,
                                      width: 60,
                                      child: Card(
                                        elevation: 5,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: data.avatar == '' ||
                                                  data.avatar.isEmpty
                                              ? Assets.image.logoDac.image(
                                                  fit: BoxFit.fitWidth,
                                                  height: 35)
                                              : CachedNetworkImage(
                                                  imageUrl: data.avatar,
                                                  fit: BoxFit.cover,
                                                  placeholder: (context, url) {
                                                    return Image.network(
                                                      data.avatar,
                                                      fit: BoxFit.cover,
                                                    );
                                                  },
                                                  errorWidget:
                                                      (context, url, error) {
                                                    return Assets.image.logoDac
                                                        .image(
                                                            fit:
                                                                BoxFit.fitWidth,
                                                            height: 35);
                                                  },
                                                ),
                                        ),
                                      ),
                                    ),
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              data.memberlevel,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: ColorName.blueprimary,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            data.positionlevel.isNotEmpty
                                                ? Text(
                                                    ' (${data.positionname.toUpperCase()})',
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: const TextStyle(
                                                        color: ColorName
                                                            .redprimary,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Text(
                                          'bergabung pada ${data.date}',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          data.username,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    subtitle: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                          _boxStorage.getUserId() ==
                                                            data.uid
                                                        ? const SizedBox
                                                            .shrink()
                                                        :  TextButton(
                                                onPressed: () async {
                                                  String localuid =
                                                      _boxStorage.getUserId();
                                                  UserModel userModel =
                                                      await fireStoreUserServices
                                                          .getCurrentFollower(
                                                              data.uid);
                                                  isrequestfollowcurrentuser =
                                                      await profileMedsosController
                                                          .checkisrequestfollowcurrentuser(
                                                              uid: localuid,
                                                              followersid:
                                                                  data.uid);
                                                  isrequestfollow =
                                                      await profileMedsosController
                                                          .checkisrequestfollow(
                                                              uid: localuid,
                                                              followersid:
                                                                  data.uid);
                                                  isfollowing =
                                                      await profileMedsosController
                                                          .checkisfollowinguser(
                                                              uid: localuid,
                                                              followersid:
                                                                  data.uid);
                                                  debugPrint(
                                                      'is status request follow current user : $isrequestfollowcurrentuser - follower : $isrequestfollow');

                                                  Get.to(ProfileFollower(
                                                    usermodel: userModel,
                                                    isrequestfollowcurrentuser:
                                                        isrequestfollowcurrentuser,
                                                    isrequestfollow:
                                                        isrequestfollow,
                                                    isfollowing: isfollowing,
                                                  ));
                                                  setState(() {});
                                                },
                                                child: const Text('Profil')),
                                            widget.isadmin
                                                ? TextButton(
                                                    onPressed: () async {
                                                      BookModel bookModel =
                                                          await fireStoreBookServices
                                                              .getSingleBook(widget
                                                                  .documentSnapshot
                                                                  .id);

                                                      if (data.memberlevel ==
                                                          'admin') {
                                                        if (bookModel.bookadmin
                                                                .length ==
                                                            1) {
                                                          Get.snackbar(
                                                              'Information',
                                                              'admin terakhir tidak bisa dikeluarkan dari buku');
                                                        } else {
                                                          _fireStoreBookServices
                                                              .removeAdminbook(
                                                                  widget
                                                                      .documentSnapshot
                                                                      .id,
                                                                  data.uid);
                                                        }
                                                      } else {
                                                        if (bookModel.bookadmin
                                                                .length ==
                                                            3) {
                                                          Get.snackbar(
                                                              'Information',
                                                              'tidak bisa menambahkan admin lagi. batas maksimal 3 admin');
                                                        } else {
                                                          _fireStoreBookServices
                                                              .addAdminbook(
                                                                  widget
                                                                      .documentSnapshot
                                                                      .id,
                                                                  data.uid);
                                                        }
                                                      }
                                                      setState(() {});
                                                    },
                                                    child: _boxStorage.getUserId() ==
                                                            data.uid
                                                        ? const SizedBox
                                                            .shrink()
                                                        : data.memberlevel ==
                                                                'admin'
                                                            ? const Text(
                                                                'Hapus admin',
                                                                style: TextStyle(
                                                                    color: ColorName
                                                                        .redprimary),
                                                              )
                                                            : const Text(
                                                                'Tambah admin'))
                                                : Container(),
                                          ],
                                        )
                                      ],
                                    ),
                                    trailing: _boxStorage.getUserId() ==
                                                            data.uid
                                                        ? const SizedBox
                                                            .shrink()
                                                        : widget.isadmin
                                        ? GestureDetector(
                                            onTap: () {
                                              _fireStoreBookServices
                                                  .deleteMemberbook(
                                                      widget
                                                          .documentSnapshot.id,
                                                      data.uid,
                                                      widget.isadmin);
                                            },
                                            child: const Icon(
                                                Icons.delete_forever))
                                        : const SizedBox(
                                            width: 10,
                                            height: 10,
                                          ),
                                  ),
                                );
                              }

                              // search alumni
                              if (data.username
                                      .toString()
                                      .toLowerCase()
                                      .isCaseInsensitiveContains(
                                          name.toLowerCase()) &&
                                  name.isNotEmpty) {
                                return GestureDetector(
                                  onTap: () {
                                    bool isadmin = false;
                                    if (data.memberlevel == 'admin') {
                                      setState(() {
                                        isadmin = true;
                                      });
                                    }
                                    debugPrint('$isadmin');
                                    showAlertDialog(context, isadmin, data);
                                    Navigator.pop(context);
                                  },
                                  child: ListTile(
                                    leading: SizedBox(
                                      height: 70,
                                      width: 60,
                                      child: Card(
                                        elevation: 5,
                                        child: Padding(
                                          padding: const EdgeInsets.all(2.0),
                                          child: data.avatar != ''
                                              ? Image.network(data.avatar)
                                              : Assets.image.logoDac.image(
                                                  fit: BoxFit.fitHeight,
                                                  height: 35),
                                        ),
                                      ),
                                    ),
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              data.memberlevel,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(
                                                  color: ColorName.blueprimary,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            data.positionlevel.isNotEmpty
                                                ? Text(
                                                    ' (${data.positionlevel.toUpperCase()})',
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: const TextStyle(
                                                        color: ColorName
                                                            .redprimary,
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                        Text(
                                          'bergabung pada ${data.date}',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        Text(
                                          data.username,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    subtitle: Column(
                                      children: [
                                        TextButton(
                                            onPressed: () async {
                                              String localuid =
                                                  _boxStorage.getUserId();
                                              UserModel userModel =
                                                  await fireStoreUserServices
                                                      .getCurrentFollower(
                                                          data.uid);
                                              isrequestfollowcurrentuser =
                                                  await profileMedsosController
                                                      .checkisrequestfollowcurrentuser(
                                                          uid: localuid,
                                                          followersid:
                                                              data.uid);
                                              isrequestfollow =
                                                  await profileMedsosController
                                                      .checkisrequestfollow(
                                                          uid: localuid,
                                                          followersid:
                                                              data.uid);
                                              isfollowing =
                                                  await profileMedsosController
                                                      .checkisfollowinguser(
                                                          uid: localuid,
                                                          followersid:
                                                              data.uid);
                                              debugPrint(
                                                  'is status request follow current user : $isrequestfollowcurrentuser - follower : $isrequestfollow');

                                              Get.to(ProfileFollower(
                                                usermodel: userModel,
                                                isrequestfollowcurrentuser:
                                                    isrequestfollowcurrentuser,
                                                isrequestfollow:
                                                    isrequestfollow,
                                                isfollowing: isfollowing,
                                              ));
                                              setState(() {});
                                            },
                                            child: const Text(
                                                'Lihat Selengkapnya'))
                                      ],
                                    ),
                                    trailing: GestureDetector(
                                        onTap: () {
                                          _fireStoreBookServices
                                              .deleteMemberbook(
                                                  widget.documentSnapshot.id,
                                                  data.uid,
                                                  widget.isadmin);
                                        },
                                        child:
                                            const Icon(Icons.delete_forever)),
                                  ),
                                );
                              }

                              return Container();
                            })
                        : Padding(
                            padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(10),
                              bottom: paddingBottomScreen == 0
                                  ? ScreenUtil().setHeight(10)
                                  : paddingBottomScreen,
                            ),
                            child: GridView.builder(
                                shrinkWrap: true,
                                physics: const ScrollPhysics(),
                                padding: EdgeInsets.zero,
                                itemCount: snapshots.data!.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 4,
                                  crossAxisSpacing: ScreenUtil().setWidth(5),
                                  mainAxisSpacing: ScreenUtil().setHeight(5),
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  BookmemberModel data = snapshots.data![index];
                                  List listbookmember =
                                      snapshots.data!;
                                  isuserloggedin = listbookmember
                                      .contains(_boxStorage.getUserId());
                                  debugPrint(
                                      'status user login : $isuserloggedin');

                                  return GestureDetector(
                                    onTap: () async {
                                      String localuid = _boxStorage.getUserId();
                                      UserModel userModel =
                                          await fireStoreUserServices
                                              .getCurrentFollower(data.uid);
                                      isrequestfollowcurrentuser =
                                          await profileMedsosController
                                              .checkisrequestfollowcurrentuser(
                                                  uid: localuid,
                                                  followersid: data.uid);
                                      isrequestfollow =
                                          await profileMedsosController
                                              .checkisrequestfollow(
                                                  uid: localuid,
                                                  followersid: data.uid);
                                      isfollowing =
                                          await profileMedsosController
                                              .checkisfollowinguser(
                                                  uid: localuid,
                                                  followersid: data.uid);
                                      debugPrint(
                                          'is status request follow current user : $isrequestfollowcurrentuser - follower : $isrequestfollow');

                                      Get.to(ProfileFollower(
                                        usermodel: userModel,
                                        isrequestfollowcurrentuser:
                                            isrequestfollowcurrentuser,
                                        isrequestfollow: isrequestfollow,
                                        isfollowing: isfollowing,
                                      ));
                                      setState(() {});
                                    },
                                    child: Column(
                                      children: [
                                        CachedNetworkImage(
                                          height: 60,
                                          imageUrl: data.avatar,
                                          fit: BoxFit.fitHeight,
                                          imageBuilder:
                                              (context, imageProvider) =>
                                                  CircleAvatar(
                                            radius: 30,
                                            backgroundImage: imageProvider,
                                            backgroundColor: Colors.transparent,
                                          ),
                                          placeholder:
                                              (context, imageProvider) {
                                            return const CircleAvatar(
                                              radius: 30,
                                              backgroundColor:
                                                  Colors.transparent,
                                              child: SizedBox.shrink(),
                                            );
                                          },
                                          errorWidget: (context, url, error) {
                                            return const Icon(Icons.error);
                                          },
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Text(
                                            data.username.toUpperCase(),
                                            overflow: TextOverflow.ellipsis,
                                            style:
                                                const TextStyle(fontSize: 12),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                }),
                          );
              },
            ),
          ],
        ));
  }

  showAlertDialog(
      BuildContext context, bool isadmin, BookmemberModel bookmemberModel) {
    // set up the button
    Widget okButton = TextButton(
      child: const Text("Setujui"),
      onPressed: () {
        if (isadmin == true) {
          _fireStoreBookServices.removeAdminbook(
              widget.documentSnapshot.id, bookmemberModel.uid);
        } else {
          _fireStoreBookServices.addAdminbook(
              widget.documentSnapshot.id, bookmemberModel.uid);
        }

        setState(() {});
      },
    );

    // set up the AlertDialog
    AlertDialog alertUpAdmin = AlertDialog(
      title: const Text("Pemberitahuan"),
      content: const Text("Apakah anda ingin menjadikan sebagai admin ?"),
      actions: [
        okButton,
      ],
    );

    // set up the AlertDialog
    AlertDialog alertDownAdmin = AlertDialog(
      title: const Text("Pemberitahuan"),
      content: const Text("Apakah anda ingin menghapus posisi admin ?"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return isadmin == false ? alertUpAdmin : alertDownAdmin;
      },
    );
  }
}
