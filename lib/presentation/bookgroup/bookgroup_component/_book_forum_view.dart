import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../gen/colors.gen.dart';
import '../bookgroup_controller.dart';

final db = FirebaseFirestore.instance;
String? value;

class BookForumView extends StatelessWidget {
  const BookForumView({super.key, required this.id});
  final String id;

  @override
  Widget build(BuildContext context) {
    final List<String> imageSliders = [
      'https://i.ibb.co/DzvJntq/example-album1.jpg',
      'https://i.ibb.co/Xxt7PKh/example-album2.png',
      'https://i.ibb.co/BcYLZqb/example-album3.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
      'https://i.ibb.co/DzvJntq/example-album1.jpg',
      'https://i.ibb.co/Xxt7PKh/example-album2.png',
      'https://i.ibb.co/BcYLZqb/example-album3.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
      'https://i.ibb.co/nRD67jx/example-album4.jpg',
    ];
    List<dynamic> listItem = [];

    int _current = 0;
    final BookGroupController bookgroupController = Get.put(BookGroupController());
    return StreamBuilder(
      // Reading Items form our Database Using the StreamBuilder widget
      stream: db.collectionGroup('forum').snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          // itemExtent: snapshot.data?.docs.length,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: snapshot.data?.docs.length,
          itemBuilder: (context, int index) {
            DocumentSnapshot documentSnapshot =
                snapshot.data.docs[index];
            //listItem = documentSnapshot['media'];

            debugPrint(documentSnapshot['mediaForum'].toString());
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 15,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    documentSnapshot['username'],
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                   Text(
                    'Dibagikan pada ${documentSnapshot['date']} ',
                    style:const TextStyle(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 250,
                    child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: CachedNetworkImage(
                            imageUrl: documentSnapshot['mediaForum'],
                            fit: BoxFit.fitWidth,
                            width: Get.width * 0.94,
                            placeholder: (context, url) {
                              return Image.asset(
                                'assets/image/example_album1.jpeg',
                                fit: BoxFit.cover,
                              );
                            },
                            errorWidget: (context, url, error) {
                              return Image.asset(
                                'assets/image/example_album1.jpeg',
                                fit: BoxFit.cover,
                              );
                            },
                          ),
                        )
                  ),
                  const SizedBox(height: 5),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: const [
                            FaIcon(FontAwesomeIcons.heart,
                                color: ColorName.purplelow),
                            SizedBox(
                              width: 20,
                            ),
                            FaIcon(FontAwesomeIcons.comment,
                                color: ColorName.purplelow),
                          ],
                        ),
                        const FaIcon(FontAwesomeIcons.bookmark,
                            color: ColorName.purplelow),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    documentSnapshot['description'],
                    style: const TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}

class Photos {
  String key;
  String url;

  Photos(
    this.key,
    this.url,
  );

  @override
  String toString() {
    return '{ $key - $url}';
  }
}
