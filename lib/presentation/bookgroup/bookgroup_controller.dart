import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../data/local/box/box_storage.dart';

class BookGroupController extends GetxController {

   final box = GetStorage();
   
  final BoxStorage _boxStorage = BoxStorage();
  var selectedGroupsId = ''.obs;


  Future<bool> checkIsMemberOrNot(List<String> list) async {
    String localuid = _boxStorage.getUserId();
    debugPrint('list => $list - $localuid');
    bool isfollowed = list.contains(localuid);
    if (isfollowed == true) {
      return true;
    }
    return false;
  }
  Future<bool> checkIsMemberRequestJoin(List<String> list) async {
    String localuid = _boxStorage.getUserId();
    debugPrint('list => $list - $localuid');
    bool isfollowed = list.contains(localuid);
    if (isfollowed == true) {
      return true;
    }
    return false;
  }
  Future<bool> checkIsAdminOrNot(List<String> list) async {
    String localuid = _boxStorage.getUserId();
    debugPrint('list => $list - $localuid');
    bool isfollowed = list.contains(localuid);
    if (isfollowed == true) {
      return true;
    }
    return false;
  }

}