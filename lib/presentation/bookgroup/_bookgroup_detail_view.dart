import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/local/box/box_storage.dart';
import '../../data/model/book/book_model.dart';
import '../../data/remote/firestore/firestore_book_services.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../book_forum/book_forum_add.dart';
import '../book_gallery/book_gallery_add_view.dart';
import '_add_book_view.dart';
import '../book/_book_view.dart';
import 'bookgroup_component/_bookgroup_info_view.dart';

class BookGroupAdministratorDetailView extends StatelessWidget {
  const BookGroupAdministratorDetailView({
    super.key,
    required this.id,
    required this.doc,
  });
  final String id;
  final BookModel doc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(Icons.arrow_back)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: Assets.image.logoDacLable
                        .image(fit: BoxFit.fitWidth, height: 50),
                  ),
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: ListView(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                showAlertDialog(context, doc);
                //  Get.to(
                //   () => const AddBookgroupView(category: 'Buku', id: ''));
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: const [
                    Icon(
                      Icons.book_sharp,
                      color: ColorName.redprimary,
                    ),
                    Text(
                      'Admin Menu',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      //  BookGroupInfo(documentSnapshot: doc),
      ]),
    );
  }

  showAlertDialog(BuildContext context, BookModel doc) {
    // set up the buttons
    Widget menubutton = SizedBox(
      height: 220,
      child: Column(
        children: [
          TextButton(
            child: const Text("tambah buku"),
            onPressed: () {
              Get.to(() => const AddBookgroupView(category: 'Buku', id: ''));
            },
          ),
          TextButton(
            child: const Text("tambah forum"),
            onPressed: () {
              Get.to(() => BookForumAddView(
                    bookmodel: doc,
                  ));
            },
          ),
          TextButton(
            child: const Text("tambah galeri"),
            onPressed: () {
              Get.to(() => AddBookGalleryView(
                    bookModel: doc,
                  ));
            },
          ),
         
           Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(ColorName.redprimary),
                        textStyle: MaterialStateProperty.all(const TextStyle(
                          fontFamily: 'Mont',
                          color: ColorName.whiteprimary,
                        ))),
                    onPressed: () {
                      String uid = boxStorage.getUserId();
                      List<String> isadminlist = doc.bookadmin;
                      bool isadmin = isadminlist.contains(uid);
                      fireStoreBookServices.deleteMemberbook(
                          doc.id, uid, isadmin);
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Keluar dari grup buku'.toUpperCase(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //  TextButton(
          //   child: const Text("Cancel"),
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          // ),
        ],
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Menu admin"),
      content: menubutton,
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

final boxStorage = BoxStorage();
final FireStoreBookServices fireStoreBookServices = FireStoreBookServices();
