import 'package:dac_apps/presentation/bookgroup/_add_book_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '_bookgroup_administrator_view.dart';

class BookGroupView extends StatefulWidget {
  const BookGroupView({super.key, required this.usertype});
  final String usertype;

  @override
  State<BookGroupView> createState() => _BookGroupViewState();
}

class _BookGroupViewState extends State<BookGroupView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Padding(
            padding: const EdgeInsets.only(top: 35.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Assets.image.logoDac
                      .image(fit: BoxFit.fitWidth, height: 50),
                GestureDetector(
                    onTap: (() => Get.to(() => const AddBookgroupView(
                          category: 'Master Buku',
                          id: ''
                        ))),
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: Row(
                        children: const [
                          Icon(
                            Icons.book_sharp,
                            color: ColorName.redprimary,
                          ),
                          Text(
                            'Tambah Master Buku',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  ),
                ]),
          ),
          backgroundColor: ColorName.whiteprimary,
        ),
      ),
      body: ListView(children:   [
        BookGroupAdministratorView(usertype: widget.usertype),
      ]),
    );
  }
}
