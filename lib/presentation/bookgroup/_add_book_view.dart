import 'dart:io';

import 'package:animated_custom_dropdown/custom_dropdown.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dac_apps/data/local/box/box_storage.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import '../../gen/colors.gen.dart';
import '../login/login_controller.dart';
import '../profile_medsos/profile_medsos_view.dart';

final db = FirebaseFirestore.instance;
String? value;

class AddBookgroupView extends StatefulWidget {
  const AddBookgroupView({super.key, required this.category, required this.id});
  final String category;
  final String id;
  @override
  State<AddBookgroupView> createState() => _AddBookgroupViewState();
}

class _AddBookgroupViewState extends State<AddBookgroupView> {
  var txtControllerPin = TextEditingController();
  var txtControllerBookname = TextEditingController();
  var txtControllerDescription = TextEditingController();
  var txtControllerSharelink = TextEditingController();
  final statusGroupBukuctrl = TextEditingController();
  final isPublicGroupBukuctrl = TextEditingController();
  final isTypeGroupBukuctrl = TextEditingController();
  final isOfficialBukuctrl = TextEditingController();
  final BoxStorage _boxStorage = BoxStorage();
  uploadImagetFirebase() async {
    String uid = _boxStorage.getUserId();
    String username = _boxStorage.getUserName();
    String avatar = _boxStorage.getUserAvatar();
    File filepath = File(imagePicked!.path);
    String? fileName = filepath.path.split('/').last;
    String bookpathImageFirestore =
        widget.id == 'Grup Buku' ? '/bookgroup/logo/' : '/bookgroup/book/logo/';
    String bookpathDatabaseFirestore =
        widget.id == 'Master Buku' ? 'bookGroups' : 'book';
    await FirebaseStorage.instance
        .ref()
        .child(bookpathImageFirestore)
        .child(fileName)
        .putFile(File(filepath.path))
        .then((taskSnapshot) {
      debugPrint("task done ==> ${filepath.path}");

      // download url when it is uploaded
      if (taskSnapshot.state == TaskState.success) {
        FirebaseStorage.instance
            .ref()
            .child(bookpathImageFirestore)
            .child(fileName)
            .getDownloadURL()
            .then((url) {
          debugPrint("Here is the URL of Image $url");
          final date = DateFormat('dd-MM-yyyy');
          final time = DateFormat('hh:mm');
          bool ismasterbook = false;
          if (widget.category == "Master Buku") {
            ismasterbook = true;
          } else {
            ismasterbook = false;
          }
          final collRef = db.collection('book');
          DocumentReference docReference = collRef.doc();
          DocumentReference docrefbookmember =
              collRef.doc(docReference.id).collection('bookmember').doc(uid);
          var countmember = 1;
          docrefbookmember.set({
            'bookmember': docReference.id,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
            'username': username,
            'avatar': avatar,
            'uid': uid,
            'memberlevel': 'anggota',
          });
          docReference.set({
            'id': docReference.id,
            'bookmasterid': docReference.id,
            'ismasterbook': ismasterbook,
            'bookname': txtControllerBookname.text,
            'description': txtControllerDescription.value.text,
            'bookimage': url,
            'date': date.format(DateTime.now()),
            'time': time.format(DateTime.now()),
            'isofficial': isOfficialBukuctrl.text,
            'ispublic': isPublicGroupBukuctrl.text,
            'istype': isTypeGroupBukuctrl.text,
            'ownerid': uid,
            'pin': txtControllerPin.text,
            'sharelink': txtControllerSharelink.text,
            'status': statusGroupBukuctrl.text,
            'totalmember': FieldValue.increment(countmember),
            'bookmember': FieldValue.arrayUnion([uid]),
            'bookadmin': FieldValue.arrayUnion([uid]),
            'bookmemberrequest': FieldValue.arrayUnion([]),
          }).then((doc) {
            debugPrint('success add book => ${docReference.id}');
            Navigator.pop(context);
          }).catchError((error) {
            debugPrint('fail add book => ${docReference.id}');
            debugPrint(error.toString());
          });
          // return url;
        }).catchError((onError) {
          debugPrint("Got Error $onError");
        });
      }
      Navigator.pop(context);
    });
  }

  final LoginController loginController = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category),
        centerTitle: true,
        backgroundColor: ColorName.redprimary,
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Column(
                children: [
                  Text(
                    'Logo ${widget.category}',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 250,
                        width: 250,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Stack(
                            children: [
                              Container(
                                color: Colors.black.withOpacity(0.2),
                                child: image != null
                                    ? SizedBox(
                                        height: 250,
                                        width: 250,
                                        child: Image.file(
                                          image!,
                                          fit: BoxFit.fitWidth,
                                        ),
                                      )
                                    : SizedBox(
                                        height: 250,
                                        width: 250,
                                        child: Center(child: Container()),
                                      ),
                              ),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      getImage();
                                    });
                                  },
                                  child: Container(
                                    width: double.infinity,
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 5),
                                    decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.3),
                                    ),
                                    child: Icon(
                                      Icons.file_copy,
                                      color: Colors.white.withOpacity(0.5),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerBookname,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerBookname,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "${widget.category} cannot be empty";
                    }

                    return null;
                  },
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Masukan Nama ${widget.category}',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerDescription,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerDescription,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Deskripsi cannot be empty";
                    }

                    return null;
                  },
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Masukan Deskripsi ${widget.category}',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerPin,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerPin,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Pin cannot be empty";
                    }

                    return null;
                  },
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Masukan Pin ${widget.category}',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20),
              child: SizedBox(
                child: TextFormField(
                  controller: txtControllerSharelink,
                  // obscureText: txtControllerDescription,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => txtControllerSharelink,
                  onSaved: (String? value) {},
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Sharelink cannot be empty";
                    }

                    return null;
                  },
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Masukan Sharelink ${widget.category}',
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(22.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _dropdownStatusBookGroups(),
                  SizedBox(
                    height: ScreenUtil().setHeight(15),
                  ),
                  _dropdownIsPublicBookGroups(),
                  SizedBox(
                    height: ScreenUtil().setHeight(15),
                  ),
                  _dropdownIsTypeBookGroups(),
                  SizedBox(
                    height: ScreenUtil().setHeight(15),
                  ),
                  _dropdownIsOfficialBookGroups(),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 30.0, right: 20, left: 20, bottom: 30),
              child: ElevatedButton(
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor: MaterialStateProperty.all<Color>(
                          ColorName.redprimary),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: const BorderSide(
                                  color: ColorName.redprimary)))),
                  onPressed: () async {
                    if (image != null) {
                      setState(() {
                        isLoading = true;
                      });
                      uploadImagetFirebase();
                      setState(() {
                        isLoading = false;
                      });
                    } else {
                      Get.snackbar(
                          'Logo Foto', "Lengkapi Logo ${widget.category}");
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        isLoading
                            ? const CircularProgressIndicator()
                            : Text("Buat ${widget.category}".toUpperCase(),
                                style: const TextStyle(fontSize: 14)),
                      ],
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  bool isLoading = false;

  @override
  final formKey = GlobalKey<FormState>();

  final List<String> listStatusGroupBuku = [
    'aktif',
    'tidak aktif',
  ];
  final List<String> listStatusGroupBukuisPublic = [
    'terbuka',
    'tertutup',
  ];
  final List<String> listStatusGroupBukuisType = [
    'exclusive',
    'premium',
    'reguler',
  ];
  final List<String> listStatusGroupBukuIsOfficial = [
    'official',
    'unofficial',
  ];

  @override
  void dispose() {
    txtControllerBookname.dispose();
    txtControllerSharelink.dispose();
    txtControllerPin.dispose();
    txtControllerDescription.dispose();
    statusGroupBukuctrl.dispose();
    isPublicGroupBukuctrl.dispose();
    isTypeGroupBukuctrl.dispose();
    isOfficialBukuctrl.dispose();
    super.dispose();
  }

  Widget _dropdownStatusBookGroups() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            'Status ${widget.category}',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'Status ${widget.category}',
            items: listStatusGroupBuku,
            controller: statusGroupBukuctrl,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                statusGroupBukuctrl.text = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _dropdownIsOfficialBookGroups() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            'Kategori ${widget.category}',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'Kategori ${widget.category}',
            items: listStatusGroupBukuIsOfficial,
            controller: isOfficialBukuctrl,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                isOfficialBukuctrl.text = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _dropdownIsPublicBookGroups() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            '${widget.category} Ini Terbuka ?',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(15),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'tentukan bentuk ${widget.category}',
            items: listStatusGroupBukuisPublic,
            controller: isPublicGroupBukuctrl,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                isPublicGroupBukuctrl.text = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _dropdownIsTypeBookGroups() {
    return Column(
      children: <Widget>[
        SizedBox(
          width: Get.width,
          child: WidgetTextMont(
            'Tipe Keunggulan ${widget.category}',
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(
          height: ScreenUtil().setHeight(5),
        ),
        SizedBox(
          width: Get.width,
          child: CustomDropdown(
            hintText: 'tentukan tipe keunggulan',
            items: listStatusGroupBukuisType,
            controller: isTypeGroupBukuctrl,
            excludeSelected: false,
            onChanged: (value) {
              setState(() {
                isTypeGroupBukuctrl.text = value;
              });
            },
          ),
        ),
      ],
    );
  }

  File? image;
  XFile? imagePicked;
  String? imageNamed;

  Future getImage() async {
    final box = GetStorage();
    final ImagePicker picker = ImagePicker();
    imagePicked = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 20);
    image = File(imagePicked!.path);
    String? fileName = image!.path.split('/').last;
    String? fileExtension = fileName.split('.').last;

    final bytes = image!.readAsBytesSync().lengthInBytes;
    final kb = bytes / 1024;
    final ukuranPhoto = kb / 1024;

    if (fileExtension == 'png' ||
        fileExtension == 'jpg' ||
        fileExtension == 'PNG' ||
        fileExtension == 'MIME' ||
        fileExtension == 'mime') {
      if (ukuranPhoto <= 1.01) {
        setState(() {
          image = File(imagePicked!.path);
          imageNamed = fileName;
          // getting a directory path for saving
          final String path = image.toString();
          box.write('photosstory', imagePicked.toString());
          debugPrint('clog ==> ${path.toString()}');
        });
      } else {
        setState(() {
          imageNamed = 'Ukuran photo belum sesuai';
          image = null;
          Get.snackbar('Gagal Upload', 'Ukuran photo belum sesuai',
              snackPosition: SnackPosition.BOTTOM,
              backgroundColor: ColorName.purplelow);
        });
      }
    } else {
      setState(() {
        image = null;
        imageNamed = 'Jenis photo belum sesuai';
        Get.snackbar('Gagal Upload', 'Jenis photo belum sesuai',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: ColorName.purplelow);
      });
    }
  }
}
