import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/remote/firestore/firestore_user_services.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../login/login_controller.dart';
import '../profile/profile_view.dart';
import '../profile_medsos/profile_medsos_view.dart';

class ProfileHeaderView extends GetView<LoginController> {
  ProfileHeaderView({super.key});

  final LoginController loginController = Get.put(LoginController());
  final FireStoreUserServices fireStoreUserServices = FireStoreUserServices();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fireStoreUserServices.getCurrentUser(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const SizedBox(
                height: 30, width: 30, child: CircularProgressIndicator());
          }
          if (snapshot.connectionState == ConnectionState.none) {
            return const Text('Server not found - Error 500');
          }

          if (snapshot.connectionState == ConnectionState.done && !snapshot.hasData) {
           
              return SizedBox(
                height: Get.height * 0.2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    WidgetTextMont(
                      'Tidak dapat memuat profil anda',
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                      textColor: Colors.grey[600]!,
                    ),
                  ],
                ),
              );
          } else{
            
            return Padding(
              padding: const EdgeInsets.only(
                  top: 5.0, left: 10, right: 10, bottom: 20),
              child: Column(
                children: [
                  InkWell(
                    onTap: () => Get.to( ProfileView(usermodel: snapshot.data!, isCurrentUser: true,)),
                    child: SizedBox(
                      height: 90,
                      width: Get.width * 0.9,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      loginController.isTime.value,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 25,
                                          color: ColorName.blackgrey),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      snapshot.data!.username,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14,
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 70,
                                  width: 60,
                                  child: Card(
                                    elevation: 5,
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: CachedNetworkImage(
                                        imageUrl:
                                           snapshot.data!.avatar,
                                        placeholder: (context, url) =>
                                           Image.network(url),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ]),
                    ),
                  ),
                ],
              ),
            );
          }
         
        });
  }
}
