import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app/widgets/input_text_form_costum.dart';
import '../../gen/colors.gen.dart';
import 'feedback_controller.dart';

class AddFeedbackView extends GetView<FeedBackController> {
  const AddFeedbackView({super.key});

  @override
  Widget build(BuildContext context) {
    final controllerFeedback = Get.put(FeedBackController());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.redprimary,
        title: const Text("Berikan masukan"),
        centerTitle: true,
      ),
      body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Obx(
            () => Column(
              children: [
                
                InputTextFormCostum(
                  iconText: const Icon(Icons.description, color: ColorName.redprimary),
                  hintText: "masukan",
                  labletext: "masukan",
                  textInputAction: TextInputAction.next,
                  controller: controllerFeedback.descC.value,
                ),
                SizedBox(
                  width: Get.width * 0.4,
                  height: 50,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: ColorName.redprimary,
                    ),
                    onPressed: () {
                      controller.addFaq(
                          controller.descC.value.text,
                          );
                    },
                    child: const Text(
                      "Kirim",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}