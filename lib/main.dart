// ignore: depend_on_referenced_packages
import 'package:catcher/catcher.dart';
import 'package:dac_apps/dac_app.dart';
import 'package:dac_apps/data/model/book/book_model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_web_plugins/url_strategy.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'app/utilities/catcher_setup.dart';
import 'data/remote/firestore/firestore_book_services.dart';
import 'presentation/book_review/book_review_view.dart';

const appName = 'DAC App';

void runEbeasiswaApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  usePathUrlStrategy();
  //await FlutterDownloader.initialize(debug: true, ignoreSsl: true);
  await GetStorage.init();
  await _firebaseSetup();
  await _handleDeepLinks();
  Catcher(
    rootWidget: const DacApp(),
    debugConfig: CatcherSetup.debug(),
    releaseConfig: CatcherSetup.release(),
    profileConfig: CatcherSetup.debug(),
  );
}

Future<void> _firebaseSetup() async {
  await Firebase.initializeApp();
  await FirebaseAppCheck.instance.activate();
  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  if (!kIsWeb) {
    await setupFlutterNotifications();
  }
}

Future<void> _handleDeepLinks() async {
  //await Future.delayed(const Duration(seconds: 3));
  final PendingDynamicLinkData? initialLink =
      await FirebaseDynamicLinks.instance.getInitialLink();
  final FireStoreBookServices fireStoreBookServices =
      Get.put(FireStoreBookServices());
  FirebaseDynamicLinks.instance.onLink.listen((dynamicLinkData) async {
    // Get.snackbar('welcome', dynamicLinkData.link.query);
    if (dynamicLinkData.link.path == '/book') {
      BookModel bookModel =
          await fireStoreBookServices.getBookDetail(dynamicLinkData.link.query);
      Get.to(BookReviewView(bookModel: bookModel));
    }
  }).onError((error) {
    // Handle errors
  });
  if (initialLink != null) {
    final Uri deepLink = initialLink.link;
    debugPrint('Deeplinks uri:$deepLink');
    if (deepLink.path == '/book') {
      debugPrint('this is deeplink path ');
      Get.snackbar('welcome', 'deeplink success');
      //  Get.to(BookReviewView(
      //   bookModel: ,
      //   deepLinkPath: '${deepLink.path}:Deep Link',
      //  ));
    } else if (deepLink.path == '/profile') {
      // Get.to(const LoginView());
    } else {
      Get.snackbar('welcome', 'deeplink failed');
      debugPrint('deep link uri : ${deepLink.path}');
    }
  }
}

@pragma('vm:entry-point')
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await setupFlutterNotifications();
  showFlutterNotification(message);
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  debugPrint('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
late AndroidNotificationChannel channel;

bool isFlutterLocalNotificationsInitialized = false;

Future<void> setupFlutterNotifications() async {
  if (isFlutterLocalNotificationsInitialized) {
    return;
  }
  channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description:
        'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  isFlutterLocalNotificationsInitialized = true;
}

void showFlutterNotification(RemoteMessage message) {
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
      notification.hashCode,
      notification.title,
      notification.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channelDescription: channel.description,
          // ignore: todo
          // TODO add a proper drawable resource to android, for now using
          //      one that already exists in example app.
          icon: 'launch_background',
        ),
      ),
    );
    debugPrint('clog ==> ${notification.title}');
    debugPrint('clog ==> ${message.data['body']}');
    debugPrint('clog ==> ${message.data['notificationCategory']}');
    debugPrint('clog ==> ${message.data['feedid']}');
    debugPrint('clog ==> ${message.data['bookid']}');
  }
}

/// Initialize the [FlutterLocalNotificationsPlugin] package.
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() {}
