import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerCustomDac extends StatefulWidget {
  const VideoPlayerCustomDac({super.key, required this.videourl});
  final String videourl;
  @override
  State<VideoPlayerCustomDac> createState() => _VideoPlayerCustomDacState();
}

class _VideoPlayerCustomDacState extends State<VideoPlayerCustomDac> {
  late VideoPlayerController controller;

  @override
  void initState() {
    loadVideoPlayer();
    super.initState();
  }

  loadVideoPlayer() {
    controller = VideoPlayerController.network(
        widget.videourl);
    controller.addListener(() {
      if(!mounted){
        setState(() {});
      } else{
        null;
      }
    });
    controller.initialize().then((value) {
      setState(() {});
    });
  }

  @override
  void dispose(){
    //...
    super.dispose();
    controller.dispose();
    //...
}


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [

       controller.value.size.height >= 300 ? AspectRatio(
          aspectRatio: 16/12,
          child: VideoPlayer(controller),
        ) : AspectRatio(
          aspectRatio: controller.value.aspectRatio,
          child: VideoPlayer(controller),
        ),
        // Container(
        //   //duration of video
        //   child:
        //       Text("Total durasi: ${controller.value.duration}"),
        // ),
        VideoProgressIndicator(controller,
            allowScrubbing: true,
            colors: const VideoProgressColors(
              backgroundColor: Colors.redAccent,
              playedColor: Colors.green,
              bufferedColor: Colors.purple,
            )),
        SizedBox(
          child: Row(
            children: [
              IconButton(
                  onPressed: () {
                    if (controller.value.isPlaying) {
                      controller.pause();
                    } else {
                      controller.play();
                    }

                    setState(() {});
                  },
                  icon: Icon(controller.value.isPlaying
                      ? Icons.pause
                      : Icons.play_arrow)),
              IconButton(
                  onPressed: () {
                    controller.seekTo(const Duration(seconds: 0));

                    setState(() {});
                  },
                  icon: const Icon(Icons.stop))
            ],
          ),
        )
      ]),
    );
  }
}
