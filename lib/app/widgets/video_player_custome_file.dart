import 'dart:io';

import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerCustomFileDac extends StatefulWidget {
  const VideoPlayerCustomFileDac({super.key, required this.videourl});
  final VideoPlayerController videourl;
  @override
  State<VideoPlayerCustomFileDac> createState() => _VideoPlayerCustomFileDacState();
}

class _VideoPlayerCustomFileDacState extends State<VideoPlayerCustomFileDac> {
 // late VideoPlayerController controller;

  @override
  void initState() {
    loadVideoPlayer();
    super.initState();
  }

  loadVideoPlayer() {
    // controller = VideoPlayerController.file(
    //     widget.videourl);
    widget.videourl.addListener(() {
      if(!mounted){
        null;
      } else{
        setState(() {});
      }
    });
    widget.videourl.initialize().then((value) {
      setState(() {});
    });
  }

  @override
  void dispose(){
    //...
    super.dispose();
    //widget.videourl.dispose();
    //...
}


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [

       widget.videourl.value.size.height >= 300 ? AspectRatio(
          aspectRatio: 16/12,
          child: VideoPlayer(widget.videourl),
        ) : AspectRatio(
          aspectRatio: widget.videourl.value.aspectRatio,
          child: VideoPlayer(widget.videourl),
        ),
        // Container(
        //   //duration of video
        //   child:
        //       Text("Total durasi: ${controller.value.duration}"),
        // ),
        VideoProgressIndicator(widget.videourl,
            allowScrubbing: true,
            colors: const VideoProgressColors(
              backgroundColor: Colors.redAccent,
              playedColor: Colors.green,
              bufferedColor: Colors.purple,
            )),
        SizedBox(
          child: Row(
            children: [
              IconButton(
                  onPressed: () {
                    if (widget.videourl.value.isPlaying) {
                      widget.videourl.pause();
                    } else {
                      widget.videourl.play();
                    }

                    setState(() {});
                  },
                  icon: Icon(widget.videourl.value.isPlaying
                      ? Icons.pause
                      : Icons.play_arrow)),
              IconButton(
                  onPressed: () {
                    widget.videourl.seekTo(const Duration(seconds: 0));

                    setState(() {});
                  },
                  icon: const Icon(Icons.stop))
            ],
          ),
        )
      ]),
    );
  }
}
