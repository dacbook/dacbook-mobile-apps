  import 'package:flutter/material.dart';

class CheckContainer extends StatefulWidget {
  const CheckContainer({Key? key, this.title = ''}) : super(key: key);
  final String title;

  @override
  State<StatefulWidget> createState() {
    return _CheckContainerState();
  }
}

class _CheckContainerState extends State<CheckContainer> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.cyanAccent,
      onTap: () {
        setState(() {
          isChecked = !isChecked;
        });
      },
      child: Container(
        padding: const EdgeInsets.all(12),
        color: isChecked ? Colors.blue : Colors.white,
        child: Text(
          widget.title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 14,
            color: isChecked ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }
}