import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  const VideoPlayerWidget(
      {Key? key,
      required this.url,
      required this.deskripsi,
      required this.category,
      required this.date,
      required this.time,
      required this.title})
      : super(key: key);
  final String url;
  final String deskripsi;
  final String date;
  final String time;
  final String category;
  final String title;
  @override
  // ignore: library_private_types_in_public_api
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.url)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: ColorName.redprimary),
      body: Center(
        child: _controller.value.isInitialized
            ? ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.title.toUpperCase(), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                  ),
                 
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AspectRatio(
                      aspectRatio: _controller.value.aspectRatio * 16 / 9,
                      child: VideoPlayer(_controller),
                    ),
                  ),
                   Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('dibagikan pada, ${widget.date}, ${widget.time}'),
                  ),
                   Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.deskripsi),
                  ),
                 
                ],
              )
            : const CircularProgressIndicator(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _controller.value.isPlaying
                ? _controller.pause()
                : _controller.play();
          });
        },
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
