
import 'package:dac_apps/gen/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../gen/assets.gen.dart';
import '../../presentation/splashscreen/splashscreen_controller.dart';

class SplashScreen extends StatelessWidget {
   const SplashScreen({super.key, required this.countryLabel, required this.countryName});
  final String countryLabel;
  final String countryName;

  @override
  Widget build(BuildContext context) {
     final SplashScreenController splashscreenController =
        Get.put(SplashScreenController()); 
    return Stack(
      children: [
        Container(
          width: Get.width,
          height: Get.height,
           decoration: BoxDecoration(
                       image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(countryName)),
                      ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(),
              Assets.image.dacSplashLogo.image(
                height: 150
              ),
              SizedBox(
                width: Get.width,
                child: Text('I Love My Country $countryLabel',
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                 style: const TextStyle(color: ColorName.orangeGlow, fontSize: 26, fontWeight: FontWeight.bold),))
            ],
          )
        ),
        splashscreenController.splashScreenLoginValidation.value == ''? const SizedBox() :   Positioned(
          width: Get.width,
            bottom: 1,
             child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
               children: [
                 Center(
                   child: Padding(
                     padding: const EdgeInsets.all(20.0),
                     child: Container(
                      decoration: BoxDecoration(
                             border: Border.all(
                               color: ColorName.blackgrey,
                               width: 5.0,
                               style: BorderStyle.solid
                             ),
                             borderRadius: BorderRadius.circular(20),
                             color: ColorName.blackgrey,
                           ),
                      height: 30,
                      width: 200,
                      child: Obx(() => Center(child: Text(splashscreenController.splashScreenLoginValidation.value, style: const TextStyle(color: ColorName.whiteprimary),))),
                             ),
                   ),
                 ),
               ],
             ),
           ),
      ],
    );
  }
}